# DMA Customer

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Table of Contents
=================

* [Architecture](#architecture)
* [Getting started](#getting-started)
* [Development flow](#development-flow)
   * [Run the app](#run-the-app)
   * [Start the mock server](#start-the-mock-server)
   * [Storybook](#storybook)
   * [Rebuild CSS](#rebuild-css)
* [Deployment](#deployment)
* [Roadmap](#roadmap)

## Getting started

```shell
$ git clone git@github.com:phanhoangloc/react-ts.git
$ yarn setup
$ yarn start
```

## Development flow

### Run the app

Start a proxy in a new process, then start your application

* Run the app with the default environment (dev)

```shell
$ yarn proxy
$ yarn start
```

* Run the app with a specific environment

**Local server**

```shell
$ ENV=dev yarn proxy
$ ENV=dev yarn start
```

**Client servers**

It's useful to run the app on a client server for debugging env-specific issues.
We need a proxy server (dealing with CORS issues) to connect to the real server.

```sh
$ ENV=sit yarn proxy
$ ENV=sit yarn start
```

```sh
$ ENV=sit-develop yarn proxy
$ ENV=sit-develop yarn start
```

```sh
$ ENV=uat yarn proxy
$ ENV=uat yarn start
```

```sh
$ ENV=uat-develop yarn proxy
$ ENV=uat-develop yarn start
```

```sh
$ ENV=prod yarn proxy
$ ENV=prod yarn start
```

### Storybook

* Open the storybook to view UI components

```shell
$ yarn storybook
```

### Rebuild CSS

When you make a change to theme configuration files, it's necessary to rebuild CSS

Theme configuration files:
- `tailwind.js`
- `colors.js`

```shell
$ yarn build:css
```
### GraphQL

- Always use `withApp` to support loading & updating states (show a spinner and disable the UI)
- Put `withApollo` HOC in the **FIRST** place
- Put `withApp` HOC in the **LAST** place
- Put **PUBLIC** queries before **PRIVATE** queries (to make `withError` working correctly)

```ts
const enhance = compose(
  ... // other HOCs
  withApollo,
  // PUBLIC queries - no auth required
  graphql(query1.query, query1.params),
  graphql(query2.query, query2.params),
  // PRIVATE queries - auth required
  graphql(query3.query, query3.params),
  graphql(query4.query, query4.params),
  // Mutations
  graphql(mutation1.mutation, query3.params),
  graphql(mutation2.mutation, query4.params),
  withApp({ notification: true }),
  ... // other HOCs
)
```

Please note that `withApp` includes `withResponsive` and `withError` by default. So there is no need to add them together.

## Bump a new version

- Checkout `master` branch

```sh
$ git checkout master
```

- Release a new version (default semver is `patch`)

```sh
$ yarn release
```

You can also apply a custom semantic version

```sh
$ VERSION=minor yarn release
```

## Deployment

* Make a new build

```shell
$ yarn build
```

* Analyze dependencies

```shell
$ yarn analyze
```

* Deploy a new release

```shell
$ yarn deploy
```

## Troubleshooting

To make sure that the application is latest, check the version, git commit id, commit date etc at {HOST}/{PATH}/info
