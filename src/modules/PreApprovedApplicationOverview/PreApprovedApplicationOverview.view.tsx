import * as R from 'ramda'
import React, { FunctionComponent } from 'react'
import { t } from '../../i18n'
import { isLoggedOn, setUserProfile } from '../PreApprovedApplication/PreApprovedApplication.util'
import {
  Button,
  Container,
  Image,
  Step,
  StepTitle,
  ThreStepWrapper,
  Title
} from './PreApprovedApplicationOverview.style'
const CLIENT_ID = '30985363837-kagbnc6vpu3t23468bec4ggo9rq1cksi.apps.googleusercontent.com';
import { useGoogleLogin } from 'react-google-login';
import { IUserProfile } from '../../types'

const icStep1 = require('./assets/step1.svg')
const icStep2 = require('./assets/step2.svg')
const icStep3 = require('./assets/step3.svg')
const googleIcon = require('./assets/google-icon.svg')

const PreApprovedApplication: FunctionComponent<any> = (props) => {

  const onLogin = () => {
    if (!isLoggedOn()) {
      signIn()
    }
    props.goNext()
  }

  const onSuccess = (response: any) => {
    const userProfile: IUserProfile = {
      name: R.pathOr('', ['profileObj', 'name'], response),
      email: R.pathOr('', ['profileObj', 'email'], response),
      tokenId: R.pathOr('', ['tokenId'], response)
    }
    console.log({ userProfile })
    setUserProfile(userProfile)
  }

  const { signIn } = useGoogleLogin({
    onSuccess,
    onFailure: props.ON_GG_LOGIN_FAILURE,
    // isSignedIn: true,
    clientId: CLIENT_ID,
    accessType: 'offline'
  });


  return (
    <Container>
      <Title>{t('preApprovedOverView.label1')}</Title>
      <Title>{t('preApprovedOverView.label2')}</Title>
      <ThreStepWrapper>
        <Step>
          <Image src={icStep1} alt={'Icon'} />
          <StepTitle>{t('preApprovedOverView.step1')}</StepTitle>
        </Step>
        <Step>
          <Image src={icStep2} alt={'Icon'} />
          <StepTitle>{t('preApprovedOverView.step2')}</StepTitle>
        </Step>
        <Step>
          <Image src={icStep3} alt={'Icon'} />
          <StepTitle>{t('preApprovedOverView.step3')}</StepTitle>
        </Step>
      </ThreStepWrapper>
      <Title>{t('preApprovedOverView.label3')}</Title>
      <Button
        id="pre_start"
        onClick={onLogin}
      >
        <div className={'mr-1'}>
          <img width={24} height={24} src={googleIcon} alt={''} />
        </div>
        <div >
          {t('preApprovedOverView.start')}
        </div>
      </Button>
    </Container>
  )
}

export default PreApprovedApplication
