import { InjectedFormikProps } from 'formik'
import { FormValues } from 'formik-validators'
import { IParentProps } from '../PreApprovedApplication.type'

interface IIdentity {
  nationalId: string
  nationalType: string
}

interface IValues extends FormValues {
  fullName: string
  dateOfBirth: string
  email: string
  maritalStatus: string
  identities: any
  educationalDegree: string
  accommodationType: string
  transportationType: string
}
interface IProps extends IParentProps {
  phoneNumber: string
  dropdownData: any
  isBScore: boolean
  prePerson: any
  currentTab: string
  setTab(currentTab: string): void
  savePersonWithValidation(input: any): void
  savePersonWithNonValidation(input: any): void
  saveDataPersonalInfomationPre(input: any): void
  showErrorMessage(message: string): void
  updateStatus(payload: any): void
  saveCurrentStep(input: any): void
  setChildStep(params: string): void
  prevStep: number
  setPrevStep(prevStep: number): void
  childStep: string
}

interface IHandlers {
  ON_SUBMIT(values: any): void
  ON_BACK(values: any): void
}

type EnhancedProps = InjectedFormikProps<IProps, IValues> & IHandlers

export { IValues, IProps, EnhancedProps, IIdentity }
