import gql from 'graphql-tag'
import { pathOr } from 'ramda'

const query = gql`
  query($preApprovedId: String) {
    person(preApprovedId: $preApprovedId)
      @rest(
        type: "pre_approved"
        path: "/approved/v1/get-person?{args}"
        endpoint: "dma-pre-credit"
      ) {
      isBScore
      prePerson @type(name: "prePerson") {
        fullName: name
        dateOfBirth
        email: string
        maritalStatus
        educationalDegree
        accommodationType
        transportationType
        identityDocuments @type(name: "identityDocuments") {
          nationalId: id
          nationalType: type
        }
      }
    }
  }
`
const params = {
  props: ({ data }: any) => {
    const prePerson = pathOr({}, ['person', 'prePerson'], data)
    const isBScore = pathOr(null, ['person', 'isBScore'], data)

    return {
      prePerson,
      isBScore,
      getPerson: data
    }
  },
  options: (props: any) => {
    const preApprovedId = pathOr('', ['preApprovedId'], props)

    return {
      variables: {
        preApprovedId
      }
    }
  },
  skip: (props: any): any => {
    const preApprovedId = pathOr('', ['preApprovedId'], props)

    return preApprovedId === ''
  }
}

export default { query, params }
