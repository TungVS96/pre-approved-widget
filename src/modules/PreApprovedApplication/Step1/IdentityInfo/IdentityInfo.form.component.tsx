import * as R from 'ramda'
import { compose, withHandlers } from 'recompose'
import { withPopup } from '../../../../hocs'
import { EnhancedProps } from '../Step1.type'
import IdentityInfo from './IdentityInfo.form'

const identityInitial = {
  nationalType: 'NATIONAL_ID',
  nationalId: ''
}

export default compose(
  withHandlers({
    ON_ADD: ({ setFieldValue, values }) => () => {
      const identities = R.pathOr([], ['identities'], values)

      setFieldValue('identities', identities.concat([identityInitial]))
    },
    ON_DELETE: ({ setFieldValue, values }: EnhancedProps) => (
      index: number
    ) => () => {
      const identities = R.pathOr([], ['identities'], values)

      setFieldValue('identities', R.remove(index, 1, identities))
    }
  }),
  withPopup
)(IdentityInfo)
