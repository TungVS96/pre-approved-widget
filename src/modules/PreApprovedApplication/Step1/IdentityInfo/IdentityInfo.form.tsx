import { subYears } from 'date-fns'
import { IPage, MARITAL_STATUS, NATIONAL_IDPASSPORT_NO_TYPE } from 'dma-types'
import { rem } from 'dma-ui'
import { css } from 'emotion'
import { getIn } from 'formik'
import { pathOr } from 'ramda'
import React, { useEffect, FunctionComponent } from 'react'
import { LIST_OF_NATIONAL_ID_TYPES } from '../../../../constants'
import { RadioInput } from '../../../../forms'
import {
  renderDropdownTextInput,
  renderInputText,
  renderTextMaskedInput
} from '../../../../forms/form'
import { PopupProps } from '../../../../hocs'
import { t } from '../../../../i18n'
import {
  getFieldProps,
  getPositiveNumberOnly,
  getTrimValue,
  toDateISO
} from '../../../../utils'
import { FieldWrapper } from '../../PreApprovedApplication.style'
import { STEP } from '../../PreApprovedApplication.type'
import { STEP1_TABNAME } from '../Step1.constant'
import { EnhancedProps } from '../Step1.type'
interface IProps extends IPage, PopupProps {
  isMD: boolean
  prePerson: any
  initialStep: any
  saveCurrentStep(input: any): void
  fetchStepDone: boolean
}
interface IHandlers {
  ON_ADD(): void
  ON_DELETE(index: number): () => void
}

const icAdd = require('./assets/ic-add.svg')
const icDelete = require('./assets/ic-delete.svg')
const today = new Date()

const IdentityInfo: FunctionComponent<EnhancedProps & IHandlers & IProps> = (
  props
) => {
  const {
    isMD,
    ON_ADD,
    ON_DELETE,
    values,
    initialStep,
    fetchStepDone
  } = props
  useEffect(() => {
    const preApprovedId = pathOr('', ['preApprovedId'], props)
    if (preApprovedId === '') return
    if (Object.keys(initialStep).length === 0) return
    if (!fetchStepDone) return
    const params = {
      step: STEP.step1,
      childStep: STEP1_TABNAME.TAB_A
    }
    const input = {
      preApprovedId,
      currentStep: JSON.stringify(params)
    }

    props.saveCurrentStep({ variables: { input } })
  }, [initialStep])
  const identities = getIn(props.values, 'identities') || []
  // const edited = !equals(initialValues, values)

  return (
    <>
      <FieldWrapper>
        {renderInputText({
          props,
          name: 'fullName',
          label: 'preApproved.step1.identityInfo.fullName.label',
          placeholder: 'preApproved.step1.identityInfo.fullName.placeholder',
          disabled: false,
          requiredField: true,
          maxLength: 50
        })}
      </FieldWrapper>
      <FieldWrapper>
        {renderTextMaskedInput({
          props,
          name: 'dateOfBirth',
          maxDate: subYears(today, 18),
          requiredField: true,
          hasErrorIcon: true,
          label: 'preApproved.step1.identityInfo.dateOfBirth.label',
          placeholder: 'preApproved.step1.identityInfo.dateOfBirth.placeholder',
          inputProps: {
            mask: '11/11/1111',
            className: css({
              fontSize: rem(14),
              '::placeholder': {
                color: '#8B8B92',
                opacity: 1
              }
            })
          },
          transformValue: (value) => toDateISO(value),
          fieldContainerStyle: css({
            border: '1px solid #ddd',
            borderRadius: rem(5),
            height: rem(48)
          })
        })}
      </FieldWrapper>
      <FieldWrapper>
        {renderInputText({
          props,
          name: 'email',
          label: 'preApproved.step1.identityInfo.email.label',
          placeholder: 'preApproved.step1.identityInfo.email.placeholder',
          disabled: false,
          requiredField: true,
          maxLength: 50
        })}
      </FieldWrapper>
      <FieldWrapper>
        <RadioInput
          {...getFieldProps('maritalStatus', props)}
          vertical={!isMD}
          hasErrorIcon={true}
          options={[
            {
              name: t('landingPage.calculator.radioOption1.label'),
              value: MARITAL_STATUS.MARRIED
            },
            {
              name: t('landingPage.calculator.radioOption2.label'),
              value: MARITAL_STATUS.SINGLE
            },
            {
              name: t('landingPage.calculator.radioOption3.label'),
              value: MARITAL_STATUS.DIVORCED
            },
            {
              name: t('landingPage.calculator.radioOption4.label'),
              value: MARITAL_STATUS.WIDOWED
            }
          ]}
          requiredField={true}
          label={'Tình trạng hôn nhân'}
        />
      </FieldWrapper>
      {identities.map((item: any, index: number) => {
        const path = `identities[${index}]`
        const nationalType = pathOr(
          '',
          ['identities', index, 'nationalType'],
          values
        )

        return (
          <div key={index.toString()} className={'w-full'}>
            <FieldWrapper>
              <div className={'flex flex-row items-center'}>
                <div className={'w-full'}>
                  {renderDropdownTextInput({
                    props,
                    name: `${path}nationalId`,
                    nameOfDropdown: `${path}nationalType`,
                    requiredField: true,
                    label: 'preApproved.step1.identityInfo.identity.label',
                    placeholder:
                      'preApproved.step1.identityInfo.identity.placeholder',
                    items: LIST_OF_NATIONAL_ID_TYPES,
                    labelStyle: css({ fontWeight: 500 }),
                    transformValue: (value) =>
                      nationalType !== NATIONAL_IDPASSPORT_NO_TYPE.PASSPORT
                        ? getPositiveNumberOnly(value)
                        : getTrimValue(value)
                  })}
                </div>
                {identities.length > 1 && (
                  <div
                    onClick={ON_DELETE(index)}
                    className={'mx-2 cursor-pointer'}
                  >
                    <img src={icDelete} width={20} height={20} />
                  </div>
                )}
              </div>
            </FieldWrapper>
            <FieldWrapper />
            {index + 1 === identities.length && (
              <div className={'flex ml-4'}>
                <div className={'flex cursor-pointer'} onClick={ON_ADD}>
                  <img src={icAdd} alt={'Icon'} width={20} height={20} />
                  <span className={'ml-2'}>Bổ sung giấy tờ tuỳ thân</span>
                </div>
              </div>
            )}
          </div>
        )
      })}
      <div className={'mt-4 w-full'} />
    </>
  )
}

export { IProps, IHandlers }
export default IdentityInfo
