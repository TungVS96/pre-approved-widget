import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    savePersonWithValidation(input: $input)
      @rest(
        type: "Person"
        endpoint: "dma-pre-credit"
        path: "/approved/v1/save-and-validate-person"
        method: "post"
      ) {
      status
      isBScore
      preApprovedId
      errorName
      errorCode
    }
  }
`
const params = {
  name: 'savePersonWithValidation'
}

export default {
  mutation,
  params
}
