import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    saveDataPersonalInfomationPre(input: $input)
      @rest(
        type: "Person"
        endpoint: "dma-pre-credit"
        path: "/approved/v1/save-person"
        method: "post"
      ) {
      status
      isBScore
      preApprovedId
    }
  }
`
const params = {
  name: 'saveDataPersonalInfomationPre'
}

export default {
  mutation,
  params
}
