import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    savePersonWithNonValidation(input: $input)
      @rest(
        type: "Person"
        endpoint: "dma-pre-credit"
        path: "/approved/v1/save-person"
        method: "post"
      ) {
      status
      isBScore
      preApprovedId
      errorCode
      errorName
    }
  }
`
const params = {
  name: 'savePersonWithNonValidation'
}

export default {
  mutation,
  params
}
