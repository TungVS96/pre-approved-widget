import { subYears } from 'date-fns'
import { required } from 'formik-validators'
import insertIf from 'insert-if'
import * as R from 'ramda'
import { isValidDate, lessThanDate, validator } from '../../../forms'
import { isEmail } from '../../../forms/Rules/isEmail.validator'
import { typeNationalId } from '../../../forms/Rules/typeNationalId.validator'
import { STEP1_TABNAME } from './Step1.constant'
import { EnhancedProps, IIdentity, IValues } from './Step1.type'

const identityInitial = {
  nationalType: 'NATIONAL_ID',
  nationalId: ''
}

const mapPropsToValues = (props: any) => {
  const { prePerson } = props

  return {
    fullName: R.pathOr('', ['fullName'], prePerson),
    dateOfBirth: R.pathOr('', ['dateOfBirth'], prePerson),
    email: R.pathOr('', ['email'], prePerson),
    maritalStatus: R.pathOr('', ['maritalStatus'], prePerson),
    identities: R.isEmpty(R.pathOr([], ['identityDocuments'], prePerson))
      ? [{ ...identityInitial }]
      : R.pathOr([], ['identityDocuments'], prePerson).map(
          (item: IIdentity) => ({
            nationalId: R.pathOr('', ['nationalId'], item),
            nationalType: R.pathOr('', ['nationalType'], item)
          })
        ),
    educationalDegree: R.pathOr('', ['educationalDegree'], prePerson) || 'UNI',
    accommodationType: R.pathOr('', ['accommodationType'], prePerson),
    transportationType: R.pathOr('', ['transportationType'], prePerson) || 'MOT'
  }
}

const today = new Date()
const todayISOString = today.toISOString()
const _18YearsAgo = subYears(today, 18).toISOString()
const configs = (currentTab: string) => {
  const isTabB = currentTab === STEP1_TABNAME.TAB_B

  return {
    fullName: [
      required('preApprovedApplication.step1.identifyInfo.fullname.required')
    ],
    dateOfBirth: [
      required(
        'preApprovedApplication.step1.identifyInfo.dateOfBirth.required'
      ),
      isValidDate('applicationOnlineFrom.step1.dateOfBirth.valid'),
      lessThanDate(
        'applicationOnlineFrom.step1.dateOfBirth.valid',
        todayISOString
      ),
      lessThanDate('applicationOnlineFrom.step1.dateOfBirth.valid', _18YearsAgo)
    ],
    email: [
      required('preApprovedApplication.step1.identifyInfo.email.required'),
      isEmail('preApprovedApplication.step1.identifyInfo.email.validate')
    ],
    maritalStatus: [
      required(
        'preApprovedApplication.step1.identifyInfo.maritalStatus.required'
      )
    ],
    educationalDegree: [
      ...insertIf(
        isTabB,
        required('preApproved.step1.educationalDegree.required')
      )
    ],
    accommodationType: [
      ...insertIf(
        isTabB,
        required('preApproved.step1.accommodationType.required')
      )
    ],
    transportationType: [
      ...insertIf(
        isTabB,
        required('preApproved.step1.transportationType.required')
      )
    ]
  }
}

const validate = (values: IValues, props: EnhancedProps) => {
  const identitiesErrors = values.identities.map((identity: any) => {
    return validator({
      nationalType: [
        required('applicationOnlineFrom.step1.nationalId.required')
      ],
      nationalId: [
        required('applicationOnlineFrom.step1.nationalId.required'),
        typeNationalId('', 'nationalType')
      ]
    })(identity, props)
  })
  const currentTab = R.pathOr('', ['currentTab'], props)
  const errors = validator(configs(currentTab))(values, props)
  if (
    !R.isEmpty(
      R.filter(
        R.compose(
          R.not,
          R.isEmpty
        ),
        identitiesErrors
      )
    )
  ) {
    return {
      ...errors,
      identities: identitiesErrors
    }
  }

  return errors
}

export { mapPropsToValues, validate }
