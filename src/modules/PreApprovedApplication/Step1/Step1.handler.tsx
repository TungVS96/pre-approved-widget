import * as R from 'ramda'
import { t } from '../../../i18n'
import { PRE_APPROVED_STATUS } from '../PreApprovedApplication.constant'
import { STEP1_TABNAME } from './Step1.constant'
import { IIdentity, IProps, IValues } from './Step1.type'

const saveTabA = (props: any, values: any) => async () => {
  try {
    const preApprovedId = R.pathOr('', ['preApprovedId'], props)
    const input = reformatPerson(props, values, preApprovedId)

    await props.savePersonWithNonValidation({
      variables: { input }
    })
    await updateStatus(props, preApprovedId)
  } catch (e) {
    props.showMessage()
  }
}

const saveTabB = (props: any, values: any) => async () => {
  try {
    const preApprovedId = R.pathOr('', ['preApprovedId'], props)
    const input = reformatPerson(props, values, preApprovedId)
    await props.savePersonWithNonValidation({
      variables: { input }
    })
  } catch (e) {
    props.showMessage()
  }
}

const updateStatus = async (props: IProps, preApprovedId: string) => {
  try {
    await props.updateStatus({
      variables: {
        input: {
          id: preApprovedId,
          status: PRE_APPROVED_STATUS.PR01
        }
      }
    })
  } catch (e) {
    props.showMessage()
  }
}

const reformatPerson = (
  props: IProps,
  values: IValues,
  preApprovedId: string
) => {
  const identities = R.pathOr([], ['identities'], values)
  const identityDocuments = identities.map((item: IIdentity, index: number) => {
    return {
      index,
      type: item.nationalType,
      id: item.nationalId,
      relationshipWithPrimaryApplication: 'SELF'
    }
  })
  const prePerson = {
    identityDocuments,
    name: R.pathOr('', ['fullName'], values),
    dateOfBirth: R.pathOr('', ['dateOfBirth'], values),
    maritalStatus: R.pathOr('', ['maritalStatus'], values),
    educationalDegree: R.pathOr('', ['educationalDegree'], values),
    accommodationType: R.pathOr('', ['accommodationType'], values),
    transportationType: R.pathOr('', ['transportationType'], values),
    email: R.pathOr('', ['email'], values),
    mobileNumber: props.phoneNumber
  }
  const urlParams = new URLSearchParams(window.location.search)
  const campaignName = urlParams.get('url_campaign')

  return {
    prePerson,
    campaignName,
    preApprovedId: preApprovedId !== '' ? parseInt(preApprovedId, 10) : '',
    userName: props.phoneNumber
  }
}

export default {
  ON_SUBMIT: (props: IProps & any) => async (values: any) => {
    try {
      const preApprovedId = R.pathOr('', ['preApprovedId'], props)
      const input = reformatPerson(props, values, preApprovedId)
      const response = await props.savePersonWithValidation({
        variables: { input }
      })

      if (props.currentTab === STEP1_TABNAME.TAB_B) {
        const statusTabB = R.pathOr(
          '',
          ['data', 'savePersonWithValidation', 'status'],
          response
        )

        if (statusTabB === '1') {
          return props.showErrorMessage(
            t('preApproved.step1.errorMessage.label')
          )
        }
        props.goNext()

        return
      }

      const preApprovedIdResponse = R.pathOr(
        '',
        ['data', 'savePersonWithValidation', 'preApprovedId'],
        response
      )

      await updateStatus(props, preApprovedIdResponse)
      const status = R.pathOr(
        '',
        ['data', 'savePersonWithValidation', 'status'],
        response
      )

      if (status === '1') {
        const errorName = R.pathOr(
          '',
          ['data', 'savePersonWithValidation', 'errorName'],
          response
        )

        props.showErrorMessage(errorName)

        return
      }
      const isBScore = R.pathOr(
        false,
        ['data', 'savePersonWithValidation', 'isBScore'],
        response
      )
      if (isBScore) {
        if (preApprovedId === '') {
          props.goNext(preApprovedIdResponse, 2)

          return
        }
        props.goNext()

        return
      }
      if (preApprovedId === '') {
        props.goNext(preApprovedIdResponse, 1)

        return
      }
      props.setTab(STEP1_TABNAME.TAB_B)
    } catch (e) {
      props.showMessage()
    }
  },
  ON_BACK: (props: any) => async (values: any) => {
    const preApprovedId = R.pathOr('', ['preApprovedId'], props)
    const input = reformatPerson(props, values, preApprovedId)

    await props.savePersonWithNonValidation({
      variables: { input }
    })
    props.setTab(STEP1_TABNAME.TAB_A)
  }
}

export { saveTabA, saveTabB }
