import withLifecycle from '@hocs/with-lifecycle'
import { pathOr } from 'ramda'
import { compose } from 'recompose'
import { withPopup } from '../../../../hocs'
import { STEP } from '../../PreApprovedApplication.type'
import { STEP1_TABNAME } from '../Step1.constant'
import BScore from './BScore.form'

export default compose(
  withPopup,
  withLifecycle({
    onDidMount: async (props: any) => {
      const preApprovedId = pathOr('', ['preApprovedId'], props)

      if (preApprovedId === '') {
        return
      }
      const params = {
        step: STEP.step1,
        childStep: STEP1_TABNAME.TAB_B
      }
      const input = {
        preApprovedId,
        currentStep: JSON.stringify(params)
      }

      props.saveCurrentStep({ variables: { input } })
    }
  })
)(BScore)
