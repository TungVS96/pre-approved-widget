import { rem } from 'dma-ui'
import { css } from 'emotion'
import { pathOr } from 'ramda'
import React, { FunctionComponent } from 'react'
import { renderDropdown } from '../../../../forms/form'
import { FieldWrapper } from '../../PreApprovedApplication.style'
// import { saveTabB } from '../Step1.handler'

const BScore: FunctionComponent<any> = (props) => {
  // const { values, history } = props

  return (
    <>
      <FieldWrapper>
        {renderDropdown({
          props,
          name: 'educationalDegree',
          label: 'preApproved.step1.educationalDegree.label',
          placeholder: 'preApproved.step1.educationalDegree.placeholder',
          disabled: false,
          requiredField: true,
          items: pathOr([], ['dropdownData', 'RC_LIST_EDU_DEGREE'], props),
          fieldContainerStyle: css({ borderRadius: rem(5) }),
          labelStyle: css({ fontWeight: 500 })
        })}
      </FieldWrapper>
      <FieldWrapper>
        {renderDropdown({
          props,
          name: 'accommodationType',
          label: 'preApproved.step1.accommodationType.label',
          placeholder: 'preApproved.step1.accommodationType.placeholder',
          disabled: false,
          requiredField: true,
          items: pathOr([], ['dropdownData', 'RC_LIST_ACCOM_TYPE'], props),
          fieldContainerStyle: css({ borderRadius: rem(5) }),
          labelStyle: css({ fontWeight: 500 })
        })}
      </FieldWrapper>
      <FieldWrapper>
        {renderDropdown({
          props,
          name: 'transportationType',
          label: 'preApproved.step1.transportationType.label',
          placeholder: 'preApproved.step1.transportationType.label',
          disabled: false,
          requiredField: true,
          items: pathOr([], ['dropdownData', 'RC_LIST_TRANSPORT_TYPE'], props),
          fieldContainerStyle: css({ borderRadius: rem(5) }),
          labelStyle: css({ fontWeight: 500 })
        })}
      </FieldWrapper>
      <div className={'mt-4 w-full'} />
    </>
  )
}

export default BScore
