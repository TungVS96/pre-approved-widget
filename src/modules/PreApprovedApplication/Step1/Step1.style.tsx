import { withClassName } from 'dma-ui'
import styled from 'react-emotion'

const Container = withClassName('w-full', 'Container')(styled('div')())

export { Container }
