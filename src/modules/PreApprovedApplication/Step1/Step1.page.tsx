import { withFormik, FormikBag } from 'formik'
import { graphql } from 'react-apollo'
import { compose, withHandlers, withState } from 'recompose'
import { withApp } from '../../../hocs'
import updateStatus from '../Mutation/updateStatus.mutation'
// import { STEP } from '../PreApprovedApplication.type'
import saveDataPersonalInfomationPre from './Mutation/saveDataPersonalInfomationPre.mutation'
import savePerson from './Mutation/savePerson.mutation'
import savePersonWithValidation from './Mutation/savePersonWithValidation.mutation'
import getPerson from './Query/getPerson.query'
import { mapPropsToValues, validate } from './Step1.config'
import { STEP1_TABNAME } from './Step1.constant'
import handlers from './Step1.handler'
import { EnhancedProps, IValues } from './Step1.type'
import Step1 from './Step1.view'

// const mapStateToProps = ({
//   auth: { phoneNumber }
// }: {
//   auth: { phoneNumber: string }
// }) => ({
//   phoneNumber
// })

const formikConfig = {
  validate,
  mapPropsToValues,
  handleSubmit: (
    values: IValues,
    { setSubmitting, props }: FormikBag<EnhancedProps, IValues>
  ) => {
    try {
      props.ON_SUBMIT(values)
    } finally {
      setSubmitting(false)
    }
  },
  enableReinitialize: true
}

export default compose(
  // connect(mapStateToProps),
  withState('currentTab', 'setTab', STEP1_TABNAME.TAB_A),
  graphql(getPerson.query, getPerson.params),
  graphql(savePerson.mutation, savePerson.params),
  graphql(savePersonWithValidation.mutation, savePersonWithValidation.params),
  graphql(updateStatus.mutation, updateStatus.params),
  graphql(
    saveDataPersonalInfomationPre.mutation,
    saveDataPersonalInfomationPre.params
  ),
  withHandlers(handlers),
  withFormik(formikConfig),
  withApp({
    updates: [
      'savePersonWithValidation',
      'savePersonWithNonValidation',
      'updateStatus'
    ]
  })
)(Step1)
