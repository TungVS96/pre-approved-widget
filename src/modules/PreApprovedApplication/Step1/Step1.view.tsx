import match from 'match-values'
import React, { FunctionComponent } from 'react'
import { BottomBarButtons } from '../BottomBarButtons/BottomBarButtons.component'
import {
  ContentWrapper,
  FieldWrapper,
  Image,
  Title
} from '../PreApprovedApplication.style'
import BScore from './BScore/BScore.form.component'
import IdentityInfo from './IdentityInfo/IdentityInfo.form.component'
import { STEP1_TABNAME } from './Step1.constant'
import { Container } from './Step1.style'
import { EnhancedProps } from './Step1.type'

const image = require('./assets/image.svg')
const Step1: FunctionComponent<EnhancedProps> = (props) => {
  const { currentTab, ON_BACK, history, values } = props

  const Form = match(currentTab, {
    [STEP1_TABNAME.TAB_B]: BScore,
    [STEP1_TABNAME.TAB_A]: IdentityInfo
  })
  // console.log({ props })
  return (
    <Container>
      <Title>Nói cho chúng tôi biết đôi điều về quý khách</Title>
      <div className={'flex flex-row w-full'}>
        <ContentWrapper>
          <Form
            {...props}
            phoneNumber={props.phoneNumber}
            history={history}
            dropdownData={props.dropdownData}
            prePerson={props.prePerson}
          />
          <FieldWrapper>
            {currentTab === STEP1_TABNAME.TAB_A && (
              <BottomBarButtons
                id={'pre_fillin_personal_info'}
                onNext={props.handleSubmit}
              />
            )}
            {currentTab === STEP1_TABNAME.TAB_B && (
              <BottomBarButtons
                id={'pre_fillin_personal_info2'}
                onNext={props.handleSubmit}
                onPrev={() => ON_BACK(values)}
              />
            )}
          </FieldWrapper>
        </ContentWrapper>
        <div className={'justify-center sm:hidden md:flex w-1/3'}>
          <Image src={image} alt={'Icon'} />
        </div>
      </div>
    </Container>
  )
}

export default Step1
