import { rem, theme as styledTheme, withClassName } from 'dma-ui'
import { css } from 'emotion'
import styled from 'react-emotion'

const ICON_SIZE_SM = 48
const ICON_SIZE_MD = 64

const Wrapper = withClassName('w-full')(styled('div')())

const ProcessWrapper = withClassName('flex flex-row')(
  styled('div')(({ theme }) => ({
    [theme.mq.md]: {
      width: '60%',
      margin: '0 auto'
    }
  }))
)

const ItemWrapper = withClassName('relative flex-1 flex flex-col')(
  styled('div')(() => ({
    ':last-child': {
      '::after': {
        display: 'none'
      }
    }
  }))
)

const solidLineStyle = (backgroundColor: string) =>
  css({
    '::after': {
      backgroundColor,
      content: '""',
      position: 'absolute',
      height: rem(2),
      width: `calc(100% - ${rem(ICON_SIZE_SM)})`,
      top: rem(ICON_SIZE_SM / 2),
      left: `calc(50% + ${rem(ICON_SIZE_SM / 2)})`
    },
    [styledTheme.mq.md]: {
      '::after': {
        height: rem(3),
        width: `calc(100% - ${rem(ICON_SIZE_MD)})`,
        top: rem(ICON_SIZE_MD / 2),
        left: `calc(50% + ${rem(ICON_SIZE_MD / 2)})`
      }
    }
  })

const IconWrapper = withClassName('flex flex-col items-center justify-center')(
  styled('div')({
    alignSelf: 'center',
    marginBottom: rem(10)
  })
)

const ItemIcon = styled('img')()
const PervasiveBackground = styled('img')({
  position: 'absolute',
  height: rem(ICON_SIZE_SM + 20),
  [styledTheme.mq.md]: {
    height: rem(ICON_SIZE_MD + 20)
  }
})

const ItemLabel = withClassName('text-brand text-center')(
  styled('p')(({ theme }) => ({
    fontSize: rem(12),
    padding: `0 ${rem(16)}`,
    [theme.mq.md]: {
      fontSize: rem(14)
    }
  }))
)

const activeIconStyle = css({
  height: rem(ICON_SIZE_SM),
  [styledTheme.mq.md]: {
    height: rem(ICON_SIZE_MD)
  }
})

const inactiveLabelStyle = css({
  fontWeight: 'normal',
  color: '#7A7A7A'
})

export {
  Wrapper,
  ProcessWrapper,
  ItemWrapper,
  ItemIcon,
  activeIconStyle,
  ItemLabel,
  IconWrapper,
  inactiveLabelStyle,
  solidLineStyle,
  PervasiveBackground
}
