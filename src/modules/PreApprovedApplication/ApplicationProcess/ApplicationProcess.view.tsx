import { theme } from 'dma-ui'
import React, { SFC } from 'react'
import { t } from '../../../i18n'
import {
  activeIconStyle,
  inactiveLabelStyle,
  solidLineStyle,
  IconWrapper,
  ItemIcon,
  ItemLabel,
  ItemWrapper,
  PervasiveBackground,
  ProcessWrapper,
  Wrapper
} from './ApplicationProcess.view.style'

interface Item {
  activeIcon: string
  inactiveIcon: string
  selectedIcon: string
  label: string
}

interface IOuterProps {
  currentStep: number
  lastStep: number
  items: Item[]
}

const pervasiveBackground = require('./assets/ic-pervasive-background.svg')
const ApplicationProcess: SFC<IOuterProps> = (props) => {
  const { currentStep = 1, lastStep = 1, items = [] } = props

  return (
    <Wrapper>
      <ProcessWrapper id={'process-wrapper'}>
        {items.map((item: any, index: number) => {
          const isCurrentStep = currentStep === index + 1

          return (
            <ItemWrapper
              key={item.label}
              className={`${
                index + 1 < lastStep
                  ? solidLineStyle(theme.colors.brand)
                  : solidLineStyle('#B4B4B4')
              }`}
            >
              <IconWrapper>
                <ItemIcon
                  className={activeIconStyle}
                  src={
                    isCurrentStep
                      ? item.selectedIcon
                      : index + 1 <= lastStep
                      ? item.activeIcon
                      : item.inactiveIcon
                  }
                />
                {isCurrentStep && (
                  <PervasiveBackground src={pervasiveBackground} />
                )}
              </IconWrapper>
              <ItemLabel
                className={index + 1 <= lastStep ? '' : inactiveLabelStyle}
              >
                {t(item.label)}
              </ItemLabel>
            </ItemWrapper>
          )
        })}
      </ProcessWrapper>
    </Wrapper>
  )
}

export { IOuterProps, Item }
export default ApplicationProcess
