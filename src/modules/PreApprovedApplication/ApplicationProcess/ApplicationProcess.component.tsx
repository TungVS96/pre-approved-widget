import { compose } from 'recompose'
import ApplicationProcess, { IOuterProps } from './ApplicationProcess.view'

export default compose<IOuterProps, IOuterProps>()(ApplicationProcess)
