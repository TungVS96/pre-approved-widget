import gql from 'graphql-tag'
import { pathOr } from 'ramda'

const query = gql`
  query($preApprovedId: String) {
    getCurrentStep(preApprovedId: $preApprovedId)
      @rest(
        type: "Document"
        path: "/approved/v1/get-current-step?{args}"
        endpoint: "dma-pre-credit"
      ) {
      step
      childStep
      childStepType
    }
  }
`
const preApprovedId = 3246
const params = {
  props: ({ data }: any) => {
    const initialStep = pathOr({}, ['getCurrentStep'], data)

    return {
      initialStep,
      preApprovedId
    }
  },
  options: (props: any) => {
    // const preApprovedId = pathOr('', ['preApprovedId'], props)

    return {
      variables: {
        preApprovedId
      }
    }
  },
  // skip: (props: any): any => {
  //   const preApprovedId = pathOr('', ['preApprovedId'], props)

  //   return preApprovedId === ''
  // }
}

export default { query, params }
