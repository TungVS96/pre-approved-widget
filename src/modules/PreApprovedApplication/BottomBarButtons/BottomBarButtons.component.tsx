import React, { FunctionComponent } from 'react'
// import Modal from '../../../components/Modal/Modal.component'
import { t } from '../../../i18n'
import {
  getButtonStyleByTheme,
  BottomBarContainer,
  Button
} from './BottomBarButtons.component.style'

interface Iprops {
  onPrev?(): void
  onNext?(): void
  containerWrapperStyle?: string
  width?: number
  nextTitle?: string
  id?: string
}

type EnhancedProps = Iprops

const BottomBarButtons: FunctionComponent<EnhancedProps> = (props) => {
  const { onPrev, onNext, nextTitle = '', id } = props

  return (
    <BottomBarContainer className={props.containerWrapperStyle}>
      {onPrev && (
        <Button
          onClick={props.onPrev}
          width={props.width}
          className={getButtonStyleByTheme('light', false)}
          type="button"
        >
          {t('applicationForm.bottomButton.back')}
        </Button>
      )}
      {onNext && (
        <Button
          id={id}
          onClick={props.onNext}
          width={props.width}
          className={getButtonStyleByTheme('brand', false)}
          type="submit"
        >
          {nextTitle !== ''
            ? t(nextTitle)
            : t('applicationForm.bottomButton.next')}
        </Button>
      )}
    </BottomBarContainer>
  )
}

export { EnhancedProps, BottomBarButtons }
export default BottomBarButtons
