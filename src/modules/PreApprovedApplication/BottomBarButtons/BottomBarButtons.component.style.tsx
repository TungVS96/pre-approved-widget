import { rem, theme as UITheme, withClassName } from 'dma-ui'
import { css } from 'emotion'
import styled from 'react-emotion'

const BottomBarContainer = withClassName('flex flex-row w-full')(
  styled('div')({})
)

const Button = withClassName('text-sm text-center w-1/2')(
  styled('button')(
    ({ disabled, width }: { disabled: boolean; width: number }) => ({
      height: rem(40),
      cursor: disabled ? 'default' : 'pointer',
      textTransform: 'uppercase',
      borderRadius: rem(5),
      width: rem(width),
      ':first-child': {
        marginRight: rem(16)
      },
      ':last-child': {
        marginRight: 0
      },
      fontWeight: 500
    })
  )
)

const getButtonStyleByTheme = (
  buttonTheme: 'brand' | 'light',
  disabled: boolean
): string => {
  if (buttonTheme === 'brand') {
    return css({
      backgroundColor: disabled ? '#CFCFCF' : UITheme.colors.brand,
      color: UITheme.colors.white
    })
  }

  return css({
    backgroundColor: UITheme.colors.white,
    color: '#EC1C24',
    border: `${rem(1)} solid #EC1C24`
  })
}

export { Button, getButtonStyleByTheme, BottomBarContainer }
