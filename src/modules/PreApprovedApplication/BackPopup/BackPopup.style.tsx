import { rem, theme, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const Container = withClassName(
  'w-full h-full flex justify-center items-center'
)(styled('div')({}))
const Card = withClassName(
  'flex flex-col relative bg-white items-center rounded-lg'
)(
  styled('div')(({ width = 400, height }: any) => ({
    height,
    borderRadius: rem(2),
    width: '90%',
    padding: `${rem(20)} ${rem(16)} ${rem(20)}`,
    overflowY: 'auto',
    [theme.mq.md]: {
      width,
      height
    }
  }))
)

const HeaderContainer = withClassName('flex flex-row justify-between w-full')(
  styled('div')({})
)

const Title = withClassName('font-semibold')(
  styled('div')({
    marginLeft: rem(8),
    fontSize: rem(18)
  })
)

const BottomButtonContainer = withClassName('flex flex-row justify-end w-full')(
  styled('div')({
    marginTop: rem(21)
  })
)

const ButtonSave = withClassName('bg-brand text-white cursor-pointer')(
  styled('button')({
    borderRadius: rem(2),
    width: rem(120),
    height: rem(40)
  })
)

const ButtonCancle = withClassName('bg-white text-black cursor-pointer')(
  styled('button')({
    borderRadius: rem(2),
    width: rem(120),
    border: `0.5px solid #DCDEE2`,
    marginRight: rem(16),
    height: rem(40)
  })
)

const ButtonDelete = withClassName(' cursor-pointer')(styled('div')({}))

export {
  Container,
  Card,
  ButtonDelete,
  Title,
  HeaderContainer,
  BottomButtonContainer,
  ButtonSave,
  ButtonCancle
}
