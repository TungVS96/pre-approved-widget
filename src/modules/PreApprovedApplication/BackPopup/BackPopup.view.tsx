import { Overlay } from '@blueprintjs/core'
import { t } from 'i18n-js'
import React from 'react'
import {
  BottomButtonContainer,
  ButtonCancle,
  ButtonDelete,
  ButtonSave,
  Card,
  Container,
  HeaderContainer,
  Title
} from './BackPopup.style'

interface IOutterProps {
  isOpen: boolean
  handleClose(): void
  handleBack(): void
  handleSave(): void
}

type EnhancedProps = IOutterProps

const BackPopup: React.SFC<EnhancedProps> = (props) => {
  const { isOpen = false, handleClose, handleBack, handleSave } = props

  return (
    <Overlay
      onClose={handleClose}
      isOpen={isOpen}
      canEscapeKeyClose={true}
      canOutsideClickClose={true}
    >
      <Container>
        <Card>
          <HeaderContainer>
            <div className={'flex flex-row'}>
              <img
                src={require('./../assets/ic-step4-selected.svg')}
                height={20}
                width={20}
              />
              <Title>{t('preApproved.BackPopup.title')}</Title>
            </div>
            <ButtonDelete onClick={handleClose}>
              <img
                src={require('./../assets/ic-step4-selected.svg')}
                height={12}
                width={12}
              />
            </ButtonDelete>
          </HeaderContainer>
          <div className={'mt-4 ml-6'}>
            {t('preApproved.BackPopup.question')}
          </div>
          <BottomButtonContainer>
            <ButtonCancle onClick={handleBack}>{'Không'}</ButtonCancle>
            <ButtonSave onClick={handleSave}>{'Có'}</ButtonSave>
          </BottomButtonContainer>
        </Card>
      </Container>
    </Overlay>
  )
}

export default BackPopup
