import { Overlay } from '@blueprintjs/core'
import React from 'react'
import {
  BottomButtonContainer,
  ButtonCancle,
  ButtonDelete,
  ButtonSave,
  Card,
  Container,
  HeaderContainer,
  Title
} from './ConfirmPopup.style'

interface IOutterProps {
  isOpen: boolean
  handleClose(): void
  handleNo?(): void
  handleYes?(): void
  title: string
  content?: string
}

type EnhancedProps = IOutterProps

const ConfirmPopup: React.FunctionComponent<EnhancedProps> = (props) => {
  const {
    isOpen = false,
    handleClose,
    handleNo,
    handleYes,
    title = '',
    content = ''
  } = props

  return (
    <Overlay
      onClose={handleClose}
      isOpen={isOpen}
      canEscapeKeyClose={true}
      canOutsideClickClose={true}
    >
      <Container>
        <Card>
          <HeaderContainer>
            <div className={'flex flex-row'}>
              <img
                src={require('./assets/ic-warning.svg')}
                height={20}
                width={20}
              />
              <Title>{title}</Title>
            </div>
            <ButtonDelete onClick={handleClose}>
              <img src={require('./assets/ic-x.svg')} height={12} width={12} />
            </ButtonDelete>
          </HeaderContainer>
          {content !== '' && <div className={'mt-4 ml-6'}>{content}</div>}
          <BottomButtonContainer>
            {handleNo && (
              <ButtonCancle onClick={handleNo}>{'Không'}</ButtonCancle>
            )}
            {handleYes && <ButtonSave onClick={handleYes}>{'Có'}</ButtonSave>}
          </BottomButtonContainer>
        </Card>
      </Container>
    </Overlay>
  )
}

export default ConfirmPopup
