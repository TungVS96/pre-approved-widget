import { Location } from 'history'
import React, { useEffect, useState } from 'react'
import { Prompt } from 'react-router-dom'
import BackPopup from './ConfirmPopup.view'

interface IProps {
  when?: boolean | undefined
  navigate(path: string): void
  shouldBlockNavigation(location: Location): boolean
  onSave?(): void
  title: string
  content: string
  stay?: boolean
}

const ConfirmPopup = ({
  when,
  navigate,
  shouldBlockNavigation,
  onSave,
  title,
  content,
  stay = false
}: IProps) => {
  const [modalVisible, setModalVisible] = useState(false)
  const [lastLocation, setLastLocation] = useState<Location | null>(null)
  const [confirmedNavigation, setConfirmedNavigation] = useState(false)
  const closeModal = () => {
    setModalVisible(false)
  }
  const handleNo = () => {
    setModalVisible(false)
    if (!stay) { setConfirmedNavigation(true) }
  }
  const handleYes = async () => {
    setModalVisible(false)
    setConfirmedNavigation(true)
    if (onSave) { await onSave() }
  }
  const handleBlockedNavigation = (nextLocation: Location): boolean => {
    if (!confirmedNavigation && shouldBlockNavigation(nextLocation)) {
      setModalVisible(true)
      setLastLocation(nextLocation)

      return false
    }

    return true
  }

  useEffect(() => {
    if (confirmedNavigation && lastLocation) {
      navigate(lastLocation.pathname)
    }
  }, [confirmedNavigation, lastLocation])

  return (
    <React.Fragment>
      <Prompt when={when} message={handleBlockedNavigation} />
      <BackPopup
        isOpen={modalVisible}
        handleYes={handleYes}
        handleNo={handleNo}
        handleClose={closeModal}
        title={title}
        content={content}
      />
    </React.Fragment>
  )
}

export default ConfirmPopup
