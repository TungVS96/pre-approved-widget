import { pathOr } from 'ramda'
import { EnhancedProps } from './PreApprovedApplication.type'

export default {
  onDidMount: async (props: EnhancedProps) => {
    const input = [
      'RC_LIST_ACCOM_TYPE',
      'RC_LIST_MARITAL_STATUS',
      'RC_LIST_EDU_DEGREE',
      'RC_LIST_TRANSPORT_TYPE',
      'RC_LOAN_PRODUCT_LIST'
    ]

    const results = await props.getDataDropdownList({ variables: { input } })
    const dropdownData = pathOr({}, ['data', 'getDataDropdownList'], results)

    props.setDropdownData(dropdownData)
  },
  onWillUnmount: async (props: EnhancedProps) => {
    // await props.resetAutoSaved()
  }
}
