import { PopupProps } from '../../hocs'
import { IDropDownItem } from '../../types'

const step1ActiveIc = require('./assets/ic-step1-active.svg')
const step1InactiveIc = require('./assets/ic-step1.svg')
const step1SelectedIc = require('./assets/ic-step1-selected.svg')
const step2ActiveIc = require('./assets/ic-step2-active.svg')
const step2InactiveIc = require('./assets/ic-step2.svg')
const step2SelectedIc = require('./assets/ic-step2-selected.svg')
const step3ActiveIc = require('./assets/ic-step3-active.svg')
const step3InactiveIc = require('./assets/ic-step3.svg')
const step3SelectedIc = require('./assets/ic-step3-selected.svg')
const step4ActiveIc = require('./assets/ic-step4-active.svg')
const step4InactiveIc = require('./assets/ic-step4.svg')
const step4SelectedIc = require('./assets/ic-step4-selected.svg')

const procedures = [
  {
    activeIcon: step1ActiveIc,
    inactiveIcon: step1InactiveIc,
    selectedIcon: step1SelectedIc,
    label: 'PreApprovedApplication.header.1'
  },
  {
    activeIcon: step2ActiveIc,
    inactiveIcon: step2InactiveIc,
    selectedIcon: step2SelectedIc,
    label: 'PreApprovedApplication.header.2'
  },
  {
    activeIcon: step3ActiveIc,
    inactiveIcon: step3InactiveIc,
    selectedIcon: step3SelectedIc,
    label: 'PreApprovedApplication.header.3'
  },
  {
    activeIcon: step4ActiveIc,
    inactiveIcon: step4InactiveIc,
    selectedIcon: step4SelectedIc,
    label: 'PreApprovedApplication.header.4'
  }
] as any[]

interface IProps {
  currentStep: STEP
  setStep(currentStep: STEP): void
  setDropdownData(dropdownData: {
    RC_LIST_ACCOM_TYPE: IDropDownItem[]
    RC_LIST_EDU_DEGREE: IDropDownItem[]
    RC_LIST_TRANSPORT_TYPE: IDropDownItem[]
  }): void
  dropdownData: {
    RC_LIST_ACCOM_TYPE: IDropDownItem[]
  }
  getDataDropdownList(payload: any): void
  saveLoanPreference(variable: any): void
  resetAutoSaved(): void
  initialStep: any
  saveCurrentStep(input: any): void
  setChildStep(childStep: string): void
  setPrevStep(prevStep: number): void
}

interface IHandlers {
  ON_BACK(): void
  GO_NEXT(id: number, step: number): void
  GO_TO_APPROVED_RESULT(): void
}

type EnhancedProps = IProps & IHandlers

interface IParentProps extends PopupProps {
  goNext(id?: number, step?: number): void
  goBack(): void
  history: History
  showMessage(): void
}

enum STEP {
  step1 = 1,
  step2 = 2,
  step3 = 3,
  step4 = 4,
  overview = 0
}

export { IProps, procedures, STEP, IHandlers, EnhancedProps, IParentProps }
