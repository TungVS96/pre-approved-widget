import { match } from 'match-values'
import React, { FunctionComponent } from 'react'
import PageHeader from './PageHeader/PageHeader.component'
import { Container } from './PreApprovedApplication.style'
import { procedures, EnhancedProps, STEP } from './PreApprovedApplication.type'
import Step1 from './Step1/Step1.page'
import Step2 from './Step2/Step2.page'
import Step3 from './Step3/Step3.page'
import Step4 from './Step4/Step4.page'
import PreApprovedApplicationOverview from '../PreApprovedApplicationOverview/PreApprovedApplicationOverview.view'

const PreApprovedApplication: FunctionComponent<EnhancedProps> = (props) => {
  const {
    currentStep,
    ON_BACK,
    GO_NEXT
  } = props
  const Form = match(currentStep, {
    [STEP.step1]: Step1,
    [STEP.step2]: Step2,
    [STEP.step3]: Step3,
    [STEP.step4]: Step4,
    _: <></>
  })

  return (
    <Container>
      {currentStep === STEP.overview ?
        <PreApprovedApplicationOverview {...props} goNext={GO_NEXT} />
        :
        <>
          <div className={'shadow'}>
            <PageHeader items={procedures} currentStep={currentStep} />
          </div>
          <Form
            {...props}
            goBack={ON_BACK}
            goNext={GO_NEXT}
            dropdownData={props.dropdownData}
            goToApprovedResult={props.GO_TO_APPROVED_RESULT}
          />
        </>}
    </Container>
  )
}

export default PreApprovedApplication
