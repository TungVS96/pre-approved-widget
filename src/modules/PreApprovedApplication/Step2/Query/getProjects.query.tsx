import gql from 'graphql-tag'
import { pathOr } from 'ramda'

const query = gql`
  query($uniq: Boolean) {
    projects(uniq: $uniq)
      @rest(
        type: "Projects"
        path: "/categories/v1/project-for-approve"
        endpoint: "dma-pre-credit"
      ) {
      value: projectCode
      name: projectName
    }
  }
`
const params = {
  props: ({ data }: any) => {
    const projects = pathOr([], ['projects'], data)

    return { data, projects }
  },
  options: (props: any) => ({
    variables: {}
  })
}

export default { query, params }
