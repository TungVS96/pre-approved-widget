import gql from 'graphql-tag'
import { isEmpty, pathOr } from 'ramda'

const query = gql`
  query($preApprovedId: String) {
    preLoan(preApprovedId: $preApprovedId)
      @rest(
        type: "pre_approved"
        path: "/approved/v1/get-loan-preference?{args}"
        endpoint: "dma-pre-credit"
      ) {
      preLoanPreferences @type(name: preLoanPreferences) {
        isInterestRateSupport
        loanAmount
        loanTermInMonths
        originalGracePeriodInMonth
        productCode
        projectCode
        projectName
        repaymentMethod
      }
    }
  }
`

const params = {
  props: ({ data }: any) => {
    const preLoan = pathOr({}, ['preLoan', 'preLoanPreferences'], data)

    return {
      preLoan
    }
  },
  options: (props: any) => {
    const preApprovedId = pathOr('', ['preApprovedId'], props)

    return {
      variables: {
        preApprovedId
      }
    }
  },
  skip: (props: any): any => {
    const preApprovedId = pathOr('', ['preApprovedId'], props)

    return isEmpty(preApprovedId)
  }
}

export default { query, params }
