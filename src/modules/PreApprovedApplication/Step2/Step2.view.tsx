import { rem } from 'dma-ui'
import { css } from 'emotion'
import { InjectedFormikProps } from 'formik'
import { FormValues } from 'formik-validators'
// import { equals } from 'ramda'
import React, { FunctionComponent } from 'react'
import {
  renderDropdown,
  renderInputText,
  renderTextInputSuggestMoney
} from '../../../forms/form'
import { PopupProps } from '../../../hocs'
// import { t } from '../../../i18n'
import { IDropDownItem } from '../../../types'
import { formatCurrency, getPositiveNumberOnly } from '../../../utils'
import BottomBarButtons from '../BottomBarButtons/BottomBarButtons.component'
// import ModalExitComponent from '../Components/ModalExit/ModalExit.component'
// import ConfirmPopup from '../ConfirmPopup/ConfirmPopup.component'
import {
  ContentWrapper,
  FieldWrapper,
  Image,
  Title
} from '../PreApprovedApplication.style'
import { IParentProps } from '../PreApprovedApplication.type'
import { Container } from './Step2.style'
interface IProps extends IParentProps, PopupProps {
  history: History
  projects: IDropDownItem[]
}
const onlyNumber = (value: string) => {
  return value.toString().replace(/[^\d]+/g, '')
}

interface IValues extends FormValues {
  projectCode: string
  projectName: string
  productCode: string
  loanAmount: string
  loanTermInYear: string
  isInterestRateSupport: string
}

interface IHandlers {
  ON_SUBMIT(values: IValues): void
  ON_BACK(values?: IValues): () => void
  saveLoanPreference(variables: any): void
  ONLY_SAVE(values?: IValues): void
}

const productList = [
  {
    name: 'Chung cư',
    value: 'CC'
  }
]

type EnhancedProps = InjectedFormikProps<IProps, IValues> & IHandlers
const renderForm = (props: EnhancedProps) => {
  const { projects = [], ON_BACK } = props

  return (
    <ContentWrapper>
      <FieldWrapper>
        {renderDropdown({
          props,
          name: 'projectCode',
          label: 'preApproved.step2.projectCode.label',
          placeholder: 'preApproved.step2.projectCode.placeholder',
          disabled: false,
          requiredField: true,
          items: projects,
          fieldContainerStyle: css({ borderRadius: rem(5) }),
          labelStyle: css({ fontWeight: 500 })
        })}
      </FieldWrapper>
      <FieldWrapper>
        {renderDropdown({
          props,
          name: 'productCode',
          label: 'preApproved.step2.productCode.label',
          placeholder: 'preApproved.step2.productCode.placeholder',
          disabled: true,
          requiredField: true,
          items: productList,
          fieldContainerStyle: css({ borderRadius: rem(5) }),
          labelStyle: css({ fontWeight: 500 })
        })}
      </FieldWrapper>
      <FieldWrapper>
        {renderTextInputSuggestMoney({
          props,
          name: 'loanAmount',
          label: 'preApproved.step2.loanAmount.label',
          placeholder: 'preApproved.step2.loanAmount.placeholder',
          disabled: false,
          requiredField: true,
          maxLength: 50,
          transformValue: (value) => getPositiveNumberOnly(value),
          format: (value) => formatCurrency({ value }),
          times: 1000000,
          suggestMoney: true
        })}
      </FieldWrapper>
      <FieldWrapper>
        {renderInputText({
          props,
          name: 'loanTermInYear',
          label: 'preApproved.step2.loanTermInYear.label',
          placeholder: 'preApproved.step2.loanTermInYear.placeholder',
          disabled: false,
          requiredField: true,
          maxLength: 2,
          transformValue: (value) => onlyNumber(value)
        })}
      </FieldWrapper>
      <div className={'mt-4 w-full'} />
      <FieldWrapper>
        <BottomBarButtons
          id={'pre_apartment_info'}
          onNext={props.handleSubmit}
          onPrev={ON_BACK(props.values)}
        />
      </FieldWrapper>
    </ContentWrapper>
  )
}

const image = require('./assets/image.svg')

const Step2: FunctionComponent<EnhancedProps> = (props) => {
  // const { initialValues, values, history } = props
  // const { values, history } = props

  // const edited = !equals(initialValues, values)
  return (
    <Container>
      <Title>Chia sẻ cho chúng tôi về căn hộ quý khách dự định mua</Title>
      <div className={'flex flex-row w-full'}>
        {renderForm(props)}
        <div className={'justify-center sm:hidden md:flex w-1/3'}>
          <Image src={image} alt={'Icon'} />
        </div>
      </div>
      {/* <ConfirmPopup
        title={t('preApproved.BackPopup.title')}
        content={t('preApproved.BackPopup.question')}
        when={edited}
        navigate={(path) => history.push(path)}
        shouldBlockNavigation={(location) => {
          if (edited && !location.pathname.includes('/pre-approved')) {
            return true
          }

          return false
        }}
        onSave={() => {
          //
        }}
      /> */}
      {/* <ModalExitComponent
        history={history}
        saveData={props.ONLY_SAVE(values)}
      /> */}
    </Container>
  )
}

export { IProps, EnhancedProps, IValues, IHandlers, productList }
export default Step2
