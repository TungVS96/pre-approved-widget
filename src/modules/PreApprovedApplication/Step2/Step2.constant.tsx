const RADIO_ANSWER = {
  YES: 'Y',
  NO: 'N'
}

export { RADIO_ANSWER }
