import { pathOr } from 'ramda'
import { parseIntMoney } from 'src/utils/number.util'
import { EnhancedProps, IValues } from './Step2.view'

const onBackSavingDataWithoutValidate = async (
  props: EnhancedProps,
  values: IValues
) => {
  const preApprovedId = pathOr('', ['preApprovedId'], props)

  const project = props.projects.find((e) => e.value === values.projectCode)
  const projectName = project && project.name

  const input = {
    preApprovedId,
    preLoanPreferences: {
      projectName,
      projectCode: values.projectCode,
      productCode: values.productCode,
      loanAmount: parseIntMoney(values.loanAmount),
      interestRate: 10.5, // hard code
      /*
        db lưu loanTermInMonths, UI hiển thị loanTermInYear, backend tự xử lý nhân chia 12, frontend không phải làm
        => nên khi gửi từ client lên sẽ lấy giá trị trên UI rồi gán giá trị này (loanTermInYear) trên UI cho trường loanTermInMonths
      */
      loanTermInMonths: parseInt(values.loanTermInYear, 10),
      isInterestRateSupport: values.isInterestRateSupport || ''
    }
  }

  await props.saveLoanPreference({
    variables: { input }
  })
}

export default {
  ON_SUBMIT: (props: EnhancedProps) => async (values: IValues) => {
    await onBackSavingDataWithoutValidate(props, values)
    props.goNext()
  },
  ON_BACK: (props: EnhancedProps) => (values: IValues) => async () => {
    await onBackSavingDataWithoutValidate(props, values)
    props.goBack()
  },
  ONLY_SAVE: (props: EnhancedProps) => (values: IValues) => async () => {
    const preApprovedId = pathOr('', ['preApprovedId'], props)

    // const values = pathOr({}, ['values'], props)
    const project = props.projects.find(
      (e: any) => e.value === values.projectCode
    )
    const projectName = project && project.name
    const input = {
      preApprovedId,
      preLoanPreferences: {
        projectName,
        projectCode: values.projectCode,
        productCode: values.productCode,
        loanAmount: parseIntMoney(values.loanAmount),
        /*
          db lưu loanTermInMonths, UI hiển thị loanTermInYear, backend tự xử lý nhân chia 12, frontend không phải làm
          => nên khi gửi từ client lên sẽ lấy giá trị trên UI rồi gán giá trị này (loanTermInYear) trên UI cho trường loanTermInMonths
        */
        loanTermInMonths: parseInt(values.loanTermInYear, 10),
        isInterestRateSupport: values.isInterestRateSupport || ''
      }
    }

    await props.saveLoanPreference({
      variables: { input }
    })
  }
}
