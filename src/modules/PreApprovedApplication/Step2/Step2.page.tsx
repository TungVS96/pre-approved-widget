import withLifecycle from '@hocs/with-lifecycle'
import { withFormik, FormikBag } from 'formik'
import { required } from 'formik-validators'
import { pathOr } from 'ramda'
import { graphql } from 'react-apollo'
import { compose, withHandlers } from 'recompose'
import { validator } from '../../../forms'
import { withApp, withPopup } from '../../../hocs'
import { STEP } from '../PreApprovedApplication.type'
import { isMinValue, valueInRange } from '../PreApprovedApplication.util'
import saveLoanPreference from './../Mutation/saveLoanPreference.mutation'
import getLoanPreference from './Query/getLoanPreference.query'
import getProjects from './Query/getProjects.query'
import handlers from './Step2.handler'
import Step2, { EnhancedProps, IValues } from './Step2.view'
const mapPropsToValues = (props: any) => {
  return {
    projectCode: pathOr('', ['preLoan', 'projectCode'], props),
    productCode: 'CC',
    projectName: pathOr('', ['preLoan', 'projectName'], props),
    loanAmount: pathOr('', ['preLoan', 'loanAmount'], props),
    /*
      db lưu loanTermInMonths, UI hiển thị loanTermInYear, backend tự xử lý nhân chia 12, frontend không phải làm
      => nên khi gửi từ client lên sẽ lấy giá trị trên UI rồi gán giá trị này (loanTermInYear) trên UI cho trường loanTermInMonths
    */
    loanTermInYear: pathOr('', ['preLoan', 'loanTermInMonths'], props),
    isInterestRateSupport: pathOr(
      '',
      ['preLoan', 'isInterestRateSupport'],
      props
    )
  }
}

const configs = {
  projectCode: [required('preApproved.step2.projectCode.required')],
  productCode: [required('preApproved.step2.productCode.required')],
  loanAmount: [
    required('preApproved.step2.loanAmount.required'),
    isMinValue('preApproved.step2.loanAmount.required', 1, true)
  ],
  loanTermInYear: [
    required('preApproved.step2.loanTermInYear.required'),
    valueInRange('preApproved.step2.loanTermInYear.required', 5, 35)
  ]
}

const validate = (values: IValues, props: EnhancedProps) => {
  return validator(configs)(values, props)
}

const formikConfig = {
  validate,
  mapPropsToValues,
  handleSubmit: (
    values: IValues,
    { setSubmitting, props }: FormikBag<EnhancedProps, any>
  ) => {
    try {
      props.ON_SUBMIT(values)
    } finally {
      setSubmitting(false)
    }
  },
  enableReinitialize: true
}

export default compose(
  graphql(getProjects.query, getProjects.params),
  graphql(getLoanPreference.query, getLoanPreference.params),
  graphql(saveLoanPreference.mutation, saveLoanPreference.params),
  withApp({ notificationModal: true }),
  withHandlers(handlers),
  withFormik(formikConfig),
  withPopup,
  withLifecycle({
    onDidMount: async (props: any) => {
      const params = {
        step: STEP.step2
      }
      const preApprovedId = pathOr('', ['preApprovedId'], props)
      const input = {
        preApprovedId,
        currentStep: JSON.stringify(params)
      }

      props.saveCurrentStep({ variables: { input } })
    }
  })
)(Step2)
