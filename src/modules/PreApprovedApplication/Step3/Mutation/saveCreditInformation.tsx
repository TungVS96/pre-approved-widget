import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    saveCreditInformation(input: $input)
      @rest(
        type: "Person"
        endpoint: "dma-pre-credit"
        path: "/approved/v1/save-credit-information"
        method: "post"
      ) {
      status
      isBScore
      preApprovedId
      errorCode
      errorName
    }
  }
`
const params = {
  name: 'saveCreditInformation'
}

export default {
  mutation,
  params
}
