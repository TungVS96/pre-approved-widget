import { pathOr } from 'ramda'
import { EnhancedProps } from './IncomesInfo.type'

export default {
  onDidMount: async (props: EnhancedProps) => {
    window.scrollTo(0, 0)
    const input = [
      'RC_LIST_ACCOM_TYPE',
      'RC_LIST_MARITAL_STATUS',
      'RC_LIST_EDU_DEGREE',
      'RC_LIST_TRANSPORT_TYPE',
      // salary
      'RC_LIST_PROFESSIONAL',
      'RC_LIST_POSITION',
      'RC_LIST_PAYMENT_METHOD',

      'RC_LIST_VERHICLE_VENDOR',
      'RC_LIST_VEHICLE_MODEL',
      'RC_LIST_PAYMENT_METHOD_RENTAL',
      'RC_LIST_HOUSERENT_TYPE',
      'RC_LIST_HOUSERENT_LOCATION',

      // housebold bussiness
      'RC_LIST_INDUSTRY_GROUP',
      'RC_LIST_PROVINCE',
      'RC_LOAN_PRODUCT_LIST'
    ]

    const results = await props.getDataDropdownList({ variables: { input } })

    const dropdownData = {
      RC_LIST_ACCOM_TYPE: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_ACCOM_TYPE'],
        results
      ),
      RC_LIST_MARITAL_STATUS: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_MARITAL_STATUS'],
        results
      ),
      RC_LIST_EDU_DEGREE: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_EDU_DEGREE'],
        results
      ),
      RC_LIST_TRANSPORT_TYPE: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_TRANSPORT_TYPE'],
        results
      ),
      RC_LIST_VERHICLE_VENDOR: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_VERHICLE_VENDOR'],
        results
      ),
      RC_LIST_VEHICLE_MODEL: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_VEHICLE_MODEL'],
        results
      ),
      RC_LIST_PAYMENT_METHOD_RENTAL: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_PAYMENT_METHOD_RENTAL'],
        results
      ),
      RC_LIST_HOUSERENT_TYPE: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_HOUSERENT_TYPE'],
        results
      ),
      RC_LIST_HOUSERENT_LOCATION: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_HOUSERENT_LOCATION'],
        results
      ),
      RC_LIST_PROFESSIONAL: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_PROFESSIONAL'],
        results
      ),
      RC_LIST_POSITION: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_POSITION'],
        results
      ),
      RC_LIST_PAYMENT_METHOD: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_PAYMENT_METHOD'],
        results
      ),
      RC_LIST_INDUSTRY_GROUP: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_INDUSTRY_GROUP'],
        results
      ),
      // RC_LIST_INDUSTRY: pathOr(
      //   [],
      //   ['data', 'getDataDropdownList', 'RC_LIST_INDUSTRY'],
      //   results
      // ),
      RC_LIST_PROVINCE: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LIST_PROVINCE'],
        results
      ),
      RC_LOAN_PRODUCT_LIST: pathOr(
        [],
        ['data', 'getDataDropdownList', 'RC_LOAN_PRODUCT_LIST'],
        results
      )
    }

    props.setDropdownData(dropdownData)
    /* const step = pathOr('', ['initialStep', 'step'], props)
    const childStep = pathOr('', ['initialStep', 'childStep'], props)
    const childStepType = pathOr('', ['initialStep', 'childStepType'], props)
    
    if (step === 3 && childStep === '' && childStepType !== '') {
      props.setTypeOfIncome(childStepType)
    } */
  }
}
