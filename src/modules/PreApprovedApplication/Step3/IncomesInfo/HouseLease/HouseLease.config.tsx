import { required } from 'formik-validators'
import * as R from 'ramda'
import { validator } from '../../../../../forms'
import { isMinValue } from '../../../PreApprovedApplication.util'

const houseRentInitial = {
  typeOfProperty: '',
  location: '',
  paymentMethod: 'FTR',
  incomeAmount: ''
}
const mapPropsToValues = (props: any) => {
  const preHouseLease = R.pathOr(
    {},
    ['preIncomes', 'houseLeaseIncomes', props.currentIndex],
    props
  )
  if (!R.isEmpty(preHouseLease)) {
    return preHouseLease
  }

  return houseRentInitial
}

const configs = {
  typeOfProperty: [
    required('PreApprovedApplication.step3.houselease.typeOfProperty.required')
  ],
  location: [
    required('PreApprovedApplication.step3.houselease.location.required')
  ],
  paymentMethod: [
    required('PreApprovedApplication.step3.houselease.paymentmethod.required')
  ],
  incomeAmount: [
    required('PreApprovedApplication.step3.houselease.incomeAmount.required'),
    isMinValue('preApproved.step2.propertyValue.required', 1, true)
  ]
}

const validate = (values: any, props: any) => {
  const errors = validator(configs)(values, props)

  return {
    ...errors
  }
}

export { mapPropsToValues, validate }
