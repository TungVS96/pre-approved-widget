import { rem } from 'dma-ui'
import { css } from 'emotion'
import { InjectedFormikProps } from 'formik'
import React, { FunctionComponent } from 'react'
import { IDropDownItem } from 'src/types'
import {
  renderDropdown,
  renderTextInputSuggestMoney
} from '../../../../../forms/form'
import { formatCurrency, getPositiveNumberOnly } from '../../../../../utils'
import BottomBarButtons from '../../../BottomBarButtons/BottomBarButtons.component'
import { Container, ContentWrapper, IncomeWrapper } from '../IncomesInfo.style'
import { IHouseLease } from '../IncomesInfo.type'
interface IInnerProps {
  onBack(): void
  dropdownData: {
    RC_LIST_HOUSERENT_TYPE: IDropDownItem[]
    RC_LIST_HOUSERENT_LOCATION: IDropDownItem[]
    RC_LIST_PAYMENT_METHOD_RENTAL: IDropDownItem[]
  }
}

type HouseLeaseEnhancedProps = InjectedFormikProps<IInnerProps, IHouseLease>
const HouseLease: FunctionComponent<HouseLeaseEnhancedProps> = (props) => {
  const {
    RC_LIST_HOUSERENT_TYPE = [],
    RC_LIST_HOUSERENT_LOCATION = [],
    RC_LIST_PAYMENT_METHOD_RENTAL = []
  } = props.dropdownData

  return (
    <Container>
      <div className={'mt-8'} />
      <ContentWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'typeOfProperty',
            label:
              'PreApprovedApplication.step3.houselease.typeOfProperty.label',
            placeholder:
              'PreApprovedApplication.step3.houselease.typeOfProperty.placeholder',
            disabled: false,
            requiredField: true,
            items: RC_LIST_HOUSERENT_TYPE,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'location',
            label: 'PreApprovedApplication.step3.houselease.location.label',
            placeholder:
              'PreApprovedApplication.step3.houselease.location.placeholder',
            disabled: false,
            requiredField: true,
            items: RC_LIST_HOUSERENT_LOCATION,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'paymentMethod',
            label:
              'PreApprovedApplication.step3.houselease.paymentmethod.label',
            placeholder:
              'PreApprovedApplication.step3.houselease.paymentmethod.placeholder',
            disabled: false,
            requiredField: true,
            items: RC_LIST_PAYMENT_METHOD_RENTAL,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderTextInputSuggestMoney({
            props,
            name: 'incomeAmount',
            label: 'PreApprovedApplication.step3.houselease.incomeAmount.label',
            placeholder:
              'PreApprovedApplication.step3.houselease.incomeAmount.placeholder',
            disabled: false,
            requiredField: true,
            transformValue: (value) => getPositiveNumberOnly(value),
            format: (value) => formatCurrency({ value }),
            times: 1000000
          })}
        </IncomeWrapper>
        <div className={'w-full'} />
        <IncomeWrapper>
          <BottomBarButtons onNext={props.handleSubmit} onPrev={props.onBack} />
        </IncomeWrapper>
      </ContentWrapper>
    </Container>
  )
}

export { IInnerProps }
export default HouseLease
