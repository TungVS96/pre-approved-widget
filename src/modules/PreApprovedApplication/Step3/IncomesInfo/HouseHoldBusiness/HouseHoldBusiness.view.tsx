import { rem } from 'dma-ui'
import { css } from 'emotion'
import { InjectedFormikProps } from 'formik'
import { pathOr } from 'ramda'
import React, { FunctionComponent } from 'react'
import { IDropDownItem } from 'src/types'
import { RadioInput } from '../../../../../forms'
import {
  renderDropdown,
  renderTextInputSuggestMoney
} from '../../../../../forms/form'
import { t } from '../../../../../i18n'
import {
  formatCurrency,
  getFieldProps,
  getPositiveNumberOnly
} from '../../../../../utils'
import BottomBarButtons from '../../../BottomBarButtons/BottomBarButtons.component'
import { Container, ContentWrapper, IncomeWrapper } from '../IncomesInfo.style'
import { IHouseHoldBusiness } from '../IncomesInfo.type'
interface IInnerProps {
  onBack(): void
  dropdownData: {
    RC_LIST_INDUSTRY_GROUP: IDropDownItem[]
    RC_LIST_INDUSTRY: IDropDownItem[]
    RC_LIST_PROVINCE: IDropDownItem[]
  }
  ownerList: any
}

type HouseHoldBusinessEnhancedProps = InjectedFormikProps<
  IInnerProps,
  IHouseHoldBusiness
>
const HouseHoldBusiness: FunctionComponent<HouseHoldBusinessEnhancedProps> = (
  props
) => {
  const {
    RC_LIST_INDUSTRY_GROUP = [],
    // RC_LIST_INDUSTRY = [],
    RC_LIST_PROVINCE = []
  } = props.dropdownData
  let industryLevel1 = pathOr(
    [],
    ['listIndustryLevel', 'industryLevel1'],
    props
  )
  industryLevel1 = industryLevel1.sort((a: any, b: any) => {
    const x = a.value
    const y = b.value

    return parseInt(x, 10) < parseInt(y, 10)
      ? -1
      : parseInt(x, 10) > parseInt(y, 10)
      ? 1
      : 0
  })
  const industryLevel2Original = pathOr(
    [],
    ['listIndustryLevel', 'industryLevel2'],
    props
  )
  const industryLevel3Original = pathOr(
    [],
    ['listIndustryLevel', 'industryLevel3'],
    props
  )
  const industryLevel2 = {}
  const industryLevel3 = {}
  industryLevel1.map((el: any) => {
    industryLevel2[el.value] = []
  })
  industryLevel2Original.map((el: any) => {
    industryLevel2[el.parentCode].push(el)
    industryLevel3[el.value] = []
  })
  industryLevel3Original.map((el: any) => {
    industryLevel3[el.parentCode].push(el)
  })
  const businessLine1 = pathOr('', ['values', 'businessLine1'], props)
  const businessLine2 = pathOr('', ['values', 'businessLine2'], props)
  let INDUSTRY_LEVEL_2 = industryLevel2[businessLine1] || []
  INDUSTRY_LEVEL_2 = INDUSTRY_LEVEL_2.sort((a: any, b: any) => {
    const x = a.value.split('_')[1]
    const y = b.value.split('_')[1]

    return parseInt(x, 10) < parseInt(y, 10)
      ? -1
      : parseInt(x, 10) > parseInt(y, 10)
      ? 1
      : 0
  })
  let INDUSTRY_LEVEL_3 = industryLevel3[businessLine2] || []
  INDUSTRY_LEVEL_3 = INDUSTRY_LEVEL_3.sort((a: any, b: any) => {
    const x = a.value.split('_')[2]
    const y = b.value.split('_')[2]

    return parseInt(x, 10) < parseInt(y, 10)
      ? -1
      : parseInt(x, 10) > parseInt(y, 10)
      ? 1
      : 0
  })

  return (
    <Container>
      <div className={'mt-8'} />
      <ContentWrapper>
        <IncomeWrapper>
          <RadioInput
            {...getFieldProps('ownerShip', props)}
            options={props.ownerList}
            vertical={false}
            label={t('PreApprovedApplication.step3.household.ownerShip.label')}
          />
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'businessType',
            label: 'PreApprovedApplication.step3.household.businessType.label',
            placeholder:
              'PreApprovedApplication.step3.household.businessType.placeholder',
            disabled: false,
            requiredField: true,
            items: RC_LIST_INDUSTRY_GROUP,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'businessLine1',
            label: 'PreApprovedApplication.step3.household.businessLine1.label',
            placeholder:
              'PreApprovedApplication.step3.household.businessLine1.placeholder',
            disabled: false,
            requiredField: true,
            items: industryLevel1,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'businessLine2',
            label: 'PreApprovedApplication.step3.household.businessLine2.label',
            placeholder:
              'PreApprovedApplication.step3.household.businessLine2.placeholder',
            disabled: false,
            requiredField: true,
            items: INDUSTRY_LEVEL_2,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'businessLine3',
            label: 'PreApprovedApplication.step3.household.businessLine3.label',
            placeholder:
              'PreApprovedApplication.step3.household.businessLine3.placeholder',
            disabled: false,
            requiredField: true,
            items: INDUSTRY_LEVEL_3,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'city',
            label: 'PreApprovedApplication.step3.household.city.label',
            placeholder:
              'PreApprovedApplication.step3.household.city.placeholder',
            disabled: false,
            requiredField: true,
            items: RC_LIST_PROVINCE,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderTextInputSuggestMoney({
            props,
            name: 'averageRevenueInLast3months',
            label:
              'PreApprovedApplication.step3.household.averageRevenueInLast3months.label',
            placeholder:
              'PreApprovedApplication.step3.household.averageRevenueInLast3months.placeholder',
            disabled: false,
            requiredField: true,
            transformValue: (value) => getPositiveNumberOnly(value),
            format: (value) => formatCurrency({ value }),
            suggestMoney: true,
            times: 1000000,
            autoComplete: 'off'
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderTextInputSuggestMoney({
            props,
            name: 'averageProfitInLast3months',
            label:
              'PreApprovedApplication.step3.household.averageProfitInLast3months.label',
            placeholder:
              'PreApprovedApplication.step3.household.averageProfitInLast3months.placeholder',
            disabled: false,
            requiredField: true,
            transformValue: (value) => getPositiveNumberOnly(value),
            format: (value) => formatCurrency({ value }),
            suggestMoney: true,
            times: 1000000,
            autoComplete: 'off'
          })}
        </IncomeWrapper>
        <div className={'w-full'} />
        <IncomeWrapper>
          <BottomBarButtons onNext={props.handleSubmit} onPrev={props.onBack} />
        </IncomeWrapper>
      </ContentWrapper>
    </Container>
  )
}

export default HouseHoldBusiness
