import { required } from 'formik-validators'
import * as R from 'ramda'
import { validator } from '../../../../../forms'
import { isMinValue } from '../../../PreApprovedApplication.util'
import { EnhancedProps } from '../IncomesInfo.type'

const preHouseHoldBusinessInitial = {
  ownerShip: 'SELF',
  businessType: '',
  businessLine1: '',
  businessLine2: '',
  businessLine3: '',
  city: '',
  averageRevenueInLast3months: '',
  averageProfitInLast3months: ''
}

const mapPropsToValues = (props: EnhancedProps) => {
  const preHouseHoldBusiness = R.pathOr(
    {},
    ['preIncomes', 'houseHoldBusinessIncome', props.currentIndex],
    props
  )
  if (!R.isEmpty(preHouseHoldBusiness)) {
    return preHouseHoldBusiness
  }

  return preHouseHoldBusinessInitial
}

const configs = {
  businessType: [
    required('PreApprovedApplication.step3.household.businessType.required')
  ],
  businessLine1: [
    required('PreApprovedApplication.step3.household.businessLine1.required')
  ],
  businessLine2: [
    required('PreApprovedApplication.step3.household.businessLine2.required')
  ],
  businessLine3: [
    required('PreApprovedApplication.step3.household.businessLine3.required')
  ],
  city: [required('PreApprovedApplication.step3.household.city.required')],
  averageRevenueInLast3months: [
    required(
      'PreApprovedApplication.step3.household.averageRevenueInLast3months.required'
    ),
    isMinValue('preApproved.step2.propertyValue.required', 1, true)
  ],
  averageProfitInLast3months: [
    required(
      'PreApprovedApplication.step3.household.averageProfitInLast3months.required'
    ),
    isMinValue('preApproved.step2.propertyValue.required', 1, true)
  ]
}

const validate = (values: any, props: any) => {
  const errors = validator(configs)(values, props)

  return {
    ...errors
  }
}

export { mapPropsToValues, validate }
