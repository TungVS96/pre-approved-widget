import { required } from 'formik-validators'
import * as R from 'ramda'
import { validator } from '../../../../../forms'
import { isNumberPercent } from '../../../../../forms/Rules'
import { isMinValue } from '../../../PreApprovedApplication.util'
import { EnhancedProps } from '../IncomesInfo.type'

const preCompanySalaryIncomeInitial = {
  ownerShip: 'SELF',
  capitalContributionRatio: '',
  recentAverageRevenue: '',
  recentAverageProfit: ''
}

const mapPropsToValues = (props: EnhancedProps) => {
  const preCompanySalaryIncome = R.pathOr(
    {},
    ['preIncomes', 'companySalaryIncome', props.currentIndex],
    props
  )
  if (!R.isEmpty(preCompanySalaryIncome)) {
    return preCompanySalaryIncome
  }

  return preCompanySalaryIncomeInitial
}

const configs = {
  capitalContributionRatio: [
    isNumberPercent('PreApprovedApplication.step3.pension.isNumberPercent')
  ],
  recentAverageRevenue: [
    required('PreApprovedApplication.step3.pension.income.required'),
    isMinValue('preApproved.step2.propertyValue.required', 1, true)
  ],
  recentAverageProfit: [
    required('PreApprovedApplication.step3.pension.income.required'),
    isMinValue('preApproved.step2.propertyValue.required', 1, true)
  ]
}

const validate = (values: any, props: any) => {
  const errors = validator(configs)(values, props)

  return {
    ...errors
  }
}

export { mapPropsToValues, validate }
