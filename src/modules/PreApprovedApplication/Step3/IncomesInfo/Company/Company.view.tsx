import { css } from 'emotion'
import { InjectedFormikProps } from 'formik'
import React, { FunctionComponent } from 'react'
import { RadioInput } from '../../../../../forms'
import {
  renderInputText,
  renderTextInputSuggestMoney
} from '../../../../../forms/form'
import { t } from '../../../../../i18n'
import {
  formatCurrency,
  getFieldProps,
  getPositiveNumberOnly
} from '../../../../../utils'
import BottomBarButtons from '../../../BottomBarButtons/BottomBarButtons.component'
import { Container, ContentWrapper, IncomeWrapper } from '../IncomesInfo.style'
import { ICompany } from '../IncomesInfo.type'

interface IInnerProps {
  onBack(): void
  ownerList: any
}

type CompanyEnhancedProps = InjectedFormikProps<IInnerProps, ICompany>
const Company: FunctionComponent<CompanyEnhancedProps> = (props) => {
  return (
    <Container>
      <div className={'mt-8'} />
      <ContentWrapper>
        <IncomeWrapper>
          <RadioInput
            {...getFieldProps('ownerShip', props)}
            options={props.ownerList}
            vertical={false}
            label={t('PreApprovedApplication.step3.company.ownerShip.label')}
          />
        </IncomeWrapper>
        <IncomeWrapper>
          {renderInputText({
            props,
            name: 'capitalContributionRatio',
            label:
              'PreApprovedApplication.step3.company.capitalContributionRatio.label',
            placeholder:
              'PreApprovedApplication.step3.company.capitalContributionRatio.placeholder',
            disabled: false,
            requiredField: true,
            // fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 }),
            maxLength: 3
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderTextInputSuggestMoney({
            props,
            name: 'recentAverageRevenue',
            label:
              'PreApprovedApplication.step3.company.recentAverageRevenue.label',
            placeholder:
              'PreApprovedApplication.step3.company.recentAverageRevenue.placeholder',
            disabled: false,
            requiredField: true,
            transformValue: (value) => getPositiveNumberOnly(value),
            format: (value) => formatCurrency({ value }),
            suggestMoney: true,
            times: 1000000
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderTextInputSuggestMoney({
            props,
            name: 'recentAverageProfit',
            label:
              'PreApprovedApplication.step3.company.recentAverageProfit.label',
            placeholder:
              'PreApprovedApplication.step3.company.recentAverageProfit.placeholder',
            disabled: false,
            requiredField: true,
            transformValue: (value) => getPositiveNumberOnly(value),
            format: (value) => formatCurrency({ value }),
            suggestMoney: true,
            times: 1000000
          })}
        </IncomeWrapper>
        <div className={'w-full'} />
        <IncomeWrapper>
          <BottomBarButtons onNext={props.handleSubmit} onPrev={props.onBack} />
        </IncomeWrapper>
      </ContentWrapper>
    </Container>
  )
}

export default Company
