import { required } from 'formik-validators'
import * as R from 'ramda'
import { validator } from '../../../../../forms'
import { isMinValue } from '../../../PreApprovedApplication.util'
const carRentInitial = {
  manufacturer: '',
  line: '',
  paymentMode: 'FTR',
  monthlyIncome: ''
}

const mapPropsToValues = (props: any) => {
  const preCarRent = R.pathOr(
    {},
    ['preIncomes', 'carRentIncomes', props.currentIndex],
    props
  )
  if (!R.isEmpty(preCarRent)) {
    return preCarRent
  }

  return carRentInitial
}

const configs = {
  manufacturer: [
    required('PreApprovedApplication.step3.carRent.manufacturer.required')
  ],
  line: [required('PreApprovedApplication.step3.carRent.line.required')],
  monthlyIncome: [
    required('PreApprovedApplication.step3.carRent.monthlyIncome.required'),
    isMinValue('preApproved.step2.propertyValue.required', 1, true)
  ]
}

const validate = (values: any, props: any) => {
  const errors = validator(configs)(values, props)

  return {
    ...errors
  }
}

export { mapPropsToValues, validate }
