import { rem } from 'dma-ui'
import { css } from 'emotion'
import { InjectedFormikProps } from 'formik'
import { pathOr } from 'ramda'
import React, { FunctionComponent } from 'react'
import { IDropDownItem } from 'src/types'
import {
  renderDropdown,
  renderTextInputSuggestMoney
} from '../../../../../forms/form'
import { formatCurrency, getPositiveNumberOnly } from '../../../../../utils'
import BottomBarButtons from '../../../BottomBarButtons/BottomBarButtons.component'
import { Container, ContentWrapper, IncomeWrapper } from '../IncomesInfo.style'
import { ICarRent } from '../IncomesInfo.type'

interface IInnerProps {
  onBack(): void
  dropdownData: {
    RC_LIST_VERHICLE_VENDOR: IDropDownItem[]
    RC_LIST_VEHICLE_MODEL: any[]
    RC_LIST_PAYMENT_METHOD_RENTAL: IDropDownItem[]
  }
}

type CarRentEnhancedProps = InjectedFormikProps<IInnerProps, ICarRent>
const CarRent: FunctionComponent<CarRentEnhancedProps> = (props) => {
  const {
    RC_LIST_VERHICLE_VENDOR = [],
    RC_LIST_PAYMENT_METHOD_RENTAL = [],
    RC_LIST_VEHICLE_MODEL = []
  } = props.dropdownData
  const lineList = RC_LIST_VEHICLE_MODEL.filter(
    (model: any) => props.values.manufacturer === pathOr('', ['vendorCode'], model)
  )

  return (
    <Container>
      <div className={'mt-8'} />
      <ContentWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'manufacturer',
            label: 'PreApprovedApplication.step3.carRent.manufacturer.label',
            placeholder:
              'PreApprovedApplication.step3.carRent.manufacturer.placeholder',
            disabled: false,
            requiredField: true,
            items: RC_LIST_VERHICLE_VENDOR,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'line',
            label: 'PreApprovedApplication.step3.carRent.line.label',
            placeholder:
              'PreApprovedApplication.step3.carRent.line.placeholder',
            disabled: false,
            requiredField: true,
            items: lineList,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'paymentMode',
            label: 'PreApprovedApplication.step3.carRent.paymentMode.label',
            placeholder:
              'PreApprovedApplication.step3.carRent.paymentMode.placeholder',
            disabled: false,
            requiredField: true,
            items: RC_LIST_PAYMENT_METHOD_RENTAL,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderTextInputSuggestMoney({
            props,
            name: 'monthlyIncome',
            label: 'PreApprovedApplication.step3.carRent.monthlyIncome.label',
            placeholder:
              'PreApprovedApplication.step3.carRent.monthlyIncome.placeholder',
            disabled: false,
            requiredField: true,
            transformValue: (value) => getPositiveNumberOnly(value),
            format: (value) => formatCurrency({ value }),
            times: 1000000
          })}
        </IncomeWrapper>
        <div className={'w-full'} />
        <IncomeWrapper>
          <BottomBarButtons onNext={props.handleSubmit} onPrev={props.onBack} />
        </IncomeWrapper>
      </ContentWrapper>
    </Container>
  )
}

export { IInnerProps }
export default CarRent
