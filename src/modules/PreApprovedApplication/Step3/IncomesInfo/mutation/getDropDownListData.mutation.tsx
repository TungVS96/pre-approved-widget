import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    getDataDropdownList(input: $input)
      @rest(
        type: "Document"
        endpoint: "dma-pre-credit"
        path: "/categories/v1/list"
        method: "post"
      ) {
      RC_LIST_ACCOM_TYPE
      RC_LIST_MARITAL_STATUS
      RC_LIST_EDU_DEGREE
      RC_LIST_TRANSPORT_TYPE
      RC_LOAN_PRODUCT_LIST
      RC_LIST_PROFESSIONAL
      RC_LIST_POSITION
      RC_LIST_PAYMENT_METHOD
      RC_LIST_HOUSERENT_TYPE
      RC_LIST_HOUSERENT_LOCATION
      RC_LIST_VERHICLE_VENDOR
      RC_LIST_VEHICLE_MODEL
      RC_LIST_PAYMENT_METHOD_RENTAL
      RC_LIST_INDUSTRY_GROUP
      RC_LIST_PROVINCE
      RC_LOAN_PRODUCT_LIST
    }
  }
`
const params = {
  name: 'getDataDropdownList'
}

export default {
  mutation,
  params
}
