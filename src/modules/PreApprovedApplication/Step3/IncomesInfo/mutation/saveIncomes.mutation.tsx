import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    saveIncomes(input: $input)
      @rest(
        type: "Incomes"
        endpoint: "dma-pre-credit"
        path: "/approved/v1/save-income"
        method: "post"
      ) {
      status
      isBScore
      errorName
    }
  }
`
const params = {
  name: 'saveIncomes'
}

export default {
  mutation,
  params
}
