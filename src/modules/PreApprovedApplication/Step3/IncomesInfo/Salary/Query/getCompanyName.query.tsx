import gql from 'graphql-tag'
import { pathOr } from 'ramda'
import { SalaryEnhancedProps } from '../Salary.view'

const query = gql`
  query($textSearch: String) {
    organization(textSearch: $textSearch)
      @rest(
        type: "pre_approved"
        path: "/categories/v1/organization?{args}"
        endpoint: "dma-pre-credit"
      ) {
      value: companyCode
      name: companyName
      blevel1
      blevel2
      blevel3
      blevel4
      isIPO
      isTop500
      reputationLevel
    }
  }
`
const params = {
  props: ({ data }: any) => {
    const organizations = pathOr([], ['organization'], data)

    return {
      data,
      organizations
    }
  },
  options: (props: SalaryEnhancedProps) => {
    return {
      variables: {
        textSearch: props.companyNameTextSearch
      }
    }
  },
  skip: (props: SalaryEnhancedProps) => {
    return props.companyNameTextSearch === ''
  }
}

export default { query, params }
