import { rem } from 'dma-ui'
import { css } from 'emotion'
import { InjectedFormikProps } from 'formik'
import React, { FunctionComponent } from 'react'
import { IDropDownItem } from 'src/types'
import {
  LIST_OF_TIME_LIVING_IN_CURRENT_ADDRESS_MONTH,
  LIST_OF_TIME_LIVING_IN_CURRENT_ADDRESS_YEAR
} from '../../../../../constants'
import { RadioInput } from '../../../../../forms'
import {
  renderDropdown,
  renderTextInputAutoComplete,
  renderTextInputSuggestMoney
} from '../../../../../forms/form'
import { t } from '../../../../../i18n'
import {
  formatCurrency,
  getFieldProps,
  getPositiveNumberOnly
} from '../../../../../utils'
import BottomBarButtons from '../../../BottomBarButtons/BottomBarButtons.component'
import { Container, ContentWrapper, IncomeWrapper } from '../IncomesInfo.style'
import { ISalary } from '../IncomesInfo.type'

interface IInnerProps {
  onBack(): void
  dropdownData: {
    RC_LIST_PROFESSIONAL: IDropDownItem[]
    RC_LIST_POSITION: IDropDownItem[]
    RC_LIST_PAYMENT_METHOD: IDropDownItem[]
  }
  organizations: IDropDownItem[]
  companyNameTextSearch: string
  setCompanyNameTextSearch(companyNameTextSearch: string): void
  setFirstCall(setFirstCall: boolean): void
  ON_CHANGE_COMPANY_NAME(): void
  ownerList: any
  saveCurrentStep(params: any): void
}

type SalaryEnhancedProps = InjectedFormikProps<IInnerProps, ISalary>
const Salary: FunctionComponent<SalaryEnhancedProps> = (props) => {
  const {
    RC_LIST_PROFESSIONAL = [],
    RC_LIST_POSITION = [],
    RC_LIST_PAYMENT_METHOD = []
  } = props.dropdownData
  const { organizations = [] } = props

  return (
    <Container>
      <div className={'mt-8'} />
      <ContentWrapper>
        <IncomeWrapper>
          <RadioInput
            {...getFieldProps('ownerShip', props)}
            options={props.ownerList}
            vertical={false}
            label={t('PreApprovedApplication.step3.salary.ownerShip.label')}
          />
        </IncomeWrapper>
        <IncomeWrapper>
          {renderTextInputAutoComplete({
            props,
            name: `companyName`,
            label: 'PreApprovedApplication.step3.salary.companyName.label',
            placeholder:
              'PreApprovedApplication.step3.salary.companyName.placeholder',
            items: organizations,
            inputProps: {
              maxLength: 100
            },
            onTextChange: props.ON_CHANGE_COMPANY_NAME,
            rightIcon: (
              <img
                src={require('./assets/ic-search.svg')}
                width={18}
                height={18}
              />
            ),
            canVerify: false
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'bprofession',
            label: 'PreApprovedApplication.step3.salary.bProfession.label',
            placeholder:
              'PreApprovedApplication.step3.salary.bProfession.placeholder',
            disabled: false,
            requiredField: true,
            items: RC_LIST_PROFESSIONAL,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'position',
            label: 'PreApprovedApplication.step3.salary.position.label',
            placeholder:
              'PreApprovedApplication.step3.salary.position.placeholder',
            disabled: false,
            requiredField: true,
            items: RC_LIST_POSITION,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          <label className={css({ fontSize: rem(13) })}>
            Quý khách đang làm ở đơn vị hiện tại bao lâu rồi?
            <span className="text-brand">*</span>
          </label>
          <div className={'flex flex-row'}>
            <div className={'w-1/2 pr-2'}>
              {renderDropdown({
                props,
                name: 'tenureYear',
                requiredField: true,
                placeholder:
                  'PreApprovedApplication.step3.salary.tenureYear.placeholder',
                items: LIST_OF_TIME_LIVING_IN_CURRENT_ADDRESS_YEAR,
                fieldContainerStyle: css({ borderRadius: rem(5) }),
                labelStyle: css({ fontWeight: 500 }),
                valueSuffix:
                  'applicationOnlineFrom.step5.salaryIncomeTenureYear.valueSuffix'
              })}
            </div>
            <div className={'w-1/2 pl-2'}>
              {renderDropdown({
                props,
                name: 'tenureMonth',
                requiredField: true,
                placeholder:
                  'PreApprovedApplication.step3.salary.tenureMonth.placeholder',
                items: LIST_OF_TIME_LIVING_IN_CURRENT_ADDRESS_MONTH,
                fieldContainerStyle: css({ borderRadius: rem(5) }),
                labelStyle: css({ fontWeight: 500 }),
                valueSuffix:
                  'applicationOnlineFrom.step5.salaryIncomeTenureMonth.valueSuffix'
              })}
            </div>
          </div>
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'paymentType',
            label: 'PreApprovedApplication.step3.salary.paymentType.label',
            placeholder:
              'PreApprovedApplication.step3.salary.paymentType.placeholder',
            disabled: false,
            requiredField: true,
            items: RC_LIST_PAYMENT_METHOD,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderTextInputSuggestMoney({
            props,
            name: 'provableStableIncome',
            label:
              'PreApprovedApplication.step3.salary.provableStableIncome.label',
            placeholder:
              'PreApprovedApplication.step3.salary.provableStableIncome.placeholder',
            disabled: false,
            requiredField: true,
            transformValue: (value) => getPositiveNumberOnly(value),
            format: (value) => formatCurrency({ value }),
            suggestMoney: true,
            times: 1000000,
            autoComplete: 'off'
          })}
        </IncomeWrapper>
        <div className={'w-full'} />
        <IncomeWrapper>
          <BottomBarButtons onNext={props.handleSubmit} onPrev={props.onBack} />
        </IncomeWrapper>
      </ContentWrapper>
    </Container>
  )
}

export { SalaryEnhancedProps }
export default Salary
