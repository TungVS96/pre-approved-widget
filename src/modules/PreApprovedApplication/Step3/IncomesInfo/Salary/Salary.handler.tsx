import { debounce } from 'debounce'

const TIME_DELAY = 1000
let timer: any = null

export default {
  ON_CHANGE_COMPANY_NAME: (props: any) => (value: string) => {
    // if (props.firstCall && value.length >= 3) {
    //   props.setFirstCall(false)
    //   props.setCompanyNameTextSearch(value)

    //   return
    // }
    clearTimeout(timer)
    timer = setTimeout(
      debounce(() => {
        props.setCompanyNameTextSearch(value)
      }),
      TIME_DELAY
    )
  }
}
