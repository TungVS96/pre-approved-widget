import withLifecycle from '@hocs/with-lifecycle'
import { withFormik, FormikBag } from 'formik'
import { pathOr } from 'ramda'
import { renameKeys } from 'ramda-adjunct'
import { graphql } from 'react-apollo'
import { compose, withHandlers, withState } from 'recompose'
import { IDropDownItem } from 'src/types'
import getCompanyName from './Query/getCompanyName.query'
import { mapPropsToValues, validate } from './Salary.config'
import handlers from './Salary.handler'
import Salary, { SalaryEnhancedProps } from './Salary.view'

const formikConfig = {
  mapPropsToValues,
  validate,
  handleSubmit: async (
    values: any,
    { setSubmitting, props }: FormikBag<any, any>
  ) => {
    try {
      const companyInfo = props.organizations.find(
        (company: IDropDownItem) => company.name === values.companyName
      )
      await props.onSaveIncome({
        ...values,
        niemYet: pathOr('', ['isIPO'], companyInfo),
        ...compose(
          renameKeys({ name: 'companyName' }),
          renameKeys({ value: 'companyCode' })
        )(companyInfo)
      })
    } finally {
      setSubmitting(false)
    }
  },
  enableReinitialize: true
}

export default compose(
  withState('companyNameTextSearch', 'setCompanyNameTextSearch', ''),
  withState('firstCall', 'setFirstCall', true),
  graphql(getCompanyName.query, getCompanyName.params),
  withFormik(formikConfig),
  withHandlers(handlers),
  withLifecycle({
    onDidMount: (props: SalaryEnhancedProps) => {
      props.setCompanyNameTextSearch(props.values.companyName || '')
    }
  })
)(Salary)
