import { required } from 'formik-validators'
import * as R from 'ramda'
import { IDropDownItem } from 'src/types'
import { validator } from '../../../../../forms'
import { t } from '../../../../../i18n'
import { isMinValue } from '../../../PreApprovedApplication.util'
import { EnhancedProps } from '../IncomesInfo.type'

const salaryInitial = {
  ownerShip: 'SELF',
  companyName: '',
  bprofession: '',
  position: '',
  tenureYear: '',
  tenureMonth: '',
  paymentType: 'TCB',
  provableStableIncome: ''
}

const mapPropsToValues = (props: EnhancedProps) => {
  const preSalary = R.pathOr(
    {},
    ['preIncomes', 'salaryIncomes', props.currentIndex],
    props
  )
  if (!R.isEmpty(preSalary)) {
    return preSalary
  }

  return salaryInitial
}

const companyInSearchList = (
  errorKey: string,
  organizations: IDropDownItem[]
) => ({ value }: { value: string }) => {
  if (
    R.isEmpty(organizations.filter((org: IDropDownItem) => org.name === value))
  ) {
    return t(errorKey)
  }

  return
}

const validate = (values: any, props: any) => {
  const organizations = R.pathOr([], ['organizations'], props)

  const errors = validator({
    ownerShip: [
      required('PreApprovedApplication.step3.salary.ownerShip.required')
    ],
    companyName: [
      required('PreApprovedApplication.step3.salary.companyName.required'),
      companyInSearchList(
        'PreApprovedApplication.step3.salary.companyName.required',
        organizations
      )
    ],
    bprofession: [
      required('PreApprovedApplication.step3.salary.bProfession.required')
    ],
    position: [
      required('PreApprovedApplication.step3.salary.position.required')
    ],
    tenureYear: [
      required('PreApprovedApplication.step3.salary.tenureYear.required')
    ],
    tenureMonth: [
      required('PreApprovedApplication.step3.salary.tenureMonth.required')
    ],
    provableStableIncome: [
      required(
        'PreApprovedApplication.step3.salary.provableStableIncome.required'
      ),
      isMinValue('preApproved.step2.propertyValue.required', 1, true)
    ]
  })(values, props)

  return {
    ...errors
  }
}

export { mapPropsToValues, validate }
