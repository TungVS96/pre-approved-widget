import { match } from 'match-values'

import { pathOr } from 'ramda'
import React, { FunctionComponent } from 'react'
import { t } from '../../../../i18n'
import { TYPE_OF_INCOME } from '../../../../types'
import BottomBarButtons from '../../BottomBarButtons/BottomBarButtons.component'
import ConfirmPopupView from '../../ConfirmPopup/ConfirmPopup.view'
import { Title } from '../../PreApprovedApplication.style'
import { STEP } from '../../PreApprovedApplication.type'
import { TYPE_OF_INFO } from '../Step3.const'
import CarRent from './CarRent/CarRent.page'
import Card from './Card/Card.view'
import Company from './Company/Company.page'
import HouseHoldBusiness from './HouseHoldBusiness/HouseHoldBusiness.page'
import HouseLease from './HouseLease/HouseLease.page'
import { ContentWrapper, IncomeWrapper } from './IncomesInfo.style'
import { incomesInfo, EnhancedProps } from './IncomesInfo.type'
import { getIncomes, getOwnerShip } from './IncomesInfo.util'
import Pension from './Pension/Pension.page'
import Salary from './Salary/Salary.page'
const renderIncomeListView = (props: EnhancedProps) => {
  const { goBack, ON_ADD, ON_EDIT, ON_NEXT, ON_DELETE } = props

  return (
    <>
      <Title>Thu nhập đến từ</Title>
      <ContentWrapper>
        {incomesInfo.map((item, index) => {
          return (
            <IncomeWrapper key={index.toString()}>
              <Card
                onDelete={ON_DELETE}
                onEdit={ON_EDIT}
                onAdd={ON_ADD}
                icon={item.icon}
                title={item.title}
                typeOfIncome={item.typeOfIncome}
                incomes={getIncomes(item.typeOfIncome, props)}
              />
            </IncomeWrapper>
          )
        })}
        <div className={'mt4 wfull'} />
        <IncomeWrapper>
          <BottomBarButtons
            id={'pre_fillin_creditinfo'}
            onPrev={goBack}
            onNext={() => {
              ON_NEXT(TYPE_OF_INFO.CREDIT_INFO, props.preIncomes)
            }}
          />
        </IncomeWrapper>
      </ContentWrapper>
    </>
  )
}

const IncomesInfo: FunctionComponent<EnhancedProps> = (props) => {
  const { typeOfIncome } = props

  const FormComponent = match(typeOfIncome, {
    [TYPE_OF_INCOME.FROM_SALARY]: Salary,
    [TYPE_OF_INCOME.FROM_SPOUSE_SALARY]: Salary,
    [TYPE_OF_INCOME.HOUSE_LEASE]: HouseLease,
    [TYPE_OF_INCOME.CAR_RENTAL]: CarRent,
    [TYPE_OF_INCOME.FROM_PENSION]: Pension,
    [TYPE_OF_INCOME.FROM_SPOUSE_PENSION]: Pension,
    [TYPE_OF_INCOME.HOUSEHOLD_BUSINESS]: HouseHoldBusiness,
    [TYPE_OF_INCOME.COMPANY_SALARY]: Company,
    _: <></>
  })
  const showNewIncome = typeOfIncome !== ''
  const ownerList = getOwnerShip(props.maritalStatus)
  // const edited = true

  return (
    <>
      {!showNewIncome && renderIncomeListView(props)}
      {showNewIncome && (
        <FormComponent
          {...props}
          dropdownData={props.dropdownData}
          onSaveIncome={props.ON_SAVE}
          onBack={props.ON_BACK}
          ownerList={ownerList}
        />
      )}
      <ConfirmPopupView
        isOpen={props.currentPopup === 'backPopup'}
        handleYes={() => {
          props.setTypeOfIncome('')
          props.setIndex(1)
          props.setPopup('')
          // set current step in grid incomes
          const params = {
            step: STEP.step3
          }
          const preApprovedId = pathOr('', ['preApprovedId'], props)
          const input = {
            preApprovedId,
            currentStep: JSON.stringify(params)
          }
          props.saveCurrentStep({ variables: { input } })
        }}
        handleNo={() => props.setPopup('')}
        handleClose={() => props.setPopup('')}
        title={t('preApproved.income.redirect.popup.title')}
        content={t('preApproved.income.redirect.popup.content')}
      />
    </>
  )
}

export default IncomesInfo
