import gql from 'graphql-tag'
import * as R from 'ramda'
import {
  ICompany,
  IHouseHoldBusiness,
  IHouseLease,
  IPension,
  ISalary,
  OWNER
} from '../IncomesInfo.type'
import { sortBy } from '../IncomesInfo.util'

const query = gql`
  query($preApprovedId: String) {
    preIncomes(preApprovedId: $preApprovedId)
      @rest(
        type: "pre_approved"
        path: "/approved/v1/get-income?{args}"
        endpoint: "dma-pre-credit"
      ) {
      maritalStatus
      preIncomes @type(name: "preIncomes") {
        salaryIncomes @type(name: "salaryIncomes") {
          bprofession
          ownerShip
          companyName
          companyCode
          position
          tenureYear
          tenureMonth
          paymentType
          provableStableIncome
          blevel1
          blevel2
          blevel3
          blevel4
          isTop500
          niemYet
          paymentMode
          reputationLevel
        }
        carRentIncomes
        companySalaryIncome
        houseHoldBusinessIncome
        houseLeaseIncomes
        pensionIncomes
      }
    }
  }
`
const params = {
  props: ({ data }: any) => {
    const maritalStatus = R.pathOr({}, ['preIncomes', 'maritalStatus'], data)
    const preIncomes = R.pathOr({}, ['preIncomes', 'preIncomes'], data)
    const carRentIncomes = R.pathOr([], ['carRentIncomes'], preIncomes)
    const carRentIncomesSorted = R.sortWith([
      R.ascend(R.prop('manufacturer')),
      R.descend(R.prop('monthlyIncome')) as any
    ])(carRentIncomes)

    const companySalaryIncome = R.pathOr(
      [],
      ['companySalaryIncome'],
      preIncomes
    )
    const customerCompanySalaryIncomes = companySalaryIncome.filter(
      (company: ICompany) => company.ownerShip === OWNER.SELF
    )
    const customerCompanySalarySortedIncomes = sortBy(
      'recentAverageProfit',
      customerCompanySalaryIncomes
    )
    const spouseCompanySalaryIncomes = companySalaryIncome.filter(
      (company: ICompany) => company.ownerShip === OWNER.SPOUSE
    )
    const spouseCompanySalarySortedIncomes = sortBy(
      'recentAverageProfit',
      spouseCompanySalaryIncomes
    )

    const houseHoldBusinessIncome = R.pathOr(
      [],
      ['houseHoldBusinessIncome'],
      preIncomes
    )
    const customerHouseHoldBusinessIncomes = houseHoldBusinessIncome.filter(
      (householdBusiness: IHouseHoldBusiness) =>
        householdBusiness.ownerShip === OWNER.SELF
    )
    const customerHouseHoldBusinessSortedIncomes = sortBy(
      'averageProfitInLast3months',
      customerHouseHoldBusinessIncomes
    )
    const spouseHouseHoldBusinessIncomes = houseHoldBusinessIncome.filter(
      (householdBusiness: IHouseHoldBusiness) =>
        householdBusiness.ownerShip === OWNER.SPOUSE
    )
    const spouseHouseHoldBusinessSortedIncomes = sortBy(
      'averageProfitInLast3months',
      spouseHouseHoldBusinessIncomes
    )

    const houseLeaseIncomes = R.pathOr([], ['houseLeaseIncomes'], preIncomes)

    const apartments = houseLeaseIncomes.filter(
      (apartment: IHouseLease) => apartment.typeOfProperty === 'APA'
    )
    const apartmentsSorted = sortBy('incomeAmount', apartments)
    const houses = houseLeaseIncomes.filter(
      (house: IHouseLease) => house.typeOfProperty === 'HOU'
    )
    const housesSorted = sortBy('incomeAmount', houses)

    const pensionIncomes = R.pathOr([], ['pensionIncomes'], preIncomes)
    const customerPensionIncomes = pensionIncomes.filter(
      (pension: IPension) => pension.ownerShip === OWNER.SELF
    )
    const customerPensionSortedIncomes = sortBy(
      'income',
      customerPensionIncomes
    )
    const spousePensionIncomes = pensionIncomes.filter(
      (pension: IPension) => pension.ownerShip === OWNER.SPOUSE
    )
    const spousePensionSortedIncomes = sortBy('income', spousePensionIncomes)

    const salaryIncomes = R.pathOr([], ['salaryIncomes'], preIncomes)
    const customerSalaryIncomes = salaryIncomes.filter(
      (salary: ISalary) => salary.ownerShip === OWNER.SELF
    )
    const customerSalarySortedIncomes = sortBy(
      'provableStableIncome',
      customerSalaryIncomes
    )
    const spouseSalaryIncomes = salaryIncomes.filter(
      (salary: ISalary) => salary.ownerShip === OWNER.SPOUSE
    )
    const spouseSalarySortedIncomes = sortBy(
      'provableStableIncome',
      spouseSalaryIncomes
    )

    return {
      data,
      maritalStatus,
      preIncomes: {
        carRentIncomes: carRentIncomesSorted,
        companySalaryIncome: customerCompanySalarySortedIncomes.concat(
          spouseCompanySalarySortedIncomes
        ),
        houseHoldBusinessIncome: customerHouseHoldBusinessSortedIncomes.concat(
          spouseHouseHoldBusinessSortedIncomes
        ),
        houseLeaseIncomes: apartmentsSorted.concat(housesSorted),
        pensionIncomes: customerPensionSortedIncomes.concat(
          spousePensionSortedIncomes
        ),
        salaryIncomes: customerSalarySortedIncomes.concat(
          spouseSalarySortedIncomes
        )
      }
    }
  },
  options: (props: any) => {
    const preApprovedId = R.pathOr('', ['preApprovedId'], props)

    return {
      variables: {
        preApprovedId
      }
    }
  },
}

export default { query, params }
