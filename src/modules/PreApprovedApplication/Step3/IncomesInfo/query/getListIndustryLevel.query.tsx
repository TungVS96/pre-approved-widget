import gql from 'graphql-tag'
import { pathOr } from 'ramda'
const query = gql`
  query($name: string) {
    listIndustryLevel(name: $name)
      @rest(
        type: "Document"
        endpoint: "dma-pre-credit"
        path: "/categories/v1/list-industry-3level"
      ) {
      industryLevel1
      industryLevel2
      industryLevel3
    }
  }
`
const params = {
  props: ({ data }: any) => {
    const listIndustryLevel = pathOr({}, ['listIndustryLevel'], data)

    return { listIndustryLevel }
  },
  options: (props: any) => {
    const name = ''

    return {
      variables: {
        name
      }
    }
  }
}

export default { query, params }
