import withLifecycle from '@hocs/with-lifecycle'
import { graphql } from 'react-apollo'
import { compose, withHandlers, withState } from 'recompose'
import { withApp } from '../../../../hocs'
import handlers from './IncomesInfo.handler'
import lifecycles from './IncomesInfo.lifecycle'
import IncomesInfo from './IncomesInfo.view'
import getDropDownListData from './mutation/getDropDownListData.mutation'
import saveIncomes from './mutation/saveIncomes.mutation'
import getListIndustryLevel from './query/getListIndustryLevel.query'
import getPreIncomes from './query/getPreIncomes.query'

export default compose(
  graphql(getPreIncomes.query, getPreIncomes.params),
  graphql(saveIncomes.mutation, saveIncomes.params),
  graphql(getDropDownListData.mutation, getDropDownListData.params),
  graphql(getListIndustryLevel.query, getListIndustryLevel.params),
  withState('typeOfIncome', 'setTypeOfIncome', ''),
  withState('currentIndex', 'setIndex', -1),
  withApp({ updates: ['saveIncomes', 'getDropDownListData'] }),
  withHandlers(handlers),
  withLifecycle(lifecycles)
)(IncomesInfo)
