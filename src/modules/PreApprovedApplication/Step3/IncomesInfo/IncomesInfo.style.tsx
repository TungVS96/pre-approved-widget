import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const Container = withClassName('w-full relative', 'Container')(
  styled('div')({
    marginBottom: rem(160)
  })
)

const ContentWrapper = withClassName('flex flex-row flex-wrap w-full')(
  styled('div')({
    paddingLeft: rem(16),
    marginBottom: rem(16)
  })
)

const IncomeWrapper = withClassName('sm:w-full md:w-1/3')(
  styled('div')({
    marginBottom: rem(16),
    paddingRight: rem(16)
  })
)

export { Container, ContentWrapper, IncomeWrapper }
