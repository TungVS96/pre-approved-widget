import { css } from 'emotion'
import * as R from 'ramda'
import React, { FunctionComponent } from 'react'
import { compose, withState } from 'recompose'
import { withPopup, PopupProps } from '../../../../../hocs'
import { TYPE_OF_INCOME } from '../../../../../types'
import { formatCurrency } from '../../../../../utils'
import ConfirmDeletePopup from '../../../ConfirmDeletePopup/ConfirmDeletePopup.view'
import {
  CardWrapper,
  Icon,
  IncomeWrapper,
  Title,
  TitleWrapper
} from './Card.style'

interface IOuterProps {
  title: string
  icon: string
  typeOfIncome: TYPE_OF_INCOME
  incomes: any[]
  onAdd(typeOfIncome: TYPE_OF_INCOME, numberOfCurrentIncomes: number): void
  onDelete(index: number, typeOfIncome: TYPE_OF_INCOME): void
  onEdit(index: number, typeOfIncome: TYPE_OF_INCOME): void
}

interface IProps extends PopupProps {
  index: number
  setIndex(index: number): void
}

const getTitle = (typeOfIncome: TYPE_OF_INCOME, income: any) => {
  if (
    typeOfIncome === TYPE_OF_INCOME.FROM_SALARY ||
    typeOfIncome === TYPE_OF_INCOME.FROM_SPOUSE_SALARY
  ) {
    return R.pathOr('', ['ownerShip'], income) === 'SELF'
      ? 'Quý khách'
      : 'Vợ/chồng quý khách'
  }

  if (typeOfIncome === TYPE_OF_INCOME.HOUSE_LEASE) {
    return R.pathOr(0, ['typeOfProperty'], income) === 'HOU'
      ? 'Nhà đất'
      : 'Chung cư'
  }

  if (typeOfIncome === TYPE_OF_INCOME.CAR_RENTAL) {
    return R.pathOr(0, ['manufacturer'], income)
  }

  if (
    typeOfIncome === TYPE_OF_INCOME.FROM_PENSION ||
    typeOfIncome === TYPE_OF_INCOME.FROM_SPOUSE_PENSION
  ) {
    return R.pathOr('', ['ownerShip'], income) === 'SELF'
      ? 'Quý khách'
      : 'Vợ/chồng quý khách'
  }

  if (typeOfIncome === TYPE_OF_INCOME.COMPANY_SALARY) {
    return R.pathOr('', ['ownerShip'], income) === 'SELF'
      ? 'Quý khách'
      : 'Vợ/chồng quý khách'
  }

  if (
    typeOfIncome === TYPE_OF_INCOME.HOUSEHOLD_BUSINESS ||
    typeOfIncome === TYPE_OF_INCOME.SPOUSE_HOUSEHOLD_BUSINESS
  ) {
    return R.pathOr('', ['ownerShip'], income) === 'SELF'
      ? 'Quý khách'
      : 'Vợ/chồng quý khách'
  }

  return ''
}

const getSubTitle = (typeOfIncome: TYPE_OF_INCOME, income: any) => {
  if (
    typeOfIncome === TYPE_OF_INCOME.FROM_SALARY ||
    typeOfIncome === TYPE_OF_INCOME.FROM_SPOUSE_SALARY
  ) {
    return formatCurrency({
      value: R.pathOr(0, ['provableStableIncome'], income)
    })
  }
  if (typeOfIncome === TYPE_OF_INCOME.HOUSE_LEASE) {
    return formatCurrency({
      value: R.pathOr(0, ['incomeAmount'], income)
    })
  }

  if (typeOfIncome === TYPE_OF_INCOME.CAR_RENTAL) {
    return formatCurrency({
      value: R.pathOr(0, ['monthlyIncome'], income)
    })
  }

  if (
    typeOfIncome === TYPE_OF_INCOME.FROM_PENSION ||
    typeOfIncome === TYPE_OF_INCOME.FROM_SPOUSE_PENSION
  ) {
    return formatCurrency({
      value: R.pathOr(0, ['income'], income)
    })
  }

  if (typeOfIncome === TYPE_OF_INCOME.COMPANY_SALARY) {
    return formatCurrency({
      value: R.pathOr(0, ['recentAverageProfit'], income)
    })
  }

  if (
    typeOfIncome === TYPE_OF_INCOME.HOUSEHOLD_BUSINESS ||
    typeOfIncome === TYPE_OF_INCOME.SPOUSE_HOUSEHOLD_BUSINESS
  ) {
    return formatCurrency({
      value: R.pathOr(0, ['averageProfitInLast3months'], income)
    })
  }

  return ''
}

type EnhancedProps = IOuterProps & IProps
const icAdd = require('./assets/ic-add.svg')
const icDelete = require('./assets/ic-delete.svg')
const icEdit = require('./assets/ic-edit.svg')
const Card: FunctionComponent<EnhancedProps> = (props) => {
  const {
    title = '',
    onAdd,
    onDelete,
    onEdit,
    icon,
    typeOfIncome,
    incomes = []
  } = props

  return (
    <div>
      <CardWrapper>
        <TitleWrapper>
          <Icon src={icon} width={44} height={44} />
          <Title>{title}</Title>
        </TitleWrapper>
        <Icon
          onClick={onAdd(typeOfIncome, incomes.length)}
          src={icAdd}
          width={14}
          height={14}
        />
      </CardWrapper>
      {incomes.map((income: any, index: number) => {
        return (
          <IncomeWrapper key={index.toString()}>
            <Title>{getTitle(typeOfIncome, income)}</Title>
            <div
              className={css({
                display: 'flex',
                flexDirection: 'row',
                minWidth: 64
              })}
            >
              <Title>{getSubTitle(typeOfIncome, income)}</Title>
              <Icon
                onClick={onEdit(index, typeOfIncome)}
                src={icEdit}
                width={20}
                height={20}
              />
              <Icon
                onClick={() => {
                  props.setIndex(index)
                  props.setPopup('warning')
                }}
                src={icDelete}
                width={20}
                height={20}
              />
            </div>
          </IncomeWrapper>
        )
      })}
      <ConfirmDeletePopup
        title={'Quý khách có chắc chắn muốn xóa nguồn thu này?'}
        isOpen={props.currentPopup === 'warning'}
        handleNo={() => props.setPopup('')}
        handleYes={async () => {
          await onDelete(props.index, typeOfIncome)
          await props.setPopup('')
        }}
        handleClose={() => props.setPopup('')}
      />
    </div>
  )
}

export default compose<EnhancedProps, IOuterProps>(
  withState('index', 'setIndex', -1),
  withPopup
)(Card)
