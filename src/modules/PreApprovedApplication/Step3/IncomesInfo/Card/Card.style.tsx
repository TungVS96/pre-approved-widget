import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const CardWrapper = withClassName('flex flex-row justify-between')(
  styled('div')({
    alignItems: 'center',
    backgroundColor: '#FAF8F8',
    padding: `${rem(8)} ${rem(16)}`,
    borderRadius: rem(5),
    boxShadow: `0px 4px 12px rgba(0, 0, 0, 0.12)`
  })
)

const TitleWrapper = withClassName('flex flex-row justify-between')(
  styled('div')({
    alignItems: 'center'
  })
)

const Title = withClassName('')(
  styled('p')({ margin: `0 ${rem(8)}`, fontSize: 14, lineHeight: rem(21) })
)

const Icon = withClassName('')(
  styled('img')({
    margin: `0 ${rem(8)}`,
    cursor: 'pointer'
  })
)

const IncomeWrapper = withClassName('flex flex-row justify-between')(
  styled('div')({
    marginTop: rem(4),
    alignItems: 'center',
    backgroundColor: '#FAF8F8',
    padding: rem(16),
    borderRadius: rem(5)
  })
)

export { CardWrapper, TitleWrapper, Title, Icon, IncomeWrapper }
