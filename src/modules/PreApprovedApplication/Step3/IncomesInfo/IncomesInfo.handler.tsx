import * as R from 'ramda'
import { isNilOrEmpty } from 'ramda-adjunct'
import { TYPE_OF_INCOME } from '../../../../types'
import { STEP } from '../../PreApprovedApplication.type'
import { EnhancedProps } from './IncomesInfo.type'
import { deleteIncomes } from './IncomesInfo.util'
export default {
  ON_BACK: (props: EnhancedProps) => () => {
    props.setPopup('backPopup')
  },
  ON_DELETE: (props: EnhancedProps) => async (
    index: number,
    typeOfIncome: TYPE_OF_INCOME
  ) => {
    try {
      const newPreIncomes = deleteIncomes({
        index,
        typeOfIncome,
        preIncomes: props.preIncomes
      })
      const preApprovedId = R.pathOr('', ['preApprovedId'], props)

      await props.saveIncomes({
        variables: {
          input: {
            preApprovedId,
            preIncomes: newPreIncomes
          }
        }
      })
      await props.data.refetch()
    } catch (e) {
      props.showMessage()
    }
  },
  ON_ADD: (props: EnhancedProps) => (
    typeOfIncome: TYPE_OF_INCOME,
    numberOfCurrentIncomes: number
  ) => () => {
    props.setTypeOfIncome(typeOfIncome)
    props.setIndex(numberOfCurrentIncomes)
  },
  ON_EDIT: (props: EnhancedProps) => (
    index: number,
    typeOfIncome: TYPE_OF_INCOME
  ) => () => {
    props.setTypeOfIncome(typeOfIncome)
    props.setIndex(index)
  },
  ON_SAVE: (props: EnhancedProps) => async (values: any) => {
    const { currentIndex, typeOfIncome } = props
    const preApprovedId = R.pathOr('', ['preApprovedId'], props)

    if (isNilOrEmpty(currentIndex) || isNilOrEmpty(typeOfIncome)) {
      return
    }

    if (typeOfIncome === TYPE_OF_INCOME.FROM_SALARY) {
      if (isNilOrEmpty(props.preIncomes.salaryIncomes)) {
        props.preIncomes.salaryIncomes = [values]
      } else {
        props.preIncomes.salaryIncomes[currentIndex] = values
      }
    }
    if (typeOfIncome === TYPE_OF_INCOME.CAR_RENTAL) {
      if (isNilOrEmpty(props.preIncomes.carRentIncomes)) {
        props.preIncomes.carRentIncomes = [values]
      } else {
        props.preIncomes.carRentIncomes[currentIndex] = values
      }
    }
    if (typeOfIncome === TYPE_OF_INCOME.HOUSE_LEASE) {
      if (isNilOrEmpty(props.preIncomes.houseLeaseIncomes)) {
        props.preIncomes.houseLeaseIncomes = [values]
      } else {
        props.preIncomes.houseLeaseIncomes[currentIndex] = values
      }
    }
    if (
      typeOfIncome === TYPE_OF_INCOME.FROM_PENSION ||
      typeOfIncome === TYPE_OF_INCOME.FROM_SPOUSE_PENSION
    ) {
      if (isNilOrEmpty(props.preIncomes.pensionIncomes)) {
        props.preIncomes.pensionIncomes = [values]
      } else {
        props.preIncomes.pensionIncomes[currentIndex] = values
      }
    }

    if (typeOfIncome === TYPE_OF_INCOME.COMPANY_SALARY) {
      if (isNilOrEmpty(props.preIncomes.companySalaryIncome)) {
        props.preIncomes.companySalaryIncome = [values]
      } else {
        props.preIncomes.companySalaryIncome[currentIndex] = values
      }
    }

    if (typeOfIncome === TYPE_OF_INCOME.HOUSEHOLD_BUSINESS) {
      if (isNilOrEmpty(props.preIncomes.houseHoldBusinessIncome)) {
        props.preIncomes.houseHoldBusinessIncome = [values]
      } else {
        props.preIncomes.houseHoldBusinessIncome[currentIndex] = values
      }
    }

    try {
      await props.saveIncomes({
        variables: {
          input: {
            preApprovedId,
            preIncomes: props.preIncomes
          }
        }
      })
      // set current step in grid incomes
      const params = {
        step: STEP.step3
      }
      const input = {
        preApprovedId,
        currentStep: JSON.stringify(params)
      }

      await props.saveCurrentStep({ variables: { input } })
      props.setTypeOfIncome('')
      props.setIndex(-1)
      await props.data.refetch()
    } catch (e) {
      props.showMessage()
    }
  },
  ON_SUBMIT: (props: EnhancedProps) => async (values: any) => {
    props.goNext()
  },
  ON_SAVE_ONLY: (props: any) => async () => {
    const { currentIndex, typeOfIncome } = props
    const preApprovedId = R.pathOr('', ['preApprovedId'], props)
    const values = R.pathOr({}, ['values'], props)
    // if (isNilOrEmpty(currentIndex) || isNilOrEmpty(typeOfIncome)) {
    //   return
    // }

    if (typeOfIncome === TYPE_OF_INCOME.FROM_SALARY) {
      if (isNilOrEmpty(props.preIncomes.salaryIncomes)) {
        props.preIncomes.salaryIncomes = [values]
      } else {
        props.preIncomes.salaryIncomes[currentIndex] = values
      }
    }
    if (typeOfIncome === TYPE_OF_INCOME.CAR_RENTAL) {
      if (isNilOrEmpty(props.preIncomes.carRentIncomes)) {
        props.preIncomes.carRentIncomes = [values]
      } else {
        props.preIncomes.carRentIncomes[currentIndex] = values
      }
    }
    if (typeOfIncome === TYPE_OF_INCOME.HOUSE_LEASE) {
      if (isNilOrEmpty(props.preIncomes.houseLeaseIncomes)) {
        props.preIncomes.houseLeaseIncomes = [values]
      } else {
        props.preIncomes.houseLeaseIncomes[currentIndex] = values
      }
    }
    if (
      typeOfIncome === TYPE_OF_INCOME.FROM_PENSION ||
      typeOfIncome === TYPE_OF_INCOME.FROM_SPOUSE_PENSION
    ) {
      if (isNilOrEmpty(props.preIncomes.pensionIncomes)) {
        props.preIncomes.pensionIncomes = [values]
      } else {
        props.preIncomes.pensionIncomes[currentIndex] = values
      }
    }

    if (typeOfIncome === TYPE_OF_INCOME.COMPANY_SALARY) {
      if (isNilOrEmpty(props.preIncomes.companySalaryIncome)) {
        props.preIncomes.companySalaryIncome = [values]
      } else {
        props.preIncomes.companySalaryIncome[currentIndex] = values
      }
    }

    if (typeOfIncome === TYPE_OF_INCOME.HOUSEHOLD_BUSINESS) {
      if (isNilOrEmpty(props.preIncomes.houseHoldBusinessIncome)) {
        props.preIncomes.houseHoldBusinessIncome = [values]
      } else {
        props.preIncomes.houseHoldBusinessIncome[currentIndex] = values
      }
    }

    try {
      await props.saveIncomes({
        variables: {
          input: {
            preApprovedId,
            preIncomes: props.preIncomes
          }
        }
      })
      props.setTypeOfIncome('')
      props.setIndex(-1)
      await props.data.refetch()
    } catch (e) {
      props.showMessage()
    }
  }
}
