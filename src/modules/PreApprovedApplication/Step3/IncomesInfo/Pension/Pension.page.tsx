// import withLifecycle from '@hocs/with-lifecycle'
import { withFormik, FormikBag } from 'formik'
// import { pathOr } from 'ramda'
import { compose } from 'recompose'
// import { STEP } from '../../../PreApprovedApplication.type'
import { mapPropsToValues, validate } from './Pension.config'
import Pension from './Pension.view'
const formikConfig = {
  mapPropsToValues,
  validate,
  handleSubmit: async (
    values: any,
    { setSubmitting, props }: FormikBag<any, any>
  ) => {
    try {
      await props.onSaveIncome(values)
    } finally {
      setSubmitting(false)
    }
  },
  enableReinitialize: true
}

export default compose(
  withFormik(formikConfig)
  /* withLifecycle({
    onDidMount: (props: any) => {
      const typeOfIncome = pathOr('', ['typeOfIncome'], props)

      const params = {
        step: STEP.step3,
        childStep: '',
        childStepType: typeOfIncome
      }
      const preApprovedId = pathOr('', ['match', 'params', 'id'], props)
      const input = {
        preApprovedId,
        currentStep: JSON.stringify(params)
      }

      props.saveCurrentStep({ variables: { input } })
    }
  }) */
)(Pension)
