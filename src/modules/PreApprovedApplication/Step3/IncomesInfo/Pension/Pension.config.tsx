import { required } from 'formik-validators'
import * as R from 'ramda'
import { validator } from '../../../../../forms'
import { isMinValue } from '../../../PreApprovedApplication.util'
import { EnhancedProps } from '../IncomesInfo.type'

const pensionInitial = {
  ownerShip: 'SELF',
  paymentMode: 'TCB',
  income: ''
}

const mapPropsToValues = (props: EnhancedProps) => {
  const prePension = R.pathOr(
    {},
    ['preIncomes', 'pensionIncomes', props.currentIndex],
    props
  )
  if (!R.isEmpty(prePension)) {
    return prePension
  }

  return pensionInitial
}

const configs = {
  income: [
    required('PreApprovedApplication.step3.pension.income.required'),
    isMinValue('preApproved.step2.propertyValue.required', 1, true)
  ]
}

const validate = (values: any, props: any) => {
  const errors = validator(configs)(values, props)

  return {
    ...errors
  }
}

export { mapPropsToValues, validate }
