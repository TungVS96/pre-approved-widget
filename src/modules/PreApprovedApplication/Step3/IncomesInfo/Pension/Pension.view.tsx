import { rem } from 'dma-ui'
import { css } from 'emotion'
import { InjectedFormikProps } from 'formik'
import React, { FunctionComponent } from 'react'
import { IDropDownItem } from 'src/types'
import { RadioInput } from '../../../../../forms'
import {
  renderDropdown,
  renderTextInputSuggestMoney
} from '../../../../../forms/form'
import { t } from '../../../../../i18n'
import {
  formatCurrency,
  getFieldProps,
  getPositiveNumberOnly
} from '../../../../../utils'
import BottomBarButtons from '../../../BottomBarButtons/BottomBarButtons.component'
import { Container, ContentWrapper, IncomeWrapper } from '../IncomesInfo.style'
import { IPension } from '../IncomesInfo.type'

interface IInnerProps {
  onBack(): void
  dropdownData: {
    RC_LIST_PAYMENT_METHOD: IDropDownItem[]
  }
  ownerList: any
}

type PensionEnhancedProps = InjectedFormikProps<IInnerProps, IPension>
const Pension: FunctionComponent<PensionEnhancedProps> = (props) => {
  const { RC_LIST_PAYMENT_METHOD = [] } = props.dropdownData

  return (
    <Container>
      <div className={'mt-8'} />
      <ContentWrapper>
        <IncomeWrapper>
          <RadioInput
            {...getFieldProps('ownerShip', props)}
            options={props.ownerList}
            vertical={false}
            label={t('PreApprovedApplication.step3.pension.ownerShip.label')}
          />
        </IncomeWrapper>
        <IncomeWrapper>
          {renderDropdown({
            props,
            name: 'paymentMode',
            label: 'PreApprovedApplication.step3.pension.paymentMode.label',
            disabled: false,
            requiredField: true,
            items: RC_LIST_PAYMENT_METHOD,
            fieldContainerStyle: css({ borderRadius: rem(5) }),
            labelStyle: css({ fontWeight: 500 })
          })}
        </IncomeWrapper>
        <IncomeWrapper>
          {renderTextInputSuggestMoney({
            props,
            name: 'income',
            label: 'PreApprovedApplication.step3.pension.income.label',
            placeholder:
              'PreApprovedApplication.step3.pension.income.placeholder',
            disabled: false,
            requiredField: true,
            transformValue: (value) => getPositiveNumberOnly(value),
            format: (value) => formatCurrency({ value }),
            suggestMoney: true,
            times: 1000000
          })}
        </IncomeWrapper>
        <div className={'w-full'} />
        <IncomeWrapper>
          <BottomBarButtons onNext={props.handleSubmit} onPrev={props.onBack} />
        </IncomeWrapper>
      </ContentWrapper>
    </Container>
  )
}

export default Pension
