import { MARITAL_STATUS } from 'dma-types'
import * as R from 'ramda'
import { TYPE_OF_INCOME } from 'src/types'
import { t } from '../../../../i18n'
import { EnhancedProps, IPreIncomes, OWNER } from './IncomesInfo.type'

const deleteIncomes = ({
  index,
  typeOfIncome,
  preIncomes
}: {
  index: number
  typeOfIncome: TYPE_OF_INCOME
  preIncomes: IPreIncomes
}) => {
  if (
    typeOfIncome === TYPE_OF_INCOME.FROM_SALARY ||
    typeOfIncome === TYPE_OF_INCOME.FROM_SPOUSE_SALARY
  ) {
    const newSalaryIncomes = R.remove(index, 1, preIncomes.salaryIncomes)

    return {
      ...preIncomes,
      salaryIncomes: newSalaryIncomes
    }
  }

  if (typeOfIncome === TYPE_OF_INCOME.HOUSE_LEASE) {
    const newHouseLeaseIncomes = R.remove(
      index,
      1,
      preIncomes.houseLeaseIncomes
    )

    return {
      ...preIncomes,
      houseLeaseIncomes: newHouseLeaseIncomes
    }
  }

  if (typeOfIncome === TYPE_OF_INCOME.CAR_RENTAL) {
    const newCarRentIncomes = R.remove(index, 1, preIncomes.carRentIncomes)

    return {
      ...preIncomes,
      carRentIncomes: newCarRentIncomes
    }
  }

  if (
    typeOfIncome === TYPE_OF_INCOME.FROM_PENSION ||
    typeOfIncome === TYPE_OF_INCOME.FROM_SPOUSE_PENSION
  ) {
    const newPensionIncomes = R.remove(index, 1, preIncomes.pensionIncomes)

    return {
      ...preIncomes,
      pensionIncomes: newPensionIncomes
    }
  }

  if (typeOfIncome === TYPE_OF_INCOME.COMPANY_SALARY) {
    const newCompanySalaryIncomes = R.remove(
      index,
      1,
      preIncomes.companySalaryIncome
    )

    return {
      ...preIncomes,
      companySalaryIncome: newCompanySalaryIncomes
    }
  }

  if (
    typeOfIncome === TYPE_OF_INCOME.HOUSEHOLD_BUSINESS ||
    typeOfIncome === TYPE_OF_INCOME.SPOUSE_HOUSEHOLD_BUSINESS
  ) {
    const newHouseHoldBusinessIncomes = R.remove(
      index,
      1,
      preIncomes.houseHoldBusinessIncome
    )

    return {
      ...preIncomes,
      houseHoldBusinessIncome: newHouseHoldBusinessIncomes
    }
  }

  return preIncomes
}

const saveIncomes = ({
  index,
  typeOfIncome,
  preIncomes
}: {
  index: number
  typeOfIncome: TYPE_OF_INCOME
  preIncomes: IPreIncomes
}) => {
  if (typeOfIncome === TYPE_OF_INCOME.HOUSE_LEASE) {
    const newHouseLeaseIncomes = R.remove(
      index,
      1,
      preIncomes.houseLeaseIncomes
    )

    return {
      ...preIncomes,
      houseLeaseIncomes: newHouseLeaseIncomes
    }
  }

  return preIncomes
}

const getOwnerShip = (maritalStatus: MARITAL_STATUS) => {
  if (maritalStatus === MARITAL_STATUS.MARRIED) {
    return [
      {
        name: t('PreApprovedApplication.step3.salary.ownerShip.option.yes'),
        value: OWNER.SELF
      },
      {
        name: t('PreApprovedApplication.step3.salary.ownerShip.option.no'),
        value: OWNER.SPOUSE
      }
    ]
  }

  return [
    {
      name: t('PreApprovedApplication.step3.salary.ownerShip.option.yes'),
      value: OWNER.SELF
    }
  ]
}

const sortBy = (key: string, data: any) => {
  return R.sortWith([R.descend(R.prop(key))])(data)
}

const getIncomes = (
  typeOfIncome: TYPE_OF_INCOME | '',
  props: EnhancedProps
) => {
  if (
    typeOfIncome === TYPE_OF_INCOME.FROM_SALARY ||
    typeOfIncome === TYPE_OF_INCOME.FROM_SPOUSE_SALARY
  ) {
    return R.pathOr([], ['preIncomes', 'salaryIncomes'], props)
  }

  if (typeOfIncome === TYPE_OF_INCOME.CAR_RENTAL) {

    return R.pathOr([], ['preIncomes', 'carRentIncomes'], props)
  }
  if (typeOfIncome === TYPE_OF_INCOME.HOUSE_LEASE) {

    return R.pathOr([], ['preIncomes', 'houseLeaseIncomes'], props)
  }

  if (
    typeOfIncome === TYPE_OF_INCOME.FROM_PENSION ||
    typeOfIncome === TYPE_OF_INCOME.FROM_SPOUSE_PENSION
  ) {

    return R.pathOr([], ['preIncomes', 'pensionIncomes'], props)
  }

  if (typeOfIncome === TYPE_OF_INCOME.COMPANY_SALARY) {

    return R.pathOr([], ['preIncomes', 'companySalaryIncome'], props)
  }

  if (typeOfIncome === TYPE_OF_INCOME.HOUSEHOLD_BUSINESS) {

    return R.pathOr([], ['preIncomes', 'houseHoldBusinessIncome'], props)
  }

  return []
}

export { deleteIncomes, saveIncomes, getOwnerShip, sortBy, getIncomes }
