import { MARITAL_STATUS } from 'dma-types'
import { GraphqlQueryControls } from 'react-apollo'
import { IDropDownItem, TYPE_OF_INCOME } from '../../../../types'
import { IParentProps } from '../../PreApprovedApplication.type'

const incomesInfo = [
  {
    title: 'Lương',
    icon: require('./assets/ic-salary.svg'),
    typeOfIncome: TYPE_OF_INCOME.FROM_SALARY
  },
  {
    title: 'Thuê nhà',
    icon: require('./assets/ic-house-lease.svg'),
    typeOfIncome: TYPE_OF_INCOME.HOUSE_LEASE
  },
  {
    title: 'Thuê xe',
    icon: require('./assets/ic-car-rent.svg'),
    typeOfIncome: TYPE_OF_INCOME.CAR_RENTAL
  },
  {
    title: 'Hộ kinh doanh',
    icon: require('./assets/ic-self.svg'),
    typeOfIncome: TYPE_OF_INCOME.HOUSEHOLD_BUSINESS
  },
  {
    title: 'Doanh nghiệp',
    icon: require('./assets/ic-company.svg'),
    typeOfIncome: TYPE_OF_INCOME.COMPANY_SALARY
  },
  {
    title: 'Lương hưu',
    icon: require('./assets/ic-pension.svg'),
    typeOfIncome: TYPE_OF_INCOME.FROM_PENSION
  }
]

enum OWNER {
  SELF = 'SELF',
  SPOUSE = 'SPOUSE'
}

interface ISalary {
  ownerShip: string
  companyName: string
  companyCode: string
  bprofession: string
  position: string
  tenureYear: string
  tenureMonth: string
  paymentType: string
  provableStableIncome: string
}

interface ICarRent {
  manufacturer: string
  line: string
  paymentMode: string
  monthlyIncome: string
}

interface IHouseLease {
  typeOfProperty: string
  location: string
  paymentMethod: string
  incomeAmount: string
}

interface IPension {
  ownerShip: string
  paymentMode: string
  income: string
}

interface ICompany {
  ownerShip: string
  capitalContributionRatio: string
  recentAverageRevenue: string
  recentAverageProfit: string
}

interface IHouseHoldBusiness {
  ownerShip: string
  businessType: string
  businessLine1: string
  businessLine2: string
  businessLine3: string
  city: string
  averageRevenueInLast3months: string
  averageProfitInLast3months: string
}

interface IPreIncomes {
  companySalaryIncome: ICompany[]
  houseHoldBusinessIncome: IHouseHoldBusiness[]
  pensionIncomes: IPension[]
  houseLeaseIncomes: IHouseLease[]
  carRentIncomes: ICarRent[]
  salaryIncomes: ISalary[]
}

interface IProps extends IParentProps {
  maritalStatus: MARITAL_STATUS
  data: GraphqlQueryControls
  getDataDropdownList(payload: any): void
  preIncomes: IPreIncomes
  saveIncomes(payload: any): void
  typeOfIncome: TYPE_OF_INCOME | ''
  setTypeOfIncome(typeOfIncome: TYPE_OF_INCOME | ''): void
  currentIndex: number
  setIndex(currentIndex: number): void
  dropdownData: {
    RC_LIST_VERHICLE_VENDOR: IDropDownItem[]
    RC_LIST_VEHICLE_MODEL: IDropDownItem[]
    RC_LIST_PAYMENT_METHOD_RENTAL: IDropDownItem[]
    RC_LIST_HOUSERENT_TYPE: IDropDownItem[]
    RC_LIST_HOUSERENT_LOCATION: IDropDownItem[]
  }
  setDropdownData(dropdownData: {
    RC_LIST_VERHICLE_VENDOR: IDropDownItem[]
    RC_LIST_VEHICLE_MODEL: IDropDownItem[]
    RC_LIST_PAYMENT_METHOD_RENTAL: IDropDownItem[]
    RC_LIST_HOUSERENT_TYPE: IDropDownItem[]
    RC_LIST_HOUSERENT_LOCATION: IDropDownItem[]
  }): void
}

interface IHandlers {
  ON_ADD(typeOfIncome: TYPE_OF_INCOME): void
  ON_BACK(): void
  ON_DELETE(index: number, typeOfIncome: TYPE_OF_INCOME): void
  ON_EDIT(index: number, typeOfIncome: TYPE_OF_INCOME): void
  ON_SAVE(values: any): void
  ON_NEXT(param: string, preIncomes: IPreIncomes): void
  ON_SAVE_ONLY(): void
  // setPreApproved(params: any): void
  saveCurrentStep(params: any): void
}

type EnhancedProps = IProps & IHandlers

export {
  incomesInfo,
  EnhancedProps,
  IProps,
  IHandlers,
  IPreIncomes,
  ISalary,
  ICarRent,
  IHouseLease,
  IPension,
  ICompany,
  IHouseHoldBusiness,
  OWNER
}
