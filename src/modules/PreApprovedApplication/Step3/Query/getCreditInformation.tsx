import gql from 'graphql-tag'
import { pathOr } from 'ramda'

const query = gql`
  query($preApprovedId: String) {
    getCreditInformation(preApprovedId: $preApprovedId)
      @rest(
        type: "pre_approved"
        path: "/approved/v1/get-credit-information?{args}"
        endpoint: "dma-pre-credit"
      ) {
      isBScore
      leadMapId
      preApprovedId
      preCreditInformation
    }
  }
`
const params = {
  props: ({ data }: any) => {
    const creditInformation = pathOr({}, ['getCreditInformation'], data)

    return {
      creditInformation
    }
  },
  options: (props: any) => {
    const preApprovedId = pathOr('', ['preApprovedId'], props)

    return {
      variables: {
        preApprovedId
      }
    }
  }
}

export default { query, params }
