import { pathOr } from 'ramda'
import React, { FunctionComponent } from 'react'
import { t } from '../../../i18n'
import ConfirmDeletePopup from '../ConfirmDeletePopup/ConfirmDeletePopup.view'
import AssetsListing from './AssetsListing/AssetsListing.page'
import CreditInfo from './Components/CreditInfo/CreditInfo.page'
import LoanInfo from './Components/LoanInfo/LoanInfo.page'
import IncomesInfo from './IncomesInfo/IncomesInfo.page'
import { TYPE_OF_INFO } from './Step3.const'
import { Container } from './Step3.style'
import { EnhancedProps } from './Step3.type'

const Step3: FunctionComponent<EnhancedProps> = (props) => {
  const { typeOfInfo, history } = props
  const RC_LOAN_PRODUCT_LIST = pathOr(
    [],
    ['dropdownData', 'RC_LOAN_PRODUCT_LIST'],
    props
  )
  const prePerson = pathOr({}, ['prePerson'], props)

  // const creditInformation = pathOr({}, ['creditInformation'], props)
  // const match = pathOr({}, ['match'], props)
  return (
    <Container>
      {typeOfInfo === '' && <IncomesInfo {...props} />}
      {typeOfInfo === TYPE_OF_INFO.CREDIT_INFO && (
        <CreditInfo
          {...props}
          ON_NEXT={props.ON_NEXT}
          goBack={props.goBack}
          match={pathOr({}, ['match'], props)}
          setTypeOfInfo={props.setTypeOfInfo}
          prePerson={prePerson}
          history={history}
          saveCurrentStep={props.saveCurrentStep}
        />
      )}
      {typeOfInfo === TYPE_OF_INFO.LOAN_INFO && (
        <LoanInfo
          {...props}
          ON_NEXT={props.ON_NEXT}
          goNext={props.goNext}
          goBack={props.goBack}
          RC_LOAN_PRODUCT_LIST={RC_LOAN_PRODUCT_LIST}
          match={pathOr({}, ['match'], props)}
          setTypeOfInfo={props.setTypeOfInfo}
          prePerson={prePerson}
          history={history}
          saveCurrentStep={props.saveCurrentStep}
        />
      )}
      {typeOfInfo === TYPE_OF_INFO.ASSET_LISTING && (
        <AssetsListing
          {...props}
          goToApprovedResult={props.goToApprovedResult}
        />
      )}
      <ConfirmDeletePopup
        title={t('PreApprovedApplication.step3.next.warning')}
        isOpen={props.currentPopup === 'next'}
        handleClose={() => props.setPopup('')}
      />
    </Container>
  )
}

export default Step3
