import { PopupProps } from '../../../hocs'
import { IParentProps } from '../PreApprovedApplication.type'

interface IProps extends IParentProps, PopupProps {
  typeOfInfo: string
  setTypeOfInfo(typeOfInfo: string): void
  infoType: string
  saveCurrentStep(params: any): void
}

interface IOutterProps {
  goToApprovedResult(): void
}

interface IHandlers {
  ON_NEXT(param: string, preIncomes: any): void
  ON_BACK(values: any): void
}

type EnhancedProps = IProps & IHandlers & IOutterProps

export { EnhancedProps, IHandlers, IOutterProps }
