import { rem } from 'dma-ui'
import { css } from 'emotion'
import { InjectedFormikProps } from 'formik'
// import { FormValues } from 'formik-validators'
import { pathOr } from 'ramda'
import React, { FunctionComponent } from 'react'
// import Popup from '../../../../../components/Popup/Popup.component'
import { renderTextInputSuggestMoney } from '../../../../../forms/form'
import { t } from '../../../../../i18n'
import { formatCurrency } from '../../../../../utils'
import BottomBarButtons from '../../../BottomBarButtons/BottomBarButtons.component'
// import ModalExitComponent from '../../../Components/ModalExit/ModalExit.component'
import { MARITAL_STATUS, TYPE_OF_INFO } from '../../Step3.const'
// import { IncomeWrapper } from '../../Step3.style'
// import ModalConfirmExitView from '../ModalConfirmExit/ModalConfirmExit.view'
interface IValues {
  hasCreditCard: string
  loans: any[]
}
interface IProps {
  goBack(): void
  ON_NEXT(param: string): void
  setHasCreditCard(param: string): void
  ADD_CREDIT_CARD(): void
  REMOVE_CREDIT_CARD(params: number): void
  SET_OWN_CREDIT(index: number, params: string): void
  savePreCreditInfo(): void
  showModal: boolean
  setShowModal(showModal: boolean): void
  setTypeOfInfo(param: string): void
  validateInput(): boolean
  history: any
  validateValues: boolean
  setValidateValues(validateValues: boolean): void
  changeStep(): void
}
type EnhancedProps = InjectedFormikProps<IProps, IValues>
import {
  ButtonAdditional,
  ButtonAdditionalLabel,
  CardContent,
  CardContentLabel,
  CardContentLeft,
  CardContentRight,
  CardHeader,
  CardHeaderIcon,
  CardHeaderLabel,
  CheckboxChecked,
  CheckboxLabel,
  CheckboxUncheck,
  ColCheckbox,
  ColCheckboxOutCard,
  ColCheckboxOutCardLeft,
  Container,
  Content,
  ContentHeader,
  ElWarning,
  Header,
  IconRemove,
  Image,
  LeftCol,
  RightCol,
  WrapperCard,
  WrapperCheckbox,
  WrapperCheckboxOutCard
} from './CreditInfo.style'
const renderListCredit = (props: EnhancedProps) => {
  const loans = pathOr([], ['values', 'loans'], props)
  const maritalStatus = pathOr('', ['prePerson', 'maritalStatus'], props)

  return (
    <>
      {loans.map((item: any, index: number) => {
        const numberCredit = (index + 1).toString().padStart(2, '0')
        const relationshipWithPrimaryApplicant =
          item.relationshipWithPrimaryApplicant

        return (
          <WrapperCard key={index}>
            <CardHeader>
              <CardHeaderLabel>
                {t('TYPES.LOAN_TYPE.CREDIT_CARD')} {numberCredit}
              </CardHeaderLabel>
              {index !== 0 && (
                <CardHeaderIcon>
                  <IconRemove
                    onClick={() => {
                      props.REMOVE_CREDIT_CARD(index)
                    }}
                    src={require('./assets/trash-icon.svg')}
                  />
                </CardHeaderIcon>
              )}
            </CardHeader>
            <CardContent>
              <CardContentLeft>
                <CardContentLabel>
                  {t('preApproved.step3.creditCard.ownerCreditCard')}
                </CardContentLabel>
                <WrapperCheckbox>
                  <ColCheckbox
                    onClick={() => {
                      props.SET_OWN_CREDIT(index, 'SELF')
                    }}
                  >
                    {relationshipWithPrimaryApplicant === 'SELF' ? (
                      <CheckboxChecked />
                    ) : (
                        <CheckboxUncheck />
                      )}
                    <CheckboxLabel>
                      {t('preApproved.step3.creditCard.ownerShip.self')}
                    </CheckboxLabel>
                  </ColCheckbox>
                  {maritalStatus === MARITAL_STATUS.MAR && (
                    <ColCheckbox
                      onClick={() => {
                        props.SET_OWN_CREDIT(index, 'SPOUSE')
                      }}
                    >
                      {relationshipWithPrimaryApplicant === 'SPOUSE' ? (
                        <CheckboxChecked />
                      ) : (
                          <CheckboxUncheck />
                        )}
                      <CheckboxLabel>
                        {t('preApproved.step3.creditCard.ownerShip.spouse')}
                      </CheckboxLabel>
                    </ColCheckbox>
                  )}
                </WrapperCheckbox>
              </CardContentLeft>
              <CardContentRight>
                {renderTextInputSuggestMoney({
                  props,
                  label: 'preApproved.step3.creditCard.limited',
                  name: `loans[${index}].creditLimit`,
                  placeholder: 'preApproved.step2.propertyValue.placeholder',
                  disabled: false,
                  requiredField: true,
                  type: 'text',
                  transformValue: (value) => formatCurrency({ value }),
                  format: (value) => formatCurrency({ value }),
                  inputWrapperStyle: css({
                    borderRadius: rem(2),
                    marginTop: rem(2),
                    height: rem(40)
                  }),
                  labelStyle: css({
                    fontWeight: 500
                  }),
                  suggestMoney: true,
                  autoComplete: 'off',
                  times: 1000000
                  // iconWarning: false
                })}
              </CardContentRight>
            </CardContent>
          </WrapperCard>
        )
      })}
      <ButtonAdditional>
        <img
          onClick={props.ADD_CREDIT_CARD}
          src={require('./assets/add-circle-outline.svg')}
        />
        <ButtonAdditionalLabel onClick={props.ADD_CREDIT_CARD}>
          {t('preApproved.step3.creditCard.additionalCreditCard')}
        </ButtonAdditionalLabel>
      </ButtonAdditional>
    </>
  )
}
const CreditInfoView: FunctionComponent<EnhancedProps> = (props) => {
  const { ON_NEXT, setHasCreditCard, validateValues } = props
  const hasCreditCard = pathOr('', ['values', 'hasCreditCard'], props)

  return (
    <Container>
      <LeftCol>
        <Header>{t('preApproved.step3.creditCard.loan')}</Header>
        <Content>
          <ContentHeader>
            {t('preApproved.step3.creditCard.questionUseExterCredit')}
          </ContentHeader>
          <WrapperCheckboxOutCard>
            <ColCheckboxOutCardLeft>
              {hasCreditCard === 'Y' ? (
                <CheckboxChecked
                  onClick={() => {
                    setHasCreditCard('Y')
                  }}
                />
              ) : (
                  <CheckboxUncheck
                    onClick={() => {
                      setHasCreditCard('Y')
                    }}
                  />
                )}
              <CheckboxLabel>
                {t('TYPES._10_DAYS_PASTDUE_PAYMENT.YES')}
              </CheckboxLabel>
            </ColCheckboxOutCardLeft>
            <ColCheckboxOutCard>
              {hasCreditCard === 'N' ? (
                <CheckboxChecked
                  onClick={() => {
                    setHasCreditCard('N')
                  }}
                />
              ) : (
                  <CheckboxUncheck
                    onClick={() => {
                      setHasCreditCard('N')
                    }}
                  />
                )}
              <CheckboxLabel>
                {t('TYPES._10_DAYS_PASTDUE_PAYMENT.NO')}
              </CheckboxLabel>
            </ColCheckboxOutCard>
            {validateValues && hasCreditCard === '' && (
              <ElWarning>
                {t('PreApprovedApplication.step3.spouse.required')}
              </ElWarning>
            )}
          </WrapperCheckboxOutCard>
          {hasCreditCard === 'Y' && renderListCredit(props)}
        </Content>
        {/* <IncomeWrapper> */}
        <BottomBarButtons
          id={'pre_fillin_creditinfo_creditcard'}
          width={163}
          onPrev={async () => {
            // props.setShowModal(true)
            await props.savePreCreditInfo()
            await props.changeStep()
            props.setTypeOfInfo('')
          }}
          onNext={async () => {
            props.handleSubmit()
            props.setValidateValues(true)
            if (!props.validateInput()) return
            await props.savePreCreditInfo()
            ON_NEXT(TYPE_OF_INFO.LOAN_INFO)
          }}
        />
        {/* </IncomeWrapper> */}
      </LeftCol>
      <RightCol>
        <Image src={require('./assets/right_image.svg')} />
      </RightCol>
    </Container>
  )
}

export { EnhancedProps, IValues }
export default CreditInfoView
