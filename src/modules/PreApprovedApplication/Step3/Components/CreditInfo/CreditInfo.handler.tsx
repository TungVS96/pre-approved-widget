import { pathOr } from 'ramda'
import { parseIntMoney } from '../../../../../utils/number.util'
import { STEP } from '../../../PreApprovedApplication.type'
export default {
  setHasCreditCard: (props: any) => (param: string) => {
    props.setFieldValue('hasCreditCard', param)
    if (param === 'N') {
      props.setFieldValue('loans', [])
      props.setTouched({})

      return
    }

    props.setFieldValue('loans', [
      {
        relationshipWithPrimaryApplicant: 'SELF',
        loanType: 'CREDIT_CARD',
        creditLimit: '0'
      }
    ])
  },
  ADD_CREDIT_CARD: (props: any) => () => {
    const creditInformationsPrev = pathOr([], ['values', 'loans'], props)
    const creditInformations = JSON.parse(
      JSON.stringify(creditInformationsPrev)
    )

    creditInformations.push({
      relationshipWithPrimaryApplicant: 'SELF',
      loanType: 'CREDIT_CARD',
      creditLimit: '0'
    })
    props.setFieldValue('loans', creditInformations)
  },
  REMOVE_CREDIT_CARD: (props: any) => (index: number) => {
    const creditInformationsPrev = pathOr([], ['values', 'loans'], props)
    const creditInformations = JSON.parse(
      JSON.stringify(creditInformationsPrev)
    )

    creditInformations.splice(index, 1)
    const touched = JSON.parse(JSON.stringify(pathOr({}, ['touched'], props)))

    if (touched.loans && touched.loans[index]) {
      // touched.loans.splice(index, 1)
      // props.setTouched('loans', touched.loans)
      props.setFieldTouched(`loans[${index}].creditLimit`, false)
    }

    props.setFieldValue('loans', creditInformations)
  },
  SET_OWN_CREDIT: (props: any) => (index: number, params: string) => {
    const creditInformationsPrev = pathOr([], ['values', 'loans'], props)
    const creditInformations = JSON.parse(
      JSON.stringify(creditInformationsPrev)
    )

    creditInformations[index].relationshipWithPrimaryApplicant = params
    props.setFieldValue('loans', creditInformations)
  },
  validateInput: (props: any) => () => {
    const loans = pathOr([], ['values', 'loans'], props)
    let flag = true
    const hasCreditCard = pathOr('', ['values', 'hasCreditCard'], props)

    if (hasCreditCard === '') {
      return false
    }
    loans.map((item: any, index: number) => {
      if (item.creditLimit === '' || parseIntMoney(item.creditLimit) === 0) {
        flag = false
      }
    })

    return flag
  },
  savePreCreditInfo: (props: any) => async () => {
    const creditInformation = pathOr({}, ['creditInformation'], props)
    const hasCreditCard = pathOr('', ['values', 'hasCreditCard'], props)
    const preCreditInformationPrev = pathOr(
      {},
      ['preCreditInformation', 'creditInformation'],
      creditInformation
    )

    let loanPrev = pathOr([], ['loans'], preCreditInformationPrev)

    loanPrev = loanPrev.filter((item: any) => item.loanType !== 'CREDIT_CARD')
    const loans = pathOr([], ['values', 'loans'], props)

    loans.map((item: any, index: number) => {
      loans[index].creditLimit = parseIntMoney(item.creditLimit)
    })
    const preCreditInformation = JSON.parse(
      JSON.stringify(preCreditInformationPrev)
    )

    preCreditInformation.hasCreditCard = hasCreditCard
    preCreditInformation.loans = [...loanPrev, ...loans]

    const input = {
      preCreditInformation: {
        creditInformation: {
          hasCreditCard: preCreditInformation.hasCreditCard,
          hasOtherLoan: preCreditInformation.hasOtherLoan,
          loans: preCreditInformation.loans
        }
      },
      isBScore: creditInformation.isBScore,
      leadMapId: creditInformation.leadMapId,
      preApprovedId: creditInformation.preApprovedId
    }

    await props.saveCreditInformation({ variables: { input } })
  },
  changeStep: (props: any) => async () => {
    const params = {
      step: STEP.step3
    }
    const preApprovedId = pathOr('', ['preApprovedId'], props)
    const input = {
      preApprovedId,
      currentStep: JSON.stringify(params)
    }

    await props.saveCurrentStep({ variables: { input } })
  }
}
