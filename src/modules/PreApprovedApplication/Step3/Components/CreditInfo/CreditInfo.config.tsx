import { required } from 'formik-validators'
import { compose, filter, identity, isEmpty, not, pathOr, pickBy } from 'ramda'
import { isNilOrEmpty } from 'ramda-adjunct'
import { validator } from '../../../../../forms'
import { t } from '../../../../../i18n'
import { parseIntMoney } from '../../../../../utils/number.util'
import { IValues } from './CreditInfo.view'
const isNotEmptyObject = compose(
  not,
  isEmpty
)
const validateValue = (errorKey: string) => ({ value }: { value: string }) => {
  if (isNilOrEmpty(value)) return
  // const newValue = normalizePhone(value)
  const isValid = parseIntMoney(value) > 0
  if (!isValid) {
    return t(errorKey)
  }
}
const mapPropsToValues = (props: any) => {
  const preCreditInformation = pathOr(
    {},
    ['creditInformation', 'preCreditInformation', 'creditInformation'],
    props
  )
  const loans = pathOr([], ['loans'], preCreditInformation).filter(
    (item: any) => item.loanType === 'CREDIT_CARD'
  )

  return {
    loans,
    hasCreditCard: pathOr('', ['hasCreditCard'], preCreditInformation)
  }
}
const validate = (values: IValues, props: any) => {
  const loans = pathOr([], ['loans'], values)
  const hasCreditCard = pathOr(null, ['hasCreditCard'], values)
  const loansErrors = hasCreditCard
    ? loans.map((items: any) => {
        const configs = {
          creditLimit: [
            required('preApproved.step2.loanAmount.required'),
            validateValue('preApproved.step2.loanAmount.required')
          ]
        }

        return pickBy(identity, validator(configs)({ ...items }, props))
      })
    : []

  if (filter(isNotEmptyObject, loansErrors).length > 0) {
    return {
      loans: loansErrors
    }
  }

  return {}
}

export { mapPropsToValues, validate }
