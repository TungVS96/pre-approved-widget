import withLifecycle from '@hocs/with-lifecycle'
import { withFormik } from 'formik'
import { pathOr } from 'ramda'
import { graphql } from 'react-apollo'
import { compose, withHandlers, withState } from 'recompose'
import { STEP } from '../../../PreApprovedApplication.type'
import saveCreditInformation from '../../Mutation/saveCreditInformation'
import getCreditInformation from '../../Query/getCreditInformation'
import { TYPE_OF_INFO } from '../../Step3.const'
import { mapPropsToValues, validate } from './CreditInfo.config'
import handlers from './CreditInfo.handler'
import CreditInfoView, { EnhancedProps } from './CreditInfo.view'
const config = {
  mapPropsToValues,
  validate,
  handleSubmit: () => () => {
    //
  },
  enableReinitialize: true
}

const enhancer = compose<EnhancedProps, any>(
  graphql(getCreditInformation.query, getCreditInformation.params),
  graphql(saveCreditInformation.mutation, saveCreditInformation.params),
  withState('showModal', 'setShowModal', false),
  withState('validateValues', 'setValidateValues', false),
  withFormik(config),
  withHandlers(handlers),
  withLifecycle({
    onDidMount: async (props: any) => {
      const params = {
        step: STEP.step3,
        childStep: TYPE_OF_INFO.CREDIT_INFO
      }
      const preApprovedId = pathOr('', ['preApprovedId'], props)
      const input = {
        preApprovedId,
        currentStep: JSON.stringify(params)
      }

      props.saveCurrentStep({ variables: { input } })
    }
  })
)

export default enhancer(CreditInfoView)
