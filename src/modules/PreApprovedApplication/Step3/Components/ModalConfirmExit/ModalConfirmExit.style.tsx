import { rem, theme, withClassName } from 'dma-ui'
import styled from 'react-emotion'
import { getFontFamily } from 'src/utils/string.util'

const Container = withClassName(
  'flex flex-col w-full h-full justify-center items-center'
)(styled('div')())

const Content = withClassName('bg-white')(
  styled('div')({
    width: '90%',
    height: '70%',
    overflowX: 'auto',
    [theme.mq.md]: {
      width: rem(524),
      height: rem(540)
    }
  })
)

const Description = withClassName('flex flex-col justify-center items-center')(
  styled('div')({
    marginTop: rem(13),
    paddingLeft: rem(20),
    paddingRight: rem(20),
    lineHeight: rem(20),
    height: rem(43),
    [theme.mq.md]: {
      height: rem(50)
    }
  })
)

const Image = withClassName('')(styled('img')())

const BottomButton = withClassName('flex')()

const TextTitle = withClassName()(
  styled('div')({
    fontStyle: 'normal',
    fontWeight: 500,
    fontSize: rem(18),
    lineHeight: rem(27),
    color: '#17233D'
  })
)

const TextField = withClassName()(
  styled('div')({
    fontStyle: 'normal',
    fontWeight: 400,
    fontSize: rem(14),
    lineHeight: rem(19),
    color: '#17233D',
    [theme.mq.md]: {
      marginLeft: rem(9.76)
    }
  })
)

const Title = withClassName('font-bold')(
  styled('div')({
    width: '100%',
    fontSize: rem(18),
    lineHeight: rem(27),
    paddingLeft: rem(20),
    paddingRight: rem(20)
  })
)

const Subtitle = withClassName('leading-normal text-center text-white')(
  styled('div')({
    width: rem(250),
    height: rem(48),
    marginTop: rem(26)
  })
)

const FormContainer = withClassName('flex sm:flex-col md:flex-row flex-wrap')(
  styled('div')(() => ({
    marginTop: rem(12),
    [theme.mq.md]: {
      marginTop: rem(17)
    }
  }))
)
const FormItem = withClassName('flex-col sm:w-1/1 md:w-1/2 ')(
  styled('div')(() => ({
    paddingLeft: rem(20),
    paddingRight: rem(20)
  }))
)
const Text = withClassName('text')(
  styled('p')({
    fontSize: rem(13),
    color: 'black',
    fontWeight: 500
  })
)
const TextPreview = withClassName('text flex items-center')(
  styled('div')({
    fontSize: rem(14),
    color: '#17233D',
    backgroundColor: '#E8EAEC',
    boxSizing: 'border-box',
    border: '1px solid #DCDEE2',
    borderRadius: '2px',
    height: rem(40),
    marginTop: rem(12),
    paddingLeft: rem(10)
  })
)
const FormItemCheckbox = withClassName('flex flex-row flex-wrap')(
  styled('div')({
    width: '100%',
    paddingLeft: rem(20),
    paddingRight: rem(20)
  })
)
const ColCheckbox = withClassName('w-1/3 flex flex-row items-center')(
  styled('div')({
    paddingTop: rem(20)
  })
)

const ButtonContainer = withClassName('flex justify-center cursor-pointer')(
  styled('button')(({}) => ({
    background: '#FFFFFF',
    border: `${rem(1)} solid #D14F4F`,
    boxSizing: 'border-box',
    borderRadius: rem(2),
    width: rem(302),
    height: rem(40),
    marginTop: rem(12),
    marginLeft: 'auto',
    marginRight: 'auto',
    color: '#C52425',
    fontSize: rem(14),
    fontWeight: 500,
    alignItems: 'center',
    justifyContent: 'center',
    [theme.mq.md]: {
      width: rem(325)
    },
    outline: 'none !important'
  }))
)
const StartButton = withClassName('text-lg font-bold md:shadow sm:mt-2')(
  styled('button')(({}) => ({
    height: '40px',
    backgroundColor: theme.colors.reddish,
    color: theme.colors.white,
    cursor: 'pointer',
    fontSize: '14px',
    opacity: 0.9,
    display: 'flex',
    width: rem(302),
    alignItems: 'center',
    justifyContent: 'center',
    textTransform: 'uppercase',
    borderRadius: rem(2),
    [theme.mq.md]: {
      width: rem(249.6)
    },
    outline: 'none !important'
  }))
)
const ContentResult = withClassName('flex flex-col')(
  styled('form')({
    marginTop: rem(20),
    marginRight: rem(32),
    width: '100%'
  })
)

const ImageBlockResult = withClassName('flex')(
  styled('div')({ marginLeft: rem(-12), marginTop: rem(-45) })
)

const ButtonBlock = withClassName('flex')(
  styled('div')({ justifyContent: 'flex-end' })
)

const ContentResultHeader = withClassName('flex-col justify-center')(
  styled('div')({
    paddingTop: rem(37),
    [theme.mq.md]: {
      paddingLeft: rem(18)
    }
  })
)
const ContentResultBody = withClassName('text-center')(
  styled('div')({
    fontSize: rem(14),
    marginTop: rem(10),
    paddingLeft: rem(20),
    paddingRight: rem(20),
    fontColor: '#17233D',
    fontFamily: getFontFamily(),
    [theme.mq.md]: {
      paddingLeft: rem(33),
      paddingRight: rem(33)
    }
  })
)
const ItemDropDown = withClassName()(
  styled('div')({
    marginBottom: rem(20),
    [theme.mq.md]: {
      marginBottom: rem(0)
    }
  })
)
const WarningCombobox = withClassName()(
  styled('div')({
    height: rem(14),
    fontSize: rem(12),
    fontWeight: 'normal',
    color: '#ED4014'
  })
)

const DropDown = withClassName('flex')(
  styled('div')({
    paddingRight: rem(15)
  })
)
const YesButton = withClassName()(
  styled('button')({
    height: rem(40),
    width: rem(150),
    color: '#fff',
    border: '0.5px solid #DCDEE2',
    boxSizing: 'border-box',
    borderRadius: rem(2),
    backgroundColor: '#da251d'
  })
)
const NoButton = withClassName()(
  styled('button')({
    color: '#17233D',
    height: rem(40),
    width: rem(150),
    border: '0.5px solid #ddd',
    boxSizing: 'border-box',
    borderRadius: rem(2),
    backgroundColor: '#fff',
    marginRight: rem(10)
  })
)

export {
  Container,
  Content,
  Description,
  Title,
  Subtitle,
  Image,
  FormContainer,
  FormItem,
  Text,
  TextPreview,
  FormItemCheckbox,
  ColCheckbox,
  ButtonContainer,
  StartButton,
  ContentResult,
  ImageBlockResult,
  ContentResultHeader,
  ContentResultBody,
  ItemDropDown,
  WarningCombobox,
  TextTitle,
  DropDown,
  BottomButton,
  ButtonBlock,
  TextField,
  YesButton,
  NoButton
}
