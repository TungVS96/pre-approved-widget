import React, { FunctionComponent } from 'react'

import { t } from '../../../../../i18n'

import {
  ButtonBlock,
  ContentResult,
  Image,
  ImageBlockResult,
  NoButton,
  TextField,
  TextTitle,
  YesButton
} from './ModalConfirmExit.style'
interface IProps {
  setShowModal(showModal: boolean): void
  typeOfInfo: string
  setTypeOfInfo(typeOfInfo: string): void
  savePreCreditInfo(): void
}
type EnhancedProps = IProps
const Modal: FunctionComponent<EnhancedProps> = (props) => {
  return (
    <ContentResult>
      <ImageBlockResult>
        <Image
          width={20.47}
          height={19.33}
          src={require('./assets/alert-circle-outline.svg')}
        />
        <TextTitle style={{ marginLeft: 9.76 }}>
          {t('preApproved.step3.loanInfo.modal.title')}
        </TextTitle>
      </ImageBlockResult>
      <TextField
        style={{ height: 38, marginTop: 21, marginLeft: 18, width: 268 }}
      >
        {t('preApproved.step3.loanInfo.modal.content')}
      </TextField>
      <ButtonBlock style={{ marginTop: 14 }}>
        <NoButton
          type="button"
          onClick={() => {
            props.setShowModal(false)
            props.setTypeOfInfo(props.typeOfInfo)
          }}
        >
          Không
        </NoButton>
        <YesButton
          type="button"
          onClick={() => {
            props.setShowModal(false)
            props.savePreCreditInfo()
            props.setTypeOfInfo(props.typeOfInfo)
          }}
        >
          Có
        </YesButton>
      </ButtonBlock>
    </ContentResult>
  )
}

export default Modal
