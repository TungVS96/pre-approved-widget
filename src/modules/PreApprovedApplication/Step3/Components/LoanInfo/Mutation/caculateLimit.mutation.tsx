import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    caculateLimit(input: $input, preApprovedId: $preApprovedId)
      @rest(
        type: "Person"
        endpoint: "dma-pre-credit"
        path: "/approved/v1/calculate-limit/{args.preApprovedId}"
        method: "post"
      ) {
      statusCalculated
      errorCode
      errorName
      apiNameError
    }
  }
`
const params = {
  name: 'caculateLimit'
}

export default {
  mutation,
  params
}
