import { rem, theme } from 'dma-ui'
import { css } from 'emotion'
import { InjectedFormikProps } from 'formik'

import { filter, pathOr } from 'ramda'
import React, { FunctionComponent } from 'react'
import {
  renderDropdown,
  renderTextInputSuggestMoney
} from '../../../../../forms/form'
import { PopupProps } from '../../../../../hocs'
import { t } from '../../../../../i18n'
import { formatCurrency } from '../../../../../utils'
import BottomBarButtons from '../../../BottomBarButtons/BottomBarButtons.component'
import { PRE_APPROVED_STATUS } from '../../../PreApprovedApplication.constant'
import ModalWarningCalculatorWish from '../../ModalWarningCalculatorWish/ModalWarningCalculatorWish.component'
import { MARITAL_STATUS, TYPE_OF_INFO } from '../../Step3.const'
interface IValues {
  hasOtherLoan: string
  loans: any[]
}
interface IProps extends PopupProps {
  caculateLimit(payload: any): void
  changeHasOtherLoan(params: string): void
  ADD_LOAN(): void
  REMOVE_LOAN(params: number): void
  SET_OWN_LOAN(index: number, params: string): void
  savePreCreditInfo(): void
  showModal: boolean
  setShowModal(showModal: boolean): void
  setTypeOfInfo(param: string): void
  validateInput(): boolean
  goNext(id?: number, step?: number): void
  updateStatus(payload: any): void
  showMessage(): void
  validateValues: boolean
  setValidateValues(validateValues: boolean): void
  history: any
}
type EnhancedProps = InjectedFormikProps<IProps, IValues>
import {
  ButtonAdditional,
  ButtonAdditionalLabel,
  CardContent,
  CardContentLabel,
  CardContentLeft,
  CardContentRight,
  CardHeader,
  CardHeaderIcon,
  CardHeaderLabel,
  CheckboxChecked,
  CheckboxLabel,
  CheckboxUncheck,
  ColCheckbox,
  ColCheckboxOutCard,
  ColCheckboxOutCardLeft,
  Container,
  Content,
  ContentHeader,
  ElWarning,
  Header,
  IconRemove,
  Image,
  LeftCol,
  RightCol,
  WrapperCard,
  WrapperCheckbox,
  WrapperCheckboxOutCard
} from './LoanInfo.style'

export const showMonthlyInstallment = (loanType: string) => {
  return (
    loanType === 'UNSECURED_OVERDRAFT' ||
    loanType === 'SECURED_OVERDRAFT' ||
    loanType === 'BUSINESS_HOUSEHOLD_LOAN_CREDIT_LIMIT'
  )
}

const updateStatus = async (props: IProps) => {
  try {
    const preApprovedId = pathOr('', ['preApprovedId'], props)
    await props.updateStatus({
      variables: {
        input: {
          id: preApprovedId,
          status: PRE_APPROVED_STATUS.PR02
        }
      }
    })
  } catch (e) {
    props.showMessage()
  }
}

const renderLoans = (props: EnhancedProps) => {
  const loans = pathOr([], ['values', 'loans'], props)
  let RC_LOAN_PRODUCT_LIST = pathOr([], ['RC_LOAN_PRODUCT_LIST'], props)

  RC_LOAN_PRODUCT_LIST = filter(
    (o: any) => o.value !== 'CREDIT_CARD',
    RC_LOAN_PRODUCT_LIST
  )

  const maritalStatus = pathOr('', ['prePerson', 'maritalStatus'], props)

  return (
    <>
      {loans.map((item: any, index: number) => {
        const numberCredit = (index + 1).toString().padStart(2, '0')
        const relationshipWithPrimaryApplicant =
          item.relationshipWithPrimaryApplicant
        const loanType = pathOr('', ['loanType'], item)

        return (
          <WrapperCard key={index}>
            <CardHeader>
              <CardHeaderLabel>
                {t('preApproved.step3.loanInfo.loanQuestion')} {numberCredit}
              </CardHeaderLabel>
              {index !== 0 && (
                <CardHeaderIcon>
                  <IconRemove
                    onClick={() => {
                      props.REMOVE_LOAN(index)
                    }}
                    src={require('./assets/trash-icon.svg')}
                  />
                </CardHeaderIcon>
              )}
            </CardHeader>
            <CardContent>
              <CardContentLeft
                className={css({
                  paddingBottom: rem(20),
                  [theme.mq.md]: {
                    paddingBottom: rem(0)
                  }
                })}
              >
                <CardContentLabel>
                  {t('preApproved.step3.loanInfo.ownerLoan')}
                </CardContentLabel>
                <WrapperCheckbox>
                  <ColCheckbox
                    onClick={() => {
                      props.SET_OWN_LOAN(index, 'SELF')
                    }}
                  >
                    {relationshipWithPrimaryApplicant === 'SELF' ? (
                      <CheckboxChecked />
                    ) : (
                        <CheckboxUncheck />
                      )}
                    <CheckboxLabel>
                      {t('preApproved.step3.creditCard.ownerShip.self')}
                    </CheckboxLabel>
                  </ColCheckbox>
                  {maritalStatus === MARITAL_STATUS.MAR && (
                    <ColCheckbox
                      onClick={() => {
                        props.SET_OWN_LOAN(index, 'SPOUSE')
                      }}
                    >
                      {relationshipWithPrimaryApplicant === 'SPOUSE' ? (
                        <CheckboxChecked />
                      ) : (
                          <CheckboxUncheck />
                        )}
                      <CheckboxLabel>
                        {t('preApproved.step3.creditCard.ownerShip.spouse')}
                      </CheckboxLabel>
                    </ColCheckbox>
                  )}
                </WrapperCheckbox>
              </CardContentLeft>
              <CardContentRight>
                {renderDropdown({
                  props,
                  name: `loans[${index}].loanType`,
                  requiredField: false,
                  label: 'applicationOnlineFrom.step4.productName.label',
                  placeholder:
                    'preApproved.step3.loanInfo.loanProductPlaceholder',
                  items: RC_LOAN_PRODUCT_LIST,
                  iconWarning: false,
                  displayFieldStyle: css({
                    height: rem(40)
                  }),
                  fieldContainerStyle: css({
                    borderRadius: rem(2)
                  }),
                  filterInputStyle: css({
                    marginTop: `${rem(2)} !important`
                  }),
                  highLight: true,
                  customArrow: true
                })}
              </CardContentRight>
              {loanType !== '' && !showMonthlyInstallment(loanType) && (
                <CardContentLeft>
                  {renderTextInputSuggestMoney({
                    props,
                    label: 'preApproved.step3.loanInfo.monthlyInstallment',
                    name: `loans[${index}].monthlyInstallment`,
                    placeholder: 'preApproved.step2.propertyValue.placeholder',
                    disabled: false,
                    requiredField: false,
                    type: 'text',
                    transformValue: (value) => formatCurrency({ value }),
                    format: (value) => formatCurrency({ value }),
                    inputWrapperStyle: css({
                      borderRadius: rem(2),
                      marginTop: rem(2),
                      height: rem(40)
                    }),
                    labelStyle: css({
                      fontWeight: 500
                    }),
                    suggestMoney: true,
                    autoComplete: 'off',
                    times: 1000000
                    // iconWarning: false
                  })}
                </CardContentLeft>
              )}
              {showMonthlyInstallment(loanType) && (
                <CardContentLeft>
                  {renderTextInputSuggestMoney({
                    props,
                    label: 'preApproved.step3.loanInfo.creditLimit',
                    name: `loans[${index}].creditLimit`,
                    placeholder: 'preApproved.step2.propertyValue.placeholder',
                    disabled: false,
                    requiredField: false,
                    type: 'text',
                    transformValue: (value) => formatCurrency({ value }),
                    format: (value) => formatCurrency({ value }),
                    inputWrapperStyle: css({
                      borderRadius: rem(2),
                      marginTop: rem(2),
                      height: rem(40)
                    }),
                    labelStyle: css({
                      fontWeight: 500
                    }),
                    suggestMoney: true,
                    autoComplete: 'off',
                    times: 1000000
                    // iconWarning: false
                  })}
                </CardContentLeft>
              )}
            </CardContent>
          </WrapperCard>
        )
      })}
      <ButtonAdditional>
        <img
          onClick={props.ADD_LOAN}
          src={require('./assets/add-circle-outline.svg')}
        />
        <ButtonAdditionalLabel onClick={props.ADD_LOAN}>
          {t('preApproved.step3.loanInfo.additional')}
        </ButtonAdditionalLabel>
      </ButtonAdditional>
    </>
  )
}

const checkIsErrorInLoanView = (values: any, errors: any) => {
  const keyErrors = Object.keys(errors)
  if (keyErrors.length > 0) {
    return true
  }
  if (values.hasOtherLoan === '') {
    return true
  }
  if (
    values.hasOtherLoan === 'Y' &&
    values.loans &&
    values.loans.length === 0
  ) {
    return true
  }
  // nếu tồn tại khoản vay mà chưa chọn loanType thì cảnh báo lỗi
  if (
    values.hasOtherLoan === 'Y' &&
    values.loans.length > 0 &&
    values.loans.filter((e: any) => !e.loanType).length > 0
  ) {
    return true
  }
  const noCreditLimit =
    values.loans.filter(
      (e: any) => showMonthlyInstallment(e.loanType) && !e.creditLimit
    ).length > 0
  if (values.hasOtherLoan === 'Y' && values.loans.length > 0 && noCreditLimit) {
    return true
  }
  const noMonthlyInstallment =
    values.loans.filter(
      (e: any) => !showMonthlyInstallment(e.loanType) && !e.monthlyInstallment
    ).length > 0
  // const noMonthlyInstallment = values.loans.filter((e: any) => !e.monthlyInstallment).length > 0
  if (
    values.hasOtherLoan === 'Y' &&
    values.loans.length > 0 &&
    noMonthlyInstallment
  ) {
    return true
  }

  return false
}

const LoanInfoView: FunctionComponent<EnhancedProps> = (props) => {
  const { changeHasOtherLoan, validateValues } = props
  const hasOtherLoan = pathOr('', ['values', 'hasOtherLoan'], props)
  const preApprovedId = pathOr('', ['preApprovedId'], props)

  return (
    <Container>
      <LeftCol>
        <Header>{t('preApproved.step3.creditCard.loan')}</Header>
        <Content>
          <ContentHeader>
            {t('preApproved.step3.loanInfo.isLoanExtenalTech')}
          </ContentHeader>
          <WrapperCheckboxOutCard>
            <ColCheckboxOutCardLeft>
              {hasOtherLoan === 'Y' ? (
                <CheckboxChecked
                  onClick={() => {
                    changeHasOtherLoan('Y')
                  }}
                />
              ) : (
                  <CheckboxUncheck
                    onClick={() => {
                      changeHasOtherLoan('Y')
                    }}
                  />
                )}
              <CheckboxLabel>
                {t('TYPES._10_DAYS_PASTDUE_PAYMENT.YES')}
              </CheckboxLabel>
            </ColCheckboxOutCardLeft>
            <ColCheckboxOutCard>
              {hasOtherLoan === 'N' ? (
                <CheckboxChecked
                  onClick={() => {
                    changeHasOtherLoan('N')
                  }}
                />
              ) : (
                  <CheckboxUncheck
                    onClick={() => {
                      changeHasOtherLoan('N')
                    }}
                  />
                )}
              <CheckboxLabel>
                {t('TYPES._10_DAYS_PASTDUE_PAYMENT.NO')}
              </CheckboxLabel>
            </ColCheckboxOutCard>
            {validateValues && hasOtherLoan === '' && (
              <ElWarning>
                {t('PreApprovedApplication.step3.spouse.required')}
              </ElWarning>
            )}
          </WrapperCheckboxOutCard>
          {hasOtherLoan === 'Y' && renderLoans(props)}
        </Content>
        {/* <IncomeWrapper> */}
        <BottomBarButtons
          width={163}
          onPrev={async () => {
            await props.savePreCreditInfo()
            props.setTypeOfInfo(TYPE_OF_INFO.CREDIT_INFO)
            // props.setShowModal(true)
          }}
          id={'pre_cal_loan'}
          nextTitle="landingPage.calculator.button.calculate"
          onNext={async () => {
            try {
              props.handleSubmit()
              props.setValidateValues(true)
              const isError = checkIsErrorInLoanView(props.values, props.errors)
              if (isError) {
                return
              }
              await props.savePreCreditInfo()
              const response = await props.caculateLimit({
                variables: { preApprovedId, input: {} }
              })

              if (
                pathOr(
                  '',
                  ['data', 'caculateLimit', 'statusCalculated'],
                  response
                ) === '1'
              ) {
                return props.setPopup('ModalWarningCalculatorWish')
              }

              await updateStatus(props)
              props.goNext()
            } catch (e) {
              props.showMessage()
            }
          }}
        />
        {/* </IncomeWrapper> */}
      </LeftCol>
      <RightCol>
        <Image src={require('./assets/right_image.svg')} />
      </RightCol>

      <ModalWarningCalculatorWish
        isOpen={props.currentPopup === 'ModalWarningCalculatorWish'}
        onClose={() => {
          props.setPopup('')
        }}
      />
    </Container>
  )
}

export { EnhancedProps, IValues }
export default LoanInfoView
