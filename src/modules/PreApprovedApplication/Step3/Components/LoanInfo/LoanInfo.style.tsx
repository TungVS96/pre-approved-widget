import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const Container = withClassName('flex', 'Container')(
  styled('div')(({}) => ({
    backgroundColor: '#FAFDFF'
  }))
)
const LeftCol = withClassName()(
  styled('div')(({ theme }) => ({
    paddingTop: rem(24),
    paddingLeft: rem(15),
    paddingRight: rem(15),
    width: '100%',
    [theme.mq.md]: {
      paddingLeft: rem(32),
      width: '60%',
      paddingRight: rem(0)
    }
  }))
)
const RightCol = withClassName()(
  styled('div')(({ theme }) => ({
    display: 'none',
    justifyContent: 'center',
    width: '40%',
    paddingTop: rem(66),
    [theme.mq.md]: {
      display: 'flex'
    }
  }))
)
const Image = withClassName('')(
  styled('img')({
    width: rem(212),
    height: rem(128)
  })
)
const Header = withClassName()(
  styled('div')({
    fontSize: rem(16),
    color: '#1D1D1F'
  })
)
const Content = withClassName()(
  styled('div')({
    paddingTop: rem(20),
    paddingBottom: rem(20)
  })
)
const ContentHeader = withClassName()(
  styled('div')({
    fontWeight: 500,
    fontSize: rem(13),
    paddingBottom: rem(10)
  })
)

const WrapperCheckbox = withClassName('flex')(styled('div')({}))
const ColCheckbox = withClassName('flex')(styled('div')())
const CheckboxLabel = withClassName()(
  styled('div')({
    paddingLeft: rem(10),
    paddingRight: rem(26),
    fontSize: rem(14),
    cursor: 'pointer'
  })
)
const CheckboxUncheck = withClassName('flex-no-shrink cursor-pointer')(
  styled('span')(
    {
      width: rem(20),
      height: rem(20),
      cursor: 'pointer'
    },
    {
      content: `url(${require('./assets/uncheck-outline.svg')})`
    }
  )
)
const CheckboxChecked = withClassName('flex-no-shrink cursor-pointer')(
  styled('span')(
    {
      width: rem(20),
      height: rem(20),
      cursor: 'pointer'
    },
    {
      content: `url(${require('./assets/checked-outline.svg')})`
    }
  )
)
const WrapperCard = withClassName()(
  styled('div')({
    marginBottom: rem(25),
    background: '#FFFFFF',
    // border: 1px solid #EFF0F2;
    boxSizing: 'border-box',
    boxShadow: `${rem(0)} ${rem(4)} ${rem(4)} rgba(0, 0, 0, 0.25)`,
    borderRadius: rem(5)
  })
)
const CardHeader = withClassName('flex')(
  styled('div')({
    height: rem(40),
    border: `${rem(1)} solid #EFF0F2`,
    borderRadius: rem(5),
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: rem(10),
    paddingRight: rem(10)
  })
)
const CardHeaderLabel = withClassName()(
  styled('div')({
    fontSize: rem(13),
    fontWeight: 500,
    color: '#1D1D1F'
  })
)
const CardHeaderIcon = withClassName()(styled('div')({}))
const CardContent = withClassName('flex flex-wrap')(
  styled('div')({
    paddingTop: rem(16),
    paddingLeft: rem(10),
    paddingRight: rem(10),
    marginTop: rem(25)
  })
)
const CardContentLabel = withClassName('w-full')(
  styled('div')({
    fontSize: rem(13),
    fontWeight: 500,
    color: '#1D1D1F',
    paddingBottom: rem(16)
  })
)
const CardContentLeft = withClassName()(
  styled('div')(({ theme }) => ({
    width: '100%',
    [theme.mq.md]: {
      width: '50%',
      paddingRight: rem(10)
    }
  }))
)
const CardContentRight = withClassName()(
  styled('div')(({ theme }) => ({
    width: '100%',
    [theme.mq.md]: {
      paddingLeft: rem(10),
      width: '50%'
    }
  }))
)
const ButtonAdditional = withClassName('flex align-center')(
  styled('div')({
    marginTop: rem(25),
    cursor: 'pointer'
  })
)
const ButtonAdditionalLabel = withClassName()(
  styled('div')({
    fontWeight: 500,
    fontSize: rem(13),
    paddingLeft: rem(5)
  })
)
const IconRemove = withClassName()(
  styled('img')({
    cursor: 'pointer'
  })
)
const WrapperCheckboxOutCard = withClassName('flex flex-wrap')(
  styled('div')({
    marginBottom: rem(25)
  })
)
const ColCheckboxOutCard = withClassName('flex')(
  styled('div')(({ theme }) => ({
    width: '100%',
    // marginBottom: rem(25),
    [theme.mq.md]: {
      width: 'auto'
    }
  }))
)
const ColCheckboxOutCardLeft = withClassName('flex')(
  styled('div')(({ theme }) => ({
    width: '100%',
    marginBottom: rem(25),
    [theme.mq.md]: {
      width: 'auto',
      marginBottom: rem(0)
    }
  }))
)
const ElWarning = withClassName()(
  styled('div')({
    width: '100%',
    color: '#ED1C24'
  })
)

export {
  Container,
  LeftCol,
  RightCol,
  Header,
  Image,
  Content,
  ContentHeader,
  WrapperCheckbox,
  ColCheckbox,
  CheckboxLabel,
  CheckboxUncheck,
  CheckboxChecked,
  WrapperCard,
  CardHeader,
  CardContent,
  CardHeaderLabel,
  CardHeaderIcon,
  CardContentLabel,
  CardContentLeft,
  CardContentRight,
  ButtonAdditional,
  ButtonAdditionalLabel,
  IconRemove,
  WrapperCheckboxOutCard,
  ColCheckboxOutCard,
  ElWarning,
  ColCheckboxOutCardLeft
}
