import { pathOr } from 'ramda'
import { parseIntMoney } from '../../../../../utils/number.util'
import { showMonthlyInstallment } from './LoanInfo.view'

const validateMonthlyInstallment = (loanType: string) => {
  return (
    loanType === 'UNSECURED_OVERDRAFT' ||
    loanType === 'SECURED_OVERDRAFT' ||
    loanType === 'BUSINESS_HOUSEHOLD_LOAN_CREDIT_LIMIT'
  )
}

export default {
  changeHasOtherLoan: (props: any) => (params: string) => {
    props.setFieldValue('hasOtherLoan', params)
    if (params === 'N') {
      props.setFieldValue('loans', [])
      props.setTouched({})

      return
    }
    props.setFieldValue('loans', [
      {
        relationshipWithPrimaryApplicant: 'SELF',
        loanType: '',
        creditLimit: '0',
        monthlyInstallment: '0'
      }
    ])
  },
  ADD_LOAN: (props: any) => () => {
    const loansPrev = pathOr([], ['values', 'loans'], props)
    const loans = JSON.parse(JSON.stringify(loansPrev))

    loans.push({
      relationshipWithPrimaryApplicant: 'SELF',
      loanType: '',
      creditLimit: '0',
      monthlyInstallment: '0'
    })
    props.setFieldValue('loans', loans)
  },
  REMOVE_LOAN: (props: any) => (index: number) => {
    const loansPrev = pathOr([], ['values', 'loans'], props)
    const loans = JSON.parse(JSON.stringify(loansPrev))

    loans.splice(index, 1)
    const touched = JSON.parse(JSON.stringify(pathOr({}, ['touched'], props)))

    if (touched.loans && touched.loans[index]) {
      // touched.loans.splice(index, 1)
      // props.setTouched('loans', touched.loans)
      props.setFieldTouched(`loans[${index}].loanType`, false)
      props.setFieldTouched(`loans[${index}].creditLimit`, false)
      props.setFieldTouched(`loans[${index}].monthlyInstallment`, false)
    }
    props.setFieldValue('loans', loans)
  },
  SET_OWN_LOAN: (props: any) => (index: number, params: string) => {
    const loansPrev = pathOr([], ['values', 'loans'], props)
    const loans = JSON.parse(JSON.stringify(loansPrev))

    loans[index].relationshipWithPrimaryApplicant = params
    props.setFieldValue('loans', loans)
  },
  validateInput: (props: any) => () => {
    const loans = pathOr([], ['values', 'loans'], props)
    let flag = true
    const hasOtherLoan = pathOr('', ['values', 'hasOtherLoan'], props)

    if (hasOtherLoan === '') {
      return false
    }

    loans.map((item: any, index: number) => {
      const loanType = pathOr('', ['loanType'], item)

      if (loanType === '') {
        flag = false

        return
      }
      if (validateMonthlyInstallment(loanType)) {
        if (
          item.monthlyInstallment === '' ||
          parseIntMoney(item.monthlyInstallment) === 0
        ) {
          flag = false

          return
        }
      } else if (
        item.creditLimit === '' ||
        parseIntMoney(item.creditLimit) === 0
      ) {
        flag = false

        return
      }
    })

    return flag
  },
  savePreCreditInfo: (props: any) => async () => {
    const creditInformation = pathOr({}, ['creditInformation'], props)
    const hasOtherLoan = pathOr('', ['values', 'hasOtherLoan'], props)
    const preCreditInformationPrev = pathOr(
      {},
      ['preCreditInformation', 'creditInformation'],
      creditInformation
    )

    let loanPrev = pathOr([], ['loans'], preCreditInformationPrev)

    loanPrev = loanPrev.filter((item: any) => item.loanType === 'CREDIT_CARD')
    const loans = pathOr([], ['values', 'loans'], props)

    loans.map((item: any, index: number) => {
      !showMonthlyInstallment(loans[index].loanType)
        ? ((loans[index].creditLimit = null),
          (loans[index].monthlyInstallment = parseIntMoney(
            item.monthlyInstallment
          )))
        : ((loans[index].monthlyInstallment = null),
          (loans[index].creditLimit = parseIntMoney(item.creditLimit)))
    })

    const preCreditInformation = JSON.parse(
      JSON.stringify(preCreditInformationPrev)
    )

    preCreditInformation.hasOtherLoan = hasOtherLoan
    preCreditInformation.loans = [...loanPrev, ...loans]

    const input = {
      preCreditInformation: {
        creditInformation: {
          hasCreditCard: preCreditInformation.hasCreditCard,
          hasOtherLoan: preCreditInformation.hasOtherLoan,
          loans: preCreditInformation.loans
        }
      },
      isBScore: creditInformation.isBScore,
      leadMapId: creditInformation.leadMapId,
      preApprovedId: creditInformation.preApprovedId
    }

    await props.saveCreditInformation({ variables: { input } })
  }
}
