import withLifecycle from '@hocs/with-lifecycle'
import { withFormik } from 'formik'
import { pathOr } from 'ramda'
import { graphql } from 'react-apollo'
import { compose, withHandlers, withState } from 'recompose'
import { withApp } from '../../../../../hocs'

import updateStatus from '../../../Mutation/updateStatus.mutation'
import { STEP } from '../../../PreApprovedApplication.type'
import saveCreditInformation from '../../Mutation/saveCreditInformation'
import getCreditInformation from '../../Query/getCreditInformation'
import { TYPE_OF_INFO } from '../../Step3.const'
import { mapPropsToValues, validate } from './LoanInfo.config'
import handlers from './LoanInfo.handler'
import LoanInfoView, { EnhancedProps } from './LoanInfo.view'
import caculateLimit from './Mutation/caculateLimit.mutation'
const config = {
  mapPropsToValues,
  validate,
  handleSubmit: () => () => {
    //
  },
  enableReinitialize: true
}

const enhancer = compose<EnhancedProps, any>(
  graphql(getCreditInformation.query, getCreditInformation.params),
  graphql(caculateLimit.mutation, caculateLimit.params),
  graphql(saveCreditInformation.mutation, saveCreditInformation.params),
  graphql(updateStatus.mutation, updateStatus.params),
  withState('showModal', 'setShowModal', false),
  withState('validateValues', 'setValidateValues', false),
  withApp({ updates: ['updateStatus', 'caculateLimit'] }),
  withFormik(config),
  withHandlers(handlers),
  withLifecycle({
    onDidMount: async (props: any) => {
      const params = {
        step: STEP.step3,
        childStep: TYPE_OF_INFO.LOAN_INFO
      }
      const preApprovedId = pathOr('', ['preApprovedId'], props)
      const input = {
        preApprovedId,
        currentStep: JSON.stringify(params)
      }

      props.saveCurrentStep({ variables: { input } })
    }
  })
)

export default enhancer(LoanInfoView)
