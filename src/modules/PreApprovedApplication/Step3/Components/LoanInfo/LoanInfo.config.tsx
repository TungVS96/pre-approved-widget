import { required } from 'formik-validators'
import { compose, filter, identity, isEmpty, not, pathOr, pickBy } from 'ramda'
import { isNilOrEmpty } from 'ramda-adjunct'
import { validator } from '../../../../../forms'
import { t } from '../../../../../i18n'
import { parseIntMoney } from '../../../../../utils/number.util'
import { showMonthlyInstallment, IValues } from './LoanInfo.view'
const isNotEmptyObject = compose(
  not,
  isEmpty
)
const validateValue = (errorKey: string) => ({ value }: { value: string }) => {
  if (isNilOrEmpty(value)) return
  // const newValue = normalizePhone(value)
  const isValid = parseIntMoney(value) > 0
  if (!isValid) {
    return t(errorKey)
  }
}
const mapPropsToValues = (props: any) => {
  const preCreditInformation = pathOr(
    {},
    ['creditInformation', 'preCreditInformation', 'creditInformation'],
    props
  )

  const loans = pathOr([], ['loans'], preCreditInformation).filter(
    (item: any) => item.loanType !== 'CREDIT_CARD'
  )

  return {
    loans,
    hasOtherLoan: pathOr('', ['hasOtherLoan'], preCreditInformation)
  }
}

const monthlyInstallmentValidate = (loanType: string) => {
  return !showMonthlyInstallment(loanType)
    ? [
        required('preApproved.step2.loanAmount.required'),
        validateValue('preApproved.step2.loanAmount.required')
      ]
    : []
}

const creditLimitValidate = (loanType: string) => {
  return showMonthlyInstallment(loanType)
    ? [
        required('preApproved.step2.loanAmount.required'),
        validateValue('preApproved.step2.loanAmount.required')
      ]
    : []
}

const validate = (values: IValues, props: any) => {
  const loans = pathOr([], ['loans'], values)
  const hasOtherLoan = pathOr('', ['hasOtherLoan'], values)
  const loansErrors =
    hasOtherLoan === 'Y'
      ? loans.map((item: any) => {
          const errors = validator({
            loanType: [
              required('applicationOnlineFrom.step4.loanType.required')
            ],
            creditLimit: creditLimitValidate(item.loanType),
            monthlyInstallment: monthlyInstallmentValidate(item.loanType)
          })(item, props)

          return pickBy(identity, errors)
        })
      : []

  if (filter(isNotEmptyObject, loansErrors).length > 0) {
    return {
      loans: loansErrors
    }
  }

  return {}
}

export { mapPropsToValues, validate }
