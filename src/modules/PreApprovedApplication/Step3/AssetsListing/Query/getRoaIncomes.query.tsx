import gql from 'graphql-tag'
import { pathOr } from 'ramda'

const query = gql`
  query($preApprovedId: String) {
    roaIncomes(preApprovedId: $preApprovedId)
      @rest(
        type: "pre_approved"
        path: "/approved/v1/pre-roa-income/{args.preApprovedId}"
        endpoint: "dma-pre-credit"
      ) {
      assetType
      accountedForAmount
      hasRevenuesFromLiq
      acceptedDeclareAmount
    }
  }
`

const params = {
  props: ({ data }: any) => {
    const roaIncomes = pathOr([], ['roaIncomes'], data)

    return {
      data,
      roaIncomes: roaIncomes.length === undefined ? [] : roaIncomes
    }
  },
  options: (props: any) => {
    const preApprovedId = pathOr('', ['preApprovedId'], props)

    return {
      variables: {
        preApprovedId
      }
    }
  }
}

export default { query, params }
