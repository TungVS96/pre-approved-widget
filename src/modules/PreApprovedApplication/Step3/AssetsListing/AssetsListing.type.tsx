import { InjectedFormikProps } from 'formik'
import { History } from 'history'
import { PopupProps } from '../../../../hocs'

enum assetType {
  SAVING = 'Saving',
  RED_BOOK = 'Red_book',
  TCB_BONDS = 'TCB_bonds',
  ESTATE = 'Estate'
}

interface IFormValue {
  accountedForAmount: string
  hasRevenuesFromLiq: string
  acceptedDeclareAmount: string
  assetType: assetType
}

interface IValues {
  saving: IFormValue
  tcbonds: IFormValue
  // Nhà đất
  estate: IFormValue
  apartment: IFormValue
}

interface IProps extends PopupProps {
  history: History
  ON_SUBMIT(values: IValues): () => void
  ON_BACK(): void

  saveRoaIncomes(payload: any): void
  caculateLimit(payload: any): void
  showMessage(): void
  roaIncomes: any
}

interface IOutterProps {
  goToApprovedResult(): void
}

type EnhancedProps = InjectedFormikProps<IProps, IValues> & IOutterProps

export { IFormValue, IValues, EnhancedProps, IOutterProps, assetType }
