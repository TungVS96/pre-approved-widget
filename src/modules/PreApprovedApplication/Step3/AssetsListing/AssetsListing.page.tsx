import { withFormik } from 'formik'
import { graphql } from 'react-apollo'
import { compose, withHandlers } from 'recompose'
import { withApp, withPopup } from '../../../../hocs'
import { mapPropsToValues, validate } from './AssetsListing.config'
import handlers from './AssetsListing.handler'
import AssetsListing from './AssetsListing.view'

import caculateLimit from '../Components/LoanInfo/Mutation/caculateLimit.mutation'
import { EnhancedProps, IOutterProps } from './AssetsListing.type'
import saveRoaIncomes from './Mutation/saveRoaIncomes.mutation'
import getRoaIncomes from './Query/getRoaIncomes.query'

const formikConfig = {
  mapPropsToValues,
  validate,
  handleSubmit: async () => {
    //
  },
  enableReinitialize: true
}

export default compose<EnhancedProps, IOutterProps>(
  withPopup,
  graphql(getRoaIncomes.query, getRoaIncomes.params),
  graphql(saveRoaIncomes.mutation, saveRoaIncomes.params),
  graphql(caculateLimit.mutation, caculateLimit.params),
  withApp({ updates: ['saveRoaIncomes', 'caculateLimit'] }),
  withFormik(formikConfig),
  withHandlers(handlers)
)(AssetsListing)
