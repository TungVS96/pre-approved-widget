enum ASSET_TYPE {
  SAVING = 'Saving',
  TCB_BONDS = 'TCB_Bonds',
  RED_BOOK = 'Red_book',
  ESTATE = 'Estate'
}

const assetsListingInfo = [
  {
    icon: require('./assets/ic-saving.svg'),
    title: 'Sổ tiết kiệm',
    assetType: ASSET_TYPE.SAVING,
    pathPrefix: 'saving',
    accountedForAmountLabel:
      'PreApprovedApplication.step3.AssetForm.saving.accountedForAmount.label',
    hasRevenuesFromLiqLabel:
      'PreApprovedApplication.step3.AssetForm.saving.hasRevenuesFromLiqLabel.label',
    acceptedDeclareAmountLabel:
      'PreApprovedApplication.step3.AssetForm.saving.acceptedDeclareAmountLabel.label'
  },
  {
    icon: require('./assets/ic-tcbonds.svg'),
    title: 'Trái phiếu',
    assetType: ASSET_TYPE.TCB_BONDS,
    pathPrefix: 'tcbonds',
    accountedForAmountLabel:
      'PreApprovedApplication.step3.AssetForm.tcbonds.accountedForAmount.label',
    hasRevenuesFromLiqLabel:
      'PreApprovedApplication.step3.AssetForm.tcbonds.hasRevenuesFromLiqLabel.label',
    acceptedDeclareAmountLabel:
      'PreApprovedApplication.step3.AssetForm.tcbonds.acceptedDeclareAmountLabel.label'
  },
  {
    icon: require('./assets/ic-house.svg'),
    title: 'Nhà đất',
    assetType: ASSET_TYPE.RED_BOOK,
    pathPrefix: 'estate',
    accountedForAmountLabel:
      'PreApprovedApplication.step3.AssetForm.estate.accountedForAmount.label',
    hasRevenuesFromLiqLabel:
      'PreApprovedApplication.step3.AssetForm.estate.hasRevenuesFromLiqLabel.label',
    acceptedDeclareAmountLabel:
      'PreApprovedApplication.step3.AssetForm.estate.acceptedDeclareAmountLabel.label'
  },
  {
    icon: require('./assets/ic-apartment.svg'),
    title: 'Chung cư',
    assetType: ASSET_TYPE.ESTATE,
    pathPrefix: 'apartment',
    accountedForAmountLabel:
      'PreApprovedApplication.step3.AssetForm.apartment.accountedForAmount.label',
    hasRevenuesFromLiqLabel:
      'PreApprovedApplication.step3.AssetForm.apartment.hasRevenuesFromLiqLabel.label',
    acceptedDeclareAmountLabel:
      'PreApprovedApplication.step3.AssetForm.apartment.acceptedDeclareAmountLabel.label'
  }
]

export { ASSET_TYPE, assetsListingInfo }
