import { getIn } from 'formik'
import { equals } from 'ramda'
import React, { FunctionComponent } from 'react'
import { compose } from 'recompose'
import { RadioInput } from '../../../../../forms'
import { renderTextInputSuggestMoney } from '../../../../../forms/form'
import { t } from '../../../../../i18n'
import {
  formatCurrency,
  getFieldProps,
  getPositiveNumberOnly
} from '../../../../../utils'
import { EnhancedProps } from '../AssetsListing.type'

interface IOutterProps {
  accountedForAmountLabel?: string
  hasRevenuesFromLiqLabel?: string
  acceptedDeclareAmountLabel?: string
  pathPrefix: string
}

const AssetForm: FunctionComponent<EnhancedProps & IOutterProps> = (props) => {
  const {
    accountedForAmountLabel = '',
    hasRevenuesFromLiqLabel = '',
    acceptedDeclareAmountLabel = '',
    pathPrefix = ''
  } = props
  const hasRevenuesFromLiq = equals(
    getIn(props.values, `${pathPrefix}.hasRevenuesFromLiq`),
    'Y'
  )

  return (
    <>
      <div className={'mt-4'} />
      {renderTextInputSuggestMoney({
        props,
        name: `${pathPrefix}.accountedForAmount`,
        label: accountedForAmountLabel,
        placeholder:
          'PreApprovedApplication.step3.salary.provableStableIncome.placeholder',
        disabled: false,
        transformValue: (value) => getPositiveNumberOnly(value),
        format: (value) => formatCurrency({ value }),
        suggestMoney: true,
        times: 1000000,
        autoComplete: 'off',
        requiredField: false
      })}
      <RadioInput
        {...getFieldProps(`${pathPrefix}.hasRevenuesFromLiq`, props)}
        options={[
          {
            name: t('applicationOnlineFrom.step3.isOwner.option.yes'),
            value: 'Y'
          },
          {
            name: t('applicationOnlineFrom.step3.isOwner.option.no'),
            value: 'N'
          }
        ]}
        vertical={false}
        label={t(hasRevenuesFromLiqLabel)}
      />
      {hasRevenuesFromLiq &&
        renderTextInputSuggestMoney({
          props,
          name: `${pathPrefix}.acceptedDeclareAmount`,
          label: acceptedDeclareAmountLabel,
          placeholder:
            'PreApprovedApplication.step3.salary.provableStableIncome.placeholder',
          disabled: false,
          requiredField: false,
          transformValue: (value) => getPositiveNumberOnly(value),
          format: (value) => formatCurrency({ value }),
          suggestMoney: true,
          times: 1000000,
          autoComplete: 'off'
        })}
    </>
  )
}

export default compose<EnhancedProps, IOutterProps>()(AssetForm)
