import * as R from 'ramda'
import { toStr } from '../../../../utils'
import { EnhancedProps, IFormValue, IValues } from './AssetsListing.type'

export default {
  ON_BACK: (props: EnhancedProps) => () => {
    props.setPopup('backPopup')
  },
  ON_SUBMIT: (props: EnhancedProps) => (values: IValues) => async () => {
    const sumAccountedForAmount = R.compose(
      R.sum,
      R.map((item: IFormValue) => parseInt(item.accountedForAmount, 10) || 0),
      R.values
    )(values)

    if (sumAccountedForAmount === 0) {
      return props.setPopup('noData')
    }

    try {
      const preApprovedId = R.pathOr('', ['preApprovedId'], props)
      const roaIncomes = R.values(values).filter(
        (income) => toStr(income.accountedForAmount) !== '0'
      )

      const input = {
        preApprovedId,
        roaIncomes
      }

      await props.saveRoaIncomes({ variables: { input } })

      const response = await props.caculateLimit({
        variables: { preApprovedId, input: {} }
      })

      if (
        R.pathOr(
          '',
          ['data', 'caculateLimit', 'statusCalculated'],
          response
        ) === '1'
      ) {
        return props.setPopup('ModalWarningCalculatorWish')
      }
      props.goToApprovedResult()
    } catch (e) {
      props.showMessage()
    }
  }
}
