import * as R from 'ramda'
import { validator } from '../../../../forms'
import { t } from '../../../../i18n'
import { toNumber } from '../../../../utils'
import { assetType, EnhancedProps, IFormValue } from './AssetsListing.type'

const formValueInitial = {
  accountedForAmount: 0,
  hasRevenuesFromLiq: '',
  acceptedDeclareAmount: 0
}

const mapPropsToValues = ({ roaIncomes = [] }: EnhancedProps) => {
  const incomes = R.reduce(
    (acc, item: IFormValue) => {
      if (item.assetType === assetType.ESTATE) {
        return {
          ...acc,
          apartment: item
        }
      }
      if (item.assetType === assetType.RED_BOOK) {
        return {
          ...acc,
          estate: item
        }
      }
      if (item.assetType === assetType.TCB_BONDS) {
        return {
          ...acc,
          tcbonds: item
        }
      }

      return { ...acc, saving: item }
    },
    {},
    roaIncomes
  )

  return {
    saving: { ...formValueInitial, assetType: assetType.SAVING },
    tcbonds: { ...formValueInitial, assetType: assetType.TCB_BONDS },
    estate: { ...formValueInitial, assetType: assetType.RED_BOOK },
    apartment: { ...formValueInitial, assetType: assetType.ESTATE },
    ...incomes
  }
}

const acceptedDeclareAmountValidate = (errorKey: string, max: number) => ({
  value
}: {
  value: string
}) => {
  if (toNumber(value) <= max) {
    return
  }

  return t(errorKey)
}

const validate = (values: any, props: EnhancedProps) => {
  const errors = {
    saving: validator({
      acceptedDeclareAmount: [
        acceptedDeclareAmountValidate(
          'preApproved.step3.AssetsListing.acceptedDeclareAmount.saving',
          values.saving.accountedForAmount
        )
      ]
    })(values.saving, props),
    tcbonds: validator({
      acceptedDeclareAmount: [
        acceptedDeclareAmountValidate(
          'preApproved.step3.AssetsListing.acceptedDeclareAmount.tcbonds',
          values.tcbonds.accountedForAmount
        )
      ]
    })(values.tcbonds, props),
    estate: validator({
      acceptedDeclareAmount: [
        acceptedDeclareAmountValidate(
          'preApproved.step3.AssetsListing.acceptedDeclareAmount.estate',
          values.estate.accountedForAmount
        )
      ]
    })(values.estate, props),
    apartment: validator({
      acceptedDeclareAmount: [
        acceptedDeclareAmountValidate(
          'preApproved.step3.AssetsListing.acceptedDeclareAmount.apartment',
          values.apartment.accountedForAmount
        )
      ]
    })(values.apartment, props)
  }

  return {
    ...errors
  }
}

export { mapPropsToValues, validate }
