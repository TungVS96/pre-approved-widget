import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const SectionContainer = withClassName('w-full', 'SectionContainer')(
  styled('div')({
    fontSize: rem(14),
    borderRadius: rem(5),
    boxShadow: `0px 4px 12px rgba(0, 0, 0, 0.12)`,
    backgroundColor: '#FAF8F8',
    padding: `${rem(8)} ${rem(16)}`
  })
)

const SectionTitle = withClassName(
  'flex items-center cursor-pointer select-none justify-between'
)(
  styled('div')(() => ({
    padding: `${rem(8)} 0`
  }))
)

const ArrowIcon = withClassName('')(
  styled('img')(() => ({
    width: rem(16),
    transition: 'transform 0.2s ease-in-out'
  }))
)

const TitleWrapper = withClassName('flex flex-row justify-between')(
  styled('div')({
    alignItems: 'center'
  })
)

const Title = withClassName('')(
  styled('p')({ margin: `0 ${rem(8)}`, fontSize: 14, lineHeight: rem(21) })
)

const Icon = withClassName('')(
  styled('img')({
    margin: `0 ${rem(8)}`,
    cursor: 'pointer'
  })
)

export { SectionContainer, SectionTitle, ArrowIcon, TitleWrapper, Title, Icon }
