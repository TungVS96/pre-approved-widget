import { Collapse } from '@blueprintjs/core'
import React, { FunctionComponent } from 'react'
import { compose, withStateHandlers, StateHandlerMap } from 'recompose'
import {
  ArrowIcon,
  Icon,
  SectionContainer,
  SectionTitle,
  Title,
  TitleWrapper
} from './Collapse.component.style'

interface IProps {
  initialShowed?: boolean
  title: string
  icon: any
}

interface IStateProps {
  showed: boolean
}

type IStateHandlerProps = StateHandlerMap<IStateProps> & {
  setShow(e: React.MouseEvent<HTMLDivElement>): void
}

type EnhancedProps = IStateProps & IStateHandlerProps & IProps

const arrow = require('./assets/ic-arrow.svg')
const Section: FunctionComponent<EnhancedProps> = (props) => {
  const { children, title = '', showed, setShow, icon } = props

  return (
    <SectionContainer>
      <SectionTitle onClick={setShow}>
        <TitleWrapper>
          <Icon src={icon} width={44} height={44} alt={'  '} />
          <Title>{title}</Title>
        </TitleWrapper>
        <ArrowIcon
          src={arrow}
          style={{
            transform: showed ? 'rotate(180deg)' : ''
          }}
        />
      </SectionTitle>
      <Collapse
        isOpen={showed}
        transitionDuration={200}
        keepChildrenMounted={true}
      >
        {children}
      </Collapse>
    </SectionContainer>
  )
}

const enhancer = compose<EnhancedProps, IProps>(
  withStateHandlers<IStateProps, IStateHandlerProps, IProps>(
    (props) => ({ showed: props.initialShowed || false }),
    {
      setShow: (state: IStateProps) => () => {
        return {
          showed: !state.showed
        }
      }
    }
  )
)

export { Section }
export default enhancer(Section)
