import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    saveRoaIncomes(input: $input)
      @rest(
        type: "Incomes"
        endpoint: "dma-pre-credit"
        path: "/approved/v1/save-roa-income"
        method: "post"
      ) {
      status
      preApprovedId
      errorCode
      errorName
    }
  }
`
const params = {
  name: 'saveRoaIncomes'
}

export default {
  mutation,
  params
}
