import React, { FunctionComponent } from 'react'
import { t } from '../../../../i18n'
import BottomBarButtons from '../../BottomBarButtons/BottomBarButtons.component'
// import ModalExitComponent from '../../Components/ModalExit/ModalExit.component'
import ConfirmPopup from '../../ConfirmPopup/ConfirmPopup.view'
import { Title } from '../../PreApprovedApplication.style'
import ModalWarningCalculatorWish from '../ModalWarningCalculatorWish/ModalWarningCalculatorWish.component'
import AssetForm from './AssetForm/AssetForm.form'
import { assetsListingInfo } from './AssetsListing.constant'
import { ContentWrapper, ItemWrapper } from './AssetsListing.style'
import { EnhancedProps } from './AssetsListing.type'
import Collapse from './Collapse/Collapse.component'

const AssetsListing: FunctionComponent<EnhancedProps> = (props) => {
  const { ON_BACK, ON_SUBMIT, values, setPopup, currentPopup } = props

  return (
    <>
      <Title>{t('PreApprovedApplication.step3.AssetForm.title')}</Title>
      <ContentWrapper>
        {assetsListingInfo.map((item, index) => {
          return (
            <ItemWrapper key={index.toString()}>
              <Collapse title={item.title} icon={item.icon}>
                <AssetForm
                  {...props}
                  pathPrefix={item.pathPrefix}
                  accountedForAmountLabel={item.accountedForAmountLabel}
                  hasRevenuesFromLiqLabel={item.hasRevenuesFromLiqLabel}
                  acceptedDeclareAmountLabel={item.acceptedDeclareAmountLabel}
                />
              </Collapse>
            </ItemWrapper>
          )
        })}
        <div className={'mt-4 w-full'} />
        <ItemWrapper>
          <BottomBarButtons
            onPrev={ON_BACK}
            nextTitle="landingPage.calculator.button.calculate"
            onNext={ON_SUBMIT(values)}
          />
        </ItemWrapper>
      </ContentWrapper>
      <ConfirmPopup
        title={'Quý khách có chắc không?'}
        content={
          'Những thông tin quý khách vừa nhập sẽ không được lưu lại. Quý khách có muốn tiếp tục?'
        }
        handleNo={() => setPopup('')}
        handleYes={() => {
          props.goToApprovedResult()
        }}
        handleClose={() => setPopup('')}
        isOpen={currentPopup === 'backPopup'}
      />
      <ConfirmPopup
        title={'Vui lòng nhập giá trị của ít nhất 1 tài sản'}
        handleClose={() => setPopup('')}
        isOpen={currentPopup === 'noData'}
      />
      <ModalWarningCalculatorWish
        isOpen={props.currentPopup === 'ModalWarningCalculatorWish'}
        onClose={() => {
          props.setPopup('')
        }}
      />
    </>
  )
}

export default AssetsListing
