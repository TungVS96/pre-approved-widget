import * as R from 'ramda'
import { EnhancedProps } from './Step3.type'

export default {
  ON_BACK: (props: EnhancedProps) => async () => {
    props.goBack()
  },
  ON_NEXT: (props: EnhancedProps) => (params: string, preIncomes: any) => {
    const hasIncomes = !R.isEmpty(
      R.values(preIncomes).filter((incomesItem) => !R.isEmpty(incomesItem))
    )

    if (!hasIncomes && props.typeOfInfo === '') {
      return props.setPopup('next')
    }

    return props.setTypeOfInfo(params)
  }
}
