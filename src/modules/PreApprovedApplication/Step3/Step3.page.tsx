import withLifecycle from '@hocs/with-lifecycle'
import { pathOr } from 'ramda'
import { graphql, withApollo } from 'react-apollo'
import { compose, withHandlers, withState } from 'recompose'
import { withPopup } from '../../../hocs'
import {
  EnhancedProps as PropsPreAprroved,
  STEP
} from '../PreApprovedApplication.type'
import getPerson from '../Step1/Query/getPerson.query'
import getCreditInformation from './Query/getCreditInformation'
import { TYPE_OF_INFO } from './Step3.const'
import handlers from './Step3.handler'
import {
  EnhancedProps as PropsStep3,
  IHandlers,
  IOutterProps
} from './Step3.type'
import Step3 from './Step3.view'

type EnhancedProps = PropsStep3 & PropsPreAprroved

export default compose<any, IOutterProps>(
  withApollo,
  graphql(getCreditInformation.query, getCreditInformation.params),
  withState('showCreditInfo', 'setShowCreditInfo', false),
  graphql(getPerson.query, getPerson.params),
  withState('typeOfInfo', 'setTypeOfInfo', ''),
  withPopup,
  withHandlers<EnhancedProps, IHandlers>(handlers),
  withLifecycle({
    onDidMount: async (props: any) => {
      const params = {
        step: STEP.step3
      }
      const preApprovedId = pathOr('', ['preApprovedId'], props)
      const input = {
        preApprovedId,
        currentStep: JSON.stringify(params)
      }

      props.saveCurrentStep({ variables: { input } })
      const childStep = pathOr('', ['childStep'], props)

      if (
        childStep === TYPE_OF_INFO.CREDIT_INFO ||
        childStep === TYPE_OF_INFO.LOAN_INFO
      ) {
        props.setTypeOfInfo(childStep)
        props.setChildStep('')
      }
      if (props.infoType !== '') props.setTypeOfInfo(props.infoType)
    }
  })
)(Step3)
