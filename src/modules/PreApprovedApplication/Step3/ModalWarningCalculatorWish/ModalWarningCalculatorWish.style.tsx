import { rem, theme, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const ImageBlock = withClassName('justify-end flex w-full p-3')(
  styled('div')({
    height: rem(26)
  })
)
const ImageTagContain = withClassName(
  'flex bg-white rounded-full justify-center content-center'
)(
  styled('div')({
    height: 12,
    width: 12
  })
)
const Image = withClassName('')(styled('img')())

const ContentResult = withClassName('bg-white')(
  styled('div')({
    width: '90%',
    height: rem(250),
    overflowX: 'auto',
    borderRadius: rem(2),
    border: '1px solid #DCDEE2',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    [theme.mq.md]: {
      width: rem(373),
      height: rem(250)
    }
  })
)
const ImageBlockResult = withClassName('justify-center flex w-full')(
  styled('div')()
)
const ContentResultHeader = withClassName('text-center')(
  styled('div')({
    fontSize: rem(14),
    fontStyle: 'normal',
    fontWeight: 500,
    lineHeight: rem(20),
    textAlign: 'center',
    fontColor: '#17233D',
    fontFamily: 'Helvetica Neue',
    paddingLeft: rem(31),
    paddingRight: rem(31),
    paddingTop: rem(24),
    [theme.mq.md]: {
      paddingLeft: rem(32),
      paddingRight: rem(32)
    }
  })
)

export {
  ContentResult,
  ContentResultHeader,
  Image,
  ImageBlock,
  ImageBlockResult,
  ImageTagContain
}
