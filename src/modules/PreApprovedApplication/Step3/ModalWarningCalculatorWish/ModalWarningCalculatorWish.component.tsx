import { compose } from 'recompose'
import ModalWarningCalculatorWish, {
  IOutterProps
} from './ModalWarningCalculatorWish.view'

const enhancer = compose<IOutterProps, IOutterProps>()

export default enhancer(ModalWarningCalculatorWish)
