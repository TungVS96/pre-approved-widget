import { Overlay } from '@blueprintjs/core'
import React, { FunctionComponent } from 'react'
import {
  ContentResult,
  ContentResultHeader,
  Image,
  ImageBlock,
  ImageBlockResult,
  ImageTagContain
} from './ModalWarningCalculatorWish.style'

interface IOutterProps {
  isOpen: boolean
  onClose(): void
}

const Modal: FunctionComponent<IOutterProps> = (props) => {
  const { isOpen, onClose } = props

  return (
    <Overlay isOpen={isOpen} onClose={onClose}>
      <ContentResult>
        <ImageBlock>
          <ImageTagContain>
            <Image
              onClick={onClose}
              src={require('../../../../components/Modal/assets/ic-close.svg')}
              width={20}
              height={20}
            />
          </ImageTagContain>
        </ImageBlock>
        <ImageBlockResult>
          <Image
            width={212}
            height={83}
            src={require('./assets/warning.svg')}
          />
        </ImageBlockResult>
        <ContentResultHeader>
          {
            'Không có khoản vay phù hợp với thông tin bạn cung cấp. Chuyên viên của chúng tôi sẽ liên lạc với Quý khách trong thời gian sớm nhất để tư vấn cụ thể hơn.'
          }
        </ContentResultHeader>
      </ContentResult>
    </Overlay>
  )
}

export { IOutterProps }
export default Modal
