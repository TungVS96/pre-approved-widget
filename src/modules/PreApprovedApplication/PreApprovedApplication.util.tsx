import * as R from 'ramda'
import { USER_PROFILE_KEY } from '../../constants'
import { t } from '../../i18n'
import { IUserProfile } from '../../types'
import { formatCurrency, toNumber } from '../../utils'

const isMinValue = (
  errorKey: string,
  minValue: number,
  isFormatCurrency?: boolean
) => ({ value }: { value: string }) => {
  if (toNumber(value) < minValue) {
    if (isFormatCurrency) {
      return t(errorKey, {
        value: formatCurrency({ value: minValue, showCurrency: true })
      })
    }

    return t(errorKey, { value: minValue })
  }

  return
}

const valueInRange = (errorKey: string, min: number, max: number) => ({
  value
}: {
  value: string
}) => {
  if (toNumber(value) < min || toNumber(value) > max) {
    return t(errorKey, { min, max })
  }

  return
}

const logout = () => {
  localStorage.removeItem(USER_PROFILE_KEY)
}

const isLoggedOn = () => {
  const userProfile = localStorage.getItem(USER_PROFILE_KEY)

  return !R.isNil(userProfile)
}

const setUserProfile = (userProfile: IUserProfile) => {

  localStorage.setItem(USER_PROFILE_KEY, JSON.stringify(userProfile))
}

const getUserProfile = () => {

  const userProfileString = localStorage.getItem(USER_PROFILE_KEY)
  if (R.isNil(userProfileString)) {
    return null
  }
  const userProfileObject = JSON.parse(userProfileString)

  return {
    name: R.pathOr('', ['name'], userProfileObject),
    email: R.pathOr('', ['email'], userProfileObject),
    tokenId: R.pathOr('', ['tokenId'], userProfileObject),
  }
}

export { isMinValue, valueInRange, logout, isLoggedOn, setUserProfile, getUserProfile }
