import withLifecycle from '@hocs/with-lifecycle'
import { isNilOrEmpty } from 'ramda-adjunct'
import { graphql, withApollo } from 'react-apollo'
import { compose, withHandlers, withState } from 'recompose'
import { withApp } from '../../hocs'
import getDropDownListData from './Mutation/getDropDownListData.mutation'
import saveCurrentStep from './Mutation/saveCurrentStep.mutation'
import saveLoanPreference from './Mutation/saveLoanPreference.mutation'
import lifecycle from './PreApprovedApplication.lifecycle'
import { IHandlers, IProps, STEP } from './PreApprovedApplication.type'
import PreApprovedApplication from './PreApprovedApplication.view'
import getCurrentStep from './Query/getCurrentStep.query'
import { STEP1_TABNAME } from './Step1/Step1.constant'

export default compose(
  withApollo,
  withState('dropdownData', 'setDropdownData', {}),
  withState('currentStep', 'setStep', STEP.overview),
  withState('prevStep', 'setPrevStep', null),
  withState('childStep', 'setChildStep', ''),
  withState('infoType', 'setInfoType', ''),
  graphql(getCurrentStep.query, getCurrentStep.params),
  graphql(getDropDownListData.mutation, getDropDownListData.params),
  graphql(saveLoanPreference.mutation, saveLoanPreference.params),
  graphql(saveCurrentStep.mutation, saveCurrentStep.params),
  withApp({ notification: true }),
  withHandlers<IProps, IHandlers>({
    GO_TO_APPROVED_RESULT: ({ setStep }: IProps) => () => {
      setStep(STEP.step4)
    },
    ON_BACK: ({ currentStep, setStep, setPrevStep }: IProps) => () => {
      setStep(currentStep - 1)
      setPrevStep(currentStep)
    },
    /*eslint no-else-return: "error"*/
    GO_NEXT: (props: IProps) => async (id, step) => {

      return props.setStep(props.currentStep + 1)
      if (isNilOrEmpty(id)) {
        return props.setStep(props.currentStep + 1)
      }

      if (step === STEP.step1) {
        /* props.history.push({
          pathname: generatePath(Routes.PreApprovedApplication, {
            id
          }),
          state: {
            step,
            tab: STEP1_TABNAME.TAB_B
          }
        }) */
        const params1 = {
          step: STEP.step1,
          childStep: STEP1_TABNAME.TAB_B
        }
        const input1 = {
          preApprovedId: id,
          currentStep: JSON.stringify(params1)
        }

        await props.saveCurrentStep({ variables: { input: input1 } })
        // props.history.push({
        //   pathname: generatePath(Routes.PreApprovedApplication, {
        //     id
        //   })
        // })

        return
      }

      if (step !== STEP.step2) return

      const params = {
        step: STEP.step2
      }
      const input = {
        preApprovedId: id,
        currentStep: JSON.stringify(params)
      }

      await props.saveCurrentStep({ variables: { input } })
      // props.history.push({
      //   pathname: generatePath(Routes.PreApprovedApplication, {
      //     id
      //   })
      // })

      /* props.history.push({
        pathname: generatePath(Routes.PreApprovedApplication, {
          id
        }),
        state: { step }
      }) */
    }
  }),
  withLifecycle(lifecycle)
)(PreApprovedApplication)
