import { rem, theme, withClassName } from 'dma-ui'
import styled from 'react-emotion'
import { getFontFamily } from '../../../../utils/string.util'

const Card = withClassName(
  'flex flex-col relative bg-white items-center rounded-lg'
)(
  styled('div')(({ width = 361, height }: any) => ({
    height,
    fontFamily: getFontFamily(),
    borderRadius: rem(2),
    width: '95%',
    padding: `${rem(32)} ${rem(16)} ${rem(24)}`,
    overflowY: 'auto',
    margin: 'auto',
    marginTop: '60%',
    [theme.mq.md]: {
      width,
      height,
      marginTop: '15%'
    }
  }))
)

const ContentWrapper = withClassName('mt-4 mx-2 text-center font-normal')(
  styled('div')({
    fontSize: rem(14),
    lineHeight: rem(20)
  })
)

const ButtonDelete = withClassName('absolute cursor-pointer')(
  styled('div')({
    top: rem(16),
    right: rem(16)
  })
)

export { Card, ContentWrapper, ButtonDelete }
