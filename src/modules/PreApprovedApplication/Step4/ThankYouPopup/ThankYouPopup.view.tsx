import { Overlay } from '@blueprintjs/core'
import React from 'react'
import { compose } from 'recompose'
import { t } from '../../../../i18n'
import { ButtonDelete, Card, ContentWrapper } from './ThankYouPopup.style'

interface IOutterProps {
  isOpen: boolean
  handleClose(): void
}

const ThankYouPopup: React.FunctionComponent<IOutterProps> = (props) => {
  const { isOpen = false, handleClose } = props

  return (
    <Overlay
      onClose={handleClose}
      isOpen={isOpen}
      canEscapeKeyClose={true}
      canOutsideClickClose={true}
    >
      <Card>
        <ButtonDelete onClick={handleClose}>
          <img src={require('./../assets/ic-x.svg')} height={12} width={12} />
        </ButtonDelete>
        <img
          src={require('./../assets/thankyou.svg')}
          height={136}
          width={195}
        />
        <div className={'mx-1 mt-6 text-center text-base font-medium'}>
          {t('preApproved.step4.thank.popup.title')}
        </div>
        <ContentWrapper>
          {t('preApproved.step4.thank.popup.content')}
        </ContentWrapper>
      </Card>
    </Overlay>
  )
}

export default compose<IOutterProps, IOutterProps>()(ThankYouPopup)
