import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    acceptLimit(input: $input, preApprovedId: $preApprovedId)
      @rest(
        type: "acceptLimit"
        endpoint: "dma-pre-credit"
        path: "/approved/v1/accept-limit/{args.preApprovedId}"
        method: "post"
      ) {
      status
      errorCode
      errorName
    }
  }
`
const params = {
  name: 'acceptLimit'
}

export default {
  mutation,
  params
}
