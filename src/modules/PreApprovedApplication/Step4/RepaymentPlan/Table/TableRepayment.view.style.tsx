import { rem, theme } from 'dma-ui'
import { css } from 'emotion'

// popup-repayment

const tablePopupFull = css({
  width: '100%',
  height: '100%',
  [theme.mq.md]: {
    width: '100%',
    height: '100%'
  }
})

const tableHeadingBlockStyle = css({
  backgroundColor: '#e8eaec',
  width: `calc(100% - ${rem(8)})`,
  [theme.mq.md]: {
    width: `calc(100% - ${rem(8)})`
  }
})

const tableHeadingItemStyle = css({
  border: '1px solid',
  borderColor: theme.colors['grey-light'],
  color: '#17233d',
  textAlign: 'center',
  padding: `${rem(6)} ${rem(6)} ${rem(6)}}`,
  fontSize: rem(9),
  flex: 2,
  ':first-of-type': {
    flex: 1
  },
  [theme.mq.md]: {
    padding: `${rem(14)} ${rem(8)} ${rem(14)}`,
    fontSize: rem(14)
  }
})

const tableDataBlockStyle = css({
  backgroundColor: theme.colors['grey-lightest'],
  height: `calc(100% - ${rem(48)})`,
  // maxHeight: `${rem(514)}`,
  width: `calc(100% - ${rem(1.2)})`,
  overflowY: 'auto',
  '::-webkit-scrollbar': {
    width: '6px',
    backgroundColor: 'white'
  },
  '::-webkit-scrollbar-thumb': {
    borderRadius: '3px',
    backgroundColor: 'rgba(0,0,0,.5)',
    height: '100px'
  },
  [theme.mq.md]: {
    // maxHeight: `${rem(592)}`,
    overflowY: 'auto',
    width: `calc(100% - ${rem(0)})`,
    height: `calc(100% - ${rem(80)})`,
    '::-webkit-scrollbar': {
      width: '8px',
      backgroundColor: 'white'
    },
    '::-webkit-scrollbar-thumb': {
      borderRadius: '4px',
      backgroundColor: 'rgba(0,0,0,.5)',
      height: '80px'
    }
  }
})

const tableItemStyle = css({
  border: '1px solid',
  borderColor: theme.colors['grey-light'],
  padding: `${rem(6)} ${rem(6)} ${rem(6)}}`,
  wordBreak: 'break-word',
  fontSize: '9px',
  flex: 2,
  textAlign: 'center',
  ':first-of-type': {
    flex: 1,
    textAlign: 'center'
  },
  [theme.mq.md]: {
    border: '1px solid',
    borderColor: theme.colors['grey-light'],
    padding: `${rem(8)} ${rem(8)} ${rem(8)}`,
    wordBreak: 'break-word',
    fontSize: '16px',
    flex: 2,
    textAlign: 'center',
    ':first-of-type': {
      flex: 1,
      textAlign: 'center'
    }
  }
})

export {
  tablePopupFull,
  tableHeadingBlockStyle,
  tableHeadingItemStyle,
  tableDataBlockStyle,
  tableItemStyle
}
