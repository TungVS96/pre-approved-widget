import React, { SFC } from 'react'

import { t } from '../../../../../i18n'
import { formatCurrency } from '../../../../../utils'
import {
  tableDataBlockStyle,
  tableHeadingBlockStyle,
  tableHeadingItemStyle,
  tableItemStyle,
  tablePopupFull
} from './TableRepayment.view.style'

interface IProps {
  tableData: IPaymentDetailRow[]
}

interface IPaymentDetailRow {
  No: number
  X: number
  Y: number
  Z: number
  W: number
}

const PaymentDetailTableRepayment: SFC<IProps> = ({ tableData }) => (
  <div className={tablePopupFull}>
    <div className={`flex flex-row w-full ${tableHeadingBlockStyle}`}>
      <p className={tableHeadingItemStyle}>
        {t('preApproved.receiveResult.table.column1')}
      </p>
      <p className={tableHeadingItemStyle}>
        {t('preApproved.receiveResult.table.column2')}
      </p>
      <p className={tableHeadingItemStyle}>
        {t('preApproved.receiveResult.table.column3')}
      </p>
      <p className={tableHeadingItemStyle}>
        {t('preApproved.receiveResult.table.column4')}
      </p>
      <p className={tableHeadingItemStyle}>
        {t('preApproved.receiveResult.table.column5')}
      </p>
    </div>
    {tableData !== undefined && tableData.length > 0 && (
      <div className={`${tableDataBlockStyle}`}>
        {tableData.map((item, index) => (
          <div key={index} className={`flex flex-row`}>
            <div className={tableItemStyle}>{item.No}</div>
            <div className={tableItemStyle}>
              {formatCurrency({
                value: Math.round(item.X),
                showCurrency: false
              })}
            </div>
            <div className={tableItemStyle}>
              {formatCurrency({
                value: item.Y,
                showCurrency: false
              })}
            </div>
            <div className={tableItemStyle}>
              {formatCurrency({
                value: item.Z,
                showCurrency: false
              })}
            </div>
            <div className={tableItemStyle}>
              {formatCurrency({
                value: item.W,
                showCurrency: false
              })}
            </div>
          </div>
        ))}
      </div>
    )}
  </div>
)

export default PaymentDetailTableRepayment

export { IPaymentDetailRow }
