import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const Container = withClassName(
  'flex flex-col w-full h-full justify-end items-center'
)(styled('div')())

const Content = withClassName('bg-white')(
  styled('div')({
    width: '100%',
    height: '70%',
    overflowX: 'auto'
  })
)
const ImageBlock = withClassName('justify-end flex w-full p-3')(styled('div')())
const ImageTagContain = withClassName(
  'flex bg-white rounded-full justify-center content-center items-center'
)(
  styled('div')({
    height: 40,
    width: 40
  })
)
const Image = withClassName('')(
  styled('img')({
    width: rem(12),
    height: rem(12)
  })
)
export {
  Container,
  Content,
  Image,
  ImageBlock,
  ImageTagContain
}
