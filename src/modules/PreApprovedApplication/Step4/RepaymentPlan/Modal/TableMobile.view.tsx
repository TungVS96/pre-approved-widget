import React, { SFC } from 'react'

import { t } from '../../../../../i18n'
import { formatCurrency } from '../../../../../utils'
import {
  rowItem,
  tableContainer,
  tableContainerFit,
  tableDataBlockStyle,
  tableHeadingBlockStyle,
  tableHeadingItemStyle,
  tableItemStyle
} from './TableMobile.style'

interface IProps {
  tableData: IPaymentDetailRow[]
}

interface IPaymentDetailRow {
  No: number
  X: number
  Y: number
  Z: number
  W: number
}

const TableRepaymentMobileForPreApproved: SFC<IProps> = ({ tableData }) => {
  const containerFit = window.innerWidth > 945

  return (
    <div className={`${containerFit ? tableContainerFit : tableContainer}`}>
      <div className={`flex flex-row w-full ${tableHeadingBlockStyle}`}>
        <p className={tableHeadingItemStyle}>
          {t('calculator.paymentestimation.table.timePreApproved')}
        </p>
        <p className={tableHeadingItemStyle}>
          {t('calculator.paymentestimation.table.remainLoanAmount')}
        </p>
        <p className={tableHeadingItemStyle}>
          {t('calculator.paymentestimation.table.paymentPerMonthPreApproved')}
        </p>
        <p className={tableHeadingItemStyle}>
          {t('calculator.paymentestimation.table.grossPerMonthPreApproved')}
        </p>
        <p className={tableHeadingItemStyle}>
          {t('calculator.paymentestimation.table.totalPerMonthPreApproved')}
        </p>
      </div>
      <div className={`${tableDataBlockStyle}`}>
        {tableData !== undefined &&
          tableData.map((item, index) => (
            <div key={index} className={`flex flex-row ${rowItem}`}>
              <div className={tableItemStyle}>{item.No}</div>
              <div className={tableItemStyle}>
                {formatCurrency({
                  value: item.X.toString(),
                  showCurrency: false
                })}
              </div>
              <div className={tableItemStyle}>
                {formatCurrency({
                  value: item.Y.toString(),
                  showCurrency: false
                })}
              </div>
              <div className={tableItemStyle}>
                {formatCurrency({
                  value: item.Z.toString(),
                  showCurrency: false
                })}
              </div>
              <div className={tableItemStyle}>
                {formatCurrency({
                  value: item.W.toString(),
                  showCurrency: false
                })}
              </div>
            </div>
          ))}
      </div>
    </div>
  )
}

const TableRepaymentMobile: SFC<IProps> = ({ tableData }) => {
  const containerFit = window.innerWidth > 945 ? true : false

  return (
    <div className={`${containerFit ? tableContainerFit : tableContainer}`}>
      <div className={`flex flex-row w-full ${tableHeadingBlockStyle}`}>
        <p className={tableHeadingItemStyle}>
          {t('calculator.paymentestimation.table.time')}
        </p>
        <p className={tableHeadingItemStyle}>
          {t('calculator.paymentestimation.table.remainLoanAmount')}
        </p>
        <p className={tableHeadingItemStyle}>
          {t('calculator.paymentestimation.table.paymentPerMonth')}
        </p>
        <p className={tableHeadingItemStyle}>
          {t('calculator.paymentestimation.table.grossPerMonth')}
        </p>
        <p className={tableHeadingItemStyle}>
          {t('landing.calculator.repaymentSchedule.totalPaymentMonthly')}
        </p>
      </div>
      <div className={`${tableDataBlockStyle}`}>
        {tableData !== undefined &&
          tableData.map((item, index) => (
            <div key={index} className={`flex flex-row ${rowItem}`}>
              <div className={tableItemStyle}>{item.No}</div>
              <div className={tableItemStyle}>
                {formatCurrency({
                  value: item.X.toString(),
                  showCurrency: false
                })}
              </div>
              <div className={tableItemStyle}>
                {formatCurrency({
                  value: item.Y.toString(),
                  showCurrency: false
                })}
              </div>
              <div className={tableItemStyle}>
                {formatCurrency({
                  value: item.Z.toString(),
                  showCurrency: false
                })}
              </div>
              <div className={tableItemStyle}>
                {formatCurrency({
                  value: item.W.toString(),
                  showCurrency: false
                })}
              </div>
            </div>
          ))}
      </div>
    </div>
  )
}

export { TableRepaymentMobile, TableRepaymentMobileForPreApproved }

export { IPaymentDetailRow }
