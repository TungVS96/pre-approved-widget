import { rem, theme } from 'dma-ui'
import { css } from 'emotion'

const tableContainerFit = css({
  width: '100%'
})
const tableContainer = css({
  width: '945px'
})
const tableHeadingBlockStyle = css({
  backgroundColor: '#E8EAEC',
  height: rem(40),
  paddingRight: '6px'
  // width: `calc(100% - ${rem(6)})`,
  // [theme.mq.md]: {
  //   width: `calc(100% - ${rem(8)})`
  // }
})
const rowItem = css({
  height: rem(40)
})
const tableHeadingItemStyle = css({
  border: '1px solid',
  borderColor: theme.colors['grey-light'],
  fontWeight: 500,
  color: '#17233D',
  padding: `${rem(10)} ${rem(6)} ${rem(10)} ${rem(20)}`,
  fontSize: rem(13),
  flex: 2,
  fontFamily:
    'Roboto,Segoe UI Regular Vietnamese,Segoe UI,Segoe WP,Tahoma,Arial,sans-serif',
  ':first-of-type': {
    flex: 1
  }
})

const tableDataBlockStyle = css({
  backgroundColor: theme.colors['grey-lightest'],
  height: 'calc(70vh - 50px)',
  overflowY: 'auto',
  '::-webkit-scrollbar': {
    width: '6px',
    backgroundColor: 'white'
  },
  '::-webkit-scrollbar-thumb': {
    borderRadius: '3px',
    backgroundColor: 'rgba(0,0,0,.5)',
    height: '100px'
  },
  [theme.mq.md]: {
    maxHeight: `${rem(380)}`,
    overflowY: 'auto',
    '::-webkit-scrollbar': {
      width: '8px',
      backgroundColor: 'white'
    },
    '::-webkit-scrollbar-thumb': {
      borderRadius: '4px',
      backgroundColor: 'rgba(0,0,0,.5)',
      height: '80px'
    }
  }
})

const tableItemStyle = css({
  border: '1px solid',
  borderColor: theme.colors['grey-light'],
  padding: `${rem(10)} ${rem(6)} ${rem(10)} ${rem(20)}`,
  color: '#17233D',
  wordBreak: 'break-word',
  fontSize: '14px',
  flex: 2,
  ':first-of-type': {
    flex: 1
  }
})

export {
  tableHeadingBlockStyle,
  tableHeadingItemStyle,
  tableDataBlockStyle,
  tableItemStyle,
  tableContainer,
  tableContainerFit,
  rowItem
}
