import { Overlay } from '@blueprintjs/core'
import React, { SFC } from 'react'
// import { t } from '../../../../../i18n'
import {
  TableRepaymentMobile,
  TableRepaymentMobileForPreApproved
} from './TableMobile.view'

import {
  Container,
  Content,
  Image,
  ImageBlock,
  ImageTagContain
} from './Modal.view.style'

interface IProps {
  isOpen: boolean
  // navigateDashboard(): void
  text: string
  tableData: any
  setShowTableDebt(status: boolean): void
  isRepayment: boolean
}

const Modal: SFC<IProps> = (props) => (
  <Overlay isOpen={props.isOpen}>
    <Container>
      <ImageBlock>
        <ImageTagContain>
          <Image
            onClick={() => {
              props.setShowTableDebt(!props.isOpen)
            }}
            src={require('../assets/close-outline.png')}
          // width={20}
          // height={20}
          />
        </ImageTagContain>
      </ImageBlock>

      <Content>
        {props.isRepayment ? (
          <TableRepaymentMobileForPreApproved {...props} />
        ) : (
            <TableRepaymentMobile {...props} />
          )}
      </Content>
    </Container>
  </Overlay>
)

export { IProps }
export default Modal
