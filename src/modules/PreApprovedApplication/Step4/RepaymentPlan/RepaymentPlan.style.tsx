import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'
import { getFontFamily } from '../../../../utils/string.util'

const ContainerOutter = withClassName('flex flex-col bg-white items-center')(
  styled('div')(() => ({
    height: 'calc(100% - 70px)',
    fontFamily: getFontFamily(),
    width: '80%',
    padding: `${rem(32)} ${rem(16)} ${rem(24)}`,
    margin: '3% 10%'
  }))
)
const Title = withClassName('text-brand text-center mb-4 md:mt-0')(
  styled('p')({
    fontSize: rem(20),
    fontWeight: rem(500),
    lineHeight: rem(32)
  })
)

const ContentWrapper = withClassName('flex sm:flex-col md:flex-row w-full')(
  styled('div')(() => ({
    height: `calc(100% - ${rem(64)})`,
    overflowX: 'auto',
    overflowY: 'hidden'
  }))
)
const ButtonDelete = withClassName('absolute cursor-pointer')(
  styled('div')({
    top: rem(16),
    right: rem(16)
  })
)

export { ContainerOutter, ContentWrapper, ButtonDelete, Title }
