import { Overlay } from '@blueprintjs/core'
import { pathOr } from 'ramda'
import React from 'react'
import { compose } from 'recompose'
import PaymentDetailTableRepayment from './Table/TableRepayment.view'
import { t } from '../../../../i18n'
import Modal from './Modal/Modal.view'
import {
  ButtonDelete,
  ContainerOutter,
  ContentWrapper,
  Title
} from './RepaymentPlan.style'

interface IOutterProps {
  isOpen: boolean
  handleClose(): void
  infoAssessment: any
}

const setScheduleRepayment = (
  // Thời gian ân hạn gốc
  originalGracePeriodInMonth: number,
  // hạn mức khoản vay
  loanAmount: number,
  // thời hạn vay
  tenorInMonth: number,
  // lãi suất
  interestRate: number,
  // Thời gian hỗ trợ lãi suất
  interestRateSupportMonth: any
) => {
  const listScheduleRepayment: any = []
  for (let i = 1; i <= tenorInMonth; i += 1) {
    const itemScheduleRepay = {
      No: i.toString().padStart(tenorInMonth.toString().length, '0'),
      W: 0,
      X: 0,
      Y: 0,
      Z: 0
    }

    itemScheduleRepay.Y =
      i <= originalGracePeriodInMonth
        ? 0
        : loanAmount / (tenorInMonth - originalGracePeriodInMonth)
    if (i === 1) {
      itemScheduleRepay.X = loanAmount - itemScheduleRepay.Y
      itemScheduleRepay.Z =
        i <= interestRateSupportMonth ? 0 : loanAmount * (interestRate / 12)
    } else {
      itemScheduleRepay.X = listScheduleRepayment[i - 2].X - itemScheduleRepay.Y
      itemScheduleRepay.Z =
        i <= interestRateSupportMonth
          ? 0
          : listScheduleRepayment[i - 2].X * (interestRate / 12)
    }

    itemScheduleRepay.W =
      i === tenorInMonth
        ? itemScheduleRepay.Y + itemScheduleRepay.Z + itemScheduleRepay.X
        : itemScheduleRepay.Y + itemScheduleRepay.Z

    listScheduleRepayment.push(itemScheduleRepay)
  }

  return listScheduleRepayment
}

const RepaymentPlan: React.FunctionComponent<IOutterProps> = (props) => {
  const { isOpen = false, handleClose } = props
  const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)

  const originalGracePeriodInMonth = pathOr(
    0,
    ['infoAssessment', 'originalGracePeriodInMonth'],
    props
  )
  const isInterestRateSupport = pathOr(
    0,
    ['infoAssessment', 'isInterestRateSupport'],
    props
  )

  const input = {
    originalGracePeriodInMonth,
    loanAmount: pathOr(0, ['infoAssessment', 'loanAmount'], props),
    tenorInMonth: pathOr(0, ['infoAssessment', 'tenorInMonth'], props),
    interestRate: pathOr(0, ['infoAssessment', 'interestRate'], props) / 100,
    interestRateSupportMonth: ['N', '', null].includes(isInterestRateSupport)
      ? 0
      : originalGracePeriodInMonth
  }

  const scheduleRepaymentData: any = setScheduleRepayment(
    input.originalGracePeriodInMonth,
    input.loanAmount,
    input.tenorInMonth,
    input.interestRate,
    input.interestRateSupportMonth
  )

  return isMobile ? (
    <Modal
      text=""
      isOpen={isOpen}
      tableData={scheduleRepaymentData}
      setShowTableDebt={handleClose}
      isRepayment={true}
    />
  ) : (
    <Overlay
      onClose={handleClose}
      isOpen={isOpen}
      canEscapeKeyClose={true}
      canOutsideClickClose={true}
    >
      <ContainerOutter>
        <ButtonDelete onClick={handleClose}>
          <img src={require('./../assets/ic-x.svg')} height={12} width={12} />
        </ButtonDelete>
        <Title>
          {t('preApproved.receiveResult.expectedRepaymentPlan.title')}
        </Title>
        <ContentWrapper>
          <PaymentDetailTableRepayment tableData={scheduleRepaymentData} />
        </ContentWrapper>
      </ContainerOutter>
    </Overlay>
  )
}

export default compose<IOutterProps, IOutterProps>()(RepaymentPlan)
