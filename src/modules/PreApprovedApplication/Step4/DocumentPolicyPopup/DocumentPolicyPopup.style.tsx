import { rem, theme, withClassName } from 'dma-ui'
import styled from 'react-emotion'
import { getFontFamily } from '../../../../utils/string.util'

const Card = withClassName('flex flex-col relative bg-white rounded-lg')(
  styled('div')(({ width = 716 }: any) => ({
    height: '80%',
    fontFamily: getFontFamily(),
    borderRadius: rem(2),
    width: '95%',
    padding: `${rem(32)} ${rem(16)} ${rem(24)}`,
    margin: 'auto',
    marginTop: '10%',
    [theme.mq.md]: {
      width,
      height: '80%',
      marginTop: '10%'
    }
  }))
)

const Title = withClassName('text-brand')(
  styled('p')({
    fontSize: rem(20),
    fontWeight: rem(500),
    lineHeight: rem(32),
    color: '#ec1c24'
  })
)

const SubTitle = withClassName('text-xs')(
  styled('p')({
    lineHeight: rem(20),
    fontStyle: 'italic',
    color: '#1D1D1F'
  })
)

const DownloadButton = withClassName('text-white bg-brand mr-4')(
  styled('button')({
    height: rem(40),
    width: rem(95),
    borderRadius: rem(2)
  })
)

const ContentWrapper = withClassName('flex sm:flex-col md:flex-row')(
  styled('div')({
    overflowY: 'scroll',
    height: '550px'
  })
)

const TextContent = withClassName('font-normal')(
  styled('p')({
    fontSize: rem(14),
    lineHeight: rem(24)
  })
)

const ButtonDelete = withClassName('absolute cursor-pointer')(
  styled('div')({
    top: rem(16),
    right: rem(16)
  })
)

const Table = withClassName('w-full mt-4')(styled('table')(() => ({})))

const TR = withClassName('')(
  styled('tr')(({ highlight = true }: { highlight: boolean }) => ({
    backgroundColor: highlight ? '#ECEEEF' : 'white',
    border: 'solid 1px #d7d7d7'
  }))
)

const TH = withClassName('py-2')(
  styled('th')(
    {
      backgroundColor: '#ECEEEF',
      fontSize: rem(13),
      fontWeight: 'bold',
      border: 'solid 1px #d7d7d7',
      color: '#1D1D1F'
    },
    ({ index }: { index: number }) => ({
      width:
        index === 1
          ? '140px'
          : index === 2
          ? '168px'
          : index === 3
          ? '184px'
          : '168px'
    })
  )
)

const TD = withClassName('p-4')(
  styled('td')(({}) => ({
    verticalAlign: 'top',
    border: 'solid 1px #d7d7d7'
  }))
)

export {
  Card,
  ContentWrapper,
  ButtonDelete,
  Title,
  SubTitle,
  TextContent,
  DownloadButton,
  Table,
  TR,
  TH,
  TD
}
