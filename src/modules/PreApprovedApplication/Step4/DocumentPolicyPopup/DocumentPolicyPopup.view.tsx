import { Overlay } from '@blueprintjs/core'
import { rem } from 'dma-ui'
import { css } from 'emotion'
import React from 'react'
import { compose } from 'recompose'
import { t } from '../../../../i18n'
import {
  ButtonDelete,
  Card,
  ContentWrapper,
  DownloadButton,
  SubTitle,
  Table,
  Title,
  TD,
  TH,
  TR
} from './DocumentPolicyPopup.style'
import { documentChecklist } from './DocumentPolicyPopup.util'

interface IOutterProps {
  isOpen: boolean
  handleClose(): void
  onDownLoad(): void
}

const renderParagraph = (paragraphs: any) => {
  return paragraphs.map((para: any, index: number, array: any) => {
    return index < array.length - 1 ? (
      <React.Fragment key={index}>
        {para}
        <br />
        <br />
      </React.Fragment>
    ) : (
      <React.Fragment key={index}>{para}</React.Fragment>
    )
  })
}

const renderTable = (props: IOutterProps) => {
  return (
    <Table>
      <tbody>
        <TR>
          <TH index={1}>Nhóm hồ sơ</TH>
          <TH index={2}>Loại hồ sơ</TH>
          <TH index={3}>Điều kiện hồ sơ</TH>
          <TH index={4}>Chứng từ có thể thay thế</TH>
        </TR>
        {documentChecklist.map((item, index) => {
          return item.type.length === 1 ? (
            <TR key={index} highlight={index % 2 !== 0}>
              <TD>{item.group}</TD>
              <TD>{item.type}</TD>
              <TD>{renderParagraph(item.condition[0])}</TD>
              <TD>{renderParagraph(item.replacement[0])}</TD>
            </TR>
          ) : (
            <React.Fragment key={index}>
              <TR highlight={index % 2 !== 0}>
                <TD rowSpan={item.type.length}>{item.group}</TD>
                <TD>{item.type[0]}</TD>
                {item.condition.length === 1 ? (
                  <TD rowSpan={item.type.length}>
                    {renderParagraph(item.condition[0])}
                  </TD>
                ) : (
                  <TD>{renderParagraph(item.condition[0])}</TD>
                )}
                <TD>{renderParagraph(item.replacement[0])}</TD>
              </TR>
              {item.type.slice(1).map((type: string, idx: number, arr: any) => {
                return (
                  <TR key={index} highlight={index % 2 !== 0}>
                    <TD>{type}</TD>
                    {item.condition.length > 1 ? (
                      <TD>{renderParagraph(item.condition[idx + 1])}</TD>
                    ) : (
                      ''
                    )}
                    {item.replacement.length > 1 ? (
                      <TD>{renderParagraph(item.replacement[idx + 1])}</TD>
                    ) : (
                      <TD />
                    )}
                  </TR>
                )
              })}
            </React.Fragment>
          )
        })}
      </tbody>
    </Table>
  )
}

const DocumentPolicyPopup: React.FunctionComponent<IOutterProps> = (props) => {
  const { isOpen = false, handleClose, onDownLoad } = props

  return (
    <Overlay
      onClose={handleClose}
      isOpen={isOpen}
      canEscapeKeyClose={true}
      canOutsideClickClose={true}
    >
      <Card>
        <ButtonDelete onClick={handleClose}>
          <img src={require('./../assets/ic-x.svg')} height={12} width={12} />
        </ButtonDelete>
        <div className={'flex flex-row items-center'}>
          <div className={'w-full'}>
            <Title>{t('preApproved.step4.documentPolicy.title')}</Title>
            <SubTitle>
              {t('preApproved.step4.documentPolicy.subTitle')}
            </SubTitle>
          </div>
          <DownloadButton onClick={onDownLoad}>
            <img
              src={require('./../assets/ic-download.svg')}
              height={22}
              width={22}
            />
            <span className={css({ lineHeight: rem(40), marginLeft: rem(8) })}>
              {t('preApproved.step4.documentPolicy.download')}
            </span>
          </DownloadButton>
        </div>
        <ContentWrapper>{renderTable(props)}</ContentWrapper>
      </Card>
    </Overlay>
  )
}

export default compose<IOutterProps, IOutterProps>()(DocumentPolicyPopup)
