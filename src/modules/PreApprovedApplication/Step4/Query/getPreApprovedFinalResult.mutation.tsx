import gql from 'graphql-tag'
import { pathOr } from 'ramda'

const query = gql`
  query($preApprovedId: String) {
    preApprovedResult(preApprovedId: $preApprovedId)
      @rest(
        type: "pre_approved"
        path: "/approved/v1/pre-approved-result/{args.preApprovedId}"
        endpoint: "dma-pre-credit"
      ) {
      name
      loanAmount
      expireDate
      projectName
      tenorInMonth
      monthlyPayment
      paymentMethod
      status
    }
  }
`

const params = {
  props: ({ data }: any) => {
    const preApprovedResult = pathOr({}, ['preApprovedResult'], data)

    return {
      data,
      preApprovedResult
    }
  },
  options: (props: any) => {
    const preApprovedId = pathOr('', ['preApprovedId'], props)

    return {
      variables: {
        preApprovedId
      }
    }
  }
}

export default { query, params }
