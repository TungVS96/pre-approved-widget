import gql from 'graphql-tag'
import { pathOr } from 'ramda'

const query = gql`
  query($preApprovedId: String) {
    infoAssessment(preApprovedId: $preApprovedId)
      @rest(
        type: "pre_approved"
        path: "/approved/v1/get-info-assessment?{args}"
        endpoint: "dma-pre-credit"
      ) {
      loanAmount
      tenorInMonth
      interestRate
      originalGracePeriodInMonth
      isInterestRateSupport
    }
  }
`

const params = {
  props: ({ data }: any) => {
    return {
      infoAssessment: {
        loanAmount: pathOr(0, ['infoAssessment', 'loanAmount'], data),
        tenorInMonth: pathOr(0, ['infoAssessment', 'tenorInMonth'], data),
        interestRate: pathOr(0, ['infoAssessment', 'interestRate'], data),
        originalGracePeriodInMonth: pathOr(
          0,
          ['infoAssessment', 'originalGracePeriodInMonth'],
          data
        ),
        isInterestRateSupport: pathOr(
          '',
          ['infoAssessment', 'isInterestRateSupport'],
          data
        )
      }
    }
  },
  options: (props: any) => {
    const preApprovedId = pathOr('', ['preApprovedId'], props)

    return {
      variables: {
        preApprovedId
      }
    }
  }
}

export default { query, params }
