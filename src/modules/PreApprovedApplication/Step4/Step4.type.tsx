import { IPage } from 'dma-types'
import { History } from 'history'
import { GraphqlQueryControls } from 'react-apollo'
import { PopupProps } from '../../../hocs'

enum POPUP_NAME {
  THANK_YOU = '1',
  LOAN_CONDITION = '2',
  REPAYMENT_PLAN = '3',
  DOCUMENT_POLICY = '4',
  HIGHER_LIMIT = '5'
}

interface IPreApprovedFinalResult {
  expireDate: string
  loanAmount: number
  monthlyPayment: number
  name: string
  paymentMethod: string
  projectName: string
  tenorInMonth: string
  status: string
}

interface IProps extends PopupProps, IPage {
  history: History
  acceptLimit(payload: any): void
  showMessage(): void
  preApprovedResult: IPreApprovedFinalResult
  data: GraphqlQueryControls
}

interface IHandlers {
  setAcceptLoanAmount(): void
  ON_ACCEPT(): void
  ON_DOWNLOAD(): void
  setTypeOfInfo(typeOfInfo: string): void
}

type EnhancedProps = IProps & IHandlers
export { EnhancedProps, POPUP_NAME, IPreApprovedFinalResult }
