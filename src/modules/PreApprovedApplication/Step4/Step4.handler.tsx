import { equals, pathOr } from 'ramda'
import { PRE_APPROVED_STATUS } from '../PreApprovedApplication.constant'
import { EnhancedProps, POPUP_NAME } from './Step4.type'

const updateStatus = async (props: any) => {
  try {
    const preApprovedId = pathOr('', ['preApprovedId'], props)
    await props.updateStatus({
      variables: {
        input: {
          id: preApprovedId,
          status: PRE_APPROVED_STATUS.PR03
        }
      }
    })
  } catch (e) {
    props.showMessage()
  }
}

export default {
  setAcceptLoanAmount: (props: any) => () => {
    const isAccept = pathOr(false, ['values', 'isAccept'], props)

    props.setFieldValue('isAccept', !isAccept)
  },
  ON_ACCEPT: (props: EnhancedProps) => async () => {
    try {
      const isAccept = pathOr(false, ['values', 'isAccept'], props)
      const status = pathOr('', ['preApprovedResult', 'status'], props)

      if (!(isAccept && equals(status, PRE_APPROVED_STATUS.PR02))) {
        return
      }
      const preApprovedId = pathOr('', ['preApprovedId'], props)

      await props.acceptLimit({ variables: { preApprovedId, input: {} } })
      await updateStatus(props)
      props.data.refetch()
      props.setPopup(POPUP_NAME.THANK_YOU)
    } catch (e) {
      props.showMessage()
    }
  },
  ON_DOWNLOAD: (props: EnhancedProps) => async () => {
    const fileName = 'Danh-muc-ho-so-can-chuan-bi.pdf'
    const serverBackend =
      process.env.REACT_APP_REST_ENDPOINT_DMA_PRE_CREDIT || ''
    const downloadURL = `${serverBackend}/approved/v1/files/${fileName}`

    const response = await fetch(downloadURL, {
      method: 'GET',
      cache: 'force-cache'
    })

    const blob = await response.blob()
    const blobURL = URL.createObjectURL(blob) || ''
    const link = document.createElement('a')

    link.href = blobURL
    link.download = fileName
    link.click()
  }
}
