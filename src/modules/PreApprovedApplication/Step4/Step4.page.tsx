import withLifecycle from '@hocs/with-lifecycle'
import { withFormik } from 'formik'
import { pathOr } from 'ramda'
import { graphql } from 'react-apollo'
import { compose, withHandlers } from 'recompose'
import { withApp, withPopup } from '../../../hocs'
import updateStatus from '../Mutation/updateStatus.mutation'
import { STEP } from '../PreApprovedApplication.type'
import acceptLimit from './Mutation/acceptLimit.mutation'
import getPreApprovedFinalResult from './Query/getPreApprovedFinalResult.mutation'
import getRepaymentsQuery from './Query/getRepayments.query'
import { mapPropsToValues, validate } from './Step4.config'
import handlers from './Step4.handler'
import Step4View from './Step4.view'

const formikConfig = {
  validate,
  mapPropsToValues,
  handleSubmit: () => {
    //
  }
}
const enhancer = compose(
  withPopup,
  graphql(getPreApprovedFinalResult.query, getPreApprovedFinalResult.params),
  graphql(getRepaymentsQuery.query, getRepaymentsQuery.params),
  graphql(acceptLimit.mutation, acceptLimit.params),
  graphql(updateStatus.mutation, updateStatus.params),
  withFormik(formikConfig),
  withApp({ updates: ['acceptLimit', 'updateStatus'] }),
  withHandlers(handlers),
  withLifecycle({
    onDidMount: async (props: any) => {
      const params = {
        step: STEP.step4
      }
      const preApprovedId = pathOr('', ['match', 'params', 'id'], props)
      const input = {
        preApprovedId,
        currentStep: JSON.stringify(params)
      }
      props.saveCurrentStep({ variables: { input } })
    }
  })
)

export default enhancer(Step4View)
