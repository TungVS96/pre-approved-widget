import { REPAYMENT_METHOD } from 'dma-types'
import { isNil, pathOr } from 'ramda'
import React, { FunctionComponent } from 'react'
import { formatCurrency, formatDate } from '../../../utils'
// import ModalExitComponent from '../Components/ModalExit/ModalExit.component'
import { PRE_APPROVED_STATUS } from '../PreApprovedApplication.constant'
import { t } from './../../../i18n'
import DocumentPolicyPopup from './DocumentPolicyPopup/DocumentPolicyPopup.view'
import HigherLimitView from './HigherLimit/HigherLimit.view'
import LoanCondition from './LoanCondition/LoanCondition.view'
import RepaymentPlan from './RepaymentPlan/RepaymentPlan.view'
import {
  ButtonConfirm,
  ButtonContainer,
  ButtonEdit,
  CardLimitApprove,
  CardLimitApproveAmount,
  CardLimitApproveExpire,
  CardLimitApprovePrepare,
  CardLimitApproveTitle,
  ColLeft,
  ColRight,
  Container,
  IconCheckbox,
  MonthlyPaymentLeft,
  MonthlyPaymentRight,
  PhraseUnderLine,
  ProjectInfo,
  ProjectInfoLeft,
  ProjectInfoRight,
  ProjectInfoRow,
  Title
} from './Step4.style'
import {
  EnhancedProps,
  IPreApprovedFinalResult,
  POPUP_NAME
} from './Step4.type'
import ThankYouPopup from './ThankYouPopup/ThankYouPopup.view'

const getPaymentMedthod = (paymentMethod: string) => {
  switch (paymentMethod) {
    case REPAYMENT_METHOD.ANNUITY:
      return 'Gốc và lãi trả đều hàng tháng'
    case REPAYMENT_METHOD.AMORTIZATION:
      return t('TYPES.REPAYMENT_METHOD.AMORTIZATION')
    default:
      return ''
  }
}

const Step4View: FunctionComponent<EnhancedProps> = (props: any) => {
  const isAccept = pathOr(false, ['values', 'isAccept'], props)
  const preApprovedResult = props.preApprovedResult as IPreApprovedFinalResult
  const showPopup = (name: string) => () => props.setPopup(name)
  const canGetHigherLimit = [
    PRE_APPROVED_STATUS.PR01,
    PRE_APPROVED_STATUS.PR02
  ].includes(props.preApprovedResult.status)
  const tenorYear =
    !isNil(preApprovedResult.tenorInMonth) &&
    parseInt(preApprovedResult.tenorInMonth, 10) / 12
  const tenorMonth =
    !isNil(preApprovedResult.tenorInMonth) &&
    parseInt(preApprovedResult.tenorInMonth, 10) % 12
  const tenorYearText = tenorYear !== 0 ? `${tenorYear} năm` : ''
  const tenorMonthText = tenorMonth !== 0 ? `${tenorMonth} tháng` : ''
  const status = pathOr('', ['preApprovedResult', 'status'], props)
  const enableAgreeButton = isAccept && status === PRE_APPROVED_STATUS.PR02

  return (
    <Container>
      <ColLeft>
        <Title>Chúc mừng {preApprovedResult.name}</Title>
        <CardLimitApprove>
          <CardLimitApproveTitle>
            Hạn mức phê duyệt trước cho khoản vay mua nhà Quý khách là:
          </CardLimitApproveTitle>
          <CardLimitApproveAmount>
            {formatCurrency({
              value: preApprovedResult.loanAmount,
              showCurrency: true
            })}
          </CardLimitApproveAmount>
          <CardLimitApproveExpire>
            Hạn mức có hiệu lực đến ngày:{' '}
            {formatDate(preApprovedResult.expireDate)}
          </CardLimitApproveExpire>
          <CardLimitApprovePrepare>
            <PhraseUnderLine onClick={showPopup(POPUP_NAME.DOCUMENT_POLICY)}>
              Hồ sơ cần chuẩn bị
            </PhraseUnderLine>
          </CardLimitApprovePrepare>
        </CardLimitApprove>
        <ProjectInfo>
          <ProjectInfoRow>
            <ProjectInfoLeft>Dự án: </ProjectInfoLeft>
            <ProjectInfoRight>{preApprovedResult.projectName}</ProjectInfoRight>
          </ProjectInfoRow>
          <ProjectInfoRow>
            <ProjectInfoLeft>Thời gian vay dự kiến: </ProjectInfoLeft>
            <ProjectInfoRight>
              {`${tenorYearText} ${tenorMonthText}`}
            </ProjectInfoRight>
          </ProjectInfoRow>
          <ProjectInfoRow>
            <ProjectInfoLeft>Phương thức trả nợ: </ProjectInfoLeft>
            <ProjectInfoRight>
              {getPaymentMedthod(preApprovedResult.paymentMethod)}
            </ProjectInfoRight>
          </ProjectInfoRow>
          <ProjectInfoRow>
            <ProjectInfoLeft>Số tiền trả nợ tối đa: </ProjectInfoLeft>
            <ProjectInfoRight>
              {preApprovedResult.monthlyPayment !== 0 &&
                formatCurrency({
                  value: preApprovedResult.monthlyPayment,
                  showCurrency: true
                })}
            </ProjectInfoRight>
          </ProjectInfoRow>
        </ProjectInfo>
        <ProjectInfoRow>
          <MonthlyPaymentLeft>
            Số tiền trả nợ hàng tháng xem tại:
          </MonthlyPaymentLeft>
          <MonthlyPaymentRight>
            <PhraseUnderLine>
              <div onClick={showPopup(POPUP_NAME.REPAYMENT_PLAN)}>
                {t('preApproved.receiveResult.expectedRepaymentPlan.title')}
              </div>
            </PhraseUnderLine>
          </MonthlyPaymentRight>
        </ProjectInfoRow>
        <ProjectInfoRow>
          <IconCheckbox
            onClick={props.setAcceptLoanAmount}
            isAccept={isAccept}
          />
          Tôi đồng ý với hạn mức và &nbsp;
          <PhraseUnderLine onClick={showPopup(POPUP_NAME.LOAN_CONDITION)}>
            Điều kiện vay vốn
          </PhraseUnderLine>
        </ProjectInfoRow>
        <ButtonContainer>
          <ButtonConfirm
            id="pre_accept_loan"
            enableAgreeButton={enableAgreeButton}
            onClick={props.ON_ACCEPT}
          >
            Đồng ý hạn mức phê duyệt
          </ButtonConfirm>
          <ButtonEdit
            id="pre_change_limit_loan"
            disabled={!canGetHigherLimit}
            onClick={showPopup(POPUP_NAME.HIGHER_LIMIT)}
          >
            Muốn thay đổi hạn mức
          </ButtonEdit>
        </ButtonContainer>
      </ColLeft>
      <ColRight>
        <img src={require('./assets/icon_result.svg')} />
      </ColRight>
      <ThankYouPopup
        isOpen={props.currentPopup === POPUP_NAME.THANK_YOU}
        handleClose={showPopup('')}
      />
      <LoanCondition
        isOpen={props.currentPopup === POPUP_NAME.LOAN_CONDITION}
        handleClose={showPopup('')}
        isMD={props.isMD}
      />
      <RepaymentPlan
        isOpen={props.currentPopup === POPUP_NAME.REPAYMENT_PLAN}
        handleClose={showPopup('')}
        infoAssessment={props.infoAssessment}
      />
      <DocumentPolicyPopup
        isOpen={props.currentPopup === POPUP_NAME.DOCUMENT_POLICY}
        handleClose={showPopup('')}
        onDownLoad={props.ON_DOWNLOAD}
      />
      <HigherLimitView
        isOpen={props.currentPopup === POPUP_NAME.HIGHER_LIMIT}
        handleClose={showPopup('')}
        setStep={props.setStep}
        setInfoType={props.setInfoType}
      />
      {/* <ModalExitComponent
        incomesView={true}
        history={props.history}
        saveData={() => {
          //
        }}
      /> */}
    </Container>
  )
}

export default Step4View
