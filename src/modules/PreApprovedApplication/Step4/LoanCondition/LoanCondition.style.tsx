import { rem, theme, withClassName } from 'dma-ui'
import styled from 'react-emotion'
import { getFontFamily } from '../../../../utils/string.util'

const Container = withClassName(
  'w-full h-full flex justify-center items-center'
)(styled('div')({}))

const Card = withClassName(
  'flex flex-col relative bg-white items-center rounded-lg'
)(
  styled('div')(({ width = 716, height }: any) => ({
    height,
    fontFamily: getFontFamily(),
    borderRadius: rem(2),
    width: '95%',
    padding: `${rem(32)} ${rem(16)} ${rem(24)}`,
    overflowY: 'auto',
    margin: 'auto',
    marginTop: '20%',
    [theme.mq.md]: {
      width,
      height,
      marginTop: '15%'
    }
  }))
)

const Title = withClassName('text-center my-4 md:mt-0')(
  styled('p')({
    fontSize: rem(20),
    fontWeight: rem(500),
    lineHeight: rem(32),
    color: '#EC1C24'
  })
)

const ContentWrapper = withClassName('flex sm:flex-col md:flex-row')(
  styled('div')({})
)

const Left = withClassName('text-center')(
  styled('div')({
    minWidth: rem(150),
    [theme.mq.md]: {
      maxWidth: rem(150)
    }
  })
)

const Right = withClassName('w-full md:px-6')(
  styled('div')({
    height: rem(336),
    overflowY: 'auto'
  })
)

const TextContent = withClassName('font-normal')(
  styled('p')({
    fontSize: rem(14),
    lineHeight: rem(24),
    color: '#1D1D1F'
  })
)

const ButtonDelete = withClassName('absolute cursor-pointer')(
  styled('div')({
    top: rem(16),
    right: rem(16)
  })
)

const VerifiedIconWrapper = withClassName('mr-2 mt-2')(
  styled('div')({
    minWidth: rem(16)
  })
)

export {
  Container,
  Card,
  ContentWrapper,
  ButtonDelete,
  Left,
  Right,
  Title,
  TextContent,
  VerifiedIconWrapper
}
