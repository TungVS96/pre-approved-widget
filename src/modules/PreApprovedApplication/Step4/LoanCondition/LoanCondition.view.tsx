import { Overlay } from '@blueprintjs/core'
import { rem } from 'dma-ui'
import { css } from 'emotion'
import React from 'react'
import { compose } from 'recompose'
import { t } from '../../../../i18n'
import {
  ButtonDelete,
  Card,
  ContentWrapper,
  Left,
  Right,
  TextContent,
  Title,
  VerifiedIconWrapper
} from './LoanCondition.style'

interface IOutterProps {
  isOpen: boolean
  handleClose(): void
  isMD: boolean
}

const conditionList = [
  {
    content: t('preApproved.step4.loanCondition.popup.content.1')
  },
  {
    content: t('preApproved.step4.loanCondition.popup.content.2')
  },
  {
    content: t('preApproved.step4.loanCondition.popup.content.3')
  },
  {
    content: t('preApproved.step4.loanCondition.popup.content.4')
  },
  {
    content: t('preApproved.step4.loanCondition.popup.content.5')
  },
  {
    content: t('preApproved.step4.loanCondition.popup.content.6')
  }
]

const LoanCondition: React.FunctionComponent<IOutterProps> = (props) => {
  const { isOpen = false, handleClose, isMD } = props

  return (
    <Overlay
      onClose={handleClose}
      isOpen={isOpen}
      canEscapeKeyClose={true}
      canOutsideClickClose={true}
    >
      <Card>
        <ButtonDelete onClick={handleClose}>
          <img src={require('./../assets/ic-x.svg')} height={12} width={12} />
        </ButtonDelete>
        {isMD && (
          <Title>{t('preApproved.step4.loanCondition.popup.title')}</Title>
        )}
        <ContentWrapper>
          <Left>
            <img
              src={require('./../assets/loancondition.svg')}
              height={140}
              width={150}
            />
            <TextContent
              className={css({ fontSize: rem(10), fontStyle: 'italic' })}
            >
              {t('preApproved.step4.loanCondition.popup.subtitle')}
            </TextContent>
          </Left>
          {!isMD && (
            <Title>{t('preApproved.step4.loanCondition.popup.title')}</Title>
          )}
          <Right>
            {conditionList.map((item, index) => {
              return (
                <div
                  className={'flex flex-row mb-4'}
                  key={`conditionList_${index}`}
                >
                  <VerifiedIconWrapper>
                    <img
                      src={require('./../assets/ic-verified.svg')}
                      height={16}
                      width={16}
                    />
                  </VerifiedIconWrapper>
                  <TextContent>{item.content}</TextContent>
                </div>
              )
            })}
          </Right>
        </ContentWrapper>
      </Card>
    </Overlay>
  )
}

export default compose<IOutterProps, IOutterProps>()(LoanCondition)
