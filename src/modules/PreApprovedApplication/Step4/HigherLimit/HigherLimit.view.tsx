import { Overlay } from '@blueprintjs/core'
import React from 'react'
import { compose } from 'recompose'
import { t } from '../../../../i18n'
import { STEP } from '../../PreApprovedApplication.type'
import { TYPE_OF_INFO } from '../../Step3/Step3.const'
import {
  ButtonConfirm,
  ButtonDelete,
  ButtonEdit,
  ContainerOutter,
  ContentWrapper,
  Title
} from './HigherLimit.style'

interface IOutterProps {
  isOpen: boolean
  handleClose(): void
  setStep(stepName: any): void
  setInfoType(name: any): void
}

const onLimitAdjust = (props: IOutterProps) => (e: any) => {
  // go to 1.7
  props.setStep(STEP.step3)
  props.setInfoType('')
}

const onHigherAdjust = (props: IOutterProps) => (e: any) => {
  // go to 1.21
  props.setStep(STEP.step3)
  props.setInfoType(TYPE_OF_INFO.ASSET_LISTING)
}

const HigherLimit: React.FunctionComponent<IOutterProps> = (props) => {
  const { isOpen = false, handleClose } = props
  // const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)

  return (
    <Overlay
      onClose={handleClose}
      isOpen={isOpen}
      canEscapeKeyClose={true}
      canOutsideClickClose={true}
    >
      <ContainerOutter>
        <ButtonDelete onClick={handleClose}>
          <img src={require('./../assets/ic-x.svg')} height={12} width={12} />
        </ButtonDelete>
        <img
          src={require('./../assets/higherLimit.svg')}
          height={143}
          width={140}
        />
        <Title>{t('preApproved.receiveResult.higherLimit.title')}</Title>
        <ContentWrapper>
          <p>{t('preApproved.receiveResult.higherLimit.content')}</p>
        </ContentWrapper>
        <ButtonConfirm>
          <div onClick={onLimitAdjust(props)}>
            {t('preApproved.receiveResult.higherLimit.button1')}
          </div>
        </ButtonConfirm>
        <ButtonEdit>
          <div onClick={onHigherAdjust(props)}>
            {t('preApproved.receiveResult.higherLimit.button2')}
          </div>
        </ButtonEdit>
      </ContainerOutter>
    </Overlay>
  )
}

export default compose<IOutterProps, IOutterProps>()(HigherLimit)
