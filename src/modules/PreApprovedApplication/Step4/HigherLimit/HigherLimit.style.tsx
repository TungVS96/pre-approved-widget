import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'
import { getFontFamily } from '../../../../utils/string.util'

const ContainerOutter = withClassName('flex flex-col bg-white items-center')(
  styled('div')(() => ({
    borderRadius: '2px',
    height: '442px',
    fontFamily: getFontFamily(),
    width: '375px',
    padding: `${rem(32)} ${rem(16)} ${rem(24)}`,
    marginTop: `calc(50vh - ${rem(221)})`,
    marginLeft: `calc(50vw - ${rem(188)})`
  }))
)
const Title = withClassName('text-center mb-4 md:mt-0')(
  styled('p')({
    fontSize: rem(20),
    fontWeight: 500,
    lineHeight: rem(32),
    color: '#EC1C24'
  })
)

const ContentWrapper = withClassName('flex sm:flex-col md:flex-row w-full')(
  styled('div')(() => ({
    height: 'auto',
    overflowX: 'auto',
    borderRadius: '2px',
    color: '#1D1D1F'
  }))
)

const ButtonDelete = withClassName('absolute cursor-pointer')(
  styled('div')({
    top: rem(16),
    right: rem(16)
  })
)

const ButtonConfirm = withClassName()(
  styled('button')(
    {
      height: rem(40),
      color: '#fff',
      borderRadius: rem(5),
      fontSize: rem(16),
      marginTop: rem(20)
    },
    ({ theme }) => ({
      backgroundColor: '#EC1C24',
      cursor: 'pointer',
      width: '100%',
      [theme.mq.md]: {
        width: rem(343)
      }
    })
  )
)
const ButtonEdit = withClassName()(
  styled('button')(({ theme }) => ({
    height: rem(40),
    backgroundColor: '#FFFFFF',
    borderRadius: rem(5),
    border: `${rem(1)} solid #EC1C24`,
    color: '#EC1C24',
    fontSize: rem(16),
    width: '100%',
    marginTop: rem(20),
    [theme.mq.md]: {
      width: rem(343)
    }
  }))
)

export {
  ContainerOutter,
  ContentWrapper,
  ButtonDelete,
  ButtonConfirm,
  ButtonEdit,
  Title
}
