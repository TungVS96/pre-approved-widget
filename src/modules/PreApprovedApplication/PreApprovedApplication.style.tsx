import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'
import { getFontFamily } from '../../utils/string.util'

const Container = withClassName('w-full', 'Container')(
  styled('div')({
    fontFamily: getFontFamily()
  })
)

const HeaderWrapper = withClassName('', 'Container')(
  styled('div')({
    fontFamily: getFontFamily()
  })
)

const ContentWrapper = withClassName(
  'flex flex-row flex-wrap sm:w-full md:w-2/3'
)(
  styled('form')({
    padding: `0 ${rem(8)}`,
    paddingBottom: rem(62)
  })
)
const FieldWrapper = withClassName('sm:w-full md:w-1/2')(
  styled('div')({
    padding: `0 ${rem(8)}`,
    alignSelf: 'center'
  })
)
const Title = withClassName('flex flex-row flex-wrap w-full text-base')(
  styled('p')({
    padding: `${rem(29)} ${rem(16)}`,
    lineHeight: rem(22)
  })
)

const Image = withClassName('')(
  styled('img')({
    height: rem(128),
    width: rem(206)
  })
)

export { Container, HeaderWrapper, ContentWrapper, FieldWrapper, Title, Image }
