import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const PageHeaderContainer = withClassName(
  'relative w-full bg-white flex justify-center'
)(
  styled('div')(({ theme }) => ({
    paddingTop: rem(16),
    paddingBottom: rem(16),
    [theme.mq.md]: {
      paddingTop: rem(24)
    }
  }))
)

export { PageHeaderContainer }
