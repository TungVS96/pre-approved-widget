import React, { SFC } from 'react'
import ApplicationProcess from '../ApplicationProcess/ApplicationProcess.component'
import { Item } from '../ApplicationProcess/ApplicationProcess.view'
import { STEP } from '../PreApprovedApplication.type'
import { PageHeaderContainer } from './PageHeader.component.style'

interface IOutterProps {
  className?: string
  onHandlers?: any
  items: Item[]
  currentStep: STEP
}

const PageHeader: SFC<IOutterProps> = (props) => {
  const { items, currentStep } = props

  return (
    <PageHeaderContainer className={props.className}>
      <ApplicationProcess
        items={items}
        currentStep={currentStep}
        lastStep={currentStep}
      />
    </PageHeaderContainer>
  )
}

export default PageHeader
