import { rem, theme, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const Container = withClassName(
  'w-full h-full flex justify-center items-center'
)(styled('div')({}))

const Card = withClassName(
  'flex flex-col relative bg-white items-center rounded-lg'
)(
  styled('div')(({ width = 400, height }: any) => ({
    height,
    borderRadius: rem(2),
    width: '90%',
    padding: rem(16),
    overflowY: 'auto',
    [theme.mq.md]: {
      width,
      height
    }
  }))
)

const HeaderContainer = withClassName('flex flex-row w-full')(styled('div')({}))

const Title = withClassName('')(
  styled('div')({
    marginLeft: rem(16),
    fontSize: rem(18),
    color: '#17233D'
  })
)

const BottomButtonContainer = withClassName('flex flex-row justify-end w-full')(
  styled('div')({
    marginTop: rem(21)
  })
)

const ButtonSave = withClassName('bg-brand text-white cursor-pointer')(
  styled('button')({
    borderRadius: rem(2),
    width: rem(120),
    height: rem(40)
  })
)

const ButtonCancle = withClassName('bg-white text-black cursor-pointer')(
  styled('button')({
    borderRadius: rem(2),
    width: rem(120),
    border: `1px solid #DCDEE2`,
    marginRight: rem(16),
    height: rem(40)
  })
)

const Icon = withClassName('')(
  styled('div')({
    minWidth: rem(20)
  })
)

const CloseButton = withClassName('cursor-pointer ml-4')(
  styled('div')({
    minWidth: rem(12)
  })
)

export {
  Container,
  Card,
  CloseButton,
  Title,
  HeaderContainer,
  BottomButtonContainer,
  ButtonSave,
  ButtonCancle,
  Icon
}
