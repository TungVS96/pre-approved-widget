import { Overlay } from '@blueprintjs/core'
import React from 'react'
import {
  BottomButtonContainer,
  ButtonCancle,
  ButtonSave,
  Card,
  CloseButton,
  Container,
  HeaderContainer,
  Icon,
  Title
} from './ConfirmDeletePopup.style'

interface IOutterProps {
  isOpen: boolean
  handleClose(): void
  handleNo?(): void
  handleYes?(): void
  title?: string
  subTitle?: string
}

type EnhancedProps = IOutterProps

const ConfirmDeletePopup: React.FunctionComponent<EnhancedProps> = (props) => {
  const {
    isOpen = false,
    handleClose,
    handleNo,
    handleYes,
    title = '',
    subTitle = ''
  } = props

  return (
    <Overlay
      onClose={handleClose}
      isOpen={isOpen}
      canEscapeKeyClose={true}
      canOutsideClickClose={true}
    >
      <Container>
        <Card>
          <HeaderContainer>
            <Icon>
              <img
                src={require('./assets/ic-warning.svg')}
                height={20}
                width={20}
              />
            </Icon>
            <Title>{title}</Title>
            <CloseButton onClick={handleClose}>
              <img src={require('./assets/ic-x.svg')} height={12} width={12} />
            </CloseButton>
          </HeaderContainer>
          <div className={'mt-4 ml-6'}>{subTitle}</div>
          <BottomButtonContainer>
            {handleNo && (
              <ButtonCancle onClick={handleNo}>{'Không'}</ButtonCancle>
            )}
            {handleYes && <ButtonSave onClick={handleYes}>{'Có'}</ButtonSave>}
          </BottomButtonContainer>
        </Card>
      </Container>
    </Overlay>
  )
}

export default ConfirmDeletePopup
