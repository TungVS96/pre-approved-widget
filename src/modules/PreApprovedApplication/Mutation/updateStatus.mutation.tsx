import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    updateStatus(input: $input)
      @rest(
        type: "Person"
        endpoint: "dma-pre-credit"
        path: "/lead/v1/update-status"
        method: "post"
      ) {
      status
      id
    }
  }
`
const params = {
  name: 'updateStatus'
}

export default {
  mutation,
  params
}
