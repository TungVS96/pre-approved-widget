import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    saveLoanPreference(input: $input)
      @rest(
        type: "Person"
        endpoint: "dma-pre-credit"
        path: "/approved/v1/save-loan-preference"
        method: "post"
      ) {
      status
      isBScore
      preApprovedId
    }
  }
`
const params = {
  name: 'saveLoanPreference'
}

export default {
  mutation,
  params
}
