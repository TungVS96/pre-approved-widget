import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    saveCurrentStep(input: $input)
      @rest(
        type: "Step"
        endpoint: "dma-pre-credit"
        path: "/approved/v1/save-current-step"
        method: "post"
      ) {
      status
    }
  }
`
const params = {
  name: 'saveCurrentStep'
}

export default {
  mutation,
  params
}
