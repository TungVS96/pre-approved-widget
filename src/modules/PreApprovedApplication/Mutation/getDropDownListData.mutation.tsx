import gql from 'graphql-tag'

const mutation = gql`
  mutation($input: any!) {
    getDataDropdownList(input: $input)
      @rest(
        type: "Document"
        endpoint: "dma-pre-credit"
        path: "/categories/v1/list"
        method: "post"
      ) {
      RC_LIST_ACCOM_TYPE
      RC_LIST_MARITAL_STATUS
      RC_LIST_EDU_DEGREE
      RC_LIST_TRANSPORT_TYPE
      RC_LOAN_PRODUCT_LIST
    }
  }
`
const params = {
  name: 'getDataDropdownList'
}

export default {
  mutation,
  params
}
