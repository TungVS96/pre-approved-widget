import React from 'react'
import { compose, withProps, withState } from 'recompose'
import ErorrPopup from '../components/ErorrPopup/ErorrPopup.component'

const withErrorNotification = (WrappedComponent: any) => ({
  showed,
  clearMessage,
  ...rest
}: any) => (
  <React.Fragment>
    <ErorrPopup isOpen={showed} onClose={clearMessage} />
    <WrappedComponent {...rest} />
  </React.Fragment>
)
const enhance = compose(
  withState('showed', 'setShow', false),
  withProps(({ setShow }) => {
    const showMessage = () => {
      window.scrollTo(0, 0)
      setShow(true)
    }
    const clearMessage = () => setShow(false)

    return {
      showMessage,
      clearMessage
    }
  }),
  withErrorNotification
)

export default enhance
