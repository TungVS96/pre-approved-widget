import React, { SFC } from 'react'
import { withManualUpdating as withUpdating } from 'react-web-hocs'

import { t } from '../i18n'

import LoadingMask from '../components/Loading/Loading.component'

const LoadingManualUpdating: SFC<any> = () => (
  <div
    style={{
      display: 'flex'
    }}
  >
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: '100%',
        height: '100%',
        backgroundColor: 'black',
        opacity: 0.5,
        top: 0,
        left: 0
      }}
    />
    <LoadingMask
      style={{
        flexDirection: 'column',
        color: 'white',
        height: '100%',
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0
      }}
      isSpinnerWhite={true}
    >
      {t('landing.feedbackPopup.processing.title')}
    </LoadingMask>
  </div>
)

const withManualUpdating = withUpdating({
  component: LoadingMask
})

const withManualUpdatingForFeedback = withUpdating({
  component: LoadingManualUpdating
})

export { withManualUpdating, withManualUpdatingForFeedback }
