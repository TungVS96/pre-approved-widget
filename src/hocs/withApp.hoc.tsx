import { withResponsive } from 'dma-ui'
import insertIf from 'insert-if'
import { isNonEmptyArray } from 'ramda-adjunct'
import { compose } from 'recompose'

import { withError } from './withError.hoc'
import withErrorNotification from './withErrorNotification.hoc'
import { withLoading } from './withLoading.hoc'
import withNotification from './withNotification.hoc'
import withNotificationModal from './withNotificationModal.hoc'
import { withUpdating } from './withUpdating.hoc'

interface IWithApp {
  loading?: boolean
  loadingPredicate?: Predicate
  notification?: boolean
  notificationModal?: boolean
  updates?: string[]
  dataKeys?: string[]
}

const withApp = ({
  loading = true,
  loadingPredicate,
  notification = false,
  notificationModal = false,
  updates = [],
  dataKeys = ['data']
}: IWithApp = {}) => (WrapppedComponent: any) => {
  const enhancers = [
    withResponsive,
    ...insertIf(loading, withLoading(loadingPredicate)),
    ...insertIf(isNonEmptyArray(updates), withUpdating(updates)),
    ...insertIf(notification, withNotification),
    ...insertIf(notificationModal, withNotificationModal),
    withError(dataKeys),
    withErrorNotification
  ]

  return compose(...enhancers)(WrapppedComponent)
}

export { withApp }
