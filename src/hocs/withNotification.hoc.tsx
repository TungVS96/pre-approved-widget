import React from 'react'
import { compose, withProps, withState } from 'recompose'

import Notification, {
  NotificationType
} from '../components/Notification/Notification.component'

const { ERROR, SUCCESS, CONFIRM } = NotificationType
const withNotification = (WrappedComponent: any) => ({
  notification,
  clearMessage,
  ...rest
}: any) => (
  <div>
    {notification.message && (
      <Notification {...notification} onClose={clearMessage} />
    )}
    <WrappedComponent {...rest} />
  </div>
)
const enhance = compose(
  withState('notification', 'updateNotification', {
    messageType: ERROR,
    message: ''
  }),
  withProps(({ notification, updateNotification }) => {
    const showMessage = (messageType: NotificationType) => (
      message: string
    ) => {
      window.scrollTo(0, 0)
      updateNotification({ message, messageType })
    }
    const showErrorMessage = showMessage(ERROR)
    const showSuccessMessage = showMessage(SUCCESS)
    const showConfirmMessage = showMessage(CONFIRM)
    const clearMessage = () =>
      updateNotification({ ...notification, message: '' })

    return {
      showErrorMessage,
      showSuccessMessage,
      showConfirmMessage,
      clearMessage
    }
  }),
  withNotification
)

export default enhance
