import withLifecycle from '@hocs/with-lifecycle'
import { pathOr } from 'ramda'
import { compose } from 'recompose'

import { TOKEN_KEY } from '../constants'
// import { actions } from '../modules/Auth/Auth.reducer'
// import Routes from '../modules/Navigator/Navigator.route'

// const mapDispatchToProps = {
//   logout: actions.logout
// }
const handleUpdate = (props: any, dataKeys: string[]) => {
  const { history } = props
  const error401 = [
    'missing authentication',
    'expired token',
    'blacklist_token'
  ]

  const redirectRoutes = dataKeys
    .map(
      (key: string): string => {
        const data = props[key]

        if (!data) return ''

        const statusCode = pathOr(
          '',
          ['error', 'networkError', 'statusCode'],
          data
        )
        const serverMessage = pathOr(
          '',
          ['error', 'networkError', 'result', 'message'],
          data
        ).toLowerCase()

        if (statusCode === 401 && error401.includes(serverMessage)) {
          // Token is missing/expired/invalid
          localStorage.setItem(TOKEN_KEY, '')
          alert('login')
        }

        if (statusCode === 403) {
          // return Routes.Forbidden
          alert('Forbidden')
        }

        if (statusCode < 500 && statusCode !== 404) return ''

        // return Routes.ServerError
        alert('ServerError')

        return ''
      }
    )
    .filter((x) => x !== '')

  if (!redirectRoutes.length) return

  history.push(redirectRoutes[0])
}

const withError = (dataKeys: string[] = ['data']) =>
  compose(
    // connect(
    //   null,
    //   mapDispatchToProps
    // ),
    withLifecycle({
      onDidMount(props: any) {
        handleUpdate(props, dataKeys)
      },
      onDidUpdate(prevProps: any, props: any) {
        handleUpdate(props, dataKeys)
      }
    })
  )

export { withError }
