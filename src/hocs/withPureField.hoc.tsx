import { compose, onlyUpdateForKeys } from 'recompose'

interface Configs {
  updatedKeys?: string[]
}

const withPureField = <T, P>({ updatedKeys = [] }: Configs = {}) => {
  const enhancers = [
    onlyUpdateForKeys(
      [
        'value',
        'verifyValue',
        'errorMessage',
        'hasTouched',
        'disabled',
        'items',
        'handleChange'
      ].concat(updatedKeys)
    )
  ]

  return compose<T, P>(...enhancers)
}

export { withPureField }
