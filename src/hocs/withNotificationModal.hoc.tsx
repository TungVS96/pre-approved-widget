import React from 'react'
import { compose, withProps, withState } from 'recompose'

import NotificationModal, {
  NotificationType
} from '../components/NotificationModal/NotificationModal.component'

const { ERROR } = NotificationType
const withNotificationModal = (WrappedComponent: any) => ({
  notification,
  clearMessage,
  ...rest
}: any) => {
  return (
    <div>
      {notification.message && (
        <NotificationModal {...notification} onClose={clearMessage} />
      )}
      <WrappedComponent {...rest} />
    </div>
  )
}
const enhance = compose(
  withState('notification', 'updateNotification', {
    messageType: ERROR,
    message: '',
    title: ''
  }),
  withProps(({ notification, updateNotification }) => {
    const showMessage = (messageType: NotificationType) => ({
      message,
      title
    }: {
      title: string
      message: string
    }) => {
      window.scrollTo(0, 0)
      updateNotification({ message, messageType, title })
    }
    const showErrorModal = showMessage(ERROR)
    const clearMessage = () =>
      updateNotification({ ...notification, message: '', title: '' })

    return {
      showErrorModal,
      clearMessage
    }
  }),
  withNotificationModal
)

export default enhance
