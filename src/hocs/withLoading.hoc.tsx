import { withLoadingCreator } from 'react-web-hocs'

import { NetworkStatus } from 'apollo-client'
import LoadingMask from '../components/Loading/Loading.component'

const defaultPredicate: Predicate = ({ data }) => {
  const { loading = false, networkStatus = NetworkStatus.loading } = data || {}

  return loading && networkStatus !== NetworkStatus.setVariables
}
const withLoading = (loadingPredicate?: Predicate) =>
  withLoadingCreator({
    predicate: loadingPredicate || defaultPredicate,
    component: LoadingMask
  })

export { withLoading }
