import { compose, withState, withStateHandlers } from 'recompose'

interface IPopupState {
  isPopupOpen: boolean
}
interface PopupProps extends IPopupState {
  currentPopup: string
  setPopup(name: string): void
  showPopup(): void
  hidePopup(): void
}

const withPopup = compose(
  // support multiple popups in a page
  // it's possible to reset the popup name
  // so we can show diff popups conditionallly
  withState('currentPopup', 'setPopup', ''),
  withStateHandlers(
    {
      isPopupOpen: false
    },
    {
      showPopup: (state: IPopupState) => () => ({
        isPopupOpen: true
      }),
      hidePopup: (state: IPopupState) => () => ({
        isPopupOpen: false
      })
    }
  )
)

export { withPopup, PopupProps }
