import { withUpdatingCreator } from 'react-web-hocs'

import LoadingMask from '../components/Loading/Loading.component'

const withUpdating = (updates: string[]) =>
  withUpdatingCreator({
    updates,
    component: LoadingMask
  })

export { withUpdating }
