import React from 'react'

import {
  CloseIcon,
  Container,
  Message,
  Wrap
} from './Notification.component.style'

enum NotificationType {
  ERROR = 'Error',
  SUCCESS = 'Success',
  CONFIRM = 'Confirm'
}
interface INotifcation {
  messageType: NotificationType
  message: string
  onClose(): void
}

// TODO allow do extra actions on a notification
// TODO: add animation when show/hide a notification
const Notification: React.FunctionComponent<INotifcation> = ({
  messageType,
  message,
  onClose
}) => (
  <Wrap>
    <Container messageType={messageType}>
      <Message>{message}</Message>
      <CloseIcon onClick={onClose}>
        <img src={require('./assets/ic-close.svg')} />
      </CloseIcon>
    </Container>
  </Wrap>
)

export { NotificationType, INotifcation }
export default Notification
