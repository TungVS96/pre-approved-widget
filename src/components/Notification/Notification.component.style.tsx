import { colors, rem, withClassName, B5 } from 'dma-ui'
import match from 'match-values'
import styled, { keyframes } from 'react-emotion'

import { NotificationType } from './Notification.component'

const showAnimationName = keyframes({
  from: {
    top: rem(32),
    opacity: 0
  },
  to: {
    top: rem(72),
    opacity: 1
  }
})

const Wrap = withClassName('absolute pin-l flex w-full z-20 justify-center')(
  styled('div')({
    top: rem(72),
    animation: `${showAnimationName} 800ms`
  })
)
const Container = withClassName(
  'flex justify-between items-center rounded shadow-md max-w-sm px-4 py-2'
)(
  styled('div')(
    {
      width: rem(512)
    },
    ({ messageType }: any) => {
      const bgColor = match(messageType, {
        [NotificationType.SUCCESS]: '#4BB543',
        [NotificationType.CONFIRM]: colors['grey-darkest'],
        _: colors['grey-darkest']
      })

      return {
        backgroundColor: bgColor
      }
    }
  )
)
const Message = styled(B5)({
  maxWidth: rem(290)
})
const CloseIcon = styled('a')({
  width: rem(24),
  height: rem(24)
})

export { Wrap, Container, CloseIcon, Message }
