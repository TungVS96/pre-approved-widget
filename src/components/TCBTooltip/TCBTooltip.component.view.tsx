import { rem } from 'dma-ui'
import { css } from 'emotion'
import React, { FunctionComponent } from 'react'
import OutsideClickHandler from 'react-outside-click-handler'

import { TooltipContent } from './TCBTooltip.component.style'

interface IOuterProps {
  content: string
  customClass?: any
}

interface IInnerProps {
  show: boolean
  setShow(show: boolean): void
  HANDLE_CLICK(): void
  HANDLE_CLICK_OUTSIDE(): void
}

const displayNoneClass = css({
  opacity: 0,
  zIndex: -1
})

const displayBlockClass = css({
  opacity: 1,
  zIndex: 1
})

type EnhancedProps = IOuterProps & IInnerProps

const tooltipIcon = require('./assets/ic-tooltip.svg')

const TCBTooltipView: FunctionComponent<EnhancedProps> = (props) => {
  return (
    <div className={`relative ${css({ height: 0 })} ${props.customClass}`}>
      <img
        className={`cursor-pointer ${css({
          position: 'relative',
          bottom: rem(3),
          marginLeft: rem(5)
        })}`}
        src={tooltipIcon}
        alt="tooltip"
        onClick={props.HANDLE_CLICK}
      />
      <OutsideClickHandler onOutsideClick={props.HANDLE_CLICK_OUTSIDE}>
        <TooltipContent
          className={props.show ? displayBlockClass : displayNoneClass}
        >
          {props.content}
        </TooltipContent>
      </OutsideClickHandler>
    </div>
  )
}

export { EnhancedProps, IInnerProps, IOuterProps }
export default TCBTooltipView
