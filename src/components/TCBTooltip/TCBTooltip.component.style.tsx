import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const TooltipContent = withClassName('absolute font-bold text-black-two')(
  styled('div')(({ theme }) => ({
    padding: `${rem(6)} ${rem(13)}`,
    width: `calc(100% - ${rem(13)})`,
    left: rem(25),
    top: 0,
    backgroundColor: theme.colors['grey-light'],
    borderRadius: rem(3),
    marginLeft: rem(5),
    transition: 'opacity 0.15s ease-in-out, z-index 0.15s',
    transform: 'translateY(-50%)',
    cursor: 'auto',
    fontSize: rem(12),
    [theme.mq.md]: {
      width: rem(200)
    },
    '::after': {
      content: '""',
      position: 'absolute',
      width: rem(6),
      height: rem(6),
      top: '50%',
      left: rem(-3),
      transform: 'rotate(45deg)',
      backgroundColor: theme.colors['grey-light'],
      borderBottom: `${rem(1)} solid ${theme.colors['grey-light']}`,
      borderRight: `${rem(1)} solid ${theme.colors['grey-light']}`
    }
  }))
)

export { TooltipContent }
