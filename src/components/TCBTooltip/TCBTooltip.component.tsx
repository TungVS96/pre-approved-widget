import { compose, withHandlers, withState } from 'recompose'

import TCBTooltipView, {
  IInnerProps,
  IOuterProps
} from './TCBTooltip.component.view'
import handlers from './TCBTooltip.handler'

export default compose<IInnerProps, IOuterProps>(
  withState('show', 'setShow', false),
  withHandlers(handlers)
)(TCBTooltipView)
