import { EnhancedProps } from './TCBTooltip.component.view'

export default {
  HANDLE_CLICK: (props: EnhancedProps) => () => {
    props.setShow(true)
  },
  HANDLE_CLICK_OUTSIDE: (props: EnhancedProps) => () => {
    props.setShow(false)
  }
}
