import { text, withKnobs } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'
import { colors, B1, B2, D1, H1, H2, H3, H4, H5, H6 } from 'dma-ui'
import React from 'react'

const dummyText = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'

storiesOf('Typography', module)
  .addDecorator(withKnobs)
  .add('H1', () => {
    const color = text('color', colors['soft-blue'])
    const _text = text('children', dummyText)

    return <H1 color={color}>{_text}</H1>
  })
  .add('H2', () => <H2>{dummyText}</H2>)
  .add('H3', () => <H3>{dummyText}</H3>)
  .add('H4', () => <H4>{dummyText}</H4>)
  .add('H5', () => <H5>{dummyText}</H5>)
  .add('H6', () => <H6>{dummyText}</H6>)
  .add('B1', () => <B1>{dummyText}</B1>)
  .add('B2', () => <B2>{dummyText}</B2>)
  .add('D1', () => <D1 color={colors.brand}>{dummyText}</D1>)
