import { EventHandler } from 'dma-types'
import React, { SFC } from 'react'

import {
  Banner,
  Button,
  Card,
  Image,
  ImageWrapper,
  Text,
  Title
} from './VerificationResult.component.style'
import successImg from './assets/approval.png'

interface IProps {
  success: boolean
  actionOnClick(event: EventHandler): void
  title: string
  text: string
  buttonText: string
}

const VerificationResult: SFC<IProps> = ({
  success,
  title,
  text,
  buttonText,
  actionOnClick
}) => {
  return (
    <Card>
      <Banner success={success}>
        <ImageWrapper>
          <Image inverted={!success} src={successImg} alt="successImg" />
        </ImageWrapper>
      </Banner>
      <Title>{title}</Title>
      <Text>{text}</Text>
      <Button type="button" onClick={actionOnClick}>
        {buttonText}
      </Button>
    </Card>
  )
}

export default VerificationResult
