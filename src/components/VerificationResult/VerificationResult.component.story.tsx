import { storiesOf } from '@storybook/react'
import { noop } from 'ramda-adjunct'
import React from 'react'

import { t } from '../../i18n'

import VerificationResult from './VerificationResult.component'

storiesOf('VerificationResult', module)
  .add('successfull', () => (
    <VerificationResult
      success={true}
      actionOnClick={noop}
      title={t('verificationResult.component.existingcustomer.title')}
      text={t('verificationResult.component.existingcustomer.text')}
      buttonText={t('verificationResult.component.story.successButton')}
    />
  ))
  .add('unsuccessful', () => (
    <VerificationResult
      success={false}
      actionOnClick={noop}
      title={t('unsuccessVerification.component.title')}
      text={t('unsuccessVerification.component.text')}
      buttonText={t('verificationResult.component.story.successButton')}
    />
  ))
