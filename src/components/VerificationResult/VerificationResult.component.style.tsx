import { rem, withClassName, H2 } from 'dma-ui'
import styled from 'react-emotion'

const Card = withClassName('text-white bg-white')(
  styled('div')({
    width: rem(448),
    height: rem(600),
    boxShadow: `0 ${rem(3)} ${rem(12)} 0 rgba(7, 42, 68, 0.1)`,
    alignSelf: 'center'
  })
)

const Title = styled(H2)({
  fontSize: rem(24),
  paddingTop: rem(23),
  paddingLeft: rem(24)
})

const Text = withClassName('text-grey-darkest')(
  styled('p')({
    fontStyle: 'italic',
    fontSize: rem(13),
    lineHeight: rem(24),
    paddingTop: rem(23),
    padding: `${rem(16)} ${rem(24)}`
  })
)

const Button = withClassName('font-bold text-center text-white bg-brand')(
  styled('button')({
    width: rem(320),
    fontSize: rem(16),
    border: `solid ${rem(1)}`,
    marginLeft: rem(64),
    marginTop: rem(84),
    padding: `${rem(15)} ${rem(24)}`
  })
)

const ImageWrapper = withClassName(
  'flex justify-center text-white items-center rounded-full'
)(
  styled('div')(
    {
      height: rem(198),
      width: rem(198),
      margin: `${rem(34)} ${rem(100)}`
    },
    ({ theme }) => ({
      border: `${rem(2)} solid ${theme.colors.white}`
    })
  )
)

const Image = withClassName('')(
  styled('img')(
    {
      height: rem(72),
      width: rem(72)
    },
    ({ inverted }: { inverted: boolean }) => ({
      transform: inverted ? 'rotate(180deg)' : ''
    })
  )
)

const Banner = withClassName('text-center text-white')(
  styled('div')(
    {
      width: rem(448),
      height: rem(300),
      border: `solid ${rem(1)}`,
      padding: `${rem(15)} ${rem(24)}`
    },
    ({ theme, success }: any) => ({
      backgroundColor: success ? theme.colors.brand : '#9fafc3'
    })
  )
)

export { Title, Text, Card, Button, Image, Banner, ImageWrapper }
