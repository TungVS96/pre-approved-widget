import React from 'react'

import {
  ClostBtn,
  Content,
  IconRequest,
  OpenBtn,
  TcbHelpContainer,
  Text
} from './TcbHelp.style'

interface IInnerProps {
  displayTcbHelp: boolean
  setDisplayTcbHelp(value: boolean): void
  className: string
  classLabel?: string
  id?: string
}

interface IOuterProps {
  contentText: string
  isShow: boolean
  label: string
}

type EnhancedProps = IInnerProps & IOuterProps

const helpCloseOutline = require('./assets/close-outline.png')
const helpCircleOutline = require('./assets/help-circle-outline.png')
const helpCircle = require('./assets/help-circle.png')

const TcbHelp: React.FunctionComponent<EnhancedProps> = (
  props: EnhancedProps
) => {
  const { contentText, isShow, label, className, classLabel } = props

  return (
    <TcbHelpContainer>
      <div className={className} style={{ display: 'flex' }}>
        {label && (
          <span
            className={classLabel ? classLabel : ''}
            style={{ fontSize: '13px', position: 'relative' }}
          >
            {label}
            <OpenBtn
              id={props.id}
              src={helpCircle}
              onMouseOver={() => props.setDisplayTcbHelp(true)}
              onMouseOut={() => props.setDisplayTcbHelp(false)}
            />
          </span>
        )}
      </div>
      {(props.displayTcbHelp || isShow) && (
        <Content>
          <IconRequest src={helpCircleOutline} />
          <Text>{contentText}</Text>
          <ClostBtn
            src={helpCloseOutline}
            onClick={() => props.setDisplayTcbHelp(false)}
          />
        </Content>
      )}
    </TcbHelpContainer>
  )
}

export default TcbHelp
