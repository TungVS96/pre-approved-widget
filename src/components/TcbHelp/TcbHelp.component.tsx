import { compose, withState } from 'recompose'
import { withPureField } from '../../hocs'
import TcbHelp from './TcbHelp.view'

const enhancer = compose<any, any>(
  withState('displayTcbHelp', 'setDisplayTcbHelp', false),
  withPureField({
    updatedKeys: ['displayTcbHelp', 'isShow']
  })
)

export default enhancer(TcbHelp)
