import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const TcbHelpContainer = withClassName('')(styled('div')(() => ({})))

const Content = withClassName('flex w-full p-2')(
  styled('div')(() => ({
    background: '#F0FAFF',
    borderRadius: '2px',
    position: 'relative',
    marginTop: rem(6)
  }))
)

const ClostBtn = withClassName('')(
  styled('img')(() => ({
    position: 'absolute',
    right: '10px',
    top: '10px',
    width: '12px',
    height: '12px'
  }))
)

const OpenBtn = withClassName('')(
  styled('img')(({ theme }) => ({
    width: '16px',
    height: '16px',
    position: 'absolute',
    [theme.mq.sm]: {
      marginLeft: '1px'
    },
    [theme.mq.md]: {
      marginLeft: '8px'
    }
  }))
)

const IconRequest = withClassName('ml-2 sm:hidden md:block')(
  styled('img')(() => ({
    width: '18px',
    height: '18px'
  }))
)

const Text = withClassName('ml-2 mr-4')(styled('div')(() => ({})))

export { TcbHelpContainer, Content, ClostBtn, OpenBtn, IconRequest, Text }
