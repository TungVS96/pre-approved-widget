import { Classes, Tooltip } from '@blueprintjs/core'
import { theme } from 'dma-ui'
import { css, cx } from 'emotion'
import React, { FunctionComponent } from 'react'

interface IProps {
  content?: string
  className?: string
  icon?: string
  position?: any
  sizeIcon?: number
}

const popoverClassNames = cx(
  'italic text-xs',
  css({
    [`.${Classes.POPOVER_CONTENT}`]: {
      backgroundColor: theme.colors['grey-darkest'],
      color: theme.colors.white
    },
    [`.${Classes.POPOVER_ARROW}-fill`]: {
      fill: theme.colors['grey-darkest']
    }
  })
)

const TooltipComponent: FunctionComponent<IProps> = ({
  content,
  className,
  position = 'top-right',
  icon = require('./assets/ic-tooltip.svg'),
  sizeIcon = 24
}) => (
  <Tooltip
    className={cx(
      Classes.POPOVER_CONTENT_SIZING,
      css({
        [`.${Classes.POPOVER_TARGET}`]: {
          display: 'initial',
          height: 24,
          marginLeft: 5
          // position: 'absolute'
        }
      })
    )}
    popoverClassName={popoverClassNames}
    content={content}
    usePortal={false}
    captureDismiss={true}
    position={position}
  >
    <img src={icon} width={sizeIcon} height={sizeIcon} className={className} />
  </Tooltip>
)

export default TooltipComponent
