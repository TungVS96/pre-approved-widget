import React, { FunctionComponent } from 'react'
import {
  Container,
  DeleteIcon,
  ImageContainer,
  PDFContainer
} from '../ImageThumbnail/ImageThumbnail.view.style'

interface IProps {
  url: string
  className?: string
  onClick(): void
  onDelete?(): void
  fileExtension: string
  isDelete?: boolean
  title?: string
}

const imageIc = require('./assets/ic-img.svg')

const renderDeleteIcon = (props: IProps) =>
  props.onDelete && (
    <DeleteIcon
      src={require('./assets/ic-delete.svg')}
      width={18}
      height={18}
      onClick={props.onDelete}
    />
  )

const ImageThumbnail: FunctionComponent<IProps> = (props) => {
  const { url, className, fileExtension = '', isDelete = true, title } = props
  const isPDF = fileExtension === 'pdf'

  if (isPDF) {
    return (
      <PDFContainer
        className={`${className} ${title ? 'viewRegisteringDocument' : ''}`}
        id={title || undefined}
      >
        <img
          src={require('./assets/ic-pdf.svg')}
          width={24}
          height={24}
          onClick={props.onClick}
        />
        {isDelete && renderDeleteIcon(props)}
      </PDFContainer>
    )
  }

  return (
    <Container>
      {isDelete && renderDeleteIcon(props)}
      <ImageContainer
        src={url ? url : imageIc}
        onClick={props.onClick}
        className={`${className} ${title ? 'viewRegisteringDocument' : ''}`}
        id={title || undefined}
      />
    </Container>
  )
}

export default ImageThumbnail
