import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const DeleteIcon = withClassName('absolute', 'DeleteIcon')(
  styled('img')({
    zIndex: 1,
    right: -9,
    top: -9
  })
)

const PDFContainer = withClassName(
  'relative cursor-pointer flex items-center justify-center bg-grey-lightest',
  'ImageContainer'
)(
  styled('div')(({ theme }: { theme: any }) => ({
    width: rem(40),
    height: rem(40),
    borderRadius: 4,
    border: `solid 1px ${theme.colors['grey-light']}`
  }))
)

const Container = withClassName('relative')(styled('div')(() => ({})))

const ImageContainer = withClassName(
  'relative cursor-pointer',
  'ImageContainer'
)(
  styled('div')(({ src }: { src: string }) => ({
    minWidth: rem(40),
    minHeight: rem(40),
    borderRadius: 4,
    backgroundImage: `url(${src})`,
    backgroundSize: 'cover'
  }))
)

export { DeleteIcon, PDFContainer, ImageContainer, Container }
