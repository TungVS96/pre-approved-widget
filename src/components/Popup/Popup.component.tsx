import { Overlay } from '@blueprintjs/core'
import React, { SFC } from 'react'

import {
  Button,
  ButtonClose,
  Container,
  Content,
  Subtitle,
  Title
} from './Popup.component.style'

enum PopupTheme {
  LIGHT = 'LIGHT',
  BRAND = 'BRAND'
}
interface ButtonType {
  title: string
  color?: string
  submit?: boolean
  onClick?(): void
}

interface IProps {
  name: string
  title: string
  message?: string
  heading?: React.ReactElement<any>
  contentStyle?: React.CSSProperties
  containerStyle?: React.CSSProperties
  isOpen: boolean
  theme?: PopupTheme
  buttons?: ButtonType[]
  onClose(): void
  canClose?: boolean
  width?: number | string
  height?: number | string
  backgroundColor?: boolean
  isPopupFeedback?: boolean
  isRegistration?: boolean
}

const Popup: SFC<IProps> = (props) => {
  const {
    canClose = true,
    contentStyle,
    containerStyle,
    isOpen,
    onClose,
    title,
    message,
    children,
    heading,
    buttons,
    backgroundColor,
    isPopupFeedback = false,
    width = 400,
    height = 200,
    isRegistration
  } = props

  return isPopupFeedback ? (
    <Container id="feedbackPopUp" style={containerStyle}>
      <Content width={width} height={height} style={contentStyle}>
        {canClose && (
          <ButtonClose
            onClick={onClose}
            style={{
              backgroundImage: `url(${require('./assets/ic-close.svg')})`
            }}
          />
        )}
        <>
          {heading}
          <Title>{title}</Title>
          {message && <Subtitle>{message}</Subtitle>}
          {children}
          {buttons && (
            <div className="flex justify-center mt-6">
              {buttons.map((item) => (
                <Button
                  type={item.submit ? 'submit' : ''}
                  key={item.title}
                  color={item.color}
                  backgroundColor={backgroundColor}
                  onClick={item.onClick}
                >
                  {item.title}
                </Button>
              ))}
            </div>
          )}
        </>
      </Content>
    </Container>
  ) : isRegistration ? (
    <Overlay isOpen={isOpen}>
      <Container id="feedbackPopup" style={containerStyle}>
        <Content width={width} height={height} style={contentStyle}>
          {canClose && (
            <ButtonClose
              onClick={onClose}
              style={{
                backgroundImage: `url(${require('./assets/ic-close.svg')})`
              }}
            />
          )}
          <>
            {heading}
            <h2 className={'font-bold mb-6 text-center text-brand'}>{title}</h2>
            <p
              className={'text-grey-darkest text-center'}
              style={{ whiteSpace: 'pre-line' }}
            >
              {message}
            </p>
            {children}
            {buttons && (
              <div className="flex justify-center mt-3">
                {buttons.map((item) => (
                  <Button
                    type={item.submit ? 'submit' : ''}
                    key={item.title}
                    color={item.color}
                    backgroundColor={backgroundColor}
                    onClick={item.onClick}
                  >
                    {item.title}
                  </Button>
                ))}
              </div>
            )}
          </>
        </Content>
      </Container>
    </Overlay>
  ) : (
    <Overlay isOpen={isOpen}>
      <Container id="feedbackPopup" style={containerStyle}>
        <Content width={width} height={height} style={contentStyle}>
          {canClose && (
            <ButtonClose
              onClick={onClose}
              style={{
                backgroundImage: `url(${require('./assets/ic-close.svg')})`
              }}
            />
          )}
          <>
            {heading}
            <Title>{title}</Title>
            {message && <Subtitle>{message}</Subtitle>}
            {children}
            {buttons && (
              <div className="flex justify-center mt-6">
                {buttons.map((item) => (
                  <Button
                    type={item.submit ? 'submit' : ''}
                    key={item.title}
                    color={item.color}
                    backgroundColor={backgroundColor}
                    onClick={item.onClick}
                  >
                    {item.title}
                  </Button>
                ))}
              </div>
            )}
          </>
        </Content>
      </Container>
    </Overlay>
  )
}

export { PopupTheme }
export default Popup
