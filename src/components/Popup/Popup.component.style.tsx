import { colors, rem, withClassName, H2 } from 'dma-ui'
import styled from 'react-emotion'

const Container = withClassName('flex w-full h-full justify-center')(
  styled('div')(({ theme }: { theme: any }) => ({
    alignItems: 'flex-start',
    overflowY: 'scroll',
    paddingTop: rem(50),
    paddingBottom: rem(50),
    [theme.mq.md]: {
      alignItems: 'center'
    }
  }))
)

const Content = withClassName('relative bg-white px-6 pt-10 pb-5')(
  styled('div')(({ width, height }: any) => ({
    width,
    height,
    marginRight: rem(16),
    marginLeft: rem(16),
    borderRadius: rem(5)
  }))
)
const Title = withClassName('font-bold mb-6')(H2)
const Subtitle = withClassName('text-grey-darkest')(
  styled('p')({
    fontSize: rem(13),
    textAlign: 'justify'
  })
)
const ButtonClose = withClassName('absolute')(
  styled('button')({
    height: rem(36),
    width: rem(36),
    top: rem(-16),
    right: rem(-16),
    borderRadius: rem(18),
    backgroundSize: 'cover',
    backgroundColor: '#fff',
    zIndex: 1,
    ':focus': {
      outline: 'none'
    }
  })
)
const Button = withClassName(
  'text-sm font-bold uppercase px-5 pt-4 pb-4 w-full'
)(
  styled('button')(
    ({
      color = colors['grey-darkest'],
      backgroundColor = false,
      type
    }: any) => ({
      color,
      backgroundColor: backgroundColor ? colors.brand : '',
      alignContent: 'flex-end',
      ...(type === 'submit' && {
        color: colors.brand
      })
    })
  )
)

export { Button, Container, Content, Title, Subtitle, ButtonClose }
