import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const Container = withClassName('flex w-full h-full justify-center')(
  styled('div')({
    alignItems: 'center'
  })
)

const Wrapper = withClassName('relative bg-white')(
  styled('div')(({ width = 343, height, theme }: any) => ({
    height,
    borderRadius: rem(2),
    width: '95%',
    maxHeight: `calc(100vh - ${rem(32)})`,
    padding: 'auto',
    overflowY: 'auto',
    [theme.mq.md]: {
      width,
      height
    }
  }))
)

const DeleteButtonContainer = withClassName('absolute cursor-pointer')(
  styled('div')({
    top: rem(14),
    right: rem(14)
  })
)

const Text = withClassName('text-sm')(
  styled('p')({
    color: '#1D1D1F',
    lineHeight: rem(20),
    marginBottom: rem(35),
    marginTop: rem(24)
  })
)

const ContentWrapper = withClassName('text-center')(
  styled('div')({
    marginTop: rem(25)
  })
)

export { Container, DeleteButtonContainer, Wrapper, Text, ContentWrapper }
