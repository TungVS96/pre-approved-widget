import { Overlay } from '@blueprintjs/core'
import React from 'react'
import {
  Container,
  ContentWrapper,
  DeleteButtonContainer,
  Text,
  Wrapper
} from './ErorrPopup.component.style'

interface IOutterProps {
  isOpen: boolean
  onClose(): void
}

const icClose = require('./assets/ic-close.svg')
const icError = require('./assets/ic-error.svg')
const ErorrPopup: React.FunctionComponent<IOutterProps> = ({
  onClose,
  isOpen
}) => (
  <Overlay isOpen={isOpen}>
    <Container>
      <Wrapper>
        <DeleteButtonContainer>
          <img onClick={onClose} src={icClose} width={12} height={12} />
        </DeleteButtonContainer>
        <ContentWrapper>
          <img src={icError} width={134} height={140} />
          <Text>
            Có lỗi xảy ra khi lưu thông tin
            <br />
            Vui lòng thử lại sau
          </Text>
        </ContentWrapper>
      </Wrapper>
    </Container>
  </Overlay>
)

export default ErorrPopup
