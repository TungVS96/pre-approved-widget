import { Overlay } from '@blueprintjs/core'
import React, { SFC } from 'react'
import {
  Button,
  Container,
  ContentWrapper,
  Image,
  Row,
  Title,
  TitleContainer,
  Wrapper
} from './Modal.component.style'

interface ButtonType {
  title: string
  color?: string
  submit?: boolean
  onClick?(): void
  id?: string
}

interface IProps {
  name: string
  title: string
  rightContent?: React.ReactNode
  contentStyle?: React.CSSProperties
  containerStyle?: React.CSSProperties
  isOpen: boolean
  buttons?: ButtonType[]
  onClose?(): void
  width?: number | string
  height?: number | string
  backgroundColor?: boolean
  type?: 'success' | 'warning' | 'error' | 'info' | ''
  header?: React.ReactNode
  id?: string
}

const getIcon = (type: string) => {
  switch (type) {
    case 'success':
      return {
        src: require('./assets/ic-success.svg')
      }
    case 'warning':
      return {
        src: require('./assets/ic-warning.svg')
      }
    case 'info':
      return {
        src: require('./assets/ic-info.svg')
      }
    default:
      return {
        src: require('./assets/ic-error.svg')
      }
  }
}

const Modal: SFC<IProps> = (props) => {
  const {
    contentStyle,
    containerStyle,
    isOpen,
    onClose,
    title,
    children,
    buttons,
    backgroundColor,
    width = 400,
    height,
    type = '',
    rightContent = '',
    header = '',
    id = ''
  } = props

  const icon = getIcon(type)

  return (
    <Overlay isOpen={isOpen}>
      <Container style={containerStyle} id={id}>
        <Wrapper width={width} height={height} style={contentStyle}>
          <TitleContainer>
            <Title>{title}</Title>
            {onClose && (
              <Image
                onClick={onClose}
                src={require('./assets/ic-close.svg')}
                width={16}
                height={16}
              />
            )}
          </TitleContainer>
          <>
            <ContentWrapper>
              {header}
              {type === '' ? (
                children
              ) : (
                <>
                  <Row>
                    <img
                      src={icon.src}
                      width={'100%'}
                      style={{ flex: 1, paddingRight: '16px' }}
                    />
                    <div style={{ flex: 3 }}>{rightContent}</div>
                  </Row>
                  {children}
                </>
              )}
            </ContentWrapper>
            {buttons && (
              <div className="flex justify-center">
                {buttons.map((item) => (
                  <Button
                    type={item.submit ? 'submit' : ''}
                    key={item.title}
                    color={item.color}
                    backgroundColor={backgroundColor}
                    onClick={item.onClick}
                  >
                    {item.title}
                  </Button>
                ))}
              </div>
            )}
          </>
        </Wrapper>
      </Container>
    </Overlay>
  )
}

export default Modal
