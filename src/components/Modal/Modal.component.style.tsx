import { colors, rem, withClassName, H4 } from 'dma-ui'
import styled from 'react-emotion'

const Container = withClassName('flex w-full h-full justify-center')(
  styled('div')({
    alignItems: 'center',
    fontFamily: 'Helvetica !important'
  })
)

const Wrapper = withClassName('relative bg-white')(
  styled('div')(({ width, height, theme }: any) => ({
    height,
    borderRadius: rem(5),
    width: '95%',
    maxHeight: `calc(100vh - ${rem(32)})`,
    padding: 'auto',
    overflowY: 'auto',
    [theme.mq.md]: {
      width,
      height
    }
  }))
)

const TitleContainer = withClassName(
  'flex flex-row justify-between items-center'
)(
  styled('div')(({ theme }: any) => ({
    borderBottom: `${rem(1)} solid ${theme.colors['grey-light']}`,
    padding: rem(16)
  }))
)

const Title = withClassName('font-bold')(H4)

const Image = withClassName('')(styled('img')())

const ContentWrapper = withClassName('')(styled('div')())

const Button = withClassName(
  'text-sm font-bold uppercase px-5 pt-4 pb-4 w-full'
)(
  styled('button')(
    ({
      color = colors['grey-darkest'],
      backgroundColor = false,
      type
    }: any) => ({
      color,
      margin: `0 ${rem(16)} ${rem(16)} ${rem(16)}`,
      backgroundColor: backgroundColor ? colors.brand : '',
      borderRadius: rem(5),
      alignContent: 'flex-end',
      ...(type === 'submit' && {
        color: colors.brand
      })
    })
  )
)

const Row = withClassName('flex flex-row')(
  styled('div')({
    padding: rem(16),
    alignItems: 'center'
  })
)

export {
  Button,
  Container,
  TitleContainer,
  Wrapper,
  Title,
  Image,
  ContentWrapper,
  Row
}
