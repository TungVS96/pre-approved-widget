import { rem, withClassName, H4 } from 'dma-ui'
import styled from 'react-emotion'

const Container = withClassName('flex w-full h-full justify-center')(
  styled('div')({
    alignItems: 'center'
  })
)

const Wrapper = withClassName('relative bg-white')(
  styled('div')(({ width = 400, height, theme }: any) => ({
    height,
    borderRadius: rem(5),
    width: '95%',
    maxHeight: `calc(100vh - ${rem(32)})`,
    padding: 'auto',
    overflowY: 'auto',
    [theme.mq.md]: {
      width,
      height
    }
  }))
)

const TitleContainer = withClassName(
  'flex flex-row justify-between items-center'
)(
  styled('div')(({ theme }: any) => ({
    borderBottom: `${rem(1)} solid ${theme.colors['grey-light']}`,
    padding: rem(16)
  }))
)

const Title = withClassName('font-bold')(H4)

const Image = withClassName('')(styled('img')())

const ContentWrapper = withClassName('')(styled('div')())

const Row = withClassName('flex flex-row')(
  styled('div')({
    padding: rem(16),
    alignItems: 'center'
  })
)

const Button = withClassName('shadow text-base text-center')(
  styled('button')(({ theme }: { theme: any }) => ({
    width: '100%',
    height: rem(48),
    backgroundColor: theme.colors.brand,
    color: theme.colors.white,
    cursor: 'pointer',
    textTransform: 'uppercase',
    borderRadius: rem(5),
    margin: rem(16)
  }))
)

export {
  Button,
  Container,
  TitleContainer,
  Wrapper,
  Title,
  Image,
  ContentWrapper,
  Row
}
