import { Overlay } from '@blueprintjs/core'
import { match } from 'match-values'
import React from 'react'
import {
  Button,
  Container,
  ContentWrapper,
  Image,
  Row,
  Title,
  TitleContainer,
  Wrapper
} from './NotificationModal.component.style'

enum NotificationType {
  ERROR = 'Error',
  SUCCESS = 'Success',
  WARNING = 'Warning'
}
interface INotifcation {
  messageType: NotificationType
  title: string
  message: string
  onClose(): void
}

const getIcon = (type: NotificationType) => {
  return match(type, {
    [NotificationType.ERROR]: require('./assets/ic-error.svg'),
    [NotificationType.SUCCESS]: require('./assets/ic-success.svg'),
    [NotificationType.WARNING]: require('./assets/ic-warning.svg')
  })
}

const NotificationModal: React.FunctionComponent<INotifcation> = ({
  messageType,
  title,
  message,
  onClose
}) => (
  <Overlay isOpen={true}>
    <Container>
      <Wrapper>
        <TitleContainer>
          <Title>{title}</Title>
          {onClose && (
            <Image
              onClick={onClose}
              src={require('./assets/ic-close.svg')}
              width={16}
              height={16}
            />
          )}
        </TitleContainer>
        <ContentWrapper>
          <Row>
            <img
              src={getIcon(messageType)}
              width={'100%'}
              style={{ flex: 1, paddingRight: '16px' }}
            />
            <div style={{ flex: 3 }}>{message}</div>
          </Row>
        </ContentWrapper>
        <div className="flex justify-center">
          <Button onClick={onClose} type="button">
            Đóng
          </Button>
        </div>
      </Wrapper>
    </Container>
  </Overlay>
)

export { NotificationType, INotifcation }
export default NotificationModal
