import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'
import { ApolloLink } from 'apollo-link'
import { setContext } from 'apollo-link-context'
import { RestLink } from 'apollo-link-rest'
import { theme } from 'dma-ui'
import { ThemeProvider } from 'emotion-theming'
import React from 'react'
import { ApolloProvider } from 'react-apollo'
import { TOKEN_KEY } from './constants'
import PreApprovedApplicationOverview from './modules/PreApprovedApplication/PreApprovedApplication.page'
const TOKEN = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IjA5MDExMTExMTEiLCJJRCI6NDgsInJvbGUiOlsiQ1VTVE9NRVIiXSwiaWF0IjoxNjEyMTcwNTU1LCJleHAiOjE2MTIyNTY5NTV9.wt8NfrWRmhMQOsTCkxAQc82Fl9EcHS8KHNu_0Zk4h14'
const authLink = setContext((_, { headers }) => {
  localStorage.setItem(TOKEN_KEY, TOKEN)
  const token = localStorage.getItem(TOKEN_KEY) || ''

  return {
    headers: {
      ...headers,
      authorization: token
    }
  }
})
const restLink = new RestLink({
  uri:
    process.env.REACT_APP_REST_ENDPOINT,
  headers: {
    'Content-Type': 'application/json'
  },
  endpoints: {
    'dma-pre-credit': process.env.REACT_APP_REST_ENDPOINT_DMA_PRE_CREDIT || ''
  },
  credentials: 'same-origin'
})
const client = new ApolloClient({
  link: ApolloLink.from([authLink, restLink]),
  cache: new InMemoryCache()
})

const fetchPolicy = 'network-only'

client.defaultOptions = {
  watchQuery: {
    fetchPolicy,
    errorPolicy: 'ignore'
  },
  query: {
    fetchPolicy,
    errorPolicy: 'all'
  }
}

const App = () => {

  return (
    <ApolloProvider client={client}>
      <ThemeProvider theme={theme}>
        <PreApprovedApplicationOverview />
      </ThemeProvider>
    </ApolloProvider>
  )
}

export default App
