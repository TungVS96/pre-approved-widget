import {
  compose,
  filter,
  join,
  prop,
  range,
  sortBy,
  toLower
} from 'ramda'
import { isNonEmptyString } from 'ramda-adjunct'
import { IDropDownItem } from '../types'
import { toStr } from './string.util'

const getNameFromValue = (data: any, value: any) => {
  const result = data.find((item: any) => item.value === value)
  if (result) return result.name
  if (parseInt(value, 10) === -1) return

  return value
}

const getValueFromName = (data: any, name: any) => {
  const result = data.find((item: any) => item.name === name)
  if (result) return result.value

  return name
}

const sortByName = (arr: IDropDownItem[]): IDropDownItem[] => {
  return sortBy(
    compose(
      toLower,
      prop('name')
    )
  )(arr)
}

const joinNonEmptyString = (list: any[], separator = '-') => {
  return compose(
    join(separator),
    filter(isNonEmptyString)
  )(list)
}

const generateKeyValue = (min: number, max: number) => {
  return range(min, max + 1).map((item) => ({
    name: item.toString(),
    value: item.toString()
  }))
}

const getName = (items: IDropDownItem[], value: string) => {
  if (!items) return ''

  const foundItem = items.find((item) => toStr(item.value) === value)

  return foundItem ? foundItem.name : ''
}

const getValue = (items: IDropDownItem[], value: string) => {
  if (!items) return ''

  const foundItem = items.find((item) => toStr(item.value) === value)

  return foundItem ? foundItem.value : ''
}

export {
  getNameFromValue,
  getValueFromName,
  sortByName,
  joinNonEmptyString,
  getName,
  getValue,
  generateKeyValue
}
