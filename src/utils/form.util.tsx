import { getIn } from 'formik'
import { compose, isEmpty, keys, propOr, reduce } from 'ramda'
import { getFieldName } from '../forms/VerifyControls/VerifyControls.component'

interface GetFieldConfigs {
  keepValues?: boolean
  canVerify?: boolean
}
interface GetFieldOutput {
  name: string
  values?: object
  value: string
  verifyValue?: object
  canVerify?: any
  hasTouched: boolean
  errorMessage: string
  handleChange(e: any): void
  handleBlur(e: any): void
  setFieldValue(key: string, value: any): void
  setFieldTouched(name: string, touched: boolean): void
}

const resetNullValues = (obj: any): any =>
  compose(
    reduce((acc, key: string) => {
      acc[key] = obj[key] === null ? '' : obj[key]

      return acc
    }, {}),
    keys
  )(obj)

const getFieldProps = (
  name: string,
  props: any,
  configs?: GetFieldConfigs
): GetFieldOutput => {
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    setFieldTouched
  } = props
  const keepValues = propOr(true, 'keepValues', configs)
  const canVerify = propOr(false, 'canVerify', configs)
  const fieldName = getFieldName({ name, canVerify })

  return {
    name,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    value: !isEmpty(getIn(values, fieldName)) ? getIn(values, fieldName) : '',
    hasTouched: getIn(touched, fieldName),
    errorMessage: getIn(errors, fieldName),
    // optional props
    ...(keepValues && { values }),
    ...(canVerify && {
      canVerify,
      verifyValue: getIn(values, name)
    })
  }
}

const getPositiveNumberOnly = (value: string = ''): string =>
  value.toString().replace(/[^\d.]+/g, '')

const getTrimValue = (value: string = ''): string => value.trim()

export { resetNullValues, getFieldProps, getPositiveNumberOnly, getTrimValue }
