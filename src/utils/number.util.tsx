
const parseIntMoney = (value: string) => {
  if (typeof value === 'string') {
    return parseInt(
      (value || '').replace(/,/g, '').replace(/[^0-9,.]+/g, ''),
      10
    )
  }

  if (typeof value === 'number') {
    return parseInt(value || '0', 10)
  }

  return value
}

export { parseIntMoney }
