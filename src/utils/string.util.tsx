import { compose, defaultTo, is, toString } from 'ramda'

const toStr = (s: any): string => {
  const _toString = (_s: any): string => {
    if (is(String, _s)) return _s.toString()

    return toString(_s)
  }

  return compose(
    _toString,
    // @ts-ignore
    defaultTo('')
  )(s)
}

const getFontFamily = () => {
  return 'Roboto, Segoe UI Regular Vietnamese, Segoe UI, Segoe WP, Tahoma, Arial, sans-serif'
}

const toSlug = (value: string, div: string = '-'): string =>
  value
    .trim()
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/[đĐ]/g, 'd')
    .replace(/([^0-9a-z-A-Z-\s])/g, '')
    .replace(/[\s]/g, div)
    .replace(new RegExp(`/${div}+/g`), div)
    .replace(new RegExp(`/^${div}+|${div}+$/g`), '')
    .toLowerCase()

export { toStr, getFontFamily, toSlug }
