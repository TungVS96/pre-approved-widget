// import vi from 'date-fns/locale/vi'
import { format, isValid, parse } from 'date-fns'
import en from 'date-fns/locale/en-US/index'
import vi from 'date-fns/locale/vi/index'

import { DATE_FORMAT, DATE_FORMAT_VN } from '../constants'

const formatDate = (
  date: Date | string,
  formatString?: string,
  locale: string = 'vi'
) => {
  const getFormat = () => {
    if (formatString) return formatString

    return locale === 'vi' ? DATE_FORMAT_VN : DATE_FORMAT
  }

  return format(date, getFormat(), {
    locale: locale === 'vi' ? vi : en,
    awareOfUnicodeTokens: true
  })
}

const isISODateString = (dateISOString: string) => {
  const regex = /^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(.[0-9]+)?(Z)?$/

  return regex.test(dateISOString)
}

const toDateISO = (dateString: string, formatString: string = DATE_FORMAT) => {
  const date = parse(dateString, formatString, new Date(), {
    locale: vi
  })
  if (isValid(date)) {
    return date.toISOString()
  }

  return dateString
}

export { formatDate, isISODateString, toDateISO }
