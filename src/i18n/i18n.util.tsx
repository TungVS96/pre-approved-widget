import * as TYPES from 'dma-types'
import * as R from 'ramda'
import { isNonEmptyArray } from 'ramda-adjunct'

import * as CURRENT_TYPES from '../types'
import { sortByName } from '../utils'
import { t } from './index'

const translateDropdown = (
  dropdownName: string,
  prefix: string | null = '',
  options: { sorted?: boolean } = {}
): CURRENT_TYPES.IDropDownItem[] => {
  const { sorted = true } = options
  const list = (TYPES[dropdownName] || CURRENT_TYPES[dropdownName]) as any
  const sortFn = sorted ? sortByName : R.identity
  const prefixName = prefix || dropdownName
  if (!list) {
    // tslint:disable-next-line
    console.error(`the dropdown ${dropdownName} does not exist in dma-types`)

    return []
  }

  if (isNonEmptyArray(list)) {
    return R.compose(
      sortFn,
      R.map((x: any) => {
        const value = x.value.trim()
        const i18nKey = `TYPES.${prefixName}.${value}`

        return {
          value,
          name: t(i18nKey)
        }
      })
    )(list)
  }

  return R.compose(
    sortFn,
    R.map(
      (key: string): CURRENT_TYPES.IDropDownItem => {
        const value = list[key].trim()
        const i18nKey = `TYPES.${prefixName}.${key}`

        return {
          value,
          name: t(i18nKey)
        }
      }
    ),
    R.keys
  )(list)
}

export { translateDropdown }
