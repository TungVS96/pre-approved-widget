import { joinNonEmptyString } from '../utils'
export * from './dropdown.constant'

const APP_KEY = 'dma-widget'
const INTEREST_RATE = 0.11
const DATE_FORMAT = 'dd/MM/yyyy'
const DATE_FORMAT_VN = 'dd/MM/yyyy'
const DATE_TIME_FORMAT = 'dd MMMM yyyy, hh:mm a'
const DATE_TIME_FORMAT_VN = "dd MMMM 'năm' yyyy, hh:mm a"
const sizeLimitUpload = 10485760

const TOKEN_KEY = joinNonEmptyString(
  [APP_KEY, process.env.REACT_APP_ENV, 'token'],
  '-'
)
const CLIENT_ID = '30985363837-kagbnc6vpu3t23468bec4ggo9rq1cksi.apps.googleusercontent.com';
const USER_PROFILE_KEY = joinNonEmptyString(
  [APP_KEY, 'user', 'profile'],
  '-'
)

export {
  DATE_FORMAT,
  DATE_FORMAT_VN,
  DATE_TIME_FORMAT,
  DATE_TIME_FORMAT_VN,
  INTEREST_RATE,
  APP_KEY,
  sizeLimitUpload,
  TOKEN_KEY,
  CLIENT_ID,
  USER_PROFILE_KEY
}
