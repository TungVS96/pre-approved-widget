import { translateDropdown } from '../i18n/i18n.util'
import { generateKeyValue } from '../utils'

const LIST_OF_NATIONAL_ID_TYPES = translateDropdown(
  'NATIONAL_IDPASSPORT_NO_TYPE'
)
const LIST_OF_TIME_LIVING_IN_CURRENT_ADDRESS_MONTH = generateKeyValue(0, 11)
const LIST_OF_TIME_LIVING_IN_CURRENT_ADDRESS_YEAR = generateKeyValue(0, 100)

export {
  LIST_OF_NATIONAL_ID_TYPES,
  LIST_OF_TIME_LIVING_IN_CURRENT_ADDRESS_MONTH,
  LIST_OF_TIME_LIVING_IN_CURRENT_ADDRESS_YEAR,
}
