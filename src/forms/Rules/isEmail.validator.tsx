import { isNil } from 'ramda'
import { isNonEmptyString } from 'ramda-adjunct'

import { t } from '../../i18n'

const isEmail = (errorKey: string) => ({ value }: { value: string }) => {
  if (isNil(value)) return
  if (!isNonEmptyString(value)) return

  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

  return regex.test(value.toLowerCase()) ? undefined : t(errorKey)
}

export { isEmail }
