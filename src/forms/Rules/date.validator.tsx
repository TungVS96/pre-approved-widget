import { addMonths, isAfter, isBefore, isSameDay } from 'date-fns'
import { isNil } from 'ramda'
import { isNonEmptyString } from 'ramda-adjunct'

import { t } from '../../i18n'
import { formatDate, isISODateString } from '../../utils'

const isValidDate = (errorKey?: string) => ({ value }: { value: string }) => {
  const errorKeyToUse = errorKey || 'errors.date.invalid'
  if (isNil(value)) return
  if (!isNonEmptyString(value)) return
  if (!isISODateString(value)) return t(errorKeyToUse)

  const date = Date.parse(value)

  return isNaN(date) ? t(errorKeyToUse) : undefined
}

const lessThanDate = (errorKey: string, dateISOString: string) => ({
  value
}: {
  value: string
}) => {
  if (isNil(value)) return
  if (!isNonEmptyString(value)) return
  if (!isISODateString(value)) return t(errorKey)

  const date = Date.parse(value)

  return isNaN(date) ||
    isAfter(date, dateISOString) ||
    isSameDay(date, dateISOString)
    ? t(errorKey)
    : undefined
}

const lessThanEqualToDate = (errorKey: string, dateISOString: string) => ({
  value
}: {
  value: string
}) => {
  if (isNil(value)) return
  if (!isNonEmptyString(value)) return
  if (!isISODateString(value)) return t(errorKey)

  const date = Date.parse(value)

  return isNaN(date) || isAfter(date, dateISOString) ? t(errorKey) : undefined
}

const lessThanEqualToDateHasSuffix = (
  errorKey: string,
  dateISOString: string
) => ({ value }: { value: string }) => {
  if (isNil(value)) return
  if (!isNonEmptyString(value)) return
  if (!isISODateString(value)) return t(errorKey)
  const dataRootPlusOneMonth = addMonths(Date.parse(dateISOString), 1)
  const date = new Date(value)
  date.setHours(0, 0, 0)
  const dataRootValue = new Date(dateISOString)

  return isAfter(date, dataRootPlusOneMonth)
    ? t(errorKey, { date: formatDate(dataRootValue) })
    : undefined
}

const greaterThanDate = (errorKey: string, dateISOString: string) => ({
  value
}: {
  value: string
}) => {
  if (isNil(value)) return
  if (!isNonEmptyString(value)) return
  if (!isISODateString(value)) return t('errors.date.invalid')

  const dateValue = Date.parse(value)
  const dataRootValue = new Date(dateISOString).setHours(0, 0, 0, 0)

  return isNaN(dateValue) ||
    isBefore(dateValue, dataRootValue) ||
    isSameDay(dateValue, dataRootValue)
    ? t(errorKey, { dateValue: formatDate(dataRootValue.toString()) })
    : undefined
}

const isGreaterThanEqualToDate = (errorKey: string, dateISOString: string) => ({
  value
}: {
  value: string
}) => {
  if (isNil(value)) return
  if (!isNonEmptyString(value)) return
  if (!isISODateString(value)) return t('errors.date.invalid')

  const dateValue = Date.parse(value)
  const dataRootValue = new Date(dateISOString).setHours(0, 0, 0, 0)

  return isNaN(dateValue) || isBefore(dateValue, dataRootValue)
    ? t(errorKey, { dateValue: formatDate(dataRootValue.toString()) })
    : undefined
}

export {
  isValidDate,
  lessThanDate,
  lessThanEqualToDate,
  lessThanEqualToDateHasSuffix,
  greaterThanDate,
  isGreaterThanEqualToDate
}
