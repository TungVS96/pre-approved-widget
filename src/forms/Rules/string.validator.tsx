import { isEmpty, isNil } from 'ramda'

import { t } from '../../i18n'

const maxLengthForCurrency = (
  lengthOfNumbers: number,
  errorKey: string = 'applicationForm.form.errors.maxLength'
) => ({ value }: { value: string }) => {
  if (isNil(value) || isEmpty(value)) return
  const maxLengthWithComma = getMaxLengthForCurrency(lengthOfNumbers)
  if (value.length > maxLengthWithComma) {
    return t(errorKey, { length: lengthOfNumbers })
  }

  return
}

const getMaxLengthForCurrency = (lengthOfNumbers: number) => {
  const length = lengthOfNumbers + Math.floor(lengthOfNumbers / 3)
  if (lengthOfNumbers % 3 === 0) {
    return Math.max(0, length - 1)
  }

  return length
}

const isUpperCaseAll = (errorKey: string) => ({ value }: { value: string }) => {
  if (isNil(value) || isEmpty(value)) return
  const lower = value.toLowerCase()
  const upper = value.toUpperCase()
  let newString = ''
  for (let i = 0; i < value.length; i = i + 1) {
    if (
      !isNaN(parseInt(value[i], 10)) ||
      lower[i] === upper[i] ||
      value[i] === ''
    ) {
      continue
    }

    newString += value[i]
  }

  const isUpperCase =
    !isEmpty(newString) && newString === newString.toUpperCase()

  if (isUpperCase) {
    return t(errorKey)
  }
}

export { maxLengthForCurrency, getMaxLengthForCurrency, isUpperCaseAll }
