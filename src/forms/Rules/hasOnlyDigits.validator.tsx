import { isNil } from 'ramda'

import { t } from '../../i18n'

const hasOnlyDigits = (errorKey?: string) => ({ value }: { value: string }) => {
  if (isNil(value)) return
  const regex = /^[0-9]*$/

  return regex.test(value)
    ? undefined
    : t(errorKey || 'applicationForm.form.errors.onlyDigits')
}

export { hasOnlyDigits }
