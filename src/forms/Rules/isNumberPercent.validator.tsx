import { isNil } from 'ramda'

import { t } from '../../i18n'

const isNumberPercent = (errorKey?: string) => ({
  value
}: {
  value: string
}) => {
  if (isNil(value)) return
  const regex = /(^100(\.0{1,19})?$)|(^([1-9]([0-9])?|0)(\.[0-9]{1,19})?$)/g

  return regex.test(value)
    ? undefined
    : t(errorKey || 'applicationForm.percentPerYear.error')
}

export { isNumberPercent }
