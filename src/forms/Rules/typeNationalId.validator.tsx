import { NATIONAL_IDPASSPORT_NO_TYPE } from 'dma-types'
import { t } from '../../i18n'

const typeNationalId = (errorKey: string, nationalTypeField?: string) => ({
  value,
  values
}: {
  value: string
  values: any
}) => {
  const regExpNumbersAndLetters = new RegExp('^[a-zA-Z0-9]+$')
  const regExpNumbers12Digits = new RegExp(/^[0-9]{12}$/g)
  const regExpNumbers9Digits = new RegExp(/^[0-9]{9}$/g)

  if (
    (nationalTypeField ? values[nationalTypeField] : values.nationalType) ===
      NATIONAL_IDPASSPORT_NO_TYPE.PASSPORT &&
    !regExpNumbersAndLetters.test(value)
  ) {
    return t('applicationForm.form.errors.nationalId.passport')
  }
  if (
    (nationalTypeField ? values[nationalTypeField] : values.nationalType) ===
      NATIONAL_IDPASSPORT_NO_TYPE.CCCD &&
    !regExpNumbers12Digits.test(value)
  ) {
    return t('applicationForm.form.errors.nationalId.cccd')
  }
  if (
    (nationalTypeField ? values[nationalTypeField] : values.nationalType) ===
      NATIONAL_IDPASSPORT_NO_TYPE.NATIONAL_ID &&
    !regExpNumbers9Digits.test(value) &&
    !regExpNumbers12Digits.test(value)
  ) {
    return t('applicationForm.form.errors.nationalId.cmnd')
  }

  return
}

export { typeNationalId }
