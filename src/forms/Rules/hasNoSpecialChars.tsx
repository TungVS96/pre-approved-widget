import { REG_EXP } from 'dma-ui'
import { isNil } from 'ramda'

import { t } from '../../i18n'

const hasNoSpecialChars = (errorKey: string, exp?: string) => ({
  value
}: {
  value: string
}) => {
  if (isNil(value)) return
  const pattern = exp || REG_EXP.STRING
  const regex = new RegExp(pattern, 'gi')

  return regex.test(value) ? undefined : t(errorKey)
}

export { hasNoSpecialChars }
