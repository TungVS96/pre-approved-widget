import { isNil } from 'ramda'
import { t } from '../../i18n'

const isPositiveNumber = (errorKey?: string) => ({
  value
}: {
  value: string
}) => {
  if (isNil(value)) return
  const numberRepresentation = Number(
    value.toString().replace(new RegExp(',', 'g'), '')
  ) // Number('') is 0
  if (
    isNaN(numberRepresentation) || // Number('abc') is NaN
    numberRepresentation < 0
  ) {
    return t(errorKey || 'applicationForm.form.errors.positiveNumber')
  }

  return Number.isInteger(numberRepresentation)
    ? undefined
    : t(errorKey || 'applicationForm.form.errors.positiveNumber')
}
const isSaleAccountNumber = (errorKey?: string) => ({
  value
}: {
  value: string
}) => {
  const pattern = new RegExp('^[0-9]{14}$')

  return pattern.test(value) ? undefined : t(errorKey)
}

export { isPositiveNumber, isSaleAccountNumber }
