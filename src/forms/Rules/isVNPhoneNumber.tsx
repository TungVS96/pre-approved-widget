import { parsePhoneNumber } from 'libphonenumber-js'
import { isNilOrEmpty } from 'ramda-adjunct'

import { t } from '../../i18n'

const getLocalPhone = (s: string) => s.replace('+84', '0')
const normalizePhone = (phone: string) => {
  const normalizedPhone = phone.replace(/[-. ]/g, '')

  try {
    return parsePhoneNumber(normalizedPhone, 'VN').number
  } catch (e) {
    return normalizedPhone
  }
}

const isVNPhoneNumber = (errorKey: string) => ({
  value
}: {
  value: string
}) => {
  if (isNilOrEmpty(value)) return
  // const newValue = normalizePhone(value)
  const regex = new RegExp(/^((09|08|07|03|05)[0-9]{8})$/)
  const isValid = regex.test(value as string)

  if (!isValid) {
    return t(errorKey)
  }
}

const isVNPhoneNumberBoolean = (value: string) => {
  if (isNilOrEmpty(value)) return
  // const newValue = normalizePhone(value)
  const regex = new RegExp(/^((09|08|07|03|05)[0-9]{8})$/)

  return regex.test(value as string)
}

export {
  isVNPhoneNumber,
  normalizePhone,
  getLocalPhone,
  isVNPhoneNumberBoolean
}
