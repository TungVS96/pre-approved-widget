import { rem } from 'dma-ui'
import { css } from 'emotion'
import { getIn, InjectedFormikProps } from 'formik'
import { isNonEmptyString } from 'ramda-adjunct'
import React, { ChangeEvent, FunctionComponent } from 'react'

import Tooltip from '../../components/Tooltip/Tooltip.component'
import { withPureField } from '../../hocs'
import VerifyControls, {
  getEditableName,
  getFieldName
} from '../VerifyControls/VerifyControls.component'
import { ValidatedIcon } from '../index'

import {
  InputContainer,
  InputErrorLabel,
  InputFieldContainer,
  InputIcon,
  InputLabel
} from './TextInput.component.style'

interface IProps {
  inputProps?: {
    [key: string]: any
  }
  name: string
  hasTouched?: boolean
  errorMessage?: string
  values?: object
  value?: string
  leftIcon?: React.ReactNode
  rightIcon?: React.ReactNode
  errorIcon?: React.ReactNode
  validatedIcon?: React.ReactNode
  handleChange(event: ChangeEvent<HTMLInputElement>): void
  handleBlur?(event: ChangeEvent<HTMLInputElement>): void
  label?: string
  labelTooltip?: string
  labelStyle?: any
  placeholder: string
  type: string
  requiredField?: boolean
  transformValue?(value: string): string
  labelSuffix?: string
  valueSuffix?: string
  disabled?: boolean
  canVerify?: boolean
  setFieldValue?(key: string, value: any): void
  hasErrorBlock?: boolean
  maxLength?: number
  valuePrefix?: React.ReactNode
  onTextChange?(value: string): any
  format?(value: string): string
  handleChangeAmount?(
    event: ChangeEvent<HTMLInputElement>,
    startPosition: number
  ): void
  handleFocus?(event: any): void
  autoComplete?: string
  inputWrapperStyle?: string
  inputStyle?: any
  numberics?: boolean
  id?: string
  highLight?: boolean
}

interface IFormTextInput<Props, Values> {
  name: string
  label?: string
  labelTooltip?: string
  labelStyle?: any
  placeholder?: string
  type?: string
  numberics?: boolean
  requiredField?: boolean
  labelSuffix?: string
  dynamicLabel?: boolean
  valueSuffix?: string
  props: InjectedFormikProps<Props, Values>
  transformValue?(value: string): string
  disabled?: boolean
  canVerify?: boolean
  inputProps?: {
    [key: string]: any
  }
  hasErrorBlock?: boolean
  setFieldValue?(key: string, value: any): void
  maxLength?: number
  valuePrefix?: React.ReactNode
  leftIcon?: React.ReactNode
  rightIcon?: React.ReactNode
  onTextChange?(value: string): any
  format?(value: any): string
  handleChangeAmount?(
    event: ChangeEvent<HTMLInputElement>,
    startPosition: number
  ): void
  handleFocus?(event: any): void
  handleBlur?(event: ChangeEvent<HTMLInputElement>): void
  autoComplete?: string
  inputStyle?: any
  highLight?: boolean
  inputWrapperStyle?: string
}

const onChange = (props: IProps) => (e: ChangeEvent<HTMLInputElement>) => {
  const start = e.target.selectionStart as number

  if (props.transformValue) {
    e.target.value = props.transformValue(e.target.value)
  }

  props.handleChange(e)
  if (props.onTextChange) {
    props.onTextChange(e.target.value)
  }

  if (props.handleChangeAmount) {
    props.handleChangeAmount(e, start)
  }
}

const getDisplayText = ({ type, numberics, value, format }: IProps) => {
  return type === 'tel' && !numberics
    ? value
    : value && value !== '0'
    ? format
      ? format(value)
      : value
    : value
}

const TextInput: FunctionComponent<IProps> = (props) => {
  const {
    type,
    leftIcon,
    rightIcon,
    errorIcon,
    validatedIcon = <ValidatedIcon />,
    placeholder,
    label,
    hasTouched,
    value = '',
    errorMessage,
    handleBlur,
    values,
    inputProps,
    requiredField = false,
    labelSuffix,
    valueSuffix,
    disabled,
    canVerify = false,
    maxLength,
    valuePrefix,
    handleFocus,
    labelTooltip,
    labelStyle,
    autoComplete,
    inputWrapperStyle,
    inputStyle,
    id,
    highLight = false
  } = props
  const fieldName = getFieldName(props)
  const hasError = hasTouched && isNonEmptyString(errorMessage)
  const hasValue = isNonEmptyString(value)
  const editable = canVerify ? getIn(values, getEditableName(props)) : !disabled
  const displayText = getDisplayText(props)

  return (
    <div className="flex flex-col w-full">
      {label && (
        <InputLabel style={labelStyle}>
          {label}
          {requiredField && <span className="text-brand">*</span>}{' '}
          {labelSuffix && (
            <span className="font-normal text-grey-darkest">{labelSuffix}</span>
          )}
          {labelTooltip && (
            <Tooltip
              position={'auto'}
              icon={require('./assets/info.svg')}
              content={labelTooltip}
              sizeIcon={17}
            />
          )}
        </InputLabel>
      )}
      <div className="flex flex-row w-full">
        <InputContainer
          className={inputWrapperStyle}
          highLight={highLight}
          hasError={hasError}
        >
          {leftIcon && <InputIcon>{leftIcon}</InputIcon>}
          {valuePrefix}
          <InputFieldContainer
            id={id}
            className={inputStyle}
            type={type}
            placeholder={placeholder}
            name={fieldName}
            onChange={onChange(props)}
            onBlur={handleBlur}
            disabled={!editable}
            value={displayText}
            maxLength={maxLength}
            onFocus={handleFocus}
            {...inputProps}
            autoComplete={autoComplete}
          />
          {hasError && errorIcon}
          {hasTouched && !hasError && hasValue && validatedIcon}
          {rightIcon}
          {valueSuffix && (
            <div
              className={`text-grey-dark text-sm font-bold ${css({
                marginRight: rem(14)
              })}`}
            >
              {valueSuffix}
            </div>
          )}
        </InputContainer>
        {canVerify && <VerifyControls {...props} />}
      </div>
      {props.hasErrorBlock && (
        <InputErrorLabel>{hasError && errorMessage}</InputErrorLabel>
      )}
    </div>
  )
}

TextInput.defaultProps = {
  hasErrorBlock: true
}

export { IFormTextInput, IProps }
export default withPureField<IProps, IProps>({
  updatedKeys: ['rightIcon', 'labelSuffix']
})(TextInput)
