import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const InputContainer = withClassName('flex flex-row items-center w-full')(
  styled('div')(
    {
      marginTop: rem(8),
      height: rem(44)
    },
    ({
      theme,
      hasError,
      highLight
    }: {
      theme: any
      borderColorKey?: string
      hasError?: boolean
      highLight?: boolean
    }) => ({
      backgroundColor: theme.colors['white-two'],
      // border: `solid 1px ${theme.colors[borderColorKey || 'grey-light']}`,
      border: `solid 1px`,
      borderColor: hasError && highLight ? '#da251d' : '#ddd',
      [theme.mq.md]: {
        marginTop: rem(11)
      }
    })
  )
)

const InputFieldContainer = withClassName('text-lg w-full')(
  styled('input')(
    {
      marginLeft: rem(21),
      height: rem(44),
      '&:-webkit-autofill': {
        backgroundColor: 'white!important'
      },
      fontSize: rem(14)
    },
    ({ theme }) => ({
      backgroundColor: 'transparent',
      color: theme.colors.black,
      '::placeholder': {
        fontFamily: 'Helvetica',
        color: theme.colors['grey-dark']
        // fontSize: rem(14),
        // opacity: 0.8,
        // [theme.mq.md]: {
        //   fontSize: rem(16)
        // }
      },
      ':focus': {
        outline: 'none'
      },
      ':disabled::placeholder': {
        opacity: 0.8
      },
      [theme.mq.md]: {
        marginLeft: rem(33),
        height: rem(64),
        fontSize: rem(14)
      }
    })
  )
)

const InputLabel = withClassName(
  'flex align-center items-center text-grey-darkest'
)(
  styled('label')({}, () => ({
    position: 'relative',
    fontSize: rem(13),
    fontWeight: 500
  }))
)

const InputIcon = withClassName(
  'flex flex-no-shrink items-center justify-center'
)(
  styled('div')(
    {
      marginLeft: rem(12),
      width: rem(24),
      height: rem(24)
    },
    ({ theme }) => ({
      [theme.mq.md]: {
        marginLeft: rem(16),
        width: rem(32),
        height: rem(32)
      }
    })
  )
)

const InputErrorLabel = withClassName('text-reddish text-xs')(
  styled('div')({
    marginTop: rem(5),
    minHeight: rem(20)
  })
)

export {
  InputContainer,
  InputFieldContainer,
  InputLabel,
  InputIcon,
  InputErrorLabel
}
