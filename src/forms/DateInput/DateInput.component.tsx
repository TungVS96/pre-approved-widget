import { DatePicker } from '@blueprintjs/datetime'
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css'
import { addYears, format, isValid } from 'date-fns'
import { css, cx } from 'emotion'
import { getIn } from 'formik'
import React, { ChangeEvent, FunctionComponent } from 'react'
import { compose, withHandlers, withState } from 'recompose'

import OutsideClickHandler from 'react-outside-click-handler'
import { withPureField } from '../../hocs'
import { IFormTextMaskedInput } from '../TextMaskedInput/TextMaskedInput.component'
import { getEditableName } from '../VerifyControls/VerifyControls.component'
import { TextMaskedInput } from '../index'

import vi from 'date-fns/locale/vi'
import { DATE_FORMAT } from '../../constants'
import handlers from './DateInput.component.handler'
import { DateInputContainer } from './DateInput.component.style'

const formatDate = (date: any, formatString: string = DATE_FORMAT) => {
  if (!isValid(date)) return date

  return format(date, formatString, {
    locale: vi
  })
}

interface IHandlers {
  SHOW_CALENDAR(): void
  ON_INPUT_FOCUS(): void
  ON_DATE_CHANGE(): void
}

interface IStateHandlers {
  showCalendar(showedCalendar: boolean): void
}

interface IStateProps {
  showedCalendar: boolean
}

interface IProps extends IFormTextMaskedInput<any, any> {
  setFieldValue(key: string, value: any): void
  minDate?: Date
  maxDate?: Date
  hasTouched: boolean
  errorMessage: string
  values?: object
  value: string
  handleChange(event: ChangeEvent<HTMLInputElement>): void
  handleBlur(event: ChangeEvent<HTMLInputElement>): void
  canVerify?: boolean
  disabled?: boolean
  validatedIcon?: React.ReactNode
  fieldContainerStyle?: string
}

type EnhancedProps = IProps & IStateHandlers & IStateProps & IHandlers

const handleChangeAmount = (e: any, startPosition: number) => {
  const start = startPosition
  if (typeof start !== 'number') return

  setTimeout(() => {
    e.target.setSelectionRange(start, start)
  }, 0)
}

const renderCalendar = (props: EnhancedProps) => {
  const { disabled = false, canVerify, values } = props
  const editable = canVerify ? getIn(values, getEditableName(props)) : !disabled

  return (
    <button
      type={'button'}
      onClick={props.SHOW_CALENDAR}
      disabled={!editable}
      className={cx(
        'flex flex-no-shrink',
        css({ ':focus': { outline: 0 }, marginRight: 12 })
      )}
    >
      <img src={require('./assets/ic-calendar.svg')} width={22} height={22} />
    </button>
  )
}

const today = new Date()
const DateInput: FunctionComponent<EnhancedProps> = (props) => {
  const {
    type = 'text',
    label,
    placeholder,
    requiredField = true,
    inputProps,
    valuePrefix,
    minDate = new Date(1949, 0, 1),
    maxDate = addYears(today, 100),
    canVerify = false,
    setFieldValue,
    validatedIcon,
    disabled = false,
    fieldContainerStyle
  } = props

  return (
    <div className="relative" style={{ zIndex: 8 }}>
      <TextMaskedInput
        {...props}
        type={type}
        label={label}
        placeholder={placeholder || ''}
        requiredField={requiredField}
        inputProps={{
          onFocus: props.ON_INPUT_FOCUS,
          ...inputProps
        }}
        formatValue={formatDate}
        handleChangeAmount={handleChangeAmount}
        rightIcon={renderCalendar(props)}
        valuePrefix={valuePrefix}
        canVerify={canVerify}
        setFieldValue={setFieldValue}
        validatedIcon={validatedIcon}
        disabled={disabled}
        fieldContainerStyle={fieldContainerStyle}
      />
      {!disabled && props.showedCalendar && (
        <OutsideClickHandler onOutsideClick={props.ON_INPUT_FOCUS}>
          <DateInputContainer>
            <DatePicker
              onChange={props.ON_DATE_CHANGE}
              minDate={minDate}
              maxDate={maxDate}
            />
          </DateInputContainer>
        </OutsideClickHandler>
      )}
    </div>
  )
}

const enhancer = compose<EnhancedProps, IProps>(
  withState('showedCalendar', 'showCalendar', false),
  withHandlers(handlers),
  withPureField({
    updatedKeys: ['showedCalendar']
  })
)

export { EnhancedProps, DateInput }
export default enhancer(DateInput)
