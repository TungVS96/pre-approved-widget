import { getTime } from 'date-fns'

import { getFieldName } from '../VerifyControls/VerifyControls.component'
import { EnhancedProps } from './DateInput.component'

const handlers = {
  SHOW_CALENDAR: (props: EnhancedProps) => () => {
    props.showCalendar(!props.showedCalendar)
  },
  ON_INPUT_FOCUS: (props: EnhancedProps) => () => {
    if (props.showedCalendar) {
      props.showCalendar(false)
    }
  },
  ON_DATE_CHANGE: (props: EnhancedProps) => (date: Date) => {
    const tzoffset = new Date().getTimezoneOffset() * 60000 // offset in milliseconds
    const dateString = new Date(getTime(date) - tzoffset).toISOString()
    const fieldName = getFieldName(props)
    if (props.onValueChange) {
      props.onValueChange(dateString)
    }
    props.setFieldValue(fieldName, dateString)
    props.showCalendar(false)
  }
}

export default handlers
