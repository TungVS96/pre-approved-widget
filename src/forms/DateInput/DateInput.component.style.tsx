import { withClassName } from 'dma-ui'
import styled from 'react-emotion'

const DateInputContainer = withClassName('shadow absolute')(
  styled('div')({
    border: `solid 1px #FDB8A3`,
    zIndex: 99,
    bottom: -210,
    right: 0
  })
)

export { DateInputContainer }
