import { rem } from 'dma-ui'
import { css } from 'emotion'
import React, { FunctionComponent } from 'react'
import { formatCurrency, toNumber } from '../../utils'
import { renderTextInput } from '../form'

interface IProps {
  name: string
  marginTop?: number
}

type EnhancedProps = IProps & any

const getMaxLengthForCurrency = (lengthOfNumbers: number) => {
  const length = lengthOfNumbers + Math.floor(lengthOfNumbers / 3)
  if (lengthOfNumbers % 3 === 0) {
    return Math.max(0, length - 1)
  }

  return length
}

let previousValue = ''

const removeComma = (str: string) => {
  return str.replace(/,/g, '')
}

const handleChangeAmount = (e: any, startPosition: number, props: any) => {
  let start = startPosition
  const value = removeComma(e.target.value)

  if (typeof start !== 'number') return

  // On this case, user adds one more digit to current input value (arbitrary cursor's position) and cause the formated value to have one additional comma. So in this case, the position of cursor on input value have to increase by 1
  if (value.length % 3 === 1 && previousValue.length < value.length) {
    start += 1
  }

  // On this case, user deletes one digit on current input value (arbitrary cursor's position) and cause the formated value to have one less comma. So in this case, the position of cursor on input value have to decrease by 1
  if (
    value.length % 3 === 0 &&
    value.length >= 3 &&
    previousValue.length > value.length
  ) {
    start -= 1
  }

  previousValue = value

  setTimeout(() => {
    e.target.setSelectionRange(start, start)
  }, 0)

  if (!props.toggleSuggestion) {
    return
  }
  if (value.length > 0 && parseInt(value, 10) > 0) {
    props.toggleSuggestion(true)
  } else {
    props.toggleSuggestion(false)
  }
  props.setCurrentField(e.target.name)
}

const handleFocusAmountInput = (props: EnhancedProps) => (e: any) => {
  previousValue = removeComma(e.target.value)

  if (props.handleFocus) {
    props.handleFocus()
  }
}

const handleClick = (props: any, evt: React.MouseEvent<HTMLDivElement>) => {
  const totalIncome = removeComma(evt.currentTarget.innerText)
  props.setFieldValue(props.name, totalIncome)
  props.toggleSuggestion(false)
}

const styleSuggestItem = css({
  ':hover': {
    backgroundColor: 'rgba(234,61,61,0.4)'
  },
  paddingLeft: rem(10),
  height: rem(35),
  display: 'flex',
  alignItems: 'center',
  cursor: 'pointer',
  borderBottom: '1px solid #ddd'
})

const styleSuggestContainer = css({
  zIndex: 99,
  position: 'absolute',
  display: 'inline-block',
  width: rem(218),
  backgroundColor: '#ffffff',
  borderLeft: '1px solid #ddd',
  borderRight: '1px solid #ddd',
  marginTop: rem(-5)
})

const renderSuggestItem = (inputValue: string, props: any) => {
  if (inputValue.length > 4) return

  const ret = []
  const numberOfLoop = 4
  const powOffset = 6 - inputValue.length
  for (let index = 1; index <= numberOfLoop; index += 1) {
    const item = (
      <div
        onClick={(e) => handleClick(props, e)}
        className={styleSuggestItem}
        key={index}
      >
        {formatCurrency({
          value: parseInt(inputValue, 10) * Math.pow(10, index + powOffset)
        })}
      </div>
    )
    ret.push(item)
  }

  return <div className={styleSuggestContainer}>{ret}</div>
}

const AmountInput: FunctionComponent<EnhancedProps> = (props) => {
  const {
    marginTop = 0,
    name,
    label = 'applicationForm.form.propertyValue.label',
    placeholder = 'applicationForm.form.propertyValue.placeholder',
    valueSuffix = 'applicationForm.vnd',
    labelSuffix,
    dynamicLabel = false,
    disabled,
    maxLength = getMaxLengthForCurrency(15),
    requiredField = true,
    tabIndex,
    labelTooltip,
    handleBlur,
    autoComplete,
    type = 'text',
    numberics = false
  } = props

  return (
    <div className={css({ marginTop: rem(marginTop) })}>
      {renderTextInput({
        props,
        name,
        label,
        type,
        numberics,
        labelTooltip,
        placeholder,
        valueSuffix,
        labelSuffix,
        dynamicLabel,
        disabled,
        requiredField,
        autoComplete,
        handleBlur,
        handleChangeAmount: (e, start) => handleChangeAmount(e, start, props),
        handleFocus: handleFocusAmountInput(props),
        inputProps: {
          maxLength,
          tabIndex
        },
        format: (value) => formatCurrency({ value }),
        transformValue: (value) => toNumber(value).toString()
      })}
      {props.isShowSuggestion &&
      props.name === props.currentField &&
      props.autoComplete === 'off'
        ? renderSuggestItem(previousValue, props)
        : null}
    </div>
  )
}

export default AmountInput
