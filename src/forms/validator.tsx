import validator, { setTranslateFn } from 'formik-validators'

import { t } from '../i18n'

setTranslateFn(t)

export default validator
