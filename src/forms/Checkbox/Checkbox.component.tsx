import { isNonEmptyString } from 'ramda-adjunct'
import React, { SFC } from 'react'

import { withPureField } from '../../hocs'
import { InputErrorLabel } from '../TextInput/TextInput.component.style'

import { TCCheckbox, TCText } from './Checkbox.component.style'

interface IProps {
  name: string
  hasTouched?: boolean
  value: string
  errorMessage?: string
  style?: object
  styleText?: object
  onChange(name: string, value: string): () => void
}

const Checkbox: SFC<IProps> = ({
  name,
  value,
  hasTouched,
  errorMessage,
  onChange,
  children,
  style = {},
  styleText = {}
}) => {
  const hasError = hasTouched && isNonEmptyString(errorMessage)

  return (
    <>
      <div className="flex flex-row items-center" style={style}>
        <TCCheckbox value={value} onClick={onChange(name, value)} />
        <TCText style={styleText}>{children}</TCText>
      </div>
      <InputErrorLabel>{hasError && errorMessage}</InputErrorLabel>
    </>
  )
}

export default withPureField<IProps, IProps>()(Checkbox)
export { Checkbox }
