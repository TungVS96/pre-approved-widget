import { storiesOf } from '@storybook/react'
import { css } from 'emotion'
import React from 'react'

import { CHECKBOX_STATE } from '../../types'
import { getFieldProps } from '../../utils'

import Checkbox from './Checkbox.component'

const name = 'checkTC'
const defaultProps = {
  errors: {
    [name]: 'error'
  },
  values: {
    [name]: ''
  },
  touched: {
    [name]: false
  }
}

const NOOP = () => () => 'nothing'

storiesOf('Checkbox', module)
  .add('default', () => (
    <div className={css({ margin: 20 })}>
      <Checkbox onChange={NOOP} {...getFieldProps(name, defaultProps)}>
        This is a label for Checkbox
      </Checkbox>
    </div>
  ))
  .add('with checked state', () => {
    const props = {
      ...defaultProps,
      values: { [name]: CHECKBOX_STATE.CHECKED }
    }

    return (
      <div className={css({ margin: 20 })}>
        <Checkbox onChange={NOOP} {...getFieldProps(name, props)}>
          This is a label for Checkbox
        </Checkbox>
      </div>
    )
  })
  .add('with error state', () => {
    const props = {
      ...defaultProps,
      touched: {
        [name]: true
      },
      errors: {
        [name]: 'Please Check the Checkbox'
      }
    }

    return (
      <div className={css({ margin: 20 })}>
        <Checkbox onChange={NOOP} {...getFieldProps(name, props)}>
          This is a label for Checkbox
        </Checkbox>
      </div>
    )
  })
