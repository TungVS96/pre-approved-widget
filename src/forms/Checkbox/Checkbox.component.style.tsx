import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const TCCheckbox = withClassName('flex-no-shrink cursor-pointer')(
  styled('span')(
    {
      width: '20px',
      height: '20px'
    },
    ({ value }: { value: string }) => ({
      content: `url(${
        value
          ? require('./assets/checkbox-on.svg')
          : require('./assets/checkbox-off.svg')
      })`
    })
  )
)

const TCText = withClassName('')(
  styled('div')(
    {
      marginLeft: rem(9),
      fontSize: '14px',
      lineHeight: '16px'
    },
    ({ theme }) => ({
      color: theme.colors['grey-dark']
    })
  )
)

const TCLink = withClassName('')(
  styled('a')(
    {
      textDecoration: 'underline'
    },
    ({ theme }) => ({
      color: theme.colors['tomato-two']
    })
  )
)

export { TCCheckbox, TCText, TCLink }
