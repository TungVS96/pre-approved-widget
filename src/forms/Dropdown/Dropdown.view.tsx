// tslint:disable:max-file-line-count cyclomatic-complexity
import { rem, NOOP } from 'dma-ui'

import { css } from 'emotion'
import { getIn } from 'formik'
import { isEmpty } from 'ramda'
import { isNonEmptyString } from 'ramda-adjunct'
import React, { FunctionComponent } from 'react'
import OutsideClickHandler from 'react-outside-click-handler'

import Tooltip from '../../components/Tooltip/Tooltip.component'
import { withPureField } from '../../hocs'
import { IDropDownItem } from '../../types'
import {
  InputErrorLabel,
  InputIcon,
  InputLabel
} from '../TextInput/TextInput.component.style'
import VerifyControls, {
  getEditableName,
  getFieldName
} from '../VerifyControls/VerifyControls.component'

import { getName, getValue } from '../../utils'
import {
  DisplayFieldContainer,
  DropdownContainer,
  DropdownFieldContainer,
  DropdownItem,
  DropDownArrow,
  ErrorContainer,
  FieldContainer,
  ValueSuffix
} from './Dropdown.style'

interface IProps {
  name: string
  showed?: boolean
  show?(show: boolean): void
  SHOW?(): void
  CLICK?(name: string, item: IDropDownItem): void
  items: IDropDownItem[]
  onChange(key: string, item: IDropDownItem): void
  errorIcon?: React.ReactNode
  values?: object
  hasTouched?: boolean
  setFieldTouched?(name: string, touched: any): void
  errorMessage?: string
  leftIcon?: React.ReactNode
  title?: string
  labelSuffix?: string
  placeholder?: string
  requiredField?: boolean
  filterInputStyle?: string
  filterDropdownStyle?: string
  labelStyle?: string
  displayFieldStyle?: string
  arrowSize?: number
  dropdownItemStyle?: string
  fieldContainerStyle?: string
  hasValidation?: boolean
  disabled?: boolean
  transformValue?(value: string): string
  format?(value: string): string
  className?: string
  ON_OUTSITE_CLICK?(e: React.MouseEvent<HTMLElement>): void
  tooltipContent?: string
  setFieldValue?(key: string, value: any): void
  canVerify?: boolean
  inputProps?: any
  messageNoData?: string
  displayName?: boolean
  textColorMessageNoData?: string
  displayAllValueAndName?: boolean
  labelIcon?: string
  labelIconOnClick?(input: any): void
  valueSuffix?: string
  iconWarning?: boolean
  highLight?: boolean
  customArrow?: boolean
}

const onKeyDown = (onClickOutside: any) => (event: any) => {
  if (event.keyCode === 9) {
    // tab key
    onClickOutside()
  }
}

const DropdownView: FunctionComponent<IProps> = (props) => {
  const {
    showed,
    items,
    SHOW,
    CLICK,
    errorIcon,
    values,
    errorMessage,
    hasTouched,
    leftIcon,
    title,
    placeholder,
    filterInputStyle,
    filterDropdownStyle,
    arrowSize,
    labelStyle,
    displayFieldStyle,
    dropdownItemStyle,
    fieldContainerStyle,
    hasValidation,
    requiredField = false,
    disabled = false,
    transformValue,
    format,
    labelSuffix,
    className,
    ON_OUTSITE_CLICK,
    tooltipContent,
    canVerify = false,
    inputProps,
    messageNoData = 'NO DATA',
    displayName = true,
    textColorMessageNoData,
    displayAllValueAndName = false,
    labelIcon = '',
    labelIconOnClick = () => {
      //
    },
    valueSuffix = '',
    iconWarning = true,
    highLight = false
  } = props
  const fieldName = getFieldName(props)
  const hasError = hasTouched && isNonEmptyString(errorMessage)
  const displayText = displayName
    ? getName(items, `${getIn(values, fieldName)}`)
    : getValue(items, `${getIn(values, fieldName)}`)
  const hasDisplayText = isNonEmptyString(displayText)
  const transformedDisplayText =
    hasDisplayText && transformValue ? transformValue(displayText) : displayText
  const formattedDisplayText =
    hasDisplayText && format
      ? format(transformedDisplayText)
      : transformedDisplayText
  const editable = canVerify ? getIn(values, getEditableName(props)) : !disabled
  const noData = isEmpty(items)

  return (
    <div className={className}>
      <OutsideClickHandler
        onOutsideClick={ON_OUTSITE_CLICK ? ON_OUTSITE_CLICK : NOOP}
      >
        {title && (
          <InputLabel className={labelStyle}>
            {title}
            {requiredField && <span className="text-brand">*</span>}{' '}
            {labelSuffix && (
              <span className="font-normal text-grey-darkest">
                {labelSuffix}
              </span>
            )}
            {isNonEmptyString(tooltipContent) && (
              <Tooltip content={tooltipContent} />
            )}
            {labelIcon && (
              <img
                src={labelIcon}
                style={{
                  width: 17,
                  height: 17,
                  marginLeft: 5
                }}
                onClick={labelIconOnClick}
              />
            )}
          </InputLabel>
        )}
        <div className="flex flex-row w-full">
          <DropdownFieldContainer
            onClick={SHOW}
            borderColorKey={hasError && 'tomato'}
            className={`relative ${filterInputStyle}`}
            disabled={!editable}
            {...inputProps}
            tabIndex={0}
            onKeyDown={onKeyDown(ON_OUTSITE_CLICK)}
          >
            <FieldContainer
              disabled={!editable}
              className={fieldContainerStyle}
              highLight={highLight}
              hasError={hasError}
            >
              {leftIcon && <InputIcon>{leftIcon}</InputIcon>}
              <DisplayFieldContainer
                className={displayFieldStyle}
                hasValue={hasDisplayText}
              >
                {hasDisplayText ? formattedDisplayText : placeholder}
              </DisplayFieldContainer>
              {hasDisplayText && valueSuffix && (
                <ValueSuffix>{valueSuffix}</ValueSuffix>
              )}
              <DropDownArrow size={arrowSize} />
              {hasError && errorIcon}
            </FieldContainer>
            {showed && (
              <DropdownContainer className={filterDropdownStyle}>
                {noData ? (
                  <p style={{ margin: 10, color: textColorMessageNoData }}>
                    {messageNoData}
                  </p>
                ) : (
                  items.map((item) => (
                    <DropdownItem
                      className={dropdownItemStyle}
                      key={item.value}
                      onClick={CLICK!(fieldName, item)}
                    >
                      {!displayAllValueAndName ? (
                        item.name
                      ) : (
                        <div>
                          {item.name}
                          <br />
                          {item.value}
                        </div>
                      )}
                    </DropdownItem>
                  ))
                )}
              </DropdownContainer>
            )}
          </DropdownFieldContainer>
          {canVerify && <VerifyControls {...props} />}
        </div>
        <ErrorContainer>
          {hasError && iconWarning && (
            <img
              width={16}
              height={16}
              src={require('./assets/ic-error.svg')}
              className={css({
                marginRight: rem(8)
              })}
            />
          )}
          {hasValidation && (
            <InputErrorLabel>{hasError && errorMessage}</InputErrorLabel>
          )}
        </ErrorContainer>
      </OutsideClickHandler>
    </div>
  )
}

DropdownView.defaultProps = {
  labelStyle: '',
  filterInputStyle: '',
  fieldContainerStyle: '',
  displayFieldStyle: '',
  filterDropdownStyle: '',
  dropdownItemStyle: '',
  arrowSize: 8,
  hasValidation: true
}

export { IProps }
export default withPureField<IProps, IProps>({
  updatedKeys: ['showed']
})(DropdownView)
