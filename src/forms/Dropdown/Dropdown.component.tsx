import { InjectedFormikProps } from 'formik'
import { compose, withHandlers, withState } from 'recompose'

import { IDropDownItem } from '../../types'

import handlers from './Dropdown.handler'
import DropdownView, { IProps } from './Dropdown.view'

interface IFormDropdown<Props, Values> {
  name: string
  label?: string
  labelSuffix?: string
  placeholder?: string
  type?: string
  requiredField?: boolean
  props: InjectedFormikProps<Props, Values>
  items: IDropDownItem[]
  filterInputStyle?: string
  filterDropdownStyle?: string
  labelStyle?: string
  displayFieldStyle?: string
  arrowSize?: number
  dropdownItemStyle?: string
  fieldContainerStyle?: string
  hasValidation?: boolean
  disabled?: boolean
  transformValue?(value: string): string
  format?(value: string): string
  className?: string
  onSelect?(name: string, item: IDropDownItem): void
  tooltipContent?: string
  canVerify?: boolean
  hasErrorBlock?: boolean
  inputProps?: any
  messageNoData?: string
  displayName?: boolean
  textColorMessageNoData?: string
  displayAllValueAndName?: boolean
  labelIcon?: string
  labelIconOnClick?(input: any): void
  valueSuffix?: string
  controlStyle?: any
  iconWarning?: boolean
  highLight?: boolean
  customArrow?: boolean
}

const enhancer = compose<IProps, IProps>(
  withState('showed', 'show', false),
  withHandlers(handlers)
)

export { IFormDropdown }
export default enhancer(DropdownView)
