import { convertToRGBA, rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const DropdownContainer = withClassName(
  'w-full absolute bg-grey-lightest',
  'DropdownContainer'
)(
  styled('div')(({ theme }) => ({
    border: `solid 1px ${theme.colors['grey-light']}`,
    borderTopWidth: 0,
    top: rem(48),
    zIndex: 8,
    maxHeight: 300,
    overflow: 'auto'
  }))
)

const DropdownItem = withClassName(
  'w-full text-base flex items-center',
  'DropdownItem'
)(
  styled('div')(({ theme, highlight }: { theme: any; highlight: boolean }) => {
    const paleRedTwo = convertToRGBA(theme.colors['pale-red-two'], 0.4)

    return {
      paddingLeft: rem(24),
      paddingTop: rem(4),
      paddingBottom: rem(4),
      minHeight: rem(44),
      userSelect: 'none',
      fontSize: rem(12),
      backgroundColor: highlight ? paleRedTwo : theme.colors['white-two'],
      color: theme.colors.black,
      ':hover': {
        backgroundColor: paleRedTwo
      }
    }
  })
)

const DisplayFieldContainer = withClassName(
  'flex text-base w-full items-center',
  'DisplayFieldContainer'
)(
  styled('div')(
    {
      height: rem(44),
      userSelect: 'none',
      textOverflow: 'ellipsis',
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      fontSize: rem(14)
    },
    ({ theme, hasValue }: { theme: any; hasValue: boolean }) => ({
      backgroundColor: 'transparent',
      color: hasValue ? theme.colors.black : theme.colors['grey-light']
    })
  )
)

const DropdownFieldContainer = withClassName(
  'w-full cursor-pointer dropdownFieldContainer',
  'DropdownFieldContainer'
)(
  styled('div')(({ theme, disabled }: { theme: any; disabled: boolean }) => ({
    marginTop: rem(12),
    height: rem(44),
    cursor: disabled ? 'default' : 'pointer',
    pointerEvents: disabled ? 'none' : 'inherit',
    outline: 'none',
    [theme.mq.md]: {
      marginTop: rem(11)
    }
  }))
)

const FieldContainer = withClassName(
  'flex flex-row items-center',
  'FieldContainer'
)(
  styled('div')(
    ({
      theme,
      hasError,
      disabled
    }: {
      theme: any
      hasError?: boolean
      disabled?: boolean
    }) => ({
      paddingLeft: rem(24),
      border: `solid 1px`,
      borderColor: hasError
        ? `${theme.colors.brand}`
        : `${theme.colors['very-light-pink']}`,
      ':focus': {
        border: `solid 1px #FDB8A3`
      },
      backgroundColor: disabled ? '#E8EAEC' : 'initial'
    })
  )
)

const ErrorContainer = withClassName('flex flex-row', 'ErrorContainer')(
  styled('div')({})
)
const ValueSuffix = withClassName('')(
  styled('span')({
    color: '#7A7A7A',
    fontSize: rem(14),
    padding: `0 ${rem(4)}`
  })
)
const DropDownArrow = withClassName('', 'Arrow')(
  styled('div')(
    {
      width: 0,
      height: 0,
      borderStyle: 'solid',
      marginRight: rem(16)
    },
    ({ size = 8 }: { theme?: any; size?: number; colorKey?: string }) => ({
      borderColor: `#808695 transparent transparent transparent`,
      borderWidth: `${size}px ${size}px 0 ${size}px`
    })
  )
)

export {
  DropdownContainer,
  DropdownItem,
  DisplayFieldContainer,
  DropdownFieldContainer,
  FieldContainer,
  ErrorContainer,
  ValueSuffix,
  DropDownArrow
}
