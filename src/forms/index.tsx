import { rem } from 'dma-ui'
import { css } from 'emotion'
import React, { SFC } from 'react'

export { default as validator } from './validator'
export { default as ValidatedIcon } from './ValidatedIcon/ValidatedIcon.view'

// Validation rules
export * from './Rules/isVNPhoneNumber'
export * from './Rules/hasNoSpecialChars'
export * from './Rules/date.validator'

const InputErrorIcon: SFC = () => (
  <div className={css({ marginRight: rem(12) })}>
    <img src={require('./assets/error.svg')} width={36} height={36} />
  </div>
)

const InputValidatedIcon: SFC = () => (
  <div className={css({ marginRight: rem(12) })}>
    <img src={require('./assets/checkmark.svg')} width={32} height={32} />
  </div>
)

// Form components
export { default as TextInput } from './TextInput/TextInput.component'
export { default as DateInput } from './DateInput/DateInput.component'
export {
  default as TextMaskedInput
} from './TextMaskedInput/TextMaskedInput.component'
export { default as Dropdown } from './Dropdown/Dropdown.component'
export { default as RadioInput } from './RadioInput/RadioInput.component'
export { default as Checkbox } from './Checkbox/Checkbox.component'
export { default as TextArea } from './TextArea/TextArea.component'
export { default as ErrorIcon } from './ErrorIcon/ErrorIcon.view'
export { InputErrorIcon, InputValidatedIcon }
