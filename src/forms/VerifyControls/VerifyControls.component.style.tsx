import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const IconActionsContainer = withClassName(
  'flex flex-row items-center',
  'IconActionsContainer'
)(
  styled('div')({
    padding: `0 ${rem(10)}`,
    marginTop: rem(12),
    height: rem(48)
  })
)

const IconActionButton = withClassName(
  'focus:outline-none',
  'IconActionButton'
)(
  styled('button')({
    padding: `0 ${rem(6)}`,
    '> img': {
      maxWidth: 'none'
    }
  })
)

const PopupContainer = withClassName(
  'bg-white rounded-lg p-8',
  'PopupContainer'
)(
  styled('div')({
    width: rem(600),
    minHeight: rem(250)
  })
)

const CloseIconContainer = withClassName(
  'absolute bg-white shadow flex items-center justify-center cursor-pointer',
  'CloseIconContainer'
)(
  styled('div')({
    width: rem(26),
    height: rem(26),
    borderRadius: rem(13),
    top: rem(-13),
    right: rem(-13)
  })
)

const TextArea = withClassName(
  'text-base w-full h-full text-grey-darkest',
  'TextArea'
)(
  styled('textarea')(({ theme }) => ({
    paddingTop: rem(12),
    paddingBottom: rem(12),
    minHeight: rem(230),
    lineHeight: 1.5,
    '::placeholder': {
      color: theme.colors['grey-dark']
    },
    ':focus': {
      outline: 'none'
    },
    ':disabled::placeholder': {
      opacity: 1
    }
  }))
)

export {
  TextArea,
  CloseIconContainer,
  PopupContainer,
  IconActionsContainer,
  IconActionButton
}
