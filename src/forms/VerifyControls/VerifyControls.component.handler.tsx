import { getIn } from 'formik'
import { ChangeEvent } from 'react'

import {
  getCommentName,
  getEditableName,
  getVerifiedName,
  IProps,
  IStateHandlers,
  IStateProps
} from './VerifyControls.component'

const handlers = {
  SET_EDIT_MODE: (props: IProps) => () => {
    const editableName = getEditableName(props)
    const editable = getIn(props.values, editableName)
    if (props.setFieldValue) {
      props.setFieldValue(editableName, !editable)
    }
  },
  SET_VERIFIED: (props: IProps) => () => {
    const verifiedName = getVerifiedName(props)
    const verified = getIn(props.values, verifiedName)
    if (props.setFieldValue) {
      props.setFieldValue(verifiedName, !verified)
    }
  },
  SHOW_COMMENT: (props: IProps & IStateHandlers & IStateProps) => () => {
    props.showComment(!props.showedComment)
  },
  ON_WRITE_COMMENT: (props: IProps) => (
    e: ChangeEvent<HTMLTextAreaElement>
  ) => {
    const commentName = getCommentName(props)
    if (props.setFieldValue) {
      props.setFieldValue(commentName, e.currentTarget.value)
    }
  }
}

export default handlers
