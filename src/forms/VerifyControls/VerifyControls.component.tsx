import { Classes, Overlay } from '@blueprintjs/core'
import { cx } from 'emotion'
import { getIn } from 'formik'
import React, { ChangeEvent, FunctionComponent } from 'react'
import { compose, withHandlers, withState } from 'recompose'

import { t } from '../../i18n'

import handlers from './VerifyControls.component.handler'
import {
  CloseIconContainer,
  IconActionsContainer,
  IconActionButton,
  PopupContainer,
  TextArea
} from './VerifyControls.component.style'

interface IProps {
  label?: string
  name: string
  values: any
  setFieldValue?(key: string, value: any): void
  canVerify?: boolean
}

interface IHandlers {
  SET_EDIT_MODE(): void
  SET_VERIFIED(): void
  SHOW_COMMENT(): void
  ON_WRITE_COMMENT(e: ChangeEvent<HTMLTextAreaElement>): void
}

interface IStateProps {
  showedComment: boolean
}

interface IStateHandlers {
  showComment(showed: boolean): void
}

// const iconEye = require('./assets/ic-eye.svg')
const iconEdit = require('./assets/ic-edit.svg')
const iconEditRed = require('./assets/ic-edit-red.svg')
const iconComment = require('./assets/ic-comment.svg')
const iconCheckboxOn = require('./assets/ic-checkbox-on.svg')
const iconCheckboxOff = require('./assets/ic-checkbox-off.svg')

const getFieldName = (props: any) => {
  const { canVerify = false, name } = props

  return canVerify ? `${name}.input` : name
}
const getFieldNameDropDownTextInput = (props: any) => {
  const { canVerify = false, nameOfDropdown } = props

  return canVerify ? `${nameOfDropdown}.input` : nameOfDropdown
}

const getEditableName = (props: any) => `${props.name}.editable`
const getVerifiedName = (props: any) => `${props.name}.verified`
const getCommentName = (props: any) => `${props.name}.comment`
const getNoEditName = (props: any) => `${props.name}.noEdit`
const getNoVerify = (props: any) => `${props.name}.noVerify`
const getRemoveVerify = (props: any) => `${props.name}.removeVerify`

const VerifyControls: FunctionComponent<
  IProps & IHandlers & IStateHandlers & IStateProps
> = (props) => {
  const verified = getIn(props.values, getVerifiedName(props))
  const comment = getIn(props.values, getCommentName(props))
  const editable = getIn(props.values, getEditableName(props))
  const noEdit = getIn(props.values, getNoEditName(props))
  const noVerify = getIn(props.values, getNoVerify(props))
  const removeVerify = getIn(props.values, getRemoveVerify(props))

  return (
    <IconActionsContainer>
      {/*<IconActionButton type='button'>
              <img src={iconEye} width={40} height={40} />
            </IconActionButton>*/}
      <IconActionButton
        type="button"
        disabled={noEdit}
        onClick={props.SET_EDIT_MODE}
      >
        <img src={editable ? iconEditRed : iconEdit} width={20} height={20} />
      </IconActionButton>
      <IconActionButton type="button" onClick={props.SHOW_COMMENT}>
        <img src={iconComment} width={20} height={20} />
      </IconActionButton>

      {!removeVerify && (
        <IconActionButton
          type="button"
          onClick={props.SET_VERIFIED}
          disabled={noVerify}
          className="verifyCheckbox"
        >
          <img
            src={verified ? iconCheckboxOn : iconCheckboxOff}
            width={20}
            height={20}
          />
        </IconActionButton>
      )}

      <Overlay
        isOpen={props.showedComment}
        className={cx(
          Classes.OVERLAY_SCROLL_CONTAINER,
          'flex',
          'items-center',
          'justify-center'
        )}
      >
        <PopupContainer>
          <div className="text-lg text-grey-darkest">{props.label}</div>
          <TextArea
            placeholder={t('applicationForm.pleaseWriteComment')}
            onChange={props.ON_WRITE_COMMENT}
            value={comment}
            maxLength={100}
          />
          <CloseIconContainer onClick={props.SHOW_COMMENT}>
            <img src={require('./assets/ic-x.svg')} width={10} height={10} />
          </CloseIconContainer>
        </PopupContainer>
      </Overlay>
    </IconActionsContainer>
  )
}

const enhancer = compose(
  withState('showedComment', 'showComment', false),
  withHandlers(handlers)
)

export {
  IProps,
  IStateHandlers,
  IStateProps,
  getCommentName,
  getEditableName,
  getVerifiedName,
  getFieldName,
  getFieldNameDropDownTextInput
}
export default enhancer(VerifyControls)
