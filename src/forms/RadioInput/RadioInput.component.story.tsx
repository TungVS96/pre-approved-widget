import { withKnobs } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'
import { noop } from 'ramda-adjunct'
import React from 'react'

import { t } from '../../i18n'
import { getFieldProps } from '../../utils'

import RadioInput from './RadioInput.component'

const defaultProps = {
  values: {
    field1: '',
    field2: 'true'
  }
}

storiesOf('RadioInput', module)
  .addDecorator(withKnobs)
  .add('default', () => (
    <RadioInput
      {...getFieldProps('field1', defaultProps)}
      label={'Is this a radio input box?'}
      options={[
        { name: t('mpContracting.steps.collateral.yes'), value: 'true' },
        { name: t('mpContracting.steps.collateral.no'), value: 'false' }
      ]}
      handleChange={noop}
      handleBlur={noop}
    />
  ))
  .add('with preselected value', () => {
    return (
      <RadioInput
        {...getFieldProps('field2', defaultProps)}
        label={'Is this a radio input box?'}
        options={[
          { name: t('mpContracting.steps.collateral.yes'), value: 'true' },
          { name: t('mpContracting.steps.collateral.no'), value: 'false' }
        ]}
        handleChange={noop}
        handleBlur={noop}
      />
    )
  })
