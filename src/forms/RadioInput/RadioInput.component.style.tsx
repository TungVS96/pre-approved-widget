import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const RadioContainer = withClassName('flex w-full')(
  styled('div')(({ vertical }: { vertical: boolean }) => ({
    flexDirection: vertical ? 'column' : 'row'
  }))
)

const RadioFieldContainer = withClassName('flex flex-row')(
  styled('div')(({ vertical }: { vertical: boolean }) => ({
    marginBottom: rem(16),
    marginRight: vertical ? 0 : rem(24),
    ':last-child': {
      marginBottom: vertical ? 0 : rem(16)
    },
    alignSelf: vertical ? 'row' : 'center'
  }))
)

const RadioInputField = withClassName('text-lg')(
  styled('input')(({ theme }) => ({
    position: 'absolute',
    left: '-9999999px',
    '+ label:last-child': {},
    '+ label:before': {
      content: "''",
      position: 'absolute',
      left: '0',
      top: '50%',
      width: rem(16),
      height: rem(16),
      border: `${rem(1)} solid #808695`,
      borderRadius: '100%',
      transform: 'translateY(-50%)'
    },
    '+ label:after': {
      content: "''",
      width: rem(12),
      height: rem(12),
      background: theme.colors.brand,
      position: 'absolute',
      top: rem(2),
      left: rem(2),
      borderRadius: '100%',
      transition: 'all 0.2s ease',
      transform: 'translateY(-50%)'
    },
    ':not(:checked) + label:after': {
      opacity: 0,
      transform: 'scale(0)'
    },
    ':checked + label:after': {
      opacity: 1,
      transform: 'scale(1)'
    },
    ':focus': {
      '+ label:before': {
        content: "''",
        width: rem(16),
        height: rem(16),
        border: `${rem(1)} solid ${theme.colors.brand}`,
        borderRadius: '100%',
        transform: 'translateY(-50%)',
        boxShadow: `0px 0px 5px ${theme.colors.brand}`
      }
    }
  }))
)

const RadioLabel = withClassName(
  'whitespace-no-wrap t1-title-bold-16px text-grey-darkest'
)(
  styled('label')(() => ({
    position: 'relative',
    paddingLeft: rem(28),
    cursor: 'pointer',
    lineHeight: rem(16),
    display: 'inline-block',
    fontSize: rem(13)
  }))
)

const ErrorContainer = withClassName('flex flex-row', 'ErrorContainer')(
  styled('div')({})
)

export {
  RadioFieldContainer,
  RadioContainer,
  RadioInputField,
  RadioLabel,
  ErrorContainer
}
