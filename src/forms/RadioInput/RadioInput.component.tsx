import { getIn } from 'formik'
import { isNonEmptyString } from 'ramda-adjunct'
import React, { FunctionComponent } from 'react'

import { rem } from 'dma-ui'
import { css } from 'emotion'
import { withPureField } from '../../hocs'
import {
  InputErrorLabel,
  InputLabel
} from '../TextInput/TextInput.component.style'
import VerifyControls, {
  getEditableName,
  getFieldName
} from '../VerifyControls/VerifyControls.component'
import {
  ErrorContainer,
  RadioContainer,
  RadioFieldContainer,
  RadioInputField,
  RadioLabel
} from './RadioInput.component.style'

interface IRadioOptions {
  value: string
  name: string
}

interface IProps {
  inputProps?: {
    [key: string]: any
  }
  values?: object
  name: string
  value: string
  errorMessage: string
  handleChange?(event: Event): void
  handleBlur?(event: Event): void
  label?: string
  className?: string
  options: IRadioOptions[]
  requiredField?: boolean
  canVerify?: boolean
  setFieldValue?(key: string, value: any): void
  disabled?: boolean
  hasErrorIcon?: boolean
  vertical?: boolean
  labelSuffix?: string
  labelStyle?: string
  rightLabelContent?: React.ReactNode
}

const RadioInput: FunctionComponent<IProps> = (props) => {
  const {
    label,
    handleChange,
    handleBlur,
    inputProps,
    className,
    options,
    values = {},
    value,
    errorMessage,
    requiredField = false,
    canVerify = false,
    disabled = false,
    vertical = true,
    hasErrorIcon = false,
    labelStyle,
    labelSuffix,
    rightLabelContent
  } = props

  const fieldName = getFieldName(props)
  const hasError = isNonEmptyString(errorMessage)
  const editable = canVerify ? getIn(values, getEditableName(props)) : !disabled

  return (
    <div className={className}>
      <div className="text-grey-darkest text-sm mb-4 relative">
        <div className="flex flex-row justify-between">
          {label && (
            <InputLabel className={labelStyle}>
              {label}
              {requiredField && <span className="text-brand">*</span>}{' '}
              {labelSuffix && (
                <span className="font-normal text-grey-darkest">
                  {labelSuffix}
                </span>
              )}
            </InputLabel>
          )}
          {rightLabelContent}
        </div>
      </div>
      <div className="flex flex-row">
        <RadioContainer vertical={vertical}>
          {options.map((option, i) => {
            const id = fieldName + option.value

            return (
              <RadioFieldContainer key={i} vertical={vertical}>
                <RadioInputField
                  type="radio"
                  name={fieldName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={option.value}
                  id={id}
                  checked={option.value === value}
                  disabled={!editable}
                  {...inputProps}
                />
                <RadioLabel htmlFor={id}>{option.name}</RadioLabel>
              </RadioFieldContainer>
            )
          })}
        </RadioContainer>
        {canVerify && <VerifyControls {...props} />}
      </div>
      <ErrorContainer>
        {hasError && hasErrorIcon && (
          <div>
            <img
              width={16}
              height={16}
              src={require('./assets/ic-error.svg')}
              className={css({
                marginRight: rem(8),
                marginTop: rem(3)
              })}
            />
          </div>
        )}
        <InputErrorLabel>{hasError && errorMessage}</InputErrorLabel>
      </ErrorContainer>
    </div>
  )
}

export default withPureField<IProps, IProps>({ updatedKeys: ['options'] })(
  RadioInput
)
