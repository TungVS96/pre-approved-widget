import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const InputContainer = withClassName(
  'flex flex-row items-center w-full bg-white inputContainer',
  'InputContainer'
)(
  styled('div')(
    ({
      theme,
      paddingLeft = 12,
      hasError
    }: {
      theme: any
      paddingLeft: number
      hasError: boolean
    }) => ({
      marginTop: rem(12),
      height: rem(48),
      paddingLeft: rem(paddingLeft),
      border: `solid 1px`,
      borderColor: hasError
        ? `${theme.colors.brand}`
        : `${theme.colors['very-light-pink']}`,
      borderRadius: rem(5),
      ':focus-within': {
        border: `solid 1px #FDB8A3`
      }
    })
  )
)

const InputFieldContainer = withClassName(
  'w-full text-grey-darkest',
  'InputFieldContainer'
)(
  styled('input')({
    height: rem(48),
    backgroundColor: 'transparent',
    lineHeight: '16px',
    '::placeholder': {
      fontFamily: 'Helvetica',
      color: '#8B8B92'
    },
    ':focus': {
      outline: 'none'
    },
    ':disabled::placeholder': {
      opacity: 1
    },
    fontSize: '14px'
  })
)

const InputLabel = withClassName('text-grey-darkest', 'InputLabel')(
  styled('div')({
    fontSize: rem(13),
    fontWeight: 500
  })
)

const InputIcon = withClassName(
  'flex flex-no-shrink items-center justify-center',
  'InputIcon'
)(
  styled('div')({
    width: rem(24),
    height: rem(24),
    marginRight: rem(11)
  })
)

const ErrorContainer = withClassName('flex flex-row', 'ErrorContainer')(
  styled('div')({
    marginTop: rem(4)
  })
)

const InputErrorLabel = withClassName('text-brand text-xs', 'InputErrorLabel')(
  styled('div')({
    minHeight: rem(24)
  })
)

export {
  InputContainer,
  InputFieldContainer,
  InputLabel,
  InputIcon,
  InputErrorLabel,
  ErrorContainer
}
