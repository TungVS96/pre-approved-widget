import { rem } from 'dma-ui'
import { css } from 'emotion'
import { getIn, InjectedFormikProps } from 'formik'
import { isNonEmptyString } from 'ramda-adjunct'
import React, { ChangeEvent, FunctionComponent } from 'react'
import { compose, withState } from 'recompose'
import { withPureField } from '../../hocs'
import VerifyControls, {
  getEditableName,
  getFieldName
} from '../VerifyControls/VerifyControls.component'
import { ValidatedIcon } from '../index'
import Bubble from './Bubble/Bubble.view'
import {
  ErrorContainer,
  InputContainer,
  InputErrorLabel,
  InputFieldContainer,
  InputIcon,
  InputLabel
} from './TextInput.component.style'

interface IInnerProps {
  focusing: boolean
  setFocus(focusing: boolean): void
}
interface IProps {
  inputProps?: {
    [key: string]: any
  }
  name: string
  hasTouched: boolean
  errorMessage: string
  values?: object
  value: string
  leftIcon?: React.ReactNode
  rightIcon?: React.ReactNode
  errorIcon?: React.ReactNode
  validatedIcon?: React.ReactNode
  handleChange(event: ChangeEvent<HTMLInputElement>): void
  handleBlur(event: ChangeEvent<HTMLInputElement>): void
  label?: string
  placeholder: string
  type: string
  requiredField?: boolean
  transformValue?(value: string): string
  labelSuffix?: string
  valueSuffix?: string
  disabled?: boolean
  canVerify?: boolean
  setFieldValue?(key: string, value: any): void
  hasErrorBlock?: boolean
  maxLength?: number
  valuePrefix?: React.ReactNode
  onTextChange?(value: string): any
  format?(value: string): string
  handleChangeAmount?(
    event: ChangeEvent<HTMLInputElement>,
    startPosition: number
  ): void
  handleFocus?(event: any): void
  inputWrapperStyle?: string
  labelStyle?: string
  numberics?: boolean
  autoComplete?: string
  conditionList?: any[]
  rightLabelContent?: React.ReactNode
  useValueFormat?: boolean
}

interface IFormTextInput<Props, Values> {
  name: string
  label?: string
  placeholder?: string
  type?: string
  requiredField?: boolean
  labelSuffix?: string
  dynamicLabel?: boolean
  valueSuffix?: string
  props: InjectedFormikProps<Props, Values>
  transformValue?(value: string): string
  disabled?: boolean
  canVerify?: boolean
  inputProps?: {
    [key: string]: any
  }
  hasErrorBlock?: boolean
  capslockOn?: boolean
  setFieldValue?(key: string, value: any): void
  maxLength?: number
  valuePrefix?: React.ReactNode
  onTextChange?(value: string): any
  format?(value: any): string
  handleChangeAmount?(
    event: ChangeEvent<HTMLInputElement>,
    startPosition: number
  ): void
  handleFocus?(event: any): void
  handleBlur?(): void
  inputWrapperStyle?: string
  labelStyle?: string
  numberics?: boolean
  autoComplete?: string
  leftIcon?: React.ReactNode
  rightIcon?: React.ReactNode
  conditionList?: any[]
  rightLabelContent?: React.ReactNode
  useValueFormat?: boolean
}

const onChange = (props: IProps) => (e: ChangeEvent<HTMLInputElement>) => {
  const start = e.target.selectionStart as number
  if (props.transformValue) {
    e.target.value = props.transformValue(e.target.value)
  }

  props.handleChange(e)
  if (props.onTextChange) {
    props.onTextChange(
      props.format && props.useValueFormat
        ? props.format(e.target.value)
        : e.target.value
    )
  }

  if (props.handleChangeAmount) {
    props.handleChangeAmount(e, start)
  }
}

const onFocus = (props: IProps & IInnerProps) => (
  e: ChangeEvent<HTMLInputElement>
) => {
  props.setFocus(true)

  // tslint:disable-next-line:no-unused-expression
  props.handleFocus && props.handleFocus(e)
}
const onBlur = (props: IProps & IInnerProps) => (
  e: ChangeEvent<HTMLInputElement>
) => {
  props.setFocus(false)
  props.handleBlur(e)
}

const getDisplayText = ({ type, numberics, value, format }: IProps) => {
  return type === 'tel' && !numberics
    ? value
    : value && value !== '0'
    ? format
      ? format(value)
      : value
    : value
}

const TextInput: FunctionComponent<IProps & IInnerProps> = (props) => {
  const {
    type,
    leftIcon,
    rightIcon,
    errorIcon,
    validatedIcon = <ValidatedIcon />,
    placeholder,
    label,
    hasTouched,
    value,
    errorMessage,
    values,
    inputProps,
    requiredField = false,
    labelSuffix,
    valueSuffix,
    disabled,
    canVerify = false,
    maxLength,
    valuePrefix,
    inputWrapperStyle,
    labelStyle,
    autoComplete,
    focusing,
    conditionList,
    rightLabelContent
  } = props
  const fieldName = getFieldName(props)
  const hasError = hasTouched && isNonEmptyString(errorMessage)
  const hasValue = isNonEmptyString(value)
  const editable = canVerify ? getIn(values, getEditableName(props)) : !disabled

  const displayText = getDisplayText(props)

  return (
    <div className="flex flex-col w-full relative">
      <div className="flex flex-row justify-between">
        {label && (
          <InputLabel className={labelStyle}>
            {label}
            {requiredField && <span className="text-brand">*</span>}{' '}
            {labelSuffix && (
              <span className="font-normal text-grey-darkest">
                {labelSuffix}
              </span>
            )}
          </InputLabel>
        )}
        {rightLabelContent}
      </div>

      <div className="flex flex-row w-full">
        <InputContainer className={inputWrapperStyle} hasError={hasError}>
          {leftIcon && <InputIcon>{leftIcon}</InputIcon>}
          {valuePrefix}
          <InputFieldContainer
            type={type}
            placeholder={placeholder}
            name={fieldName}
            onChange={onChange(props)}
            onBlur={onBlur(props)}
            disabled={!editable}
            value={displayText}
            maxLength={maxLength}
            onFocus={onFocus(props)}
            {...inputProps}
            autoComplete={autoComplete}
          />
          {hasError && errorIcon}
          {hasTouched && !hasError && hasValue && validatedIcon}
          {rightIcon}
          {valueSuffix && (
            <div
              className={`text-grey-dark text-sm font-bold ${css({
                marginRight: rem(28)
              })}`}
            >
              {valueSuffix}
            </div>
          )}
        </InputContainer>
        {canVerify && <VerifyControls {...props} />}
      </div>
      {props.hasErrorBlock && (
        <ErrorContainer>
          {hasError && (
            <div>
              <img
                width={16}
                height={16}
                src={require('./assets/ic-error.svg')}
                className={css({
                  marginRight: rem(8)
                })}
              />
            </div>
          )}
          <InputErrorLabel>{hasError && errorMessage}</InputErrorLabel>
        </ErrorContainer>
      )}
      {conditionList && focusing && (
        <div
          className={css({
            position: 'absolute',
            bottom: -60,
            width: '100%'
          })}
        >
          <Bubble
            title={'Mật khẩu cần đầy đủ các yếu tố sau:'}
            conditionList={conditionList}
          />
        </div>
      )}
    </div>
  )
}

TextInput.defaultProps = {
  hasErrorBlock: true
}

export { IFormTextInput, IProps }
export default compose<IProps & IInnerProps, IProps>(
  withState('focusing', 'setFocus', null),
  withPureField<IProps, IProps>({
    updatedKeys: ['rightIcon', 'labelSuffix', 'hasErrorBlock', 'focusing']
  })
)(TextInput)
