import React, { FunctionComponent } from 'react'
import {
  ConditionItem,
  ConditionWrapper,
  Container,
  Title
} from './Bubble.style'

interface ICondition {
  label: string
  verified: boolean
}

interface IProps {
  conditionList: ICondition[]
  title?: string
}

const Bubble: FunctionComponent<IProps> = (props) => {
  const { conditionList, title } = props

  return (
    <Container>
      <Title>{title}</Title>
      <ConditionWrapper>
        {conditionList.map((condition: ICondition, index: number) => {
          return (
            <ConditionItem
              key={`${index}_condition`}
              verified={condition.verified}
            >
              {condition.label}
            </ConditionItem>
          )
        })}
      </ConditionWrapper>
    </Container>
  )
}

export default Bubble
