import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const Container = withClassName('w-full relative bg-white', 'Container')(
  styled('div')({
    zIndex: 1,
    height: 'auto',
    border: `${rem(1)} solid #E8E8E8`,
    marginBottom: `${rem(8)}`,
    padding: `${rem(8)}`,
    boxSizing: 'border-box',
    borderRadius: rem(5)
    // 'Container:after': {
    //   position: 'absolute',
    //   width: rem(50),
    //   height: rem(50),
    //   borderTop: `${rem(2)} solid red`,
    //   borderRight: `${rem(2)} solid red`,
    //   borderBottom: `${rem(2)} solid red`,
    //   borderLeft: `${rem(2)} solid red`,
    //   top: '100%',
    //   left: '50%',
    //   marginLeft: rem(-25)
    // }
  })
)

const Title = withClassName('')(
  styled('p')(({}) => ({
    color: '#808080',
    fontSize: rem(12),
    fontStyle: 'normal',
    lineHeight: rem(24)
  }))
)
const ConditionWrapper = withClassName('flex flex-wrap')(
  styled('div')(({ verified }: { verified: boolean }) => ({
    color: verified ? '#31BE1D' : '#606060',
    justifyContent: 'space-between'
  }))
)
const ConditionItem = withClassName('')(
  styled('div')(({ verified }: { verified: boolean }) => ({
    color: verified ? '#31BE1D' : '#606060',
    fontWeight: 500,
    fontSize: rem(12),
    lineHeight: rem(24)
  }))
)

export { Container, ConditionWrapper, ConditionItem, Title }
