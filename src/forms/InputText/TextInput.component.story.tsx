import { storiesOf } from '@storybook/react'
import { css } from 'emotion'
import React from 'react'

import { t } from '../../i18n'
import { getFieldProps } from '../../utils'
import { ErrorIcon } from '../index'

import TextInput from './TextInput.component'

const defaultProps = {
  errors: {
    name: 'error'
  },
  values: {
    name: ''
  },
  touched: {
    name: false
  }
}

const leftIconClassName = css({
  width: 32,
  height: 32,
  backgroundColor: '#ED1C24'
})

const NOOP = () => 'nothing'

storiesOf('TextInput', module)
  .add('default', () => (
    <div className={css({ margin: 20 })}>
      <TextInput
        {...getFieldProps('name', defaultProps)}
        type={'text'}
        label={t('login.form.username')}
        placeholder={t('login.form.usernamePlaceholder')}
        leftIcon={<div className={`flex-no-shrink ${leftIconClassName}`} />}
        errorIcon={<ErrorIcon />}
        handleChange={NOOP}
        handleBlur={NOOP}
      />
    </div>
  ))
  .add('with error state', () => {
    const props = {
      ...defaultProps,
      touched: {
        name: true
      },
      errors: {
        name: 'Please enter Name'
      }
    }

    return (
      <div className={css({ margin: 20 })}>
        <TextInput
          {...getFieldProps('name', props)}
          type={'text'}
          label={t('login.form.username')}
          placeholder={t('login.form.usernamePlaceholder')}
          leftIcon={<div className={`flex-no-shrink ${leftIconClassName}`} />}
          errorIcon={<ErrorIcon />}
          handleChange={NOOP}
          handleBlur={NOOP}
        />
      </div>
    )
  })
  .add('without left icon', () => (
    <div className={css({ margin: 20 })}>
      <TextInput
        {...getFieldProps('name', defaultProps)}
        type={'text'}
        label={t('home.form.name')}
        placeholder={t('home.form.namePlaceholder')}
        errorIcon={<ErrorIcon />}
        handleChange={NOOP}
        handleBlur={NOOP}
      />
    </div>
  ))
  .add('with required field indicator', () => (
    <div className={css({ margin: 20 })}>
      <TextInput
        {...getFieldProps('name', defaultProps)}
        type={'text'}
        requiredField={true}
        label={t('login.form.username')}
        placeholder={t('login.form.usernamePlaceholder')}
        leftIcon={<div className={`flex-no-shrink ${leftIconClassName}`} />}
        errorIcon={<ErrorIcon />}
        handleChange={NOOP}
        handleBlur={NOOP}
      />
    </div>
  ))
  .add('with prefilled text', () => {
    const props = Object.assign({}, defaultProps, {
      values: { name: 'John Doe' }
    })

    return (
      <div className={css({ margin: 20 })}>
        <TextInput
          {...getFieldProps('name', props)}
          type={'text'}
          requiredField={true}
          label={t('login.form.username')}
          placeholder={t('login.form.usernamePlaceholder')}
          leftIcon={<div className={`flex-no-shrink ${leftIconClassName}`} />}
          errorIcon={<ErrorIcon />}
          handleChange={NOOP}
          handleBlur={NOOP}
        />
      </div>
    )
  })
  .add('with text and disabled', () => {
    const props = Object.assign({}, defaultProps, {
      values: { name: 'John Doe' }
    })

    return (
      <div className={css({ margin: 20 })}>
        <TextInput
          {...getFieldProps('name', props)}
          type={'text'}
          requiredField={true}
          label={t('login.form.username')}
          placeholder={t('login.form.usernamePlaceholder')}
          leftIcon={<div className={`flex-no-shrink ${leftIconClassName}`} />}
          errorIcon={<ErrorIcon />}
          handleChange={NOOP}
          handleBlur={NOOP}
          disabled={true}
        />
      </div>
    )
  })
