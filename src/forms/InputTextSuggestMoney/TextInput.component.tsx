import { rem } from 'dma-ui'
import { css } from 'emotion'
import { getIn, InjectedFormikProps } from 'formik'
import { isNonEmptyString } from 'ramda-adjunct'
import React, { ChangeEvent, FunctionComponent } from 'react'
import OutsideClickHandler from 'react-outside-click-handler'
import { compose, withState } from 'recompose'
import { withPureField } from '../../hocs'
import VerifyControls, {
  getEditableName,
  getFieldName
} from '../VerifyControls/VerifyControls.component'
import { ValidatedIcon } from '../index'

import {
  ContainerListSuggest,
  ErrorContainer,
  InputContainer,
  InputErrorLabel,
  InputFieldContainer,
  InputIcon,
  InputLabel,
  ItemSuggest
} from './TextInput.component.style'

import { formatCurrency } from '../../utils'

interface IProps {
  inputProps?: {
    [key: string]: any
  }
  name: string
  hasTouched: boolean
  errorMessage: string
  values?: object
  value: string
  leftIcon?: React.ReactNode
  rightIcon?: React.ReactNode
  errorIcon?: React.ReactNode
  validatedIcon?: React.ReactNode
  handleChange(event: ChangeEvent<HTMLInputElement>): void
  label?: string
  placeholder: string
  type: string
  requiredField?: boolean
  transformValue?(value: string): string
  labelSuffix?: string
  valueSuffix?: string
  disabled?: boolean
  canVerify?: boolean
  setFieldValue?(key: string, value: any): void
  hasErrorBlock?: boolean
  maxLength?: number
  valuePrefix?: React.ReactNode
  onTextChange?(value: string): any
  format?(value: string): string
  handleChangeAmount?(
    event: ChangeEvent<HTMLInputElement>,
    startPosition: number
  ): void
  handleFocus?(event: any): void
  handleBlur?(event: any): void
  inputWrapperStyle?: string
  labelStyle?: string
  numberics?: boolean
  autoComplete?: string
  suggestMoney?: boolean
  times?: number
  setDisplaySuggestMoney?(value: boolean): void
  displaySuggestMoney?: boolean
  outsideClick?(): void
  suggestMoneyMaxLength?: number
  suggestMoneyAllowTypeZero?: boolean
  setFieldTouched?(key: string, value: any): void
  iconWarning?: boolean
}

interface IFormTextInput<Props, Values> {
  name: string
  label?: string
  placeholder?: string
  type?: string
  requiredField?: boolean
  labelSuffix?: string
  dynamicLabel?: boolean
  valueSuffix?: string
  props: InjectedFormikProps<Props, Values>
  transformValue?(value: string): string
  disabled?: boolean
  canVerify?: boolean
  inputProps?: {
    [key: string]: any
  }
  hasErrorBlock?: boolean
  capslockOn?: boolean
  setFieldValue?(key: string, value: any): void
  maxLength?: number
  valuePrefix?: React.ReactNode
  onTextChange?(value: string): any
  format?(value: any): string
  handleChangeAmount?(
    event: ChangeEvent<HTMLInputElement>,
    startPosition: number
  ): void
  handleFocus?(event: any): void
  handleBlur?(event: any): void
  outsideClick?(): void
  inputWrapperStyle?: string
  labelStyle?: string
  numberics?: boolean
  autoComplete?: string
  suggestMoney?: boolean
  suggestMoneyMaxLength?: number
  suggestMoneyAllowTypeZero?: boolean
  times?: number
  iconWarning?: boolean
}

const onChange = (props: IProps) => (e: ChangeEvent<HTMLInputElement>) => {
  const start = e.target.selectionStart as number
  if (!props.suggestMoney) {
    if (props.transformValue) {
      e.target.value = props.transformValue(e.target.value)
    }

    props.handleChange(e)
    if (props.onTextChange) {
      props.onTextChange(e.target.value)
    }

    if (props.handleChangeAmount) {
      props.handleChangeAmount(e, start)
    }

    return
  }

  let valueText = e.target.value

  if (
    e.target &&
    e.target.value &&
    e.target.value.length > (props.suggestMoneyMaxLength || 21)
  ) {
    valueText = valueText.substring(0, props.suggestMoneyMaxLength || 21)
  }

  const valueNumber = parseFloat(
    (valueText || '').replace(/,/g, '').replace(/[^0-9,.]+/g, '')
  )

  if (props.transformValue) {
    e.target.value =
      props.transformValue(valueText) === '0' || !valueNumber
        ? props.suggestMoneyAllowTypeZero
          ? '0'
          : ''
        : props.transformValue(valueNumber.toString())
  }

  props.handleChange(e)
  if (!valueNumber) {
    return
  }

  const { times = 10000 } = props

  if (props.setDisplaySuggestMoney) {
    props.setDisplaySuggestMoney(true)
  }
  suggestList = renderItem(valueNumber, times, props)
  if (props.transformValue && valueNumber > 0) {
    e.target.value = props.transformValue(valueText)
  }
}

const getDisplayText = ({ type, numberics, value, format }: IProps) => {
  return type === 'tel' && !numberics
    ? value
    : value && value !== '0'
    ? format
      ? format(value)
      : value
    : value
}

const onSelect = (props: any) => (value: number) => {
  props.setDisplaySuggestMoney(false)
  const fieldName = getFieldName(props)
  props.setFieldValue(fieldName, value)
}

const renderItem = (valueNumber: number, times: number, props: any) => {
  if (valueNumber < 1) {
    return <></>
  }
  const length = valueNumber.toString().replace(/,/g, '').length
  let dividendStr = '1'

  // tslint:disable prefer-for-of
  for (let i = 1; i < length; i = i + 1) {
    dividendStr = dividendStr.concat('0')
  }
  const dividend = parseInt(dividendStr, 10)

  if (props.transformValue) {
    return (
      <>
        {length < 5 && (
          <ContainerListSuggest>
            {length < 3 && (
              <div
                className="p-2"
                onClick={(e) => {
                  onSelect(props)((valueNumber * times) / dividend)
                  if (props.outsideClick) {
                    props.outsideClick()
                  }
                }}
              >
                {formatCurrency({ value: (valueNumber * times) / dividend })}
              </div>
            )}
            <div
              className="p-2"
              onClick={(e) => {
                onSelect(props)((valueNumber * times * 10) / dividend)
                if (props.outsideClick) {
                  props.outsideClick()
                }
              }}
            >
              {formatCurrency({ value: (valueNumber * times * 10) / dividend })}
            </div>
            <div
              className="p-2"
              onClick={(e) => {
                onSelect(props)((valueNumber * times * 100) / dividend)
                if (props.outsideClick) {
                  props.outsideClick()
                }
              }}
            >
              {formatCurrency({
                value: (valueNumber * times * 100) / dividend
              })}
            </div>
            <div
              className="p-2"
              onClick={(e) => {
                onSelect(props)((valueNumber * times * 1000) / dividend)
                if (props.outsideClick) {
                  props.outsideClick()
                }
              }}
            >
              {formatCurrency({
                value: (valueNumber * times * 1000) / dividend
              })}
            </div>
          </ContainerListSuggest>
        )}
      </>
    )
  }

  return (
    <>
      {length < 5 && (
        <div>
          {length < 3 && (
            <ItemSuggest>{(valueNumber * times) / dividend}</ItemSuggest>
          )}
          <ItemSuggest>{(valueNumber * times * 10) / dividend}</ItemSuggest>
          <ItemSuggest>{(valueNumber * times * 100) / dividend}</ItemSuggest>
          <ItemSuggest>{(valueNumber * times * 1000) / dividend}</ItemSuggest>
        </div>
      )}
    </>
  )
}
let suggestList = <div />

const InputTextSuggestMoney: FunctionComponent<IProps> = (props) => {
  const {
    type,
    leftIcon,
    rightIcon,
    errorIcon,
    validatedIcon = <ValidatedIcon />,
    placeholder,
    label,
    hasTouched,
    value,
    errorMessage,
    handleBlur,
    values,
    inputProps,
    requiredField = false,
    labelSuffix,
    valueSuffix,
    disabled,
    canVerify = false,
    maxLength,
    valuePrefix,
    handleFocus,
    inputWrapperStyle,
    labelStyle,
    autoComplete,
    suggestMoney,
    iconWarning = true
  } = props
  const fieldName = getFieldName(props)
  const hasError = hasTouched && isNonEmptyString(errorMessage)
  const hasValue = isNonEmptyString(value)
  const editable = canVerify ? getIn(values, getEditableName(props)) : !disabled
  const displayText = getDisplayText(props)

  return (
    <div className="flex flex-col w-full">
      <div className="flex flex-row space-between">
        {label && (
          <InputLabel className={labelStyle}>
            {label}
            {requiredField && <span className="text-brand">*</span>}{' '}
            {labelSuffix && (
              <span className="font-normal text-grey-darkest">
                {labelSuffix}
              </span>
            )}
          </InputLabel>
        )}
      </div>
      <OutsideClickHandler
        onOutsideClick={() => {
          if (props.outsideClick) {
            props.outsideClick()
          }
          if (props.setDisplaySuggestMoney) {
            props.setDisplaySuggestMoney(false)
          }
        }}
      >
        <div className="flex flex-row w-full">
          <InputContainer className={inputWrapperStyle} hasError={hasError}>
            {leftIcon && <InputIcon>{leftIcon}</InputIcon>}
            {valuePrefix}
            <InputFieldContainer
              type={type}
              placeholder={placeholder}
              name={fieldName}
              onChange={(e: any) => {
                if (props.setFieldTouched) {
                  props.setFieldTouched(fieldName, true)
                }
                onChange(props)(e)
              }}
              onFocus={handleFocus}
              onBlur={(e: any) => {
                if (handleBlur) {
                  handleBlur(e)
                }
              }}
              onKeyDown={(e: any) => {
                if (e.keyCode === 9 && props.setDisplaySuggestMoney) {
                  props.setDisplaySuggestMoney(false)
                }
              }}
              disabled={!editable}
              value={displayText}
              maxLength={maxLength}
              {...inputProps}
              autoComplete={autoComplete}
            />
            {suggestMoney && props.displaySuggestMoney && suggestList}
            {hasError && errorIcon}
            {hasTouched && !hasError && hasValue && validatedIcon}
            {rightIcon}
            {valueSuffix && (
              <div
                className={`text-grey-dark text-sm font-bold ${css({
                  marginRight: rem(28)
                })}`}
              >
                {valueSuffix}
              </div>
            )}
          </InputContainer>

          {canVerify && <VerifyControls {...props} />}
        </div>
      </OutsideClickHandler>
      {props.hasErrorBlock && (
        <ErrorContainer>
          {hasError && iconWarning && (
            <div>
              <img
                width={16}
                height={16}
                src={require('./assets/ic-error.svg')}
                className={css({
                  marginRight: rem(8)
                })}
              />
            </div>
          )}
          <InputErrorLabel>{hasError && errorMessage}</InputErrorLabel>
        </ErrorContainer>
      )}
    </div>
  )
}

InputTextSuggestMoney.defaultProps = {
  hasErrorBlock: true
}

const enhancer = compose<IProps, IProps>(
  withState('displaySuggestMoney', 'setDisplaySuggestMoney', false),
  withPureField({
    updatedKeys: ['rightIcon', 'labelSuffix', 'displaySuggestMoney']
  })
)

export { IFormTextInput, IProps }
export default enhancer(InputTextSuggestMoney)
