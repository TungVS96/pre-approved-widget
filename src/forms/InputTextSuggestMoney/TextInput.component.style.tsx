import { convertToRGBA, rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const InputContainer = withClassName(
  'flex flex-row items-center w-full bg-white inputContainer',
  'InputContainer'
)(
  styled('div')(
    ({
      theme,
      hasError
    }: {
      theme: any
      paddingLeft: number
      hasError: boolean
    }) => ({
      marginTop: rem(12),
      height: rem(48),
      paddingLeft: rem(12),
      border: `solid 1px`,
      borderColor: hasError
        ? `${theme.colors.brand}`
        : `${theme.colors['very-light-pink']}`,
      borderRadius: rem(5),
      ':focus-within': {
        border: `solid 1px #FDB8A3`
      },
      position: 'relative'
    })
  )
)

const InputFieldContainer = withClassName(
  'w-full text-grey-darkest',
  'InputFieldContainer'
)(
  styled('input')(({}) => ({
    height: rem(48),
    backgroundColor: 'transparent',
    lineHeight: 1.5,
    '::placeholder': {
      fontFamily: 'Helvetica',
      color: '#DDDDDDDD',
      opacity: 1
    },
    ':focus': {
      outline: 'none'
    },
    ':disabled::placeholder': {
      opacity: 1
    },
    fontSize: rem(14)
  }))
)

const InputLabel = withClassName('text-grey-darkest', 'InputLabel')(
  styled('label')({
    fontSize: rem(13),
    fontWeight: rem(500)
  })
)

const InputIcon = withClassName(
  'flex flex-no-shrink items-center justify-center',
  'InputIcon'
)(
  styled('div')({
    width: rem(24),
    height: rem(24),
    marginRight: rem(16)
  })
)

const ErrorContainer = withClassName('flex flex-row', 'ErrorContainer')(
  styled('div')({
    marginTop: rem(4)
  })
)

const InputErrorLabel = withClassName('text-brand text-xs', 'InputErrorLabel')(
  styled('div')({
    color: '#ED1C24',
    minHeight: rem(24)
  })
)

const ContainerListSuggest = withClassName('')(
  styled('div')({
    border: '1px solid #DCDEE2',
    boxSizing: 'border-box',
    boxShadow: '0px 2px 4px #DFE1E5',
    borderRadius: '2px',
    position: 'absolute',
    zIndex: 10,
    backgroundColor: 'white',
    width: '100%',
    top: '2.9rem',
    left: '0px'
  })
)

const ItemSuggest = withClassName()(
  styled('div')(({ theme }) => {
    const paleRedTwo = convertToRGBA(theme.colors['pale-red-two'], 0.4)

    return {
      cursor: 'pointer',
      ':hover': {
        backgroundColor: paleRedTwo
      }
    }
  })
)

export {
  InputContainer,
  InputFieldContainer,
  InputLabel,
  InputIcon,
  InputErrorLabel,
  ErrorContainer,
  ContainerListSuggest,
  ItemSuggest
}
