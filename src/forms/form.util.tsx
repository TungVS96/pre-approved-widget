const formatOnlyNumber = (value: string) => {
  return value.replace(/[^0-9]/g, '')
}

const formatPassword = (value: string) => {
  let handleValue = value.replace(/\s/g, '')
  handleValue = handleValue.normalize('NFD')
  handleValue = handleValue.replace(/[\u0300-\u036f]/g, '')
  handleValue = handleValue.replace(/[đĐ]/g, (m) => (m === 'đ' ? 'd' : 'D'))

  return handleValue
}

export { formatPassword, formatOnlyNumber }
