import { EnhancedProps } from './TextInputAutoComplete.component'

const getFieldName = (props: any) => {
  const { canVerify = false, name } = props

  return canVerify ? `${name}.input` : name
}

const handlers = {
  ON_SELECT: (props: EnhancedProps) => (value: string) => {
    const fieldName = getFieldName(props)
    props.setFieldValue(fieldName, value)
    if (props.onSelect) {
      props.onSelect(value)
    }
  }
}

export default handlers
