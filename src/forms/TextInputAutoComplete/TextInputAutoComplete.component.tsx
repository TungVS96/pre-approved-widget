import { rem } from 'dma-ui'
import { css } from 'emotion'
import { getIn, InjectedFormikProps } from 'formik'
import { isEmpty } from 'ramda'
import { isNonEmptyString } from 'ramda-adjunct'
import React, { ChangeEvent, FunctionComponent } from 'react'
import AutoComplete from 'react-autocomplete'
import { compose, withHandlers } from 'recompose'
import TCBTooltip from '../../../src/components/TCBTooltip/TCBTooltip.component'

import { withPureField } from '../../hocs'
import { IDropDownItem } from '../../types'
import { DropdownItem } from '../Dropdown/Dropdown.style'
import {
  ErrorContainer,
  InputContainer,
  InputErrorLabel,
  InputIcon,
  InputLabel
} from '../InputText/TextInput.component.style'
import VerifyControls, {
  getEditableName,
  getFieldName
} from '../VerifyControls/VerifyControls.component'
import { ValidatedIcon } from '../index'
import handlers from './TextInputAutoComplete.component.handler'
import { styles } from './TextInputAutoComplete.component.style'

interface IHandlers {
  ON_SELECT(value: string): void
}

interface IProps {
  inputProps?: {
    [key: string]: any
  }
  name: string
  hasTouched?: boolean
  errorMessage?: string
  values?: object
  value: string
  leftIcon?: React.ReactNode
  rightIcon?: React.ReactNode
  errorIcon?: React.ReactNode
  validatedIcon?: React.ReactNode
  handleChange(event: ChangeEvent<HTMLInputElement>): void
  handleBlur(event: ChangeEvent<HTMLInputElement>): void
  label?: string
  placeholder: string
  type: string
  requiredField?: boolean
  transformValue?(value: string): string
  labelSuffix?: string
  valueSuffix?: string
  disabled?: boolean
  setFieldValue(key: string, value: string): void
  items: IDropDownItem[]
  onSelect?(value: string): void
  canVerify?: boolean
  onTextChange?(value: string): any
  tooltipContent?: string
  hasErrorBlock?: boolean
}

interface IFormTextInputAutoComplete<Props, Values> {
  name: string
  value?: string
  label?: string
  placeholder: string
  type?: string
  requiredField?: boolean
  labelSuffix?: string
  valueSuffix?: string
  props: InjectedFormikProps<Props, Values>
  transformValue?(value: string): string
  disabled?: boolean
  inputProps?: {
    [key: string]: any
  }
  items: IDropDownItem[]
  onSelect?(value: string): void
  onTextChange?(value: string): any
  tooltipContent?: string
  hasErrorBlock?: boolean
  leftIcon?: any
  rightIcon?: any
  canVerify?: boolean
}

const getItemValue = (item: IDropDownItem) => item.name
const renderItem = (item: IDropDownItem, isHighlighted: boolean) => (
  <div key={item.value}>
    <DropdownItem highlight={isHighlighted}>{item.name}</DropdownItem>
  </div>
)
const onChange = (props: EnhancedProps) => (
  e: ChangeEvent<HTMLInputElement>
) => {
  const fieldName = getFieldName(props)
  props.setFieldValue(fieldName, e.target.value)

  if (props.onTextChange) {
    props.onTextChange(e.target.value)
  }
}

const shouldItemRender = (item: IDropDownItem, value: string) => {
  return !isEmpty(item)
}

type EnhancedProps = IProps & IHandlers

const TextInputAutoComplete: FunctionComponent<EnhancedProps> = (props) => {
  const {
    leftIcon,
    rightIcon,
    errorIcon,
    validatedIcon = <ValidatedIcon />,
    placeholder,
    label,
    hasTouched,
    errorMessage,
    handleBlur,
    values,
    value,
    inputProps,
    requiredField = false,
    labelSuffix,
    valueSuffix,
    disabled,
    items,
    canVerify = false,
    tooltipContent = ''
  } = props
  const fieldName = getFieldName(props)
  const hasError = hasTouched && isNonEmptyString(errorMessage)
  const hasValue = isNonEmptyString(value)
  const editable = canVerify ? getIn(values, getEditableName(props)) : !disabled

  return (
    <div>
      {label && (
        <div className="flex flex-row relative">
          <InputLabel>
            {label}
            {requiredField && <span className="text-brand">*</span>}{' '}
            {labelSuffix && (
              <span className="font-normal text-grey-darkest">
                {labelSuffix}
              </span>
            )}
          </InputLabel>
          {tooltipContent && <TCBTooltip content={tooltipContent} />}
        </div>
      )}
      <div className={`flex flex-row relative`}>
        <InputContainer paddingLeft={0} hasError={hasError}>
          {leftIcon && <InputIcon>{leftIcon}</InputIcon>}
          <AutoComplete
            shouldItemRender={shouldItemRender}
            items={items}
            getItemValue={getItemValue}
            renderItem={renderItem}
            value={value}
            onSelect={props.ON_SELECT}
            wrapperStyle={styles.wrapperStyle}
            inputProps={{
              placeholder,
              disabled: !editable,
              name: fieldName,
              className: styles.inputClassName,
              onBlur: handleBlur,
              ...inputProps
            }}
            // @ts-ignore
            menuStyle={styles.menuStyle}
            onChange={onChange(props)}
          />
          {rightIcon && <InputIcon>{rightIcon}</InputIcon>}
          {hasError && errorIcon}
          {hasTouched && !hasError && hasValue && validatedIcon}
          {valueSuffix && (
            <div
              className={`text-grey-dark text-sm font-bold ${css({
                marginRight: rem(28)
              })}`}
            >
              {valueSuffix}
            </div>
          )}
        </InputContainer>
        {canVerify && <VerifyControls {...props} />}
      </div>
      {props.hasErrorBlock && (
        <ErrorContainer>
          {hasError && (
            <div>
              <img
                width={16}
                height={16}
                src={require('./assets/ic-error.svg')}
                className={css({
                  marginRight: rem(8)
                })}
              />
            </div>
          )}
          <InputErrorLabel>{hasError && errorMessage}</InputErrorLabel>
        </ErrorContainer>
      )}
    </div>
  )
}

const enhancer = compose<IProps, IProps>(
  withHandlers(handlers),
  withPureField()
)

TextInputAutoComplete.defaultProps = {
  hasErrorBlock: true
}

export { EnhancedProps, IFormTextInputAutoComplete, TextInputAutoComplete }
export default enhancer(TextInputAutoComplete)
