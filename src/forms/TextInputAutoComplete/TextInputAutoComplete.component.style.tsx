import { rem, theme } from 'dma-ui'
import { css, cx } from 'emotion'

const styles = {
  wrapperStyle: {
    width: '100%'
  },
  inputClassName: cx(
    'text-base w-full text-grey-darkest',
    css({
      paddingLeft: rem(24),
      height: rem(48),
      fontSize: rem(14),
      backgroundColor: 'transparent',
      lineHeight: 1.5,
      '::placeholder': {
        color: theme.colors['grey-dark'],
        fontSize: rem(14)
      },
      ':focus': {
        outline: 'none'
      },
      ':disabled::placeholder': {
        opacity: 1
      },
      '> input': {
        backgroundColor: '#ED1C24'
      }
    })
  ),
  menuStyle: {
    position: 'absolute',
    backgroundColor: theme.colors['grey-lightest'],
    top: rem(61),
    left: 0,
    maxHeight: 300,
    zIndex: 99,
    overflow: 'auto',
    width: '100%'
  }
}

export { styles }
