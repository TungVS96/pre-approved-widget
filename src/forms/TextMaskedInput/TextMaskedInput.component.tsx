import { rem } from 'dma-ui'
import { css } from 'emotion'
import { getIn, InjectedFormikProps } from 'formik'
import { isNonEmptyString } from 'ramda-adjunct'
import React, { ChangeEvent, FunctionComponent } from 'react'
import { withPureField } from '../../hocs'
import {
  getEditableName,
  getFieldName
} from '../VerifyControls/VerifyControls.component'
import { ValidatedIcon } from '../index'

import {
  ErrorContainer,
  InputContainer,
  InputErrorLabel,
  InputFieldContainer,
  InputIcon,
  InputLabel
} from './TextMaskedInput.component.style'

interface IProps {
  inputProps?: {
    [key: string]: any
  }
  name: string
  hasTouched: boolean
  errorMessage: string
  values?: object
  value: string
  leftIcon?: React.ReactNode
  rightIcon?: React.ReactNode
  errorIcon?: React.ReactNode
  validatedIcon?: React.ReactNode
  handleChange(event: ChangeEvent<HTMLInputElement>): void
  handleBlur(event: ChangeEvent<HTMLInputElement>): void
  label?: string
  placeholder: string
  type: string
  requiredField?: boolean
  formatValue?(value: string): string
  transformValue?(value: string): string
  onValueChange?(value: string): void
  valuePrefix?: React.ReactNode
  canVerify?: boolean
  setFieldValue?(key: string, value: any): void
  disabled?: boolean
  handleChangeAmount?(
    event: ChangeEvent<HTMLInputElement>,
    startPosition: number
  ): void
  fieldContainerStyle?: string
  hasErrorIcon?: boolean
}

interface IFormTextMaskedInput<Props, Values> {
  name: string
  label?: string
  placeholder?: string
  type?: string
  requiredField?: boolean
  hasErrorIcon?: boolean
  valuePrefix?: React.ReactNode
  props: InjectedFormikProps<Props, Values>
  transformValue?(value: string): string
  onValueChange?(value: string): void
  rightIcon?: React.ReactNode
  inputProps?: {
    [key: string]: any
  }
  disabled?: boolean
  minDate?: Date
  maxDate?: Date
  handleChangeAmount?(
    event: ChangeEvent<HTMLInputElement>,
    startPosition: number
  ): void
  fieldContainerStyle?: string
}

const onChange = (props: IProps) => (e: ChangeEvent<HTMLInputElement>) => {
  const start = e.target.selectionStart as number

  if (props.transformValue) {
    e.target.value = props.transformValue(e.target.value)
  }

  if (props.onValueChange) {
    props.onValueChange(e.target.value)
  }

  props.handleChange(e)
  if (props.handleChangeAmount) {
    props.handleChangeAmount(e, start)
  }
}

const TextMaskedInput: FunctionComponent<IProps> = (props) => {
  const {
    type,
    leftIcon,
    rightIcon,
    errorIcon,
    validatedIcon = <ValidatedIcon />,
    placeholder,
    label,
    hasTouched,
    errorMessage,
    handleBlur,
    values,
    value,
    inputProps,
    requiredField = false,
    valuePrefix,
    canVerify = false,
    disabled = false,
    formatValue,
    fieldContainerStyle,
    hasErrorIcon = false
  } = props

  const fieldName = getFieldName(props)
  const hasError = hasTouched && isNonEmptyString(errorMessage)
  const formattedValue = formatValue ? formatValue(value) : value
  const hasValue = isNonEmptyString(formattedValue)
  const editable = canVerify ? getIn(values, getEditableName(props)) : !disabled

  return (
    <>
      {label && (
        <InputLabel>
          {label}
          {requiredField && <span className="text-brand">*</span>}
        </InputLabel>
      )}
      <div className="flex flex-row">
        <InputContainer className={fieldContainerStyle}>
          {leftIcon && <InputIcon>{leftIcon}</InputIcon>}
          {valuePrefix}
          <InputFieldContainer
            type={type}
            placeholder={placeholder}
            name={fieldName}
            onChange={onChange(props)}
            onBlur={handleBlur}
            value={formattedValue}
            disabled={!editable}
            {...inputProps}
          />
          {hasError && errorIcon}
          {hasTouched && !hasError && hasValue && validatedIcon}
          {rightIcon}
        </InputContainer>
        {/* {canVerify && <VerifyControls {...props} />} */}
      </div>
      <ErrorContainer>
        {hasError && hasErrorIcon && (
          <div>
            <img
              width={16}
              height={16}
              src={require('./assets/ic-error.svg')}
              className={css({
                marginRight: rem(8)
              })}
            />
          </div>
        )}
        <InputErrorLabel>{hasError && errorMessage}</InputErrorLabel>
      </ErrorContainer>
    </>
  )
}

export { IFormTextMaskedInput }
export default withPureField<IProps, IProps>()(TextMaskedInput)
