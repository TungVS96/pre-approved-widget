import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'
import MaskedInput from 'react-maskedinput'

const InputContainer = withClassName(
  'flex flex-row items-center w-full masked-input',
  'InputContainer'
)(
  styled('div')(({ theme }) => ({
    marginTop: rem(8),
    height: rem(44),
    paddingLeft: rem(12),
    border: `solid 1px ${theme.colors['grey-light']}`,
    ':focus-within': {
      border: `solid 1px #FDB8A3`
    },
    backgroundColor: theme.colors['white-two'],
    [theme.mq.md]: {
      marginTop: rem(11)
    }
  }))
)

const InputFieldContainer = withClassName(
  'text-base w-full text-grey-darkest',
  'InputFieldContainer'
)(
  styled(MaskedInput)(({ theme }) => ({
    height: rem(44),
    backgroundColor: 'transparent',
    lineHeight: 1.5,
    '::placeholder': {
      fontFamily: 'Helvetica',
      color: theme.colors['grey-dark']
    },
    ':focus': {
      outline: 'none'
    },
    ':disabled::placeholder': {
      opacity: 1
    }
  }))
)

const InputLabel = withClassName('text-sm text-grey-darkest', 'InputLabel')(
  styled('label')(({}) => ({
    fontSize: rem(14),
    fontWeight: 500,
    marginTop: rem(24)
    /*[theme.mq.md]: {
      fontSize: rem(16)
    }*/
  }))
)

const InputIcon = withClassName(
  'flex flex-no-shrink items-center justify-center',
  'InputIcon'
)(
  styled('div')({
    width: rem(24),
    height: rem(24),
    marginRight: rem(16)
  })
)

const InputErrorLabel = withClassName('text-brand text-xs', 'InputErrorLabel')(
  styled('div')({
    minHeight: rem(24)
  })
)

const ErrorContainer = withClassName('flex flex-row', 'ErrorContainer')(
  styled('div')({
    marginTop: rem(4)
  })
)

export {
  InputContainer,
  InputFieldContainer,
  InputLabel,
  InputIcon,
  InputErrorLabel,
  ErrorContainer
}
