// tslint:disable:max-file-line-count cyclomatic-complexity
import React from 'react'
import { t } from '../i18n'
import { IDropDownItem } from '../types'
import { getFieldProps } from '../utils'
import DateInput from './DateInput/DateInput.component'
import Dropdown, { IFormDropdown } from './Dropdown/Dropdown.component'
import DropdownTextInput, {
  IFormDropdownTextInput
} from './DropdownTextInput/DropdownTextInput.component'
import InputText, {
  IFormTextInput as IFormTextInput1
} from './InputText/TextInput.component'
import InputTextSuggestMoney, {
  IFormTextInput as IFormInputTextSuggestMoney
} from './InputTextSuggestMoney/TextInput.component'

import TextInput, { IFormTextInput } from './TextInput/TextInput.component'
import TextInputAutoComplete, {
  IFormTextInputAutoComplete
} from './TextInputAutoComplete/TextInputAutoComplete.component'
import { IFormTextMaskedInput } from './TextMaskedInput/TextMaskedInput.component'

const onDropDownChange = (props: IFormDropdown<any, any>) => (
  key: string,
  item: IDropDownItem
) => {
  if (props.onSelect) {
    props.onSelect(key, item)
  }

  props.props.setFieldValue(key, item.value)
}

const renderDropdown = <Props, Values>(props: IFormDropdown<Props, Values>) => {
  const {
    name,
    label,
    placeholder,
    requiredField = true,
    items,
    transformValue,
    disabled,
    format,
    labelSuffix,
    className,
    tooltipContent,
    inputProps,
    messageNoData = 'NO DATA',
    displayName = true,
    textColorMessageNoData,
    displayAllValueAndName = false,
    displayFieldStyle,
    fieldContainerStyle,
    valueSuffix,
    hasValidation,
    iconWarning = true,
    highLight = false,
    customArrow = false,
    filterInputStyle
  } = props

  return (
    <Dropdown
      {...getFieldProps(name, props.props)}
      title={label && t(label)}
      labelSuffix={labelSuffix && t(labelSuffix)}
      placeholder={placeholder && t(placeholder)}
      onChange={onDropDownChange(props)}
      items={items}
      requiredField={requiredField}
      transformValue={transformValue}
      format={format}
      disabled={disabled}
      className={className}
      tooltipContent={tooltipContent}
      inputProps={inputProps}
      messageNoData={messageNoData}
      displayName={displayName}
      textColorMessageNoData={textColorMessageNoData}
      displayAllValueAndName={displayAllValueAndName}
      displayFieldStyle={displayFieldStyle}
      fieldContainerStyle={fieldContainerStyle}
      valueSuffix={t(valueSuffix)}
      hasValidation={hasValidation}
      iconWarning={iconWarning}
      highLight={highLight}
      customArrow={customArrow}
      filterInputStyle={filterInputStyle}
    />
  )
}

const renderDropdownTextInput = <Props, Values>(
  props: IFormDropdownTextInput<Props, Values>
) => {
  const {
    name, // text input name
    nameOfDropdown,
    label,
    placeholder,
    requiredField = true,
    items,
    transformValue,
    disabled,
    format,
    labelSuffix,
    className,
    tooltipContent,
    inputProps,
    messageNoData = 'NO DATA',
    displayName = true,
    textColorMessageNoData,
    displayAllValueAndName = false,
    displayFieldStyle
  } = props

  return (
    <DropdownTextInput
      {...getFieldProps(name, props.props)}
      nameOfDropdown={nameOfDropdown}
      title={label && t(label)}
      labelSuffix={labelSuffix && t(labelSuffix)}
      placeholder={placeholder && t(placeholder)}
      onChange={onDropDownChange(props)}
      items={items}
      requiredField={requiredField}
      transformValue={transformValue}
      format={format}
      disabled={disabled}
      className={className}
      tooltipContent={tooltipContent}
      inputProps={inputProps}
      messageNoData={messageNoData}
      displayName={displayName}
      textColorMessageNoData={textColorMessageNoData}
      displayAllValueAndName={displayAllValueAndName}
      displayFieldStyle={displayFieldStyle}
    />
  )
}

const renderInputText = <Props, Values>({
  name,
  type = 'text',
  labelSuffix,
  dynamicLabel = false,
  label,
  placeholder,
  requiredField = true,
  props,
  inputProps,
  transformValue,
  valueSuffix,
  disabled = false,
  maxLength,
  valuePrefix,
  onTextChange,
  format,
  handleChangeAmount,
  handleFocus,
  hasErrorBlock,
  inputWrapperStyle,
  labelStyle,
  leftIcon,
  rightIcon,
  conditionList,
  rightLabelContent
}: IFormTextInput1<Props, Values>) => (
  <InputText
    {...getFieldProps(name, props)}
    type={type}
    label={label && t(label)}
    placeholder={placeholder && t(placeholder)}
    requiredField={requiredField}
    inputProps={inputProps}
    transformValue={transformValue}
    labelSuffix={labelSuffix && (dynamicLabel ? labelSuffix : t(labelSuffix))}
    valueSuffix={valueSuffix && t(valueSuffix)}
    disabled={disabled}
    maxLength={maxLength}
    valuePrefix={valuePrefix}
    onTextChange={onTextChange}
    format={format}
    handleChangeAmount={handleChangeAmount}
    handleFocus={handleFocus}
    hasErrorBlock={hasErrorBlock}
    inputWrapperStyle={inputWrapperStyle}
    labelStyle={labelStyle}
    leftIcon={leftIcon}
    rightIcon={rightIcon}
    conditionList={conditionList}
    rightLabelContent={rightLabelContent}
  />
)

const renderTextInput = <Props, Values>({
  name,
  type = 'text',
  labelSuffix,
  labelTooltip,
  dynamicLabel = false,
  label,
  labelStyle,
  placeholder,
  requiredField = true,
  props,
  inputProps,
  transformValue,
  valueSuffix,
  disabled = false,
  maxLength,
  valuePrefix,
  onTextChange,
  format,
  handleChangeAmount,
  handleFocus,
  // handleBlur,
  leftIcon,
  rightIcon,
  autoComplete,
  numberics = false,
  inputStyle,
  highLight = false,
  inputWrapperStyle
}: IFormTextInput<Props, Values>) => (
  <TextInput
    {...getFieldProps(name, props)}
    type={type}
    label={label && t(label)}
    labelTooltip={labelTooltip}
    labelStyle={labelStyle}
    placeholder={placeholder && t(placeholder)}
    requiredField={requiredField}
    inputProps={inputProps}
    transformValue={transformValue}
    labelSuffix={labelSuffix && (dynamicLabel ? labelSuffix : t(labelSuffix))}
    valueSuffix={valueSuffix && t(valueSuffix)}
    disabled={disabled}
    maxLength={maxLength}
    valuePrefix={valuePrefix}
    onTextChange={onTextChange}
    format={format}
    handleChangeAmount={handleChangeAmount}
    handleFocus={handleFocus}
    // handleBlur={handleBlur}
    leftIcon={leftIcon}
    rightIcon={rightIcon}
    autoComplete={autoComplete}
    numberics={numberics}
    inputStyle={inputStyle}
    highLight={highLight}
    inputWrapperStyle={inputWrapperStyle}
  />
)
const renderTextMaskedInput = <Props, Values>({
  name,
  type = 'text',
  label,
  placeholder,
  requiredField = true,
  props,
  inputProps,
  transformValue,
  valuePrefix,
  onValueChange,
  minDate,
  maxDate,
  disabled = false,
  fieldContainerStyle = '',
  hasErrorIcon = false
}: IFormTextMaskedInput<Props, Values>) => (
  // @ts-ignore
  <DateInput
    {...getFieldProps(name, props)}
    type={type}
    label={label && t(label)}
    placeholder={placeholder && t(placeholder)}
    requiredField={requiredField}
    inputProps={inputProps}
    transformValue={transformValue}
    valuePrefix={valuePrefix}
    setFieldValue={props.setFieldValue}
    onValueChange={onValueChange}
    disabled={disabled}
    minDate={minDate}
    maxDate={maxDate}
    fieldContainerStyle={fieldContainerStyle}
    hasErrorIcon={hasErrorIcon}
  />
)

const renderTextInputSuggestMoney = <Props, Values>({
  name,
  type = 'text',
  suggestMoney = true,
  labelSuffix,
  dynamicLabel = false,
  label,
  placeholder,
  requiredField = true,
  props,
  inputProps,
  transformValue,
  valueSuffix,
  disabled = false,
  maxLength,
  valuePrefix,
  onTextChange,
  format,
  handleChangeAmount,
  handleFocus,
  // handleBlur,
  inputWrapperStyle,
  labelStyle,
  outsideClick,
  suggestMoneyAllowTypeZero = true,
  times,
  autoComplete = 'off',
  numberics,
  iconWarning = true
}: IFormInputTextSuggestMoney<Props, Values>) => (
  <InputTextSuggestMoney
    {...getFieldProps(name, props)}
    type={type}
    label={label && t(label)}
    placeholder={placeholder && t(placeholder)}
    requiredField={requiredField}
    inputProps={inputProps}
    transformValue={transformValue}
    labelSuffix={labelSuffix && (dynamicLabel ? labelSuffix : t(labelSuffix))}
    valueSuffix={valueSuffix && t(valueSuffix)}
    disabled={disabled}
    maxLength={maxLength}
    valuePrefix={valuePrefix}
    onTextChange={onTextChange}
    format={format}
    handleChangeAmount={handleChangeAmount}
    handleFocus={handleFocus}
    // handleBlur={handleBlur}
    inputWrapperStyle={inputWrapperStyle}
    labelStyle={labelStyle}
    suggestMoney={suggestMoney}
    outsideClick={outsideClick}
    suggestMoneyAllowTypeZero={suggestMoneyAllowTypeZero}
    times={times}
    autoComplete={autoComplete}
    numberics={numberics}
    iconWarning={iconWarning}
  />
)

const renderTextInputAutoComplete = <Props, Values>({
  name,
  type = 'text',
  labelSuffix,
  label,
  placeholder,
  requiredField = true,
  props,
  inputProps,
  transformValue,
  valueSuffix,
  disabled = false,
  items,
  onSelect,
  onTextChange,
  tooltipContent = '',
  leftIcon,
  rightIcon,
  canVerify
}: IFormTextInputAutoComplete<Props, Values>) => (
  <TextInputAutoComplete
    {...getFieldProps(name, props)}
    type={type}
    label={label && t(label)}
    placeholder={t(placeholder)}
    requiredField={requiredField}
    inputProps={inputProps}
    transformValue={transformValue}
    labelSuffix={labelSuffix && t(labelSuffix)}
    valueSuffix={valueSuffix && t(valueSuffix)}
    disabled={disabled}
    setFieldValue={props.setFieldValue}
    items={items}
    onSelect={onSelect}
    onTextChange={onTextChange}
    tooltipContent={tooltipContent}
    leftIcon={leftIcon}
    rightIcon={rightIcon}
    canVerify={canVerify}
  />
)

export {
  renderDropdown,
  renderTextInput,
  renderInputText,
  renderTextMaskedInput,
  renderTextInputSuggestMoney,
  renderDropdownTextInput,
  renderTextInputAutoComplete
}
