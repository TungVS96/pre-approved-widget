import { rem } from 'dma-ui'
import { css } from 'emotion'
import React, { SFC } from 'react'

const InputErrorIcon: SFC = () => (
  <div className={css({ marginRight: rem(12) })}>
    <img src={require('./assets/error.svg')} width={36} height={36} />
  </div>
)

export default InputErrorIcon
