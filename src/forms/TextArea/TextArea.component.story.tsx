import { storiesOf } from '@storybook/react'
import { css } from 'emotion'
import { getFieldProps } from 'formik-validators'
import React from 'react'

import { t } from '../../i18n'
import { ErrorIcon } from '../index'

import TextArea from './TextArea.component'

const defaultProps = {
  errors: {
    name: 'error'
  },
  values: {
    name: ''
  },
  touched: {
    name: false
  }
}

const leftIconClassName = css({
  width: 32,
  height: 32,
  backgroundColor: '#ED1C24'
})

const NOOP = () => 'nothing'

storiesOf('TextArea', module)
  .add('default', () => (
    <div className={css({ margin: 20 })}>
      <TextArea
        name={'name'}
        label={t('login.form.username')}
        placeholder={t('login.form.usernamePlaceholder')}
        leftIcon={<div className={`flex-no-shrink ${leftIconClassName}`} />}
        errorIcon={<ErrorIcon />}
        {...getFieldProps(defaultProps, 'name')}
        handleChange={NOOP}
        handleBlur={NOOP}
      />
    </div>
  ))
  .add('with error state', () => {
    const props = {
      ...defaultProps,
      touched: {
        name: true
      },
      errors: {
        name: 'Please enter Name'
      }
    }

    return (
      <div className={css({ margin: 20 })}>
        <TextArea
          name={'name'}
          label={t('login.form.username')}
          placeholder={t('login.form.usernamePlaceholder')}
          leftIcon={<div className={`flex-no-shrink ${leftIconClassName}`} />}
          errorIcon={<ErrorIcon />}
          {...getFieldProps(props, 'name')}
          handleChange={NOOP}
          handleBlur={NOOP}
        />
      </div>
    )
  })
  .add('without left icon', () => (
    <div className={css({ margin: 20 })}>
      <TextArea
        name={'name'}
        label={t('login.form.username')}
        placeholder={t('login.form.usernamePlaceholder')}
        errorIcon={<ErrorIcon />}
        {...getFieldProps(defaultProps, 'name')}
        handleChange={NOOP}
        handleBlur={NOOP}
      />
    </div>
  ))
  .add('with required field indicator', () => (
    <div className={css({ margin: 20 })}>
      <TextArea
        name={'name'}
        requiredField={true}
        label={t('login.form.username')}
        placeholder={t('login.form.usernamePlaceholder')}
        leftIcon={<div className={`flex-no-shrink ${leftIconClassName}`} />}
        errorIcon={<ErrorIcon />}
        {...getFieldProps(defaultProps, 'name')}
        handleChange={NOOP}
        handleBlur={NOOP}
      />
    </div>
  ))
