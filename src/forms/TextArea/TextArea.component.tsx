import { theme } from 'dma-ui'
import { css } from 'emotion'
import { isNonEmptyString } from 'ramda-adjunct'
import React, { SFC } from 'react'

import {
  InputContainer,
  InputErrorLabel,
  InputFieldContainer,
  InputIcon,
  InputLabel
} from './TextArea.component.style'

interface IProps {
  inputProps?: {
    [key: string]: any
  }
  name: string
  touched: boolean
  error: string
  value: string
  leftIcon?: React.ReactNode
  rightIcon?: React.ReactNode
  errorIcon?: React.ReactNode
  validatedIcon?: React.ReactNode
  handleChange(event: Event): void
  handleBlur(event: Event): void
  label?: string
  placeholder: string
  requiredField?: boolean
  whiteBackground?: boolean
  customStyle?: any
}

const TextArea: SFC<IProps> = ({
  leftIcon,
  rightIcon,
  errorIcon,
  validatedIcon,
  placeholder,
  label,
  name,
  touched,
  error,
  handleChange,
  handleBlur,
  value,
  inputProps,
  customStyle,
  requiredField = false,
  whiteBackground = false
}) => {
  const hasTouched = touched
  const errorMessage = error
  const hasError = hasTouched && isNonEmptyString(errorMessage)

  return (
    <>
      {label && (
        <InputLabel>
          {label}
          {requiredField && <span className="text-brand">*</span>}
        </InputLabel>
      )}
      <InputContainer
        borderColorKey={hasError && 'brand'}
        className={`${
          whiteBackground ? css({ backgroundColor: theme.colors.white }) : ''
        } ${customStyle}`}
      >
        {leftIcon && <InputIcon>{leftIcon}</InputIcon>}
        <InputFieldContainer
          placeholder={placeholder}
          name={name}
          onChange={handleChange}
          onBlur={handleBlur}
          value={value}
          {...inputProps}
        />
        {hasError && errorIcon}
        {hasTouched && !hasError && validatedIcon}
        {rightIcon}
      </InputContainer>
      <InputErrorLabel>{hasError && errorMessage}</InputErrorLabel>
    </>
  )
}

export default TextArea
