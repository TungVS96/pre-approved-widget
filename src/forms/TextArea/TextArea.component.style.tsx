import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const InputContainer = withClassName(
  'flex flex-row items-center w-full h-full bg-grey-lightest'
)(
  styled('div')(
    {
      marginTop: rem(12),
      paddingLeft: rem(24)
    },
    ({ theme, borderColorKey }: { theme: any; borderColorKey?: string }) => ({
      border: `solid 1px ${theme.colors[borderColorKey || 'very-light-pink']}`
    })
  )
)

const InputFieldContainer = withClassName(
  'text-base w-full h-full text-grey-darkest'
)(
  styled('textarea')(({ theme }) => ({
    paddingTop: rem(12),
    paddingBottom: rem(12),
    backgroundColor: 'transparent',
    lineHeight: 1.5,
    '::placeholder': {
      fontFamily: 'Helvetica',
      color: theme.colors['grey-dark']
    },
    ':focus': {
      outline: 'none'
    },
    ':disabled::placeholder': {
      opacity: 1
    }
  }))
)

const InputLabel = withClassName('text-base font-bold text-grey-darkest')(
  'label'
)

const InputIcon = withClassName(
  'flex flex-no-shrink items-center justify-center'
)(
  styled('div')({
    width: rem(24),
    height: rem(24),
    marginRight: rem(16)
  })
)

const InputErrorLabel = withClassName('text-brand text-xs')(
  styled('div')({
    marginTop: rem(5),
    height: rem(20)
  })
)

export {
  InputContainer,
  InputFieldContainer,
  InputLabel,
  InputIcon,
  InputErrorLabel
}
