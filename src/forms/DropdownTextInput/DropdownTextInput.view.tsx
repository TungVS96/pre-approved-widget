// tslint:disable:max-file-line-count cyclomatic-complexity
/* tslint:disable */
import { rem, Arrow, NOOP } from 'dma-ui'
import { css } from 'emotion'
import { getIn } from 'formik'
import { isEmpty, pathOr } from 'ramda'
import { isNonEmptyString } from 'ramda-adjunct'
import React, { ChangeEvent, FunctionComponent } from 'react'
import OutsideClickHandler from 'react-outside-click-handler'
import Tooltip from '../../components/Tooltip/Tooltip.component'
import { withPureField } from '../../hocs'
import { IDropDownItem } from '../../types'
import { getName, getValue } from '../../utils'
import {
  InputErrorLabel,
  InputFieldContainer,
  InputIcon,
  InputLabel
} from '../TextInput/TextInput.component.style'
import VerifyControls, {
  getEditableName,
  getFieldName,
  getFieldNameDropDownTextInput
} from '../VerifyControls/VerifyControls.component'
import { ValidatedIcon } from '../index'
import {
  DisplayFieldContainer,
  DropdownContainer,
  DropdownFieldContainer,
  DropdownItem,
  ErrorContainer,
  FieldContainer,
  InputContainer
} from './DropdownTextInput.style'

interface IProps {
  name: string
  nameOfDropdown?: string
  showed?: boolean
  show?(show: boolean): void
  SHOW?(): void
  CLICK?(name: string, item: IDropDownItem): void
  items: IDropDownItem[]
  onChange(key: string, item: IDropDownItem): void
  errorIcon?: React.ReactNode
  values?: object
  hasTouched?: boolean
  setFieldTouched?(name: string, touched: any): void
  errorMessage?: string
  leftIcon?: React.ReactNode
  title?: string
  labelSuffix?: string
  placeholder?: string
  requiredField?: boolean
  filterInputStyle?: string
  filterDropdownStyle?: string
  labelStyle?: string
  displayFieldStyle?: string
  arrowSize?: number
  dropdownItemStyle?: string
  hasValidation?: boolean
  disabled?: boolean
  format?(value: string): string
  className?: string
  ON_OUTSITE_CLICK?(e: React.MouseEvent<HTMLElement>): void
  tooltipContent?: string
  canVerify?: boolean
  inputProps?: any
  messageNoData?: string
  displayName?: boolean
  textColorMessageNoData?: string
  displayAllValueAndName?: boolean
  labelIcon?: string
  labelIconOnClick?(input: any): void

  label?: string
  labelTooltip?: string
  type?: string
  numberics?: boolean
  dynamicLabel?: boolean
  valueSuffix?: string
  transformValue?(value: string): string
  hasErrorBlock?: boolean
  setFieldValue?(key: string, value: any): void
  maxLength?: number
  valuePrefix?: React.ReactNode
  rightIcon?: React.ReactNode
  onTextChange?(value: string): any
  format?(value: any): string
  handleChangeAmount?(
    event: ChangeEvent<HTMLInputElement>,
    startPosition: number
  ): void
  handleFocus?(event: any): void
  handleBlur?(event: ChangeEvent<HTMLInputElement>): void
  autoComplete?: string
  inputWrapperStyle?: string
  inputStyle?: string
  validatedIcon?: React.ReactNode
  value?: string
  handleChange(event: ChangeEvent<HTMLInputElement>): void
  valueOfDropDown?: string
}

const onKeyDown = (onClickOutside: any) => (event: any) => {
  if (event.keyCode === 9) {
    // tab key
    onClickOutside()
  }
}

const onTextChange = (props: IProps) => (e: ChangeEvent<HTMLInputElement>) => {
  const start = e.target.selectionStart as number

  if (props.transformValue) {
    e.target.value = props.transformValue(e.target.value)
  }

  props.handleChange(e)
  if (props.onTextChange) {
    props.onTextChange(e.target.value)
  }

  if (props.handleChangeAmount) {
    props.handleChangeAmount(e, start)
  }
}

const getDisplayText = ({ type, numberics, value, format }: IProps) => {
  return type === 'tel' && !numberics
    ? value
    : value && value !== '0'
    ? format
      ? format(value)
      : value
    : value
}

const DropdownTextInputView: FunctionComponent<IProps> = (props) => {
  const {
    showed,
    items,
    SHOW,
    CLICK,
    errorIcon,
    values,
    errorMessage,
    hasTouched,
    leftIcon,
    title,
    placeholder,
    filterInputStyle,
    filterDropdownStyle,
    arrowSize,
    labelStyle,
    displayFieldStyle,
    dropdownItemStyle,
    hasValidation,
    requiredField = false,
    disabled = false,
    format,
    labelSuffix,
    className,
    ON_OUTSITE_CLICK,
    tooltipContent,
    canVerify = false,
    inputProps,
    messageNoData = 'NO DATA',
    displayName = true,
    textColorMessageNoData,
    displayAllValueAndName = false,
    labelIcon = '',
    type,
    rightIcon,
    handleBlur,
    valueSuffix,
    maxLength,
    valuePrefix,
    handleFocus,
    autoComplete,
    inputWrapperStyle,
    inputStyle,
    validatedIcon = <ValidatedIcon />,
    value = '',
    labelIconOnClick = () => {
      //
    }
  } = props

  const fieldNameDropdown = getFieldNameDropDownTextInput(props)
  const hasError = hasTouched && isNonEmptyString(errorMessage)
  let dropdownDisplayText = displayName
    ? getName(items, getIn(values, fieldNameDropdown))
    : getValue(items, getIn(values, fieldNameDropdown))
  if (isEmpty(dropdownDisplayText)) {
    dropdownDisplayText = pathOr('', [0, 'name'], items)
  }
  const formattedDisplayText = format
    ? format(dropdownDisplayText)
    : dropdownDisplayText
  const noData = isEmpty(items)

  const fieldName = getFieldName(props)
  const hasValue = isNonEmptyString(value)
  const editable = canVerify ? getIn(values, getEditableName(props)) : !disabled
  const displayText = getDisplayText(props)

  return (
    <div className={className}>
      <OutsideClickHandler
        onOutsideClick={ON_OUTSITE_CLICK ? ON_OUTSITE_CLICK : NOOP}
      >
        {title && (
          <InputLabel className={labelStyle}>
            {title}
            {requiredField && <span className="text-brand">*</span>}{' '}
            {labelSuffix && (
              <span className="font-normal text-grey-darkest">
                {labelSuffix}
              </span>
            )}
            {isNonEmptyString(tooltipContent) && (
              <Tooltip content={tooltipContent} />
            )}
            {labelIcon && (
              <img
                src={labelIcon}
                style={{
                  width: 17,
                  height: 17,
                  marginLeft: 5
                }}
                onClick={labelIconOnClick}
              />
            )}
          </InputLabel>
        )}
        <div className="flex flex-row w-full'">
          <DropdownFieldContainer
            onClick={SHOW}
            borderColorKey={hasError && 'tomato'}
            className={`relative ${filterInputStyle}`}
            disabled={!editable}
            {...inputProps}
            tabIndex={0}
            onKeyDown={onKeyDown(ON_OUTSITE_CLICK)}
          >
            <FieldContainer disabled={!editable}>
              {leftIcon && <InputIcon>{leftIcon}</InputIcon>}
              <DisplayFieldContainer
                className={displayFieldStyle}
                hasValue={true}
              >
                {formattedDisplayText}
              </DisplayFieldContainer>
              <Arrow size={arrowSize} />
              {hasError && errorIcon}
            </FieldContainer>
            {showed && (
              <DropdownContainer className={filterDropdownStyle}>
                {noData ? (
                  <p style={{ margin: 10, color: textColorMessageNoData }}>
                    {messageNoData}
                  </p>
                ) : (
                  items.map((item) => (
                    <DropdownItem
                      className={dropdownItemStyle}
                      key={item.value}
                      onClick={CLICK!(fieldNameDropdown, item)}
                    >
                      {!displayAllValueAndName ? (
                        item.name
                      ) : (
                        <div>
                          {item.name}
                          <br />
                          {item.value}
                        </div>
                      )}
                    </DropdownItem>
                  ))
                )}
              </DropdownContainer>
            )}
          </DropdownFieldContainer>

          <InputContainer className={inputWrapperStyle}>
            {leftIcon && <InputIcon>{leftIcon}</InputIcon>}
            {valuePrefix}
            <InputFieldContainer
              className={inputStyle}
              type={type}
              placeholder={placeholder}
              name={fieldName}
              onChange={onTextChange(props)}
              onBlur={handleBlur}
              disabled={!editable}
              value={displayText}
              maxLength={maxLength}
              onFocus={handleFocus}
              {...inputProps}
              autoComplete={autoComplete}
            />
            {hasError && errorIcon}
            {hasTouched && !hasError && hasValue && validatedIcon}
            {rightIcon}
            {valueSuffix && (
              <div
                className={`text-grey-dark text-sm font-bold ${css({
                  marginRight: rem(14)
                })}`}
              >
                {valueSuffix}
              </div>
            )}
          </InputContainer>

          {canVerify && <VerifyControls {...props} />}
        </div>
        <ErrorContainer>
          {hasError && (
            <img
              width={16}
              height={16}
              src={require('./assets/ic-error.svg')}
              className={css({
                marginRight: rem(8)
              })}
            />
          )}
          {hasValidation && (
            <InputErrorLabel>{hasError && errorMessage}</InputErrorLabel>
          )}
        </ErrorContainer>
      </OutsideClickHandler>
    </div>
  )
}

DropdownTextInputView.defaultProps = {
  labelStyle: '',
  filterInputStyle: '',
  displayFieldStyle: '',
  filterDropdownStyle: '',
  dropdownItemStyle: '',
  arrowSize: 8,
  hasValidation: true
}

export { IProps }
export default withPureField<IProps, IProps>({
  updatedKeys: ['showed']
})(DropdownTextInputView)
