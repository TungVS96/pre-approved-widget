import { convertToRGBA, rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const DropdownContainer = withClassName(
  'w-full absolute bg-grey-lightest',
  'DropdownContainer'
)(
  styled('div')(({ theme }) => ({
    border: `solid 1px ${theme.colors['grey-light']}`,
    borderTopWidth: 0,
    top: rem(44),
    zIndex: 19,
    maxHeight: 300,
    overflow: 'auto'
  }))
)

const DropdownItem = withClassName(
  'w-full text-base flex items-center',
  'DropdownItem'
)(
  styled('div')(({ theme, highlight }: { theme: any; highlight: boolean }) => {
    const paleRedTwo = convertToRGBA(theme.colors['pale-red-two'], 0.4)

    return {
      paddingLeft: rem(24),
      paddingTop: rem(4),
      paddingBottom: rem(4),
      minHeight: rem(42),
      userSelect: 'none',
      fontSize: rem(12),
      backgroundColor: highlight ? paleRedTwo : theme.colors['white-two'],
      color: theme.colors.black,
      ':hover': {
        backgroundColor: paleRedTwo
      }
    }
  })
)

const DisplayFieldContainer = withClassName(
  'flex text-base w-full items-center',
  'DisplayFieldContainer'
)(
  styled('div')(
    {
      fontSize: rem(14),
      height: rem(44),
      userSelect: 'none',
      textOverflow: 'ellipsis',
      overflow: 'hidden',
      whiteSpace: 'nowrap'
    },
    ({ theme, hasValue }: { theme: any; hasValue: boolean }) => ({
      backgroundColor: 'transparent',
      color: hasValue ? theme.colors.black : theme.colors['grey-light']
    })
  )
)

const DropdownFieldContainer = withClassName(
  'w-full cursor-pointer dropdownFieldContainer',
  'DropdownFieldContainer'
)(
  styled('div')(({ theme, disabled }: { theme: any; disabled: boolean }) => ({
    marginTop: rem(12),
    height: rem(48),
    width: `45%`,
    cursor: disabled ? 'default' : 'pointer',
    pointerEvents: disabled ? 'none' : 'inherit',
    outline: 'none',
    [theme.mq.md]: {
      marginTop: rem(12)
    }
  }))
)

const FieldContainer = withClassName(
  'flex flex-row items-center',
  'FieldContainer'
)(
  styled('div')(
    ({ theme, borderColorKey }: { theme: any; borderColorKey?: string }) => ({
      paddingLeft: rem(24),
      borderLeft: `solid 1px ${theme.colors[borderColorKey || 'grey-light']}`,
      borderTop: `solid 1px ${theme.colors[borderColorKey || 'grey-light']}`,
      borderBottom: `solid 1px ${theme.colors[borderColorKey || 'grey-light']}`,
      borderRadius: `${rem(5)} ${rem(0)} ${rem(0)} ${rem(5)}`,
      height: rem(48)
    })
  )
)

const InputContainer = withClassName('flex flex-row items-center w-full')(
  styled('div')(
    {
      marginTop: rem(12),
      height: rem(48)
    },
    ({ theme, borderColorKey }: { theme: any; borderColorKey?: string }) => ({
      backgroundColor: theme.colors['white-two'],
      border: `solid 1px ${theme.colors[borderColorKey || 'grey-light']}`,
      borderRadius: `${rem(0)} ${rem(5)} ${rem(5)} ${rem(0)}`,
      [theme.mq.md]: {
        marginTop: rem(12)
      }
    })
  )
)

const ErrorContainer = withClassName('flex flex-row', 'ErrorContainer')(
  styled('div')({})
)

export {
  DropdownContainer,
  DropdownItem,
  DisplayFieldContainer,
  DropdownFieldContainer,
  FieldContainer,
  ErrorContainer,
  InputContainer
}
