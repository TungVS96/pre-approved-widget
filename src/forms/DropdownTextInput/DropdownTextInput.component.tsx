import { InjectedFormikProps } from 'formik'
import { compose, withHandlers, withState } from 'recompose'

import { IDropDownItem } from '../../types'

import handlers from './DropdownTextInput.handler'
import DropdownTextInputView, { IProps } from './DropdownTextInput.view'

interface IFormDropdownTextInput<Props, Values> {
  name: string
  nameOfDropdown?: string
  label?: string
  labelSuffix?: string
  placeholder?: string
  type?: string
  requiredField?: boolean
  props: InjectedFormikProps<Props, Values>
  items: IDropDownItem[]
  filterInputStyle?: string
  filterDropdownStyle?: string
  labelStyle?: string
  displayFieldStyle?: string
  arrowSize?: number
  dropdownItemStyle?: string
  fieldContainerStyle?: string
  hasValidation?: boolean
  disabled?: boolean
  transformValue?(value: string): string
  format?(value: string): string
  className?: string
  onSelect?(name: string, item: IDropDownItem): void
  tooltipContent?: string
  canVerify?: boolean
  hasErrorBlock?: boolean
  inputProps?: any
  messageNoData?: string
  displayName?: boolean
  textColorMessageNoData?: string
  displayAllValueAndName?: boolean
  labelIcon?: string
  labelIconOnClick?(input: any): void
}

const enhancer = compose<IProps, IProps>(
  withState('showed', 'show', false),
  withHandlers(handlers)
)

export { IFormDropdownTextInput }
export default enhancer(DropdownTextInputView)
