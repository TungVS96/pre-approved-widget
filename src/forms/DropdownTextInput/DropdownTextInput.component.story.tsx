import { storiesOf } from '@storybook/react'
import { css } from 'emotion'
import React from 'react'

import { getFieldProps } from '../../utils'
import { ErrorIcon } from '../index'

import Dropdown from './DropdownTextInput.component'

const name = 'project'
const defaultProps = {
  errors: {
    [name]: 'error'
  },
  values: {
    [name]: ''
  },
  touched: {
    [name]: false
  }
}

const NOOP = () => () => 'nothing'
const items = [
  {
    name: 'Vin City',
    value: 'vinCity'
  }
]
const leftIconClassName = css({
  width: 32,
  height: 32,
  backgroundColor: '#ED1C24'
})

storiesOf('Dropdown', module)
  .add('default', () => (
    <div className={css({ margin: 20 })}>
      <Dropdown
        {...getFieldProps(name, defaultProps)}
        onChange={NOOP}
        items={items}
        errorIcon={<ErrorIcon />}
        leftIcon={<div className={`flex-no-shrink ${leftIconClassName}`} />}
      />
    </div>
  ))
  .add('with error state', () => {
    const props = {
      ...defaultProps,
      touched: {
        [name]: true
      },
      errors: {
        [name]: 'Please select one item'
      }
    }

    return (
      <div className={css({ margin: 20 })}>
        <Dropdown
          {...getFieldProps(name, props)}
          onChange={NOOP}
          items={items}
          errorIcon={<ErrorIcon />}
          leftIcon={<div className={`flex-no-shrink ${leftIconClassName}`} />}
        />
      </div>
    )
  })
