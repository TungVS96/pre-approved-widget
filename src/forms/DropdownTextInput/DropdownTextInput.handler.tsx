import { IDropDownItem } from '../../types'

import { IProps } from './DropdownTextInput.view'

export default {
  SHOW: ({ showed, show, setFieldTouched, name }: IProps) => () => {
    show!(!showed)
    setFieldTouched!(name, true)
  },
  CLICK: ({ onChange, show }: IProps) => (
    name: string,
    item: IDropDownItem
  ) => () => {
    show!(false)
    onChange(name, item)
  },
  ON_OUTSITE_CLICK: ({ show }: IProps) => () => {
    show!(false)
  }
}
