import { rem } from 'dma-ui'
import { css } from 'emotion'
import React, { SFC } from 'react'

export const InputValidatedIcon: SFC = () => (
  <div className={css({ marginRight: rem(12) })}>
    <img src={require('./assets/checkmark.svg')} width={16} height={16} />
  </div>
)

export default InputValidatedIcon
