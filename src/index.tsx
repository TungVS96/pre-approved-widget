import * as React from 'react'
import * as ReactDOM from 'react-dom'

import App from './App'
import { unregister } from './registerServiceWorker'
import './theme/index.css'

const render = () => {
  return <App />
}

ReactDOM.render(render(), document.getElementById('tcb-pre-approved') as HTMLElement)
unregister()
