import { theme } from 'dma-ui'
import { ThemeProvider } from 'emotion-theming'
import React from 'react'

const themeDecorator = (getStory: any) => (
  <ThemeProvider theme={theme}>{getStory()}</ThemeProvider>
)

export { themeDecorator }
