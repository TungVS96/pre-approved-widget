interface IDropDownItem {
  name: string
  value: string
  parent?: string
}

interface IUserProfile {
  name: string
  email: string
  tokenId: string
}

export {
  IDropDownItem,
  IUserProfile
}
