enum CITY_VALUES {
  NUOC_NGOAI = 'NUOC-NGOAI',
  TINH_KHAC = 'TINH-KHAC'
}

enum PRE_APP_STATUS {
  L01 = 'L01', // Lead cần tư vấn
  L02 = 'L02', // Lead-Đãđăngkývayvốnonline
  L03 = 'L03', // Lead-Đã huỷ
  L04 = 'L04', // Lead-Đang cân nhắc tạo hồ sơ
  L05 = 'L05', // Lead-đang đượctư vấn
  L06 = 'L06', // Lead-đang được tư vấn
  L07 = 'L07', // Lead-đã huỷ bởi MC
  L08 = 'L08', // Lead-chuyển khởi tạo đơn vay vốn
  P01 = 'P01', // Preapp- Khởi tạo hồ sơ
  P02 = 'P02', // Preapp- Đamg upload hồ sơ
  P03 = 'P03', // Preapp- Hồ sơ hoàn thiện
  P04 = 'P04', // Preapp- Khách hàng huỷ
  P05 = 'P05', // Preapp- dừng xử lý
  P06 = 'P06' // Lead-chuyển khởi tạo đơn vay vốn
}

enum APP_STATUS {
  REVIEW = 'review',
  DOCUMENT = 'document',
  PRE_APPROVAL = 'pre-approval',
  APPROVAL = 'approval',
  DIS_APPROVAL = 'dis-approval',
  CONTRACTING_UPLOAD = 'contracting-upload',
  CONTRACTS_DRAFT = 'contracts-draft',
  CONTRACTS_READY = 'contracts-ready-to-sign',
  PRE_DISBURSE = 'pre-disburse',
  DISBURSED = 'disbursed'
}

enum CONTENT_TYPES {
  PDF = 'application/pdf',
  JPG = 'image/jpeg',
  PNG = 'image/png'
}

enum APPLICATION_STEPS {
  ONE = 'step1',
  TWO = 'step2',
  THREE = 'step3',
  FOUR = 'step4',
  FIVE = 'step5',
  SIX = 'step6',
  SEVEN = 'step7',
  SEVEN_FINAL_CHECKLIST = 'step7-final-checklist',
  SEVEN_UPLOAD_DOCUMENTS = 'step7-upload-documents'
}

enum CHECKBOX_STATE {
  CHECKED = 'checked',
  UNCHECKED = ''
}

enum COLLATERALS {
  TAI_SAN_HINH_THANH_TU_VON_VAY = '1'
}

enum TYPE_OF_INCOME {
  HOUSEHOLD_BUSINESS = 'household_business',
  SPOUSE_HOUSEHOLD_BUSINESS = 'spouse_household_business',
  SELF_EMPLOYED = 'self_employed',
  COMPANY_SALARY = 'company_salary_incomes',
  SPOUSE_SELF_EMPLOYED = 'spouse_self_employed',
  FROM_SALARY = 'from_salary',
  FROM_SPOUSE_SALARY = 'from_spouse_salary',
  FROM_PENSION = 'from_pension',
  FROM_SPOUSE_PENSION = 'from_spouse_pension',
  HOUSE_LEASE = 'house_lease',
  CAR_RENTAL = 'car_rental',
  APARTMENT_ROA = 'from_roa_apartment',
  SAVING_ROA = 'from_roa_saving',
  TC_BOND = 'from_tcbond',
  REDBOOK_ROA = 'from_estate_roa'
}

enum PLACE_OF_ISSUES_FOR_CCCD {
  CUC_CANH_SAT_DKQL_CU_TRU_AND_DLOG_VE_DAN_CU = 'CUC_CANH_SAT_DKQL_CU_TRU_AND_DLOG_VE_DAN_CU'
}

// todo: will be removed
enum PHOTO_CATEGORY {
  PERSONAL = 'PERSONAL',
  LOAN_PREFERENCE = 'LOAN_PREFERENCE',
  COLLATERAL = 'COLLATERAL',
  SOURCE_OF_INCOME = 'SOURCE_OF_INCOME',
  CREDIT_INFORMATION = 'CREDIT_INFORMATION'
}

enum MIME_TYPES {
  PDF = 'data:application/pdf',
  JPG = 'data:image/jpeg',
  PNG = 'data:image/png'
}

enum APPLICATION_STATUS {
  VERIFIED = 'Verified',
  PENDING = 'Pending'
}

enum HTTP_STATUS_CODE {
  PAYLOAD_TOO_LARGE = 413
}

enum DOCUMENT_SECTION {
  COLLATERAL_DOCUMENTS = 'collateralsOwnerDocuments',
  CREDIT_INFORMATION = 'creditInformation',
  INCOME_FROM_SALARY = 'incomeFromSalary',
  INCOME_FROM_RENTING_CAR = 'incomeFromRentingCar',
  INCOME_FROM_RENTING_HOUSE = 'incomeFromRentingHouse'
}

// need the exact values for IDC mapping
enum TYPE_OF_PROPERTY {
  HOUSE = 'HOU',
  APARTMENT = 'APA'
}

enum TYPE_INDUSTRY_GROUP {
  TRADE_SERVICE = 'COM',
  MANUFACTURING_PROCESSING = 'PRO'
}

export {
  DOCUMENT_SECTION,
  CITY_VALUES,
  APPLICATION_STEPS,
  CHECKBOX_STATE,
  COLLATERALS,
  TYPE_OF_INCOME,
  PLACE_OF_ISSUES_FOR_CCCD,
  PHOTO_CATEGORY,
  MIME_TYPES,
  APPLICATION_STATUS,
  APP_STATUS,
  HTTP_STATUS_CODE,
  TYPE_OF_PROPERTY,
  CONTENT_TYPES,
  TYPE_INDUSTRY_GROUP,
  PRE_APP_STATUS
}
