export * from './enum.type'
export * from './interface.type'

type SetFieldValue = (name: string, value: any) => void

export { SetFieldValue }
