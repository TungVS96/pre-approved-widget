const emoji = require('node-emoji')

const colors = {
  black: 30,
  red: 31,
  green: 32,
  yellow: 33,
  blue: 34,
  magenta: 35,
  cyan: 36,
  white: 37
}
const print = (text, colorCode = colors.white) =>
  `\x1b[${colorCode}m${text}\x1b[0m`
const createLogger = (text, colorCode, emoName) => {
  const _text = typeof text === 'string' ? print(text, colorCode) : text

  if (!emoName) {
    console.log(_text)
    return
  }

  console.log(emoji.get(emoName), _text)
}

const prn = {
  log: (text) => createLogger(text, colors.white),
  info: (text) => createLogger(text, colors.cyan),
  success: (text, emoName) =>
    createLogger(text, colors.yellow, emoName || 'white_check_mark'),
  warn: (text, emoName) =>
    createLogger(text, colors.green, emoName || 'warning'),
  error: (text, emoName) => createLogger(text, colors.red, emoName || 'x')
}

const separator = () => {
  console.log()
}
const runSync = (command) =>
  require('child_process')
    .execSync(command)
    .toString()
    .trim()

exports.print = print
exports.prn = prn
exports.separator = separator
exports.runSync = runSync
