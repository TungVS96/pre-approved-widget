/*
 * Validate translation files
 * 1. Is a valid JSON string?
 * 2. Have the same number of keys?
 */

const R = require('ramda')
const { prn } = require('./utils')

const en = require('../src/i18n/locales/en.json')
const vi = require('../src/i18n/locales/vi.json')

const jsonContent = { en, vi }
const validateJSON = (lang) => {
  try {
    JSON.parse(JSON.stringify(jsonContent[lang]))
  } catch (e) {
    prn.error(`${lang}: Not a valid JSON`)
    return false
  }
  return true
}

// CHECK 1
validateJSON('en')
validateJSON('vi')

const keys = {
  vi: R.keys(vi),
  en: R.keys(en)
}
const diffKeys = R.compose(
  R.reject((x) => {
    return keys.vi.includes(x)
  })
)(keys.en)

// CHECK 2
if (diffKeys.length > 0) {
  prn.error('Opps! Different number of keys.')
  prn.log(`EN has ${keys.en.length} keys`)
  prn.log(`VI has ${keys.vi.length} keys`)
  prn.log('Missing keys', diffKeys)
} else {
  prn.success('Both files has the same length. Good!')
}
