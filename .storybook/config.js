import { configure, addDecorator } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import '../src/theme/index.css'

import { themeDecorator } from '../src/decorators/theme.decorator'

addDecorator(
  withInfo({
    inline: false,
    header: false
  })
)
addDecorator(themeDecorator)

// automatically import all files ending in *.story.tsx
const req = require.context('../src', true, /.story.tsx$/)
function loadStories() {
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
